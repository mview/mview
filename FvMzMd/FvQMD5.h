#ifndef FVMD5_H
#define FVMD5_H

#include <FvQKOL.h>

typedef int TSocket;

#pragma pack(push, 1)
typedef union _MD5Digest {
    struct {
        long A, B, C, D;
    };
    unsigned char v[16];
} TMD5Digest;
#pragma pack(pop)
typedef TMD5Digest* PMD5Digest;
typedef TMD5Digest THashData;

/* The MD5DigestToStr function converts the result of
  a hashsum evaluation function into a string of
  hexadecimal digits */
KOLString MD5DigestToStr(const TMD5Digest Digest);
TMD5Digest& MD5StrToDigest(const KOLString Value);
bool IsEmpty(const TMD5Digest Digest);
/* The MD5DigestCompare function compares two
  TMD5Digest record variables. This function returns
  TRUE if parameters are equal or FALSE otherwise */
bool MD5DigestCompare(const TMD5Digest Digest1, const TMD5Digest Digest2);

#endif
