#ifndef KOL_H
#define KOL_H

#include <mzfc_inc.h>
#include <list>
#include <vector>
#include <WinReg.h>

using std::list;
using std::vector;

typedef CMzStringW WideString;
typedef CMzStringW KOLString;
typedef list < CMzStringW > TKOLStrList;
typedef TKOLStrList* PKOLStrList;
typedef WCHAR KOLChar;
typedef KOLChar* PKOLChar;
#define _L(x)      L ## x

class TObj;
typedef void (TObj::*TObjectMethod)(void); // fake the functor
typedef TObj* PObj;

#pragma pack(push, 1)
typedef union _UFUNCTION_OBJ {
    struct {
        PObj Data;
        TObjectMethod Code;
    };
    long long Quad;
} UFUNCTIONOBJ;
#pragma pack(pop)

class TObj {
protected:
	/*list<_UFUNCTION_OBJ>*/CHAR *fAutoFree;
protected:
	void DoDestroy();
	void Final();
public:
	void Free();
	void Add2AutoFree(PObj Obj);
public:
	TObj();
	~TObj();
};

enum TDirItemAction { diSkip, diAccept, diCancel };

//typedef LPWIN32_FIND_DATA PFindFileData;
struct TFindFileData : WIN32_FIND_DATA {
    HANDLE FindHandle;
};
typedef TFindFileData* PFindFileData;

class TDirList {
/* Allows easy directory scanning. This is not visual object, but
   storage to simplify working with directory content. */
protected:
    vector <TFindFileData> FList;
    KOLString FPath;
    TKOLStrList fFilters;
private:
    bool SatisfyFilter( KOLChar* FileName, DWORD FileAttr, DWORD FindAttr );
public:
    PFindFileData Get(int Idx);
    int GetCount();
    KOLString GetNames(int Idx);
    bool GetIsDirectory(int Idx);
    void Clear();
    void ScanDirectory( const KOLString DirPath, const KOLString Filter, DWORD Attr );
    void ScanDirectoryEx( const KOLString DirPath, const KOLString Filters, DWORD Attr );
};

typedef TDirList* PDirList;

void ShowMessage(const CMzStringW& msg, HWND hwnd = 0);
WideString WAnsiLowerCase(const WideString S);

/* AnsiCompareStr compares S1 to S2, with case-sensitivity. The compare
operation is controlled by the current Windows locale. The return value
is the same as for CompareStr. */
int AnsiCompareStrNoCase(LPWSTR S1, LPWSTR S2);

/* Obvious. */
KOLString GetStartDir();
KOLString GetLanguageCode();
KOLString ExcludeTrailingChar( const KOLString S, KOLChar C );
KOLString ExcludeTrailingPathDelimiter(const KOLString S);

/* Returns only path part from exact path to file. */
KOLString ExtractFilePath( const KOLString Path );

/* Extracts file name from exact path to file. */
KOLString ExtractFileName( const KOLString Path );

/* Extracts extension from file name (returns it with dot '.' first) */
KOLString ExtractFileExt( const KOLString Path );

/* Returns address of the last of delimiters given by Delimiters parameter
among characters of Str. If there are no delimiters found, position of
the null terminator in Str is returned. This function is intended
mainly to use in filename parsing functions. */
KOLChar* __DelimiterLast( KOLChar* Str, KOLChar* Delimiters );

/* Fast scans string Str of length Len searching character Chr.
Pointer to a character next to found or to Str[Len] (if no one found)
is returned. */

PKOLChar StrScanLen(PKOLChar Str, KOLChar Chr, int Len);

int IndexOfChar( const KOLString S, KOLChar Chr );
/* Returns index of given character (1..Length(S)), or
   -1 if a character not found. */

int IndexOfCharsMin( const KOLString S, const KOLString Chars );
/* Returns index (in string S) of those character, what is taking place
   in Chars string and located nearest to start of S. If no such
   characters in string S found, -1 is returned. */

int IndexOfWideCharsMin( const WideString S, const WideString Chars );
/* Returns index (in wide string S) of those wide character, what
   is taking place in Chars wide string and located nearest to start of S.
   If no such characters in string S found, -1 is returned. */

KOLString TrimLeft(const KOLString S);
/* Removes spaces, tabulations and control characters from the starting
   of string S. */
KOLString TrimRight(const KOLString S);
/* Removes spaces, tabulates and other control characters from the
   end of string S. */
KOLString Trim( const KOLString S);
/* Makes TrimLeft and TrimRight for given string. */


KOLString Parse( KOLString& S, const KOLString Separators );
/* Returns first characters of string S, separated from others by
   one of characters, taking place in Separators string, assigning
   a tail of string (after found separator) to source string. If
   no separator characters found, source string S is returned, and
   source string itself becomes empty. */

bool StrSatisfy( const KOLString S, const KOLString Mask );
/* Returns True, if S is satisfying to a given Mask (which can contain
   wild card symbols '*' and '?' interpreted correspondent as 'any
   set of characters' and 'single any character'. If there are no
   such wild card symbols in a Mask, result is True only if S is matching
   to Mask string.) */
bool _StrSatisfy( KOLChar* S, KOLChar* Mask );

int S2Int( LPWSTR S );
/* Converts null-terminated string to Integer. Scanning stopped when any
   non-digit character found. Even empty string or string not containing
   valid integer number silently converted to 0. */
int Str2Int(const KOLString Value);
/* Converts string to integer. First character, which can not be
   recognized as a part of number, regards as a separator. Even
   empty string or string without number silently converted to 0. */
KOLString Int2Hex( DWORD Value, int Digits );
/* Converts integer Value into string with hex number. Digits parameter
   determines minimal number of digits (will be completed by adding
   necessary number of leading zeros). */
KOLString Num2Bytes( double Value );
/* Converts double float to string, considering it as a bytes count.
   If Value is sufficiently large, number is represented in kilobytes (with
   following letter K), or in megabytes (M), gigabytes (G) or terabytes (T).
   Resulting string number is truncated to two decimals (.XX) or to one (.X),
   if the second is 0. */
KOLString Int2Str(int Value);
int random(int from, int to);

#endif
bool Find_First( const KOLString FilePathName, TFindFileData& F );
void Find_Close( TFindFileData& F );
bool FileExists( const KOLString FileName);
long long FileSize( const KOLString Path );
/* Returns file size in bytes without opening it. If file too large
   to represent its size as Integer, -1 is returned. */

//[Registry FUNCTIONS DECLARATIONS]

HKEY RegKeyOpenRead( HKEY Key, const KOLString SubKey );
/* Opens registry key for read operations (including enumerating of sub keys).
   Pass either handle of opened earlier key or one of constants
   HKEY_CLASSES_ROOT, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, HKEY_USERS
   as a first parameter. If not successful, 0 is returned. */
HKEY RegKeyOpenWrite( HKEY Key, const KOLString SubKey );
/* Opens registry key for write operations (including adding new values or
   sub keys), as well as for read operations too. See also RegKeyOpenRead. */
HKEY RegKeyOpenCreate(HKEY Key, const KOLString SubKey );
/* Creates and opens key. */
KOLString RegKeyGetStr(HKEY Key, const KOLString ValueName );
/* Reads key, which must have type REG_SZ (null-terminated string). If
   not successful, empty string is returned. This function as well as all
   other registry manipulation functions, does nothing, if Key passed is 0
   (without producing any error). */
KOLString RegKeyGetStrEx( HKEY Key, const KOLString ValueName );
/* Like RegKeyGetStr, but accepts REG_EXPAND_SZ type, expanding all
   environment variables in resulting string.
   |<br>
   Code provided by neuron, e-mailto:neuron@hollowtube.mine.nu */
DWORD RegKeyGetDw( HKEY Key, const KOLString ValueName );
/* Reads key value, which must have type REG_DWORD. If ValueName passed
   is '' (empty string), unnamed (default) value is reading. If not
   successful, 0 is returned. */
bool RegKeySetStr(HKEY Key, const KOLString ValueName, const KOLString Value );
/* Writes new key value as null-terminated string (type REG_SZ). If not
   successful, returns False. */
bool RegKeySetStrEx( HKEY Key, const KOLString ValueName, const KOLString Value, bool expand );
/* Writes new key value as REG_SZ or REG_EXPAND_SZ. - by neuron, e-mail to:neuron@hollowtube.mine.nu */
bool RegKeySetDw( HKEY Key, const KOLString ValueName, DWORD Value );
/* Writes new key value as DWORD (with type REG_DWORD). Returns False,
   if not successful. */
void RegKeyClose( HKEY Key );
/* Closes key, opened using RegKeyOpenRead or RegKeyOpenWrite. (But does
   nothing, if Key passed is 0). */
bool RegKeyDelete( HKEY Key, const KOLString SubKey );
/* Deletes key. Does nothing if key passed is 0 (returns FALSE). */
bool RegKeyDeleteValue( HKEY Key, const KOLString SubKey );
/* Deletes value. - by neuron, e-mail to:neuron@hollowtube.mine.nu */
//bool RegKeyExists( HKEY Key, const AnsiString SubKey );
/* Returns TRUE, if given sub key exists under given Key. */
bool RegKeyValExists( HKEY Key, const KOLString ValueName );
/* Returns TRUE, if given value exists under the Key.
*/
int RegKeyValueSize( HKEY Key, const KOLString ValueName );
/* Returns a size of value. This is a size of buffer needed to store
   registry key value. For string value, size returned is equal to a
   length of string plus 1 for terminated null character. */
int RegKeyGetBinary( HKEY Key, const KOLString ValueName, LPVOID Buffer, int Count );
/* Reads binary data from a registry, writing it to the Buffer.
   It is supposed that size of Buffer provided is at least Count bytes.
   Returned value is actual count of bytes read from the registry and written
   to the Buffer.
   |<br>
   This function can be used to get data of any type from the registry, not
   only REG_BINARY. */
bool RegKeySetBinary( HKEY Key, const KOLString ValueName, LPVOID Buffer, int Count );
/* Stores binary data in the registry. */
time_t RegKeyGetDateTime(HKEY Key, const KOLString ValueName);
/* Returns DateTime variable stored in registry in binary format. */
bool RegKeySetDateTime(HKEY Key, const KOLString ValueName, time_t DateTime);
/* Stores DateTime variable in the registry. */

//-------------------------------------------------------
// registry functions by Valerian Luft <luft@valerian.de>
//-------------------------------------------------------
bool RegKeyGetSubKeys( const HKEY Key, PKOLStrList List );
/* The function enumerates sub keys of the specified open registry key.
   True is returned, if successful.
*/
bool RegKeyGetValueNames(const HKEY Key, PKOLStrList List );
/* The function enumerates value names of the specified open registry key.
   True is returned, if successful.
*/
DWORD RegKeyGetValueTyp (const HKEY Key, const KOLString ValueName);
/* The function receives the type of data stored in the specified value.
   |<br>
   If the function fails, the return value is the Key value.
   |<br>
   If the function succeeds, the return value return will be one of the following:
   |<br>
   REG_BINARY , REG_DWORD, REG_DWORD_LITTLE_ENDIAN,
   REG_DWORD_BIG_ENDIAN, REG_EXPAND_SZ, REG_LINK , REG_MULTI_SZ,
   REG_NONE, REG_RESOURCE_LIST, REG_SZ
*/