#include "MainUnt.h"
#include "AddonFuncUnt.h"
#include "resource.h"
#include <QPainter>

void CMainWnd::ShowFlowingOnStageFork( PageMarkDirection direction )
{
    fScrollDirection = direction;
    UFUNCTION func;
    func.Data = this;
    func.Code = &CMainWnd::OnFlowingKeeperThreadExecute;
    //EnterCriticalSection(&fFlowingcs);
    if (fFlowingKeeperThread != 0) {
        delete fFlowingKeeperThread;
        fFlowingKeeperThread = 0;
    }
    fFlowingKeeperThread = new TThread(func, THREAD_PRIORITY_ABOVE_NORMAL);
}

int CMainWnd::OnFlowingKeeperThreadExecute(TThread* Sender) {
    fDiscardClick = true;
    EnterCriticalSection(&fFlowingcs);
    int limitstart = GetTickCount();
    int width = (fFullScreen?720:615), height = 480;
    QImage groundimage, coverimage;
    int groundindex, coverindex;
    if (fScrollDirection == pmdRight || fScrollDirection == pmdDown) {
        groundindex = NormalizeScrollIndex(fCurrentScrollIndex + 1);
        coverindex = NormalizeScrollIndex(fCurrentScrollIndex);
    } else if (fScrollDirection == pmdLeft || fScrollDirection == pmdUp) {
        groundindex = NormalizeScrollIndex(fCurrentScrollIndex - 1);
        coverindex = NormalizeScrollIndex(fCurrentScrollIndex);
    }
    if (fBuildingPrefetch && fPrefetchBuildingIndex == groundindex) {
        // last building running now
        RETAILMSG(true, (L"Waiting for last Building...\n"));
        EnterCriticalSection(&fBuildingcs); // wait for signal
        LeaveCriticalSection(&fBuildingcs); // fake leave
        RETAILMSG(true, (L"Last Building done.\n"));
    } else {
        BuildSinglePrefetch(groundindex, width, height, true);
    }
    if (fFetchedQImages.contains(groundindex)) {
        if (fFetchedQImages[groundindex].FitImage) {
            groundimage = *fFetchedQImages[groundindex].FitImage;
        }
    }

    if (fBuildingPrefetch && fPrefetchBuildingIndex == coverindex) {
        // last building running now
        EnterCriticalSection(&fBuildingcs); // wait for signal
        LeaveCriticalSection(&fBuildingcs); // fake leave
    } else {
        BuildSinglePrefetch(coverindex, width, height, true);
    }
    if (fFetchedQImages.contains(coverindex)) {
        if (fFetchedQImages[coverindex].FitImage) {
            coverimage = *fFetchedQImages[coverindex].FitImage;
        }
    }

    if (groundimage.isNull()){
        groundimage = QImage(width, height, QImage::Format_ARGB32);
        groundimage.fill(0xFF000000);
    }
    if (coverimage.isNull()) {
        coverimage = QImage(width, height, QImage::Format_ARGB32);
        coverimage.fill(0xFFFFFFFF);
    }
    int scaletime = -1;
    if (groundimage.width() < width && groundimage.height() < height) {
        groundimage = groundimage.copy((groundimage.width() - width) / 2, (groundimage.height() - height) / 2, width, height );
    } else {
        PrepareScaledQPicture(groundimage, width, height, scaletime);
    }
    if (coverimage.width() < width && coverimage.height() < height) {
        coverimage = coverimage.copy((coverimage.width() - width) / 2, (coverimage.height() - height) / 2, width, height );
    } else {
        PrepareScaledQPicture(coverimage, width, height, scaletime);
    }

    //if (groundimage.width() != width || groundimage.height() != height) {
    //    groundimage = groundimage.copy((groundimage.width() - width) / 2, (groundimage.height() - height) / 2, width, height );
    //}

    int offsetx = 0, offsety = 0;
    qreal rotation = 0, opacity = 1;
    int step = 7;
    while (GetTickCount() - limitstart < 1800 && opacity > 0) {
        MixPhotoOnStage(groundimage, coverimage, offsetx, offsety, rotation, opacity);
        if (fScrollDirection == pmdRight) {
            offsetx += 12;
            offsety += 26;
            if (step < 0) {
                rotation += 3;
            }
        } else if (fScrollDirection == pmdLeft) {
            offsetx += 12;
            offsety -= 26;
            if (step < 0) {
                rotation -= 3;
            }
        } else if (fScrollDirection == pmdUp) {
            offsetx += 40;
        } else if (fScrollDirection == pmdDown) {
            offsetx -= 40;
        }
        if (step < 0) {
            opacity -= 0.15;
        }
        step--;
    }
    fDiscardClick = false;
    LeaveCriticalSection(&fFlowingcs);
    //Sender->Terminate();
    //delete Sender;
    return 0;
}


void CMainWnd::MixPhotoOnStage(const QImage& groundimage, const QImage& coverimage, int offsetx, int offsety, qreal rotation, qreal opacity)
{
    LPBYTE dest;
    bool blockable = false;
    int surfacerotation = 0;
    DDSURFACEDESC surface;
    bool stagesizematched = CheckStageMatchDesign(surface, surfacerotation, dest, blockable);

    if (stagesizematched) {
        if (surfacerotation == 0 || surfacerotation == 180) {
            // Time to scale
            //QImage qimage(surface.dwHeight, surface.dwWidth, QImage::Format_ARGB32);
            QImage qimage = groundimage.copy((groundimage.width() - int(surface.dwHeight)) / 2, (groundimage.height() - int(surface.dwWidth)) / 2, surface.dwHeight, surface.dwWidth );
            QPainter painter(&qimage); // draw on qimage
            //painter.drawImage(0, 0, groundimage); // hey ground, 100% fill
            if (rotation != 0) {
                QMatrix matrix;
                matrix.rotate(rotation);
                painter.setMatrix(matrix, true);
            }
            if (opacity < 1) {
                painter.setOpacity(opacity);
            }
            painter.setRenderHint(QPainter::Antialiasing, false);
            painter.drawImage(offsetx, offsety, coverimage);
            if (fPaperBorder) {
                // Add frame on cover
                // anti aliasing here more effective than pre-bordered cover image
                int frameborder = 10;
                QRect rect = coverimage.rect();
                rect.moveTopLeft(QPoint(offsetx, offsety));
                rect.adjust(-frameborder, -frameborder, frameborder, frameborder);
                painter.setPen(QPen(QColor(0xF7F0D9), frameborder * 2 + 1, Qt::SolidLine));
                painter.setBrush(Qt::NoBrush);
                painter.setRenderHints(QPainter::Antialiasing | QPainter::NonCosmeticDefaultPen);
                painter.drawRoundedRect(rect, 3, 3, Qt::AbsoluteSize);
            }
            painter.end();

            PaintFlushStage(qimage, dest, surface);
        }
        //stage1.UnLockData(true);
    }
}

void CMainWnd::ShowAnimationOnStageFork()
{
    if (fAnimationKeeperThread != 0) {
        if (fPageMark || fZoomMark)
            return;
        delete fAnimationKeeperThread;
        fAnimationKeeperThread = 0;
    }
    UFUNCTION func;
    func.Data = this;
    func.Code = &CMainWnd::OnAnimationKeeperThreadExecute;
    //EnterCriticalSection(&fFlowingcs);

    fAnimationKeeperThread = new TThread(func, THREAD_PRIORITY_NORMAL);
}

int CMainWnd::OnAnimationKeeperThreadExecute(TThread* Sender) {
    int oldindex = fCurrentScrollIndex;
    //ExtraInfoLevel oldextra = fExtraInfo;
    //fExtraInfo = eilBench;
    if (!fFetchedQImages.contains(oldindex)) {
        return -1;
    }
    EnterCriticalSection(&fAnimatecs);
    fKeepAni = true;
    //stage1.Flip();
    int framecount = -1;
    bool alphaworkaround = false;
    QImage qimage;
    TPrefetchRecItem& preqimage = fFetchedQImages[oldindex]; // mosttime
    if (preqimage.AnimateStore->fileName().isEmpty()) {
        preqimage.AnimateStore->setDevice(preqimage.AnimateStore->device()); // Rewind
    } else {
        preqimage.AnimateStore->setFileName(preqimage.AnimateStore->fileName()); // Rewind
    }
    while (oldindex == fCurrentScrollIndex && fStageShowMode == ssmFit && fKeepAni) {
        // TODO: check insertion
        bool insertion = !fFetchedQImages.contains(oldindex);
        if (insertion) {
            break;
        }

        //QImage qimage;
        EnterCriticalSection(&fPreqMapcs);
        if (preqimage.FromAnimate == false || preqimage.AnimateStore == NULL || oldindex != fCurrentScrollIndex) {
            LeaveCriticalSection(&fPreqMapcs);
            break;
        }
        int waittick = -1;
        if (preqimage.FromAnimate && preqimage.AnimateStore != NULL && oldindex == fCurrentScrollIndex) {
            waittick = preqimage.AnimateStore->nextImageDelay();

            if (preqimage.AnimateStore->canRead()) {
                //RETAILMSG(true, (L"frame: %d\n", preqimage.AnimateStore->currentImageNumber()));
                if (preqimage.AnimateStore->currentImageNumber() == -1 && framecount == -1) {
                    preqimage.AnimateStore->read(&qimage); // hatsukoi
                    alphaworkaround = qimage.hasAlphaChannel();
                    if (alphaworkaround) {
                        ReplaceAlphaWithChecker(qimage);
                    }
                } else {
                    // n, 0
                    // 0, m
                    // TODO: Crop Detection
                    // TODO: merge workaround into qgif
                    QImage nextimage;
                    preqimage.AnimateStore->read(&nextimage);
                    if (alphaworkaround) {
                        ReplaceAlphaWithChecker(nextimage);
                    }
                    QPainter nextpainter(&qimage);
                    nextpainter.drawImage((qimage.width() - nextimage.width()) / 2, 0, nextimage);
                    nextpainter.end();
                    //RETAILMSG(qimage.width() != nextimage.width(), (L"netximage w%d drawled on w%d, Alpha: %S\n", nextimage.width(), qimage.width(), nextimage.hasAlphaChannel()?"true":"false"));
                }
            } else {
                if (framecount == -1) {
                    framecount = preqimage.AnimateStore->currentImageNumber() + 1; // only once
                }
                //preqimage.AnimateStore->jumpToImage(0); // Qgifhandler can't jump to 0
                if (preqimage.AnimateStore->fileName().isEmpty()) {
                    preqimage.AnimateStore->setDevice(preqimage.AnimateStore->device()); // Rewind
                } else {
                    preqimage.AnimateStore->setFileName(preqimage.AnimateStore->fileName()); // Rewind
                }
                LeaveCriticalSection(&fPreqMapcs);
                continue;
            }

        }
        LeaveCriticalSection(&fPreqMapcs);

        // TODO: fast paint
        bool isvalid = qimage.isNull() == false;
        LPBYTE dest;
        bool blockable = false;
        int rotation = 0;
        DDSURFACEDESC surface;
        bool stagesizematched = CheckStageMatchDesign(surface, rotation, dest, blockable);

        if (stagesizematched) {
            if (isvalid) {
                if (fOverlayMode) {
                    // TODO: blockable check
                    if (qimage.width() != int(surface.dwHeight)) {
                        qimage = qimage.copy((qimage.width() - int(surface.dwHeight)) / 2, 0, int(surface.dwHeight), qimage.height());
                    }
                    LPBYTE src = qimage.bits();
                    int width = qimage.width();
                    int height = qimage.height();

                    // qimage supports 1, 8, 16, 32
                    if (qimage.depth() == 32) {
                        if ((int(surface.dwWidth) > qimage.height())) {
                            // move dest
                            // add border
                            int startoffset = ((int(surface.dwWidth) - qimage.height())) / 2 * surface.dwHeight * 4;
                            memset(dest, 0, startoffset);
                            dest += startoffset;
                            memset(dest + surface.dwHeight * qimage.height(), 0, surface.dwWidth * surface.dwHeight * 4 - surface.dwHeight * qimage.height() - startoffset);
                        } else {
                            // trim src
                            src += (qimage.height() - int(surface.dwWidth)) / 2 * qimage.bytesPerLine();
                        }
                        memcpy(dest, src, min(surface.dwWidth * surface.dwHeight * 4, width * height * 4));
                        //armmemcpy(dest, src, min(surface.dwWidth * surface.dwHeight * 4, width * height * 4));
                        //optmemcpy(dest, src, min(surface.dwWidth * surface.dwHeight * 4, width * height * 4));
                    } else if (qimage.depth() == 16) {

                    }
                    stage1.UnLockData(true);
                } else {
                    // GDIMode
                    int width = qimage.width();
                    int height = qimage.height();
                    HDC tempDC = GetDC(m_hWnd);

                    // Extract from QImage
                    // Define the header
                    BITMAPINFO bmi;
                    memset(&bmi, 0, sizeof(bmi));
                    bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
                    bmi.bmiHeader.biWidth       = width;
                    bmi.bmiHeader.biHeight      = -height;
                    bmi.bmiHeader.biPlanes      = 1;
                    bmi.bmiHeader.biBitCount    = 32;
                    bmi.bmiHeader.biCompression = BI_RGB;
                    bmi.bmiHeader.biSizeImage   = width * height * 4;

                    // Create the pixmap
                    uchar *pixels = 0;
                    HBITMAP bitmap = CreateDIBSection(tempDC, &bmi, DIB_RGB_COLORS, (void **) &pixels, 0, 0);

                    // Copy over the data
                    QImage::Format imageFormat = QImage::Format_RGB32;
                    if (qimage.format() != imageFormat) {
                        qimage = qimage.convertToFormat(imageFormat);
                    }
                    //const QImage image = qimage.convertToFormat(imageFormat);
                    int bytes_per_line = width * 4;
                    for (int y = 0; y < height; ++y) {
                        memcpy(pixels + y * bytes_per_line, qimage.scanLine(y), bytes_per_line);
                    }
                    // end of code sniper

                    HBITMAP HB1 = bitmap;//QPixmap::fromImage(qimage).toWinHBITMAP();
                    HDC MemDC = CreateCompatibleDC(tempDC);
                    HBITMAP oldBitmap = HBITMAP(SelectObject(MemDC, HB1));

                    BLENDFUNCTION bf = {AC_SRC_OVER, 0, 255, AC_SRC_ALPHA};
                    //int starttime = GetTickCount();
                    bool failed = true;
                    if (fEmulatorDetected) {
                        failed = BitBlt(tempDC, max(0, (fStageSize.stage1width - width) / 2), max(0, (fStageSize.stage1height - height) / 2), min(fStageSize.stage1width, width), min(fStageSize.stage1height, height),
                            MemDC, max(0, (width - fStageSize.stage1width) / 2), max(0, (height - fStageSize.stage1height) / 2), SRCCOPY) == FALSE;
                    } else {
                        failed = AlphaBlend(tempDC, max(0, (fStageSize.stage1width - width) / 2), max(0, (fStageSize.stage1height - height) / 2), min(fStageSize.stage1width, width), min(fStageSize.stage1height, height),
                            MemDC, max(0, (width - fStageSize.stage1width) / 2), max(0, (height - fStageSize.stage1height) / 2), min(fStageSize.stage1width, width), min(fStageSize.stage1height, height), bf) == FALSE;
                    }
                    //drawtime = GetTickCount() - starttime;
                    RETAILMSG(failed, (_T("draw failed in PaintStage.\n")));
                    DeleteObject(SelectObject(MemDC, HGDIOBJ(oldBitmap)));
                    DeleteDC(MemDC);

                    ReleaseDC(m_hWnd, tempDC);
                }
            }

        } else {
            // we should never go here
            NKDbgPrintfW(L"Stage Mismatched in AnimationThread\n");
        }
        int starttick = GetTickCount();
        while (GetTickCount() - starttick < waittick && fKeepAni) {
            ProcessMessage(); // TODO: ProcessQuit Message
            Sleep(0);
        }
    }
    RETAILMSG(true, (L"Leave Animation Thread.\n"));
    //if (fExtraInfo == eilSilent) {
    //    fExtraInfo = oldextra;
    //}
    LeaveCriticalSection(&fAnimatecs); // TODO: Painter
    //Sender->Terminate();
    //delete Sender;
    return 0;
}

void CMainWnd::TryStopAnimation() 
{
    if (fAnimationKeeperThread == NULL) {
        return;
    } else if (fAnimationKeeperThread->GetTerminated() == false) {
        fKeepAni = false;
        fPauseAni = true;
        delete fAnimationKeeperThread; // self WaitFor?
        fAnimationKeeperThread = 0;
    }
}

void CMainWnd::PaintStageFork() {

    if (fStagePainterThread != 0) {
        //delete fStagePainterThread;
        //fStagePainterThread = 0;
        //delete fPrefetchBuilderThread;
        //fPrefetchBuilderThread = 0;
        if (fStagePainterThread->GetTerminated()) {
            NKDbgPrintfW(L"Reuse fStagePainterThread.\n");
            fStagePainterThread->Execute();
        } else {
            // Wait
            NKDbgPrintfW(L"Wait next chance for fStagePainterThread.\n");
        }
    } else {
        UFUNCTION func;
        func.Data = this;
        func.Code = &CMainWnd::OnStagePainterThreadExecute;
        fStagePainterThread = new TThread(func);
    }
}

int CMainWnd::OnStagePainterThreadExecute(TThread* Sender) {
    PaintStage(fCurrentScrollIndex);
    return 0;
}

void CMainWnd::ShowWaitingMarkOnStageFork()
{
    // This let TryStopAnimation Stop either "OnStage" Animation
    // and TryRefresh Stage check both Waiting or Animation Stockers
    if (fAnimationKeeperThread != 0) {
        if (fPageMark || fZoomMark)
            return;
        delete fAnimationKeeperThread;
        fAnimationKeeperThread = 0;
    }
    UFUNCTION func;
    func.Data = this;
    func.Code = &CMainWnd::OnWaitingMarkKeeperThreadExecute;
    //EnterCriticalSection(&fFlowingcs);

    fAnimationKeeperThread = new TThread(func, THREAD_PRIORITY_ABOVE_NORMAL);
}

int CMainWnd::OnWaitingMarkKeeperThreadExecute(TThread* Sender) {
    int oldindex = fCurrentScrollIndex;
    EnterCriticalSection(&fAnimatecs);
    fKeepAni = true;
    int waittick = 500;

    QImage qimage;
    QImage waitingstripe = NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_WAITINGSTRIPE));
    int stripetop = 0;
    while (oldindex == fCurrentScrollIndex && fStageShowMode == ssmFit && fKeepAni) {
        if (fMarkBaseCache.isNull()) {
            break;
        }

        QImage qimage = fMarkBaseCache;

        // TODO: fast paint
        bool isvalid = qimage.isNull() == false;
        LPBYTE dest;
        bool blockable = false;
        int rotation = 0;
        DDSURFACEDESC surface;
        bool stagesizematched = CheckStageMatchDesign(surface, rotation, dest, blockable);

        if (stagesizematched) {
            if (isvalid) {
                QPainter painter(&qimage);
                if (stripetop >= waitingstripe.height()) {
                    stripetop = 0;
                }
                painter.drawImage((qimage.width() - waitingstripe.width()) / 2, (qimage.height() - 140) / 2, waitingstripe, 0, stripetop, -1, 140);
                stripetop += 140;
                painter.end();

                PaintFlushStage(qimage, dest, surface, NULL);
            }

        } else {
            // we should never go here
            NKDbgPrintfW(L"Stage Mismatched in WaitingMark AnimationThread\n");
        }
        int starttick = GetTickCount();
        while (GetTickCount() - starttick < waittick && fKeepAni) {
            ProcessMessage(); // TODO: ProcessQuit Message
            Sleep(1);
        }
    }
    RETAILMSG(true, (L"Leave Waiting Mark Thread.\n"));
    LeaveCriticalSection(&fAnimatecs); // TODO: Painter
    return 0;
}
