#include <mzfc_inc.h>
#include "AddonFuncUnt.h"
#include <FvMzKOL.h>
#include "reader/exifdefine.h"

#include <QtGlobal>
#include <QtEndian>
#include <QCryptographicHash>
#include <QPainter>
#include <..\src\3rdparty\zlib\zlib.h>

QImage NewQImageFromResource( HMODULE hModule, LPCTSTR lpType, LPCTSTR lpName )
{
    QImage Result;
    LoadQImageFromResource(Result, hModule, lpType, lpName);
    return Result;
}

bool LoadQImageFromResource(QImage& qimage, HMODULE hModule, LPCTSTR lpType, LPCTSTR lpName) {
    HRSRC resinfo = FindResource(hModule, lpName, lpType);
    HGLOBAL resdata = LoadResource(hModule, resinfo);
    DWORD ressize = SizeofResource(hModule, resinfo);
    LPTSTR resbyte = LPTSTR(LockResource(resdata));
    qimage.loadFromData((uchar*)resbyte, ressize);
    return resbyte != 0;
}

void NormalizeGrayScaleQImage( QImage &qimage )
{
    LPBYTE src = qimage.bits();
    QImage viaimage = QImage(qimage.width(), qimage.height(), QImage::Format_RGB32);
    LPDWORD var = LPDWORD(viaimage.bits());
    LPBYTE srcend = src + qimage.width() * qimage.height();
    while (src < srcend)
    {
        *var = *src | *src << 8 | *src << 16;
        var++;
        src++;
    }
    qimage = viaimage;
}

void ReplaceAlphaWithChecker( QImage& qimage )
{
    // TODO: optimize for huge piece
    // Inplace Draw, Inplace QImage format assign / Premulti
    QImage box(32, 32, qimage.format());
    QPainter pmp(&box);
    pmp.fillRect(0, 0, 16, 16, Qt::lightGray);
    pmp.fillRect(16, 16, 16, 16, Qt::lightGray);
    pmp.fillRect(0, 16, 16, 16, Qt::darkGray);
    pmp.fillRect(16, 0, 16, 16, Qt::darkGray);
    pmp.end();

    QImage viaimage = QImage(qimage);
    QPainter checkermaker(&qimage);
    QBrush checker;
    checker.setTextureImage(box);
    checkermaker.fillRect(qimage.rect(), checker);
    checkermaker.drawImage(0, 0, viaimage);
    checkermaker.end();

    if (qimage.format() == QImage::Format_ARGB32) {
        qimage = qimage.convertToFormat(QImage::Format_RGB32);
    }
}

int GetFreePhysMemory() {
    MEMORYSTATUS ms;
    ms.dwLength = sizeof (MEMORYSTATUS);
    GlobalMemoryStatus(&ms);
    return ms.dwAvailPhys;
}


QImage NewQImageFromEXIFThumbnail( QFile& qfile )
{
    quint16 NextMarker = readuint16(qfile, true);
    if (NextMarker != M_SOI) {
        return QImage();
    }
    bool app1found = false, eoifound = false;
    int app1offset;
    while (app1found == false && eoifound == false) {
        NextMarker = readuint16(qfile, true);
        app1found = NextMarker == M_APP1;
        eoifound = NextMarker == M_EOI;
        if (app1found) {
            app1offset = qfile.pos();
            break;
        }
        if (NextMarker == M_SOS) { // SOS
            // Jump more bytes
            qfile.seek(qfile.size() - 2);
            NextMarker = readuint16(qfile, true); // replace
            // skip pending
            int fpos2 = 0;
            while(NextMarker != M_EOI) {
                qfile.seek(qfile.size() - 2 - (++fpos2)); // rock back
                NextMarker = readuint16(qfile, true);
            } 
        }
        if (NextMarker != M_EOI) {
            quint16 len = readuint16(qfile, true);
            qfile.seek(qfile.pos() + len - sizeof(len));
        }
    }
    if (app1found) {
        quint16 len = readuint16(qfile, true);
        QByteArray EXIF = qfile.read(6);
        if (QString::fromAscii(EXIF.constData()) == "Exif") {
            quint64 offset = qfile.pos(); // - EXIF.size();
            QByteArray align = qfile.read(2);
            bool islittleendian = align[0] == 'I';
            quint16 tagMark = readuint16(qfile, !islittleendian); // 2a

            quint32 offsetFirstIFD = readuint32(qfile, !islittleendian); // 8

            if(offsetFirstIFD!=8) 
                qfile.seek(offset + offsetFirstIFD);

            // ifdMainImage
            quint16 nDirEntry = readuint16(qfile, !islittleendian); // C

            qfile.seek(qfile.pos() + 0xC * nDirEntry); // TODO: nComponent

            quint32 nextIFDoffset = readuint32(qfile, !islittleendian); // 4E6

            if (nextIFDoffset > 0) {
                // ifdThumbnailImage
                qfile.seek(offset + nextIFDoffset);
                quint16 nDirEntry = readuint16(qfile, !islittleendian); // 8

                //qfile.seek(qfile.pos() + 0xC * nDirEntry);
                //quint32 nextIFDoffset;
                qint32 thumbOffset = 0;
                qint32 thumbLength = 0;
                qint16 compression = 10;

                for (int i = 0; i < nDirEntry; i++)
                {
                    quint16 tagNumber = readuint16(qfile, !islittleendian);

                    switch (tagNumber)
                    {
                    case Compression:
                        // + 8
                        qfile.seek(qfile.pos() + 6); // format, nComponet
                        compression = readuint16(qfile, !islittleendian);
                        qfile.seek(qfile.pos() + 2); // padding
                        break;
                    case ThumbnailOffset:
                    case StripOffsets:
                        qfile.seek(qfile.pos() + 6);
                        thumbOffset = readuint32(qfile, !islittleendian);
                        break;
                    case ThumbnailLength:
                    case StripByteCounts:
                        qfile.seek(qfile.pos() + 6);
                        thumbLength = readuint32(qfile, !islittleendian);
                        break;
                    default:
                        qfile.seek(qfile.pos() + 10);
                    }
                }

                if(thumbLength && thumbOffset) {
                    qfile.seek(offset + thumbOffset);
                    //printf("Thumbnail Offset = %#Lx\n",offset + thumbOffset);
                    if(compression == 6) {
                        //local quad JpegFileEnd2 = JpegFileEnd;
                        //JpegFileEnd = FTell() + thumbLength;
                        //struct JPGFILE thumbnail;
                        //JpegFileEnd = JpegFileEnd2;
                        QByteArray thumbbuf = qfile.read(thumbLength);
                        const QImage Result = QImage::fromData(thumbbuf);
                        return Result;
                    }
                    else {
                        //char imageData[thumbLength];
                    }
                }

            }
        }
    }
    return QImage();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// sticky safe conversation from ANSI string to wide string
wstring StringToWideString(const string& Source,unsigned int Codepage) {
    int len;
#ifdef _WIN32
    len = MultiByteToWideChar(Codepage, MB_PRECOMPOSED, Source.c_str(), static_cast<int>(Source.length()), NULL, 0);
#elif defined _LINUX
    char* oldlocale	= setlocale(LC_CTYPE, "GBK");
    char* savedlocale = strdup(oldlocale);
    len = mbstowcs(NULL, Source.c_str(), 0);
#endif
    if (0 >= len){
        return wstring(_L(""));
    } else {
        WCHAR * DestBuf = LPWSTR(malloc(sizeof(WCHAR)*(len + 1)));
        if (NULL==DestBuf) {
            return wstring(_L(""));
        } 
        DestBuf[len] = '\0';
#ifdef _WIN32
        bool done = (0 <= MultiByteToWideChar(Codepage, MB_PRECOMPOSED, Source.c_str(), static_cast<int>(Source.length()), DestBuf, len));
#elif defined _LINUX
        bool done = (0 <= mbstowcs(DestBuf, Source.c_str(), Source.length()));
        (void)setlocale(LC_CTYPE, savedlocale);
        free(savedlocale);
#endif
        if (done) {
            const wstring Result = wstring(DestBuf);
            free(DestBuf);
            return Result;
        } else {
            free(DestBuf);
            return wstring(_L(""));
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////

quint16 readuint16( QFile& qfile, bool bigendian /*= false*/, bool peek /*= false*/ )
{
    quint16 Result;
    qfile.read((char*)&Result, sizeof(Result));
    if (bigendian) {
        Result = qFromBigEndian(Result);
    } else {
        Result = qFromLittleEndian(Result);
    }
    if (peek) {
        qfile.seek(qfile.pos() - sizeof(Result));
    }
    return Result;
}

quint32 readuint32( QFile& qfile, bool bigendian /*= false*/, bool peek /*= false*/ )
{
    quint32 Result;
    qfile.read((char*)&Result, sizeof(Result));
    if (bigendian) {
        Result = qFromBigEndian(Result);
    } else {
        Result = qFromLittleEndian(Result);
    }
    if (peek) {
        qfile.seek(qfile.pos() - sizeof(Result));
    }
    return Result;
}

__declspec( naked ) void _cdecl fakememcpy(__out_bcount_full_opt(_Size) void * _Dst, __in_bcount_opt(_Size) const void * _Src, __in size_t _Size) {
    //_asm {
    //}
    _Dst = 0;
}

void *optmemcpy(__out_bcount_full_opt(_Size) void * _Dst, __in_bcount_opt(_Size) const void * _Src, __in size_t _Size) {
    char *dst8 = (char *)_Dst;
    char *src8 = (char *)_Src;

    if (_Size & 1) {
        dst8[0] = src8[0];
        dst8 += 1;
        src8 += 1;
    }

    _Size /= 2;
    while (_Size--) {
        dst8[0] = src8[0];
        dst8[1] = src8[1];

        dst8 += 2;
        src8 += 2;
    }
    return _Dst;
}

TImageType GuessMIMEType(QIODevice* file) {
    TImageType Result = itUnknown;
    if (file == NULL) {
        return Result;
    }
    if (!file->isOpen()) {
        file->open(QIODevice::ReadOnly);
    }
    if (file->isReadable() == false) {
        return Result;
    }
    byte headerbuf[6] = {0};
    file->read(LPSTR(&headerbuf[0]), sizeof headerbuf);

    if (*PDWORD(&headerbuf[0]) == 0x00424949 || *PDWORD(&headerbuf[0]) == 0x42004D4D) {
        // II4200, MM0042
        Result = itTarga;
    }
    if (*PWORD(&headerbuf[0]) == 0x4D42) {
        // BM
        Result = itBitmap;
    }
    if (*PWORD(&headerbuf[0]) == 0xD8FF) {
        // JFIF Magic
        Result = itJPEG;
    }
    if (*PDWORD(&headerbuf[0]) == 0x38464947 && headerbuf[4] == 0x39) {
        // GIF89 Magic
        Result = itGif;
    }
    if (*PDWORD(&headerbuf[0]) == 0x474E5089) {
        // JFIF Magic
        Result = itPNG;
    }
    if (*PDWORD(&headerbuf[0]) == 0x01BC4949 || *PDWORD(&headerbuf[0]) == 0xBC014D4D) {
        // IIBC01, MM01BC
        Result = itHDPhoto;
    }
    if (*PDWORD(&headerbuf[0]) == 0x04034B50) {
        // PK Magic
        Result = itPKArchive;
    }
    if (*PDWORD(&headerbuf[0]) == 0x21726152 && headerbuf[4] == 0x1A && headerbuf[5] == 0x07) {
        // Rar!1A07 Magic
        Result = itRarArchive;
    }
    return Result;
}

TImageType GuessMIMEType(LPWSTR filename) {
    TImageType Result = itUnknown;

    QFile inputfile(QString::fromUtf16((ushort*)filename));

    Result = GuessMIMEType(&inputfile);

    inputfile.close();
    return Result;
}

void DrawSplashOnWorld() {
    ImagingHelper helper;
    helper.LoadImage(GetStartDir() + L"SplashScreen.png", false, true, true);
    HDC desktopDC = GetDC(0);
    RECT rect = MzGetWorkArea();
    rect.left = (RECT_WIDTH(rect) - helper.GetImageWidth()) / 2;
    rect.top = (RECT_HEIGHT(rect) - helper.GetImageHeight()) / 2;
    rect.right = rect.left + helper.GetImageWidth();
    rect.bottom = rect.top + helper.GetImageHeight();
    helper.Draw(desktopDC, &rect);
    ReleaseDC(0, desktopDC);
}

bool RotateWorld(DWORD Orientation) {
    // TODO: detect emulator
    DEVMODE devmode = {0};
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmFields = DM_DISPLAYORIENTATION; // | DM_BITSPERPEL;
    devmode.dmDisplayOrientation = Orientation;
    // devmode.dmBitsPerPel = 32; // TODO: Emulator 1M framebuffer code limit
    bool changefailed = ChangeDisplaySettingsEx(NULL, &devmode, NULL, CDS_TEST, NULL) != DISP_CHANGE_SUCCESSFUL;
    if (!changefailed /*&& devmode.dmDisplayOrientation != Orientation*/) {
        devmode.dmDisplayOrientation = Orientation;
        // *devmode.dmBitsPerPel = 32;
        //changefailed = ChangeDisplaySettingsEx(NULL, &devmode, NULL, 0, NULL) != DISP_CHANGE_SUCCESSFUL;
        int retcode = ChangeDisplaySettingsEx(NULL, &devmode, NULL, 0, NULL);
        changefailed = retcode != DISP_CHANGE_SUCCESSFUL;
        RETAILMSG(changefailed, (L"ChangeDisplaySettingsEx Failed with Result: %d\n", retcode));
    }
    return !changefailed;
}

bool IsEmulator() {
    /*DEVMODE devmode = {0};
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmFields = DM_BITSPERPEL;
    devmode.dmBitsPerPel = 32;
    // Passing NULL for the lpDevMode parameter and 0 for the dwFlags parameter is the easiest way to return to the default mode after a dynamic mode change. 
    bool changefailed = ChangeDisplaySettingsEx(NULL, &devmode, NULL, CDS_TEST, NULL) != DISP_CHANGE_SUCCESSFUL;
    return changefailed;*/
    bool Result = false;
    HKEY DllKey = RegKeyOpenRead(HKEY_LOCAL_MACHINE, L"Drivers\\Display\\S3C2410\\CONFIG");
    if (DllKey > 0) {
        if (AnsiCompareStrNoCase(RegKeyGetStr(DllKey, L"DisplayDll"), L"DeviceEmulator_lcd.dll") == 0) {
            Result = true;
        }
        RegKeyClose(DllKey);
    }
    return Result;
}

bool Check0619ROM()
{
    bool Result = false;
    // check checksum of alarm.dll, 1st file
    //QString filename = QString::fromUtf16((ushort*)GetStartDir().C_Str()) + "alarm.dll";
    QFile inputfile("\\Windows\\alarm.dll");
    inputfile.open(QIODevice::ReadOnly);
    if (inputfile.isReadable() == false) {
        return Result;
    }

    QCryptographicHash alarmhash(QCryptographicHash::Sha1);
    alarmhash.addData(inputfile.readAll());

    if (alarmhash.result().toHex() == "e570afbad549cea54c106c40883361c2852aa0bf") {
        Result = true;
    }

    return Result;
}

extern "C" {
int Q_ZEXPORT uncompress2 (
Bytef *dest,
uLongf *destLen,
const Bytef *source,
uLong sourceLen)
{
    z_stream stream;
    int err;

    stream.next_in = (Bytef*)source;
    stream.avail_in = (uInt)sourceLen;
    stream.total_in = (uInt)sourceLen;
    /* Check for source > 64K on 16-bit machine: */
    if ((uLong)stream.avail_in != sourceLen) return Z_BUF_ERROR;

    stream.next_out = dest;
    stream.avail_out = (uInt)*destLen;
    stream.total_out = (uInt)*destLen;
    if ((uLong)stream.avail_out != *destLen) return Z_BUF_ERROR;

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;

    err = inflateInit2(&stream, -MAX_WBITS);

    if (err != Z_OK) return err;

    err = inflate(&stream, Z_FINISH);

    if (err != Z_STREAM_END) {
        inflateEnd(&stream);
        if (err == Z_NEED_DICT || (err == Z_BUF_ERROR && stream.avail_in == 0))
            return Z_DATA_ERROR;
        return err;
    }
    *destLen = stream.total_out;

    err = inflateEnd(&stream);
    return err;
}
}

// take from qbytearray.cpp
QByteArray qUncompressEx(const uchar* data, int nbytes, int outputbytes)
{
    if (!data) {
        qWarning("qUncompress: Data is null");
        return QByteArray();
    }
    if (nbytes <= 0) {
        if (nbytes < 0 || (outputbytes != 0))
            qWarning("qUncompress: Input data is corrupted");
        return QByteArray();
    }
    ulong expectedSize = outputbytes;
    ulong len = qMax(expectedSize, 1ul);
    QByteArray baunzip;
    int res;
    do {
        baunzip.resize(len);
        res = ::uncompress2((uchar*)baunzip.data(), &len,
            (uchar*)data, nbytes);

        switch (res) {
        case Z_OK:
            if ((int)len != baunzip.size())
                baunzip.resize(len);
            break;
        case Z_MEM_ERROR:
            qWarning("qUncompress: Z_MEM_ERROR: Not enough memory");
            break;
        case Z_BUF_ERROR:
            len *= 2;
            break;
        case Z_DATA_ERROR:
            qWarning("qUncompress: Z_DATA_ERROR: Input data is corrupted");
            break;
        }
    } while (res == Z_BUF_ERROR);

    if (res != Z_OK)
        baunzip = QByteArray();

    return baunzip; // @CopyRecord?
}

TImageType ExtToMIMEType( const QString& extname )
{
    TImageType Result = itUnknown;
    QString pureext = extname.toLower(); // TODO: remove dot
    if (pureext == "tga") {
        Result = itTarga;
    }
    if (pureext == "bmp") {
        Result = itBitmap;
    }
    if (pureext == "png") {
        Result = itPNG;
    }
    if (pureext == "jpg") {
        Result = itJPEG;
    }
    if (pureext == "gif") {
        Result = itGif;
    }
    if (pureext == "wdp") {
        Result = itHDPhoto;
    }
    if (pureext == "zip") {
        Result = itPKArchive;
    }
    if (pureext == "rar") {
        Result = itRarArchive;
    }
    return Result;
}

QString MIMETypeToExt( TImageType type )
{
    QString Result = "";
    switch (type)
    {
    case itUnknown:
        Result = "unknown";
        break;
    case itBitmap:
        Result = "bmp";
        break;
    case itPNG:
        Result = "png";
        break;
    case itJPEG:
        Result = "jpg";
        break;
    case itGif:
        Result = "gif";
        break;
    case itHDPhoto:
        Result = "wdp";
        break;
    case itPKArchive:
        Result = "zip";
        break;
    case itRarArchive:
        Result = "rar";
        break;
    }
    return Result;
}
