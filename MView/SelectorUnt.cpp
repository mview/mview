#include "SelectorUnt.h"
#include "resource.h"
#include "StringInterner.h"


void RadioList::DrawItem( HDC hdcDst, int nIndex, RECT* prcItem, RECT *prcWin, RECT *prcUpdate )
{
    // draw the high-light background for the selected item
    if (nIndex == GetSelectedIndex())
    {
        MzDrawSelectedBg(hdcDst, prcItem);
    }

    // draw an image on the left
    ImagingHelper *pimg = ImagingHelper::GetImageObject(GetMzResModuleHandle(), (nIndex == GetSelectedIndex()?MZRES_IDR_PNG_SELECTED:MZRES_IDR_PNG_SELECT_BOX), true);
    RECT rcImg = *prcItem;
    rcImg.right = rcImg.left + MZM_MARGIN_MAX * 2;
    if (pimg)
    {
        pimg->Draw(hdcDst, &rcImg, false, false);
    }

    // draw the text
    RECT rcText = *prcItem;
    rcText.left = rcImg.right;
    ListItem* pItem = GetItem(nIndex);
    if (pItem)
    {
        MzDrawText(hdcDst, pItem->Text.C_Str(), &rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
    }
}

int RadioList::OnLButtonDown( UINT fwKeys, int xPos, int yPos )
{
    int nIndex = CalcIndexOfPos(xPos, yPos);
    if (nIndex != -1) {
        SetSelectedIndex(nIndex);
        Invalidate();
        Update();
    }
    return 0;
}

MZ_IMPLEMENT_DYNAMIC(RadioWnd);

BOOL RadioWnd::OnInitDialog()
{
    // Must all the Init of parent class first!
    if (!CMzWndEx::OnInitDialog())
    {
        return FALSE;
    }

    // Then init the controls & other things in the window
    int y = 0;
    y += MZM_HEIGHT_CAPTION;

    m_List.SetPos(0,0,GetWidth(),GetHeight()-MZM_HEIGHT_TEXT_TOOLBAR);
    //m_List.SetID(MZ_IDC_LIST);
    m_List.EnableScrollBarV(true);
    m_List.EnableNotifyMessage(true);
    m_List.SetItemHeight(MZM_HEIGHT_BUTTONEX);
    m_List.SetSelectedIndex(0);

    //add items to list
    ListItem item;

    if (fRadioList != NULL) {
        for (TKOLStrList::iterator it = fRadioList->begin(); it != fRadioList->end(); it++) {
            item.Text = *it;
            m_List.AddItem(item);
        }
    }

    m_List.SetSelectedIndex(*fResult);
    AddUiWin(&m_List);

    m_Toolbar.SetPos(0,GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR,GetWidth(),MZM_HEIGHT_TEXT_TOOLBAR);
    m_Toolbar.SetButton(0, true, true, _tr(IDS_STRING_RETURN));
    m_Toolbar.EnableLeftArrow(true);
    m_Toolbar.SetButton(2, true, true, _tr(IDS_STRING_CANCEL));
    m_Toolbar.SetID(MZ_IDC_TOOLBAR1);
    AddUiWin(&m_Toolbar);

    return TRUE;
}

void RadioWnd::OnMzCommand( WPARAM wParam, LPARAM lParam )
{
    UINT_PTR id = LOWORD(wParam);
    switch(id)
    {
    case MZ_IDC_TOOLBAR1:
        {
            int nIndex = lParam;
            if (nIndex == 2)
            {
                // exit the modal dialog
                EndModal(ID_CANCEL);
                return;
            }

            if (nIndex == 0)
            {
                //saving settings...
                *fResult = m_List.GetSelectedIndex();
                //then exit the dialog
                EndModal(ID_OK);
                return;
            }
        }
    }
}
