////////////////////////////////////////////////////////////////////////////////////////////
// Helper for MainUnt.
// Some collection for non UI related worker
////////////////////////////////////////////////////////////////////////////////////////////
#include <mzfc_inc.h>
#include <mzfc/ImagingHelper.h>


#include <FvMzKOL.h>
#include "MainUnt.h"
#include "resource.h"
#include "StringInterner.h"
#include "DbCentre.h"
#include "AddonFuncUnt.h"

#include "Reader/tinyjpeg/tinyjpeg.h"
#include "Filter/Resample.h"
#include <QImage>
#include <QLabel>
#include <QImageReader>
#include <QPainter>
#include <QApplication>


void CMainWnd::exitmessage(const TCHAR* msg) {
    if (fOverlayMode) {
        stage1.HideOverlay();
    }
    MzMessageBoxEx(m_hWnd, CMzStringW(msg), _T(""), MZ_OK, 0);
    if (fOverlayMode) {
        stage1.ShowOverlay();
    }
}

static int filesize(FILE *fp)
{
    long pos;
    fseek(fp, 0, SEEK_END);
    pos = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return pos;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// sticky safe conversation from ANSI string to wide string
wstring StringToWideString(const string& Source) {
    int len;
#ifdef _WIN32
    len = MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, Source.c_str(), static_cast<int>(Source.length()), NULL, 0);
#elif defined _LINUX
    char* oldlocale	= setlocale(LC_CTYPE, "GBK");
    char* savedlocale = strdup(oldlocale);
    len = mbstowcs(NULL, Source.c_str(), 0);
#endif
    if (0 >= len){
        return wstring(_L(""));
    } else {
        WCHAR * DestBuf = LPWSTR(malloc(sizeof(WCHAR)*(len + 1)));
        if (NULL==DestBuf) {
            return wstring(_L(""));
        } 
        DestBuf[len] = '\0';
#ifdef _WIN32
        bool done = (0 <= MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, Source.c_str(), static_cast<int>(Source.length()), DestBuf, len));
#elif defined _LINUX
        bool done = (0 <= mbstowcs(DestBuf, Source.c_str(), Source.length()));
        (void)setlocale(LC_CTYPE, savedlocale);
        free(savedlocale);
#endif
        if (done) {
            const wstring Result = wstring(DestBuf);
            free(DestBuf);
            return Result;
        } else {
            free(DestBuf);
            return wstring(_L(""));
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////


void CMainWnd::PaintWin2(HDC hdc, RECT* prcUpdate) {
    if (fOverlayMode) {
        CMzWndEx::PaintWin(hdc, prcUpdate);
        return;
    }

    // Copy of PaintStage?
    CMzStringW filename = fStagefile;
    ImagingHelper *helper = new ImagingHelper;
    RECT rect;
    int /*starttime,*/ loadtime = -1, decodetime = -1, drawtime = -1, scaletime = -1;
    HDC tempDC = hdc;//GetDC(m_hWnd);
    if (fFullScreen) {
        rect.left = rect.top = 0;
        rect.right = rect.left + GetWidth();
        rect.bottom = rect.top + 720;
    } else {
        rect.left = rect.top = 0;
        rect.right = rect.left + GetWidth();
        rect.bottom = rect.top + GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR;
    }
    int canvaswidth = rect.right - rect.left;
    int canvasheight = rect.bottom - rect.top;
    int orgwidth = -1, orgheight = -1;
    if (fJpegCodecIndex == jcmQJpeg) {
        int width, height;

        //int starttime = GetTickCount();
        //QImage qimage(QString::fromUtf16((ushort*)(filename.C_Str())));
        //loadtime = GetTickCount() - starttime;
        QImage qimage;
        TPrefetchRecItem& preqimage = fFetchedQImages[fCurrentScrollIndex];
        if (preqimage.FitImage == NULL || preqimage.FitFullscreen != fFullScreen || preqimage.FitExpired) {
            int starttime = GetTickCount();
            qimage.load(QString::fromUtf16((ushort*)(filename.C_Str())));
            loadtime = GetTickCount() - starttime;
            // drop original
            QImage *smart = (QImage*)InterlockedExchange(((long*)&preqimage.FitImage), 0);
            if (smart) {
                delete smart;
            }

            orgwidth = qimage.width();
            orgheight = qimage.height();
        } else {
            qimage = *preqimage.FitImage; // @CopyRecord
            loadtime = preqimage.ExtraInfo.loadtime;
            scaletime = preqimage.ExtraInfo.scaletime;
            orgwidth = preqimage.ExtraInfo.originalwidth;
            orgheight = preqimage.ExtraInfo.originalheight;
            drawtime = preqimage.ExtraInfo.drawtime;
        }
        if (qimage.format() != QImage::Format_Invalid) {
            width = qimage.width();
            height = qimage.height();
            if (width - canvaswidth > 80 || height - canvasheight > 120) {
                // Crop Scale
                PrepareScaledQPicture(qimage, canvaswidth, canvasheight, scaletime);
            }
            if (qimage.depth() == 8) {
                if (qimage.colorTable().empty()) {
                    NormalizeGrayScaleQImage(qimage);
                } else {
                    qimage = qimage.convertToFormat(QImage::Format_ARGB32, Qt::AutoColor); // ColorTable
                }
            }

            if (qimage.height() != canvasheight) {
                qimage = qimage.copy((qimage.width() - canvaswidth) / 2, (qimage.height() - canvasheight) / 2, canvaswidth, canvasheight );
            }
            width = qimage.width();
            height = qimage.height();
            if (fPageMark) {
                DrawPageMarkOnQImage(qimage, fScrollDirection);
            } else if (fExtraInfo) {
                TExtraInfoRec info;
                info.filename = filename;
                info.filesize = FileSize(filename);
                info.originalwidth = orgwidth; // warning
                info.originalheight = orgheight;
                info.drawtime = drawtime;
                info.loadtime = loadtime;
                info.scaletime = scaletime;
                info.decodetime = decodetime;
                info.codecindex = fJpegCodecIndex;
                info.allfilecount = fScrollFiles.size();
                info.fileindex = fCurrentScrollIndex;
                DrawExtraInfoOnQImage(qimage, canvaswidth, canvasheight, info);
            }
            //::BitBlt(qimage.getDC(), 0, 0, qimage.width(), qimage.height(), tempDC, canvaswidth, canvasheight, SRCCOPY);

            int w = qimage.width();
            int h = qimage.height();

            HDC display_dc = tempDC;

            // Define the header
            BITMAPINFO bmi;
            memset(&bmi, 0, sizeof(bmi));
            bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
            bmi.bmiHeader.biWidth       = w;
            bmi.bmiHeader.biHeight      = -h;
            bmi.bmiHeader.biPlanes      = 1;
            bmi.bmiHeader.biBitCount    = 32;
            bmi.bmiHeader.biCompression = BI_RGB;
            bmi.bmiHeader.biSizeImage   = w * h * 4;

            // Create the pixmap
            uchar *pixels = 0;
            HBITMAP bitmap = CreateDIBSection(display_dc, &bmi, DIB_RGB_COLORS, (void **) &pixels, 0, 0);

            // Copy over the data
            QImage::Format imageFormat = QImage::Format_RGB32;
            const QImage image = qimage.convertToFormat(imageFormat);
            int bytes_per_line = w * 4;
            for (int y=0; y<h; ++y)
                memcpy(pixels + y * bytes_per_line, image.scanLine(y), bytes_per_line);

            HBITMAP HB1 = bitmap;//QPixmap::fromImage(qimage).toWinHBITMAP();
            HDC MemDC = CreateCompatibleDC(tempDC);
            SelectObject(MemDC, HB1);
            int starttime = GetTickCount();
            BitBlt(tempDC, 0, 0, canvaswidth, canvasheight, MemDC, (width - canvaswidth) / 2, (height - canvasheight) / 2, SRCCOPY);
            drawtime = GetTickCount() - starttime;
            //DeleteObject(HB1);
            DeleteDC(MemDC);

            preqimage.ExtraInfo.drawtime = drawtime; // save for refresh
        }
    } else {
        int starttime = GetTickCount();
        helper->LoadImage(filename, true, true, false);
        loadtime = GetTickCount() - starttime;
        if (helper->IsLoadedToDC()) {
            decodetime = 0;
        }
        starttime = GetTickCount();
        helper->Draw(tempDC, &rect, false, true);
        drawtime = GetTickCount() - starttime;
    }
    if (fExtraInfo && fJpegCodecIndex != jcmQJpeg) {
        rect.top = rect.bottom - 36;
        HFONT oldFont = HFONT(SelectObject(tempDC, HGDIOBJ(FontHelper::GetFont(16))));
        QString speedtext = QString("%1 / %2 %3") \
            .arg(fCurrentScrollIndex + 1).arg(fScrollFiles.size()).arg(QString::fromUtf16((ushort*)ExtractFileName(filename).C_Str()));
        MzDrawText(tempDC, LPTSTR(speedtext.utf16()), &rect, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
        speedtext = QString("Mode: GDI (%1) Load: [%2] Decode: [%3] Draw: [%4]") \
            .arg(QString::fromUtf16((ushort*)CodecNames[fJpegCodecIndex == jcmQJpeg?fJpegCodecIndex:jcmImagingHelper].C_Str())) \
            .arg(loadtime).arg(decodetime).arg(drawtime);
        MzDrawText(tempDC, LPTSTR(speedtext.utf16()), &rect, DT_LEFT | DT_BOTTOM | DT_SINGLELINE | DT_END_ELLIPSIS);
        //DeleteObject(SelectObject(hdcDst, oldFont));
        SelectObject(tempDC, HGDIOBJ(oldFont));
    }
    //ReleaseDC(m_hWnd, tempDC);

    delete helper;

    CMzWndEx::PaintWin(hdc, prcUpdate);
}

void CMainWnd::PaintStage(CMzStringW filename)
{
    if (!FileExists(filename)) {
        exitmessage(L"Cannot open filename");
    }
    ImagingHelper *helper = new ImagingHelper;
    RECT rect;
    int /*starttime,*/ loadtime = -1, decodetime = -1, drawtime = -1, scaletime = -1;
    if (fOverlayMode){
        rect.left = rect.top = 0;
        rect.right = rect.left + GetWidth();
        if (fFullScreen) {
            rect.bottom = rect.top + 720;
        } else {
            rect.bottom = rect.top + GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR;
        }
        bool isjpeg = WAnsiLowerCase(ExtractFileExt(filename)) == _T(".jpg"); // TODO: JFIF header detection
        bool ispng = WAnsiLowerCase(ExtractFileExt(filename)) == _T(".png"); // TODO: PNG8 header detection
        if ((isjpeg && fJpegCodecIndex != jcmImagingHelper) || (ispng && fJpegCodecIndex == jcmQJpeg)) {
            // JPEG Codecs
            if (fJpegCodecIndex == jcmTinyjpeg) {
                FILE *fp;
                unsigned int length_of_file;
                unsigned int width, height;
                unsigned char *buf;
                struct jdec_private *jdec;
                unsigned char *components[3];

                int starttime = GetTickCount();
                /* Load the Jpeg into memory */
                fp = _wfopen(filename, L"rb");
                if (fp == NULL)
                    exitmessage(L"Cannot open filename\n");
                length_of_file = filesize(fp);
                buf = (unsigned char *)malloc(length_of_file + 4);
                if (buf == NULL)
                    exitmessage(L"Not enough memory for loading file\n");
                fread(buf, length_of_file, 1, fp);
                fclose(fp);
                loadtime = GetTickCount() - starttime;

                /* Decompress it */
                jdec = tinyjpeg_init();
                if (jdec == NULL)
                    exitmessage(L"Not enough memory to alloc the structure need for decompressing\n");

                if (tinyjpeg_parse_header(jdec, buf, length_of_file)<0)
                    exitmessage(StringToWideString(tinyjpeg_get_errorstring(jdec)).c_str());

                /* Get the size of the image */
                tinyjpeg_get_size(jdec, &width, &height);

                printf("Decoding JPEG image...\n");
                if (tinyjpeg_decode(jdec, TINYJPEG_FMT_BGR24) < 0)
                    exitmessage(StringToWideString(tinyjpeg_get_errorstring(jdec)).c_str());

                /* 
                * Get address for each plane (not only max 3 planes is supported), and
                * depending of the output mode, only some components will be filled 
                * RGB: 1 plane, YUV420P: 3 planes, GREY: 1 plane
                */
                tinyjpeg_get_components(jdec, components);

                decodetime = GetTickCount() - starttime - loadtime;
                loadtime = decodetime + loadtime; // all

                LPBYTE src, dest;
                src = components[0];

                //unsigned int bufferlen = width * height * 3;

                DDSURFACEDESC surface;
                if (stage1.LockData(&surface)) {
                    int starttime = GetTickCount();

                    dest = LPBYTE(surface.lpSurface);
                    for (int y = 0; y < min(height, surface.dwHeight); y++)
                    {
                        int x;
                        for (x = 0; x < min(width, surface.dwWidth); x++)
                        {
                            memcpy(dest, src, 3);
                            src += 3;
                            dest += surface.lXPitch;
                        }
                        if (width != x)
                            src += (width - x) * 3;
                        dest += (surface.lPitch - x * surface.lXPitch);
                    }
                    if (stage1.UnLockData(!fExtraInfo) == false) {
                        exitmessage(L"stage1.UnLockData(true) == false");
                    }
                    drawtime = GetTickCount() - starttime;
                    //stage1.Flip();
                }

                /* Only called this if the buffers were allocated by tinyjpeg_decode() */
                tinyjpeg_free(jdec);
                /* else called just free(jdec); */

                free(buf);
            } else if (fJpegCodecIndex == jcmQJpeg) {
                // Overlay, QImage
                int width, height, orgwidth, orgheight;

                // TODO: check index
                QImage qimage;
                bool insertion = !fFetchedQImages.contains(fCurrentScrollIndex);
                TPrefetchRecItem& preqimage = fFetchedQImages[fCurrentScrollIndex]; // silent insert?
                if (insertion) {
                    EnterCriticalSection(&fPreqcs);
                    preqimage.FitImage = NULL;
                    preqimage.LargeImage = NULL;
                    preqimage.FitFullscreen = fFullScreen;
                    preqimage.FitExpired = true;
                    LeaveCriticalSection(&fPreqcs);
                }
                if (insertion 
                    || ( fStageShowMode == ssmFit && (preqimage.FitImage == NULL || preqimage.FitFullscreen != fFullScreen || preqimage.FitExpired) ) 
                    || ( fStageShowMode == ssmOriginal && preqimage.LargeImage == NULL)
                ) {
                    // drop last fit
                    int starttime = GetTickCount();
                    qimage.load(QString::fromUtf16((ushort*)(filename.C_Str())));
                    loadtime = GetTickCount() - starttime;
                    // drop original
                    QImage *smart = (QImage*)InterlockedExchange(((long*)&preqimage.FitImage), 0);
                    if (smart && insertion == false) {
                        delete smart;
                    }

                    orgwidth = qimage.width();
                    orgheight = qimage.height();
                } else {
                    // not insertion, can use
                    if (fStageShowMode == ssmFit) {
                        qimage = *preqimage.FitImage; // @CopyRecord
                    } else if (fStageShowMode == ssmOriginal) {
                        qimage = *preqimage.LargeImage; // Dangerous?
                    }
                    loadtime = preqimage.ExtraInfo.loadtime;
                    scaletime = preqimage.ExtraInfo.scaletime;
                    orgwidth = preqimage.ExtraInfo.originalwidth;
                    orgheight = preqimage.ExtraInfo.originalheight;
                }
                //QImage qimage(QString::fromUtf16((ushort*)(filename.C_Str())));
                if (qimage.format() != QImage::Format_Invalid) {
                    width = qimage.width();
                    height = qimage.height();
                    LPBYTE src, dest;

                    DDSURFACEDESC surface;
                    if (stage1.LockData(&surface)) {
                        dest = LPBYTE(surface.lpSurface);
                        bool blockable = false;
                        int rotation = 0;
                        if (surface.lXPitch < -4 && surface.lPitch == 4) {
                            rotation = 90;
                            dest =  dest + (surface.dwWidth - 1) * surface.lXPitch;
                            blockable = true;
                            // we should have safe hit here if M8 does not replace TTF
                        }
                        if (rotation == 90 || rotation == 270) {
                            // Time to scale
                            if (fStageShowMode == ssmOriginal) {
                                // Store for next 
                                if (preqimage.LargeImage == NULL) {
                                    QImage *smart = new QImage(qimage); // @CopyRecord
                                    InterlockedExchange((long*)&preqimage.LargeImage, (long)smart);
                                }
                                // only copy
                                qimage = qimage.copy((qimage.width() - int(surface.dwHeight)) / 2 + fOriginalOffsetY, (qimage.height() - int(surface.dwWidth)) / 2 - fOriginalOffsetX, surface.dwHeight, surface.dwWidth );
                            } else {
                                if (width > height){
                                    if (height - int(surface.dwWidth) > 80 || width - int(surface.dwHeight) > 120) {
                                        // Crop Scale
                                        PrepareScaledQPicture(qimage, surface.dwHeight, surface.dwWidth, scaletime); // auto rotate
                                    }
                                } else {
                                    if (width - int(surface.dwWidth) > 80 || height - int(surface.dwHeight) > 120) {
                                        // Crop Scale
                                        PrepareScaledQPicture(qimage, surface.dwHeight, surface.dwWidth, scaletime); // auto rotate
                                    } else {
                                        if (width != height) {
                                            // Safe for Rotate?
                                            QMatrix matrix;
                                            matrix.rotate(-90);
                                            qimage = qimage.transformed(matrix, Qt::FastTransformation);
                                        }
                                    }
                                }
                            }
                        } else {
                            // Time to scale
                            // we never runhere
                            if (width - surface.dwWidth > 80 || height - surface.dwHeight > 120) {
                                // Crop Scale
                                PrepareScaledQPicture(qimage, surface.dwWidth, surface.dwHeight, scaletime);
                            }
                        }
                        // either Orginal n Fit get qimage here
                        // try normalize 8bit GrayScale un-cropped manga jpeg
                        if (blockable && qimage.depth() == 8) {
                            //stage1.SetTransparency(12);
                            if (qimage.colorTable().empty()) {
                                NormalizeGrayScaleQImage(qimage);
                            } else {
                                qimage = qimage.convertToFormat(QImage::Format_ARGB32, Qt::AutoColor); // ColorTable
                            }
                        }
                        // TODO: 90, 270, 0
                        if (blockable && (qimage.width() != surface.dwHeight || qimage.height() != surface.dwWidth)) {
                            if (qimage.hasAlphaChannel()) {
                                // checker
                                QImage box(32, 32, qimage.format());
                                QPainter pmp(&box);
                                pmp.fillRect(0, 0, 16, 16, Qt::lightGray);
                                pmp.fillRect(16, 16, 16, 16, Qt::lightGray);
                                pmp.fillRect(0, 16, 16, 16, Qt::darkGray);
                                pmp.fillRect(16, 0, 16, 16, Qt::darkGray);
                                pmp.end();

                                QImage viaimage = QImage(qimage);
                                QPainter checkermaker(&viaimage);
                                QBrush checker;
                                checker.setTextureImage(box);
                                checkermaker.fillRect(viaimage.rect(), checker);
                                checkermaker.drawImage(0, 0, qimage);
                                checkermaker.end();
                                qimage = viaimage; // copy
                            }
                            qimage = qimage.copy((qimage.width() - int(surface.dwHeight)) / 2, (qimage.height() - int(surface.dwWidth)) / 2, surface.dwHeight, surface.dwWidth );
                        }

                        width = qimage.width();
                        height = qimage.height();

                        // Store for next 
                        if (preqimage.FitImage == NULL) {
                            if (fStageShowMode == ssmFit) {
                                EnterCriticalSection(&fPreqcs);
                                QImage *smart = new QImage(qimage); // @CopyRecord
                                InterlockedExchange((long*)&preqimage.FitImage, (long)smart);
                                preqimage.FitExpired = false; // interlocked?
                                preqimage.FitFullscreen = fFullScreen;
                                preqimage.FileIndex = fCurrentScrollIndex; // TODO: add in argument
                                LeaveCriticalSection(&fPreqcs);
                            }
                            preqimage.ExtraInfo.codecindex = fJpegCodecIndex;
                            preqimage.FitRoted = (rotation == 90 || rotation == 270);
                            preqimage.ExtraInfo.loadtime = loadtime;
                            preqimage.ExtraInfo.scaletime = scaletime;
                            preqimage.ExtraInfo.originalheight = orgheight;
                            preqimage.ExtraInfo.originalwidth = orgwidth;
                        }
                        // better to make sure 32bit here
                        // Draw text on screen / image
                        if (fPageMark) {
                            if (fFullScreen) {
                                DrawWidgetsOnQImage(qimage);
                            }
                            DrawPageMarkOnQImage(qimage, fScrollDirection);
                        } else if (fExtraInfo && fStageShowMode == ssmFit) { // TODO: fix huge font
#ifdef DEBUG
                            src = qimage.bits();
                            if (qimage.depth() == 32) {
                                int starttime = GetTickCount();
                                memcpy(dest, src, min(surface.dwWidth * surface.dwHeight * 4, width * height * 4));
                                drawtime = GetTickCount() - starttime;
                            } else if (qimage.depth() == 16) {

                            }
#endif
                            TExtraInfoRec info;
                            info.filename = filename;
                            info.filesize = FileSize(filename);
                            info.originalwidth = orgwidth;
                            info.originalheight = orgheight;
                            info.drawtime = drawtime;
                            info.loadtime = loadtime;
                            info.scaletime = scaletime;
                            info.decodetime = decodetime;
                            info.codecindex = fJpegCodecIndex;
                            info.allfilecount = fScrollFiles.size();
                            info.fileindex = fCurrentScrollIndex;
                            if (rotation == 90 || rotation == 270) {
                                DrawExtraInfoOnQImage(qimage, surface.dwHeight, surface.dwWidth, info);
                            } else {
                                DrawExtraInfoOnQImage(qimage, surface.dwWidth, surface.dwHeight, info);
                            }
                        }
                        //if (fFullScreen) {
                        //    DrawWidgetsOnQImage(qimage);
                        //}
                        // we will get 480*730 or 480*615 here
                        src = qimage.bits();
                        //width = qimage.width();
                        //height = qimage.height();

                        int starttime = GetTickCount();

                        // qimage supports 1, 8, 16, 32
                        if (qimage.depth() == 32) {
                            memcpy(dest, src, min(surface.dwWidth * surface.dwHeight * 4, width * height * 4));
                        } else if (qimage.depth() == 16) {

                        }
                        drawtime = GetTickCount() - starttime;
                        preqimage.ExtraInfo.drawtime = drawtime;

                        if (stage1.UnLockData(true/*!fExtraInfo*/) == false) {
                            exitmessage(L"stage1.UnLockData(true) == false");
                        }
                        if (rotation == 90 || rotation == 270) {
                            BuildExtraPrefetchFork(surface.dwHeight, surface.dwWidth);
                        } else {
                            BuildExtraPrefetchFork(surface.dwWidth, surface.dwHeight);
                        }
                    }
                } else {
                    QList<QByteArray> formats = QImageReader::supportedImageFormats();
                    QString unsupportHint = "QJpeg Load failed. avalible formats:\n";
                    for (QList<QByteArray>::iterator it = formats.begin(); it != formats.end(); it++)
                    {
                        unsupportHint += it->data();
                        if (it->data() != formats.last()) {
                            unsupportHint += " ";
                        };
                    }
                    exitmessage(LPTSTR(unsupportHint.utf16()));
                }
            }
        } else if (fJpegCodecIndex == jcmLibJPEG) {

        } else {
            // ImagingHelper
            int starttime = GetTickCount();
            helper->LoadImage(filename, false, true, false);
            
            loadtime = GetTickCount() - starttime;
            if (helper->IsLoadedToDC()) {
                decodetime = 0;
            }
            HDC tempDC = stage1.BeginDraw();
            starttime = GetTickCount();
            helper->Draw(tempDC, &rect, false, true);
            drawtime = GetTickCount() - starttime;
            stage1.EndDraw();
        }
        if (fExtraInfo && fJpegCodecIndex != jcmQJpeg) {
            // Jpeg decode?
            HDC tempDC = stage1.BeginDraw();
            helper->Draw(tempDC, &rect, false, true);

            rect.top = rect.bottom - 36;
            //InflateRect(&rect, -3, -3);
            HFONT oldFont = HFONT(SelectObject(tempDC, HGDIOBJ(FontHelper::GetFont(16))));
            QString speedtext = QString("%1 / %2 %3") \
                .arg(fCurrentScrollIndex + 1).arg(fScrollFiles.size()).arg(QString::fromUtf16((ushort*)ExtractFileName(filename).C_Str()));
            MzDrawText(tempDC, LPTSTR(speedtext.utf16()), &rect, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
            speedtext = QString("Mode: Overlay (%1) Load: %2 Decode: %3 Scale: %4 Draw: %5 ") \
                .arg(QString::fromUtf16((ushort*)CodecNames[isjpeg?fJpegCodecIndex:jcmImagingHelper].C_Str())) \
                .arg(loadtime).arg(decodetime).arg(scaletime).arg(drawtime);
            MzDrawText(tempDC, LPTSTR(speedtext.utf16()), &rect, DT_LEFT | DT_SINGLELINE| DT_BOTTOM | DT_END_ELLIPSIS);
            SelectObject(tempDC, HGDIOBJ(oldFont));

            stage1.EndDraw();
            //stage1.Flip();
        }
    } else {
        // GDI Mode
        HDC tempDC = GetDC(m_hWnd);
        if (fFullScreen) {
            rect.left = rect.top = 0;
            rect.right = rect.left + GetWidth();
            rect.bottom = rect.top + 720;
        } else {
            rect.left = rect.top = 0;
            rect.right = rect.left + GetWidth();
            rect.bottom = rect.top + GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR;
        }
        int canvaswidth = rect.right - rect.left;
        int canvasheight = rect.bottom - rect.top;
        int orgwidth = -1, orgheight = -1;
        if (fJpegCodecIndex == jcmQJpeg) {
            int width, height;

            QImage qimage;
            TPrefetchRecItem& preqimage = fFetchedQImages[fCurrentScrollIndex];
            if (preqimage.FitImage == NULL || preqimage.FitFullscreen != fFullScreen || preqimage.FitExpired) {
                int starttime = GetTickCount();
                qimage.load(QString::fromUtf16((ushort*)(filename.C_Str())));
                loadtime = GetTickCount() - starttime;
                // drop original
                //if (preqimage.FitImage != NULL){
                //    delete preqimage.FitImage;
                //    preqimage.FitImage = NULL;
                //}
                QImage *smart = (QImage*)InterlockedExchange(((long*)&preqimage.FitImage), 0);
                if (smart) {
                    delete smart;
                }

                orgwidth = qimage.width();
                orgheight = qimage.height();
            } else {
                qimage = *preqimage.FitImage; // @CopyRecord
                loadtime = preqimage.ExtraInfo.loadtime;
                scaletime = preqimage.ExtraInfo.scaletime;
                orgwidth = preqimage.ExtraInfo.originalwidth;
                orgheight = preqimage.ExtraInfo.originalheight;
                drawtime = preqimage.ExtraInfo.drawtime;
            }
            if (qimage.format() != QImage::Format_Invalid) {
                width = qimage.width();
                height = qimage.height();
                if (width - canvaswidth > 80 || height - canvasheight > 120) {
                    // Crop Scale
                    PrepareScaledQPicture(qimage, canvaswidth, canvasheight, scaletime);
                }
                if (qimage.depth() == 8) {
                    if (qimage.colorTable().empty()) {
                        NormalizeGrayScaleQImage(qimage);
                    } else {
                        qimage = qimage.convertToFormat(QImage::Format_ARGB32, Qt::AutoColor); // ColorTable
                    }
                }

                if (qimage.height() != canvasheight) {
                    qimage = qimage.copy((qimage.width() - canvaswidth) / 2, (qimage.height() - canvasheight) / 2, canvaswidth, canvasheight );
                }
                width = qimage.width();
                height = qimage.height();
                if (fPageMark) {
                    DrawPageMarkOnQImage(qimage, fScrollDirection);
                } else if (fExtraInfo) {
                    TExtraInfoRec info;
                    info.filename = filename;
                    info.filesize = FileSize(filename);
                    info.originalwidth = orgwidth; // warning
                    info.originalheight = orgheight;
                    info.drawtime = drawtime;
                    info.loadtime = loadtime;
                    info.scaletime = scaletime;
                    info.decodetime = decodetime;
                    info.codecindex = fJpegCodecIndex;
                    info.allfilecount = fScrollFiles.size();
                    info.fileindex = fCurrentScrollIndex;
                    DrawExtraInfoOnQImage(qimage, canvaswidth, canvasheight, info);
                }
                //::BitBlt(qimage.getDC(), 0, 0, qimage.width(), qimage.height(), tempDC, canvaswidth, canvasheight, SRCCOPY);

                int w = qimage.width();
                int h = qimage.height();

                HDC display_dc = tempDC;

                // Define the header
                BITMAPINFO bmi;
                memset(&bmi, 0, sizeof(bmi));
                bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
                bmi.bmiHeader.biWidth       = w;
                bmi.bmiHeader.biHeight      = -h;
                bmi.bmiHeader.biPlanes      = 1;
                bmi.bmiHeader.biBitCount    = 32;
                bmi.bmiHeader.biCompression = BI_RGB;
                bmi.bmiHeader.biSizeImage   = w * h * 4;

                // Create the pixmap
                uchar *pixels = 0;
                HBITMAP bitmap = CreateDIBSection(display_dc, &bmi, DIB_RGB_COLORS, (void **) &pixels, 0, 0);

                // Copy over the data
                QImage::Format imageFormat = QImage::Format_RGB32;
                const QImage image = qimage.convertToFormat(imageFormat);
                int bytes_per_line = w * 4;
                for (int y=0; y<h; ++y)
                    memcpy(pixels + y * bytes_per_line, image.scanLine(y), bytes_per_line);

                HBITMAP HB1 = bitmap;//QPixmap::fromImage(qimage).toWinHBITMAP();
                HDC MemDC = CreateCompatibleDC(tempDC);
                SelectObject(MemDC, HB1);
                int starttime = GetTickCount();
                BitBlt(tempDC, 0, 0, canvaswidth, canvasheight, MemDC, (width - canvaswidth) / 2, (height - canvasheight) / 2, SRCCOPY);
                drawtime = GetTickCount() - starttime;
                //DeleteObject(HB1);
                DeleteDC(MemDC);

                preqimage.ExtraInfo.drawtime = drawtime; // save for refresh
            }
        } else {
            int starttime = GetTickCount();
            helper->LoadImage(filename, true, true, false);
            loadtime = GetTickCount() - starttime;
            if (helper->IsLoadedToDC()) {
                decodetime = 0;
            }
            starttime = GetTickCount();
            helper->Draw(tempDC, &rect, false, true);
            drawtime = GetTickCount() - starttime;
        }
        if (fExtraInfo && fJpegCodecIndex != jcmQJpeg) {
            rect.top = rect.bottom - 36;
            HFONT oldFont = HFONT(SelectObject(tempDC, HGDIOBJ(FontHelper::GetFont(16))));
            QString speedtext = QString("%1 / %2 %3") \
                .arg(fCurrentScrollIndex + 1).arg(fScrollFiles.size()).arg(QString::fromUtf16((ushort*)ExtractFileName(filename).C_Str()));
            MzDrawText(tempDC, LPTSTR(speedtext.utf16()), &rect, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
            speedtext = QString("Mode: GDI (%1) Load: [%2] Decode: [%3] Draw: [%4]") \
                .arg(QString::fromUtf16((ushort*)CodecNames[fJpegCodecIndex == jcmQJpeg?fJpegCodecIndex:jcmImagingHelper].C_Str())) \
                .arg(loadtime).arg(decodetime).arg(drawtime);
            MzDrawText(tempDC, LPTSTR(speedtext.utf16()), &rect, DT_LEFT | DT_BOTTOM | DT_SINGLELINE | DT_END_ELLIPSIS);
            //DeleteObject(SelectObject(hdcDst, oldFont));
            SelectObject(tempDC, HGDIOBJ(oldFont));
        }
        ReleaseDC(m_hWnd, tempDC);
        this->UpdateWindow();
    }
    
    delete helper;
}

void CMainWnd::PaintStage()
{
    PaintStage(fStagefile);
}

bool CMainWnd::PrepareScaledQPicture( QImage& qimage, int destwidth, int destheight, int& scaletime )
{
    int width = qimage.width();
    int height = qimage.height();

    // Better Quality than in viaimage 
    if (qimage.depth() == 8) {
        if (qimage.colorTable().empty()) {
            NormalizeGrayScaleQImage(qimage);
        } else {
            qimage = qimage.convertToFormat(QImage::Format_ARGB32);
        }
    }

    Qt::TransformationMode scaleQ = double(width * height) / (destwidth * destheight) > 4? Qt::FastTransformation:Qt::SmoothTransformation;
    if ( fabs(double(width) / height - double(destheight) / destwidth) < 0.25 || double(width) / height - double(destwidth) / destheight > 0.5) {
        int starttime = GetTickCount();
        qimage = qimage.scaled(destheight, destwidth, Qt::KeepAspectRatio, scaleQ);
        scaletime = GetTickCount() - starttime;
        QMatrix matrix;
        matrix.rotate(fOverlayMode?-90:90); 
        qimage = qimage.transformed(matrix, Qt::FastTransformation);
    } else {
        int starttime = GetTickCount();
        //qimage = qimage.scaled(newwidth, newheight, Qt::KeepAspectRatio, Qt::FastTransformation);
        qimage = qimage.scaled(destwidth, destheight, Qt::KeepAspectRatio, scaleQ);
        scaletime = GetTickCount() - starttime;
    }
    //if (destheight - qimage.height() > 2 || destwidth - qimage.width() > 2) {
    if (destheight != qimage.height() || destwidth != qimage.width()) {
        QImage viaimage = QImage(destwidth, destheight, qimage.format());
        QPainter painter(&viaimage);
        // GrayScale may corrupted here.
        painter.drawImage(int(destwidth - qimage.width()) / 2, int(destheight - qimage.height()) / 2, qimage);
        qimage = viaimage;
    }
    return true;
}

void CMainWnd::DrawExtraInfoOnQImage(QImage& qimage, int stagewidth, int stageheight, TExtraInfoRec& extrainfo)
{
    QPainter hintwriter(&qimage);
    QRect rect;
    bool Rotated = stagewidth > stageheight;
    if (Rotated) {
        rect.setRect(min(qimage.width(), stagewidth) - 31, -2, 34, min(qimage.height(), stageheight) + 4);
    } else {
        rect.setRect(-2, min(qimage.height(), stageheight) - 31, min(qimage.width(), stagewidth) + 4, 34);
    }
    hintwriter.setPen(QPen(QColor(0x14C906), 2, Qt::SolidLine));
    //hintwriter.setBrush(QBrush((Qt::green, Qt::LinearGradientPattern)));
    QLinearGradient linearGradient(0, 0, 474, 36);
    linearGradient.setColorAt(0.0, Qt::white);
    linearGradient.setColorAt(0.2, Qt::lightGray);
    linearGradient.setColorAt(0.6, Qt::lightGray);
    linearGradient.setColorAt(1.0, Qt::darkGray);
    hintwriter.setBrush(linearGradient);
    hintwriter.setRenderHint(QPainter::Antialiasing);
    hintwriter.setOpacity(0.5);
    hintwriter.drawRoundedRect(rect, 4, 4, Qt::AbsoluteSize);

    hintwriter.setOpacity(0.75);
    QFont litefont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 25);
    litefont.setWeight(QFont::DemiBold - 3);
    litefont.setStyleStrategy(QFont::PreferAntialias);
    hintwriter.setFont(litefont);
    hintwriter.setPen(QPen(QColor(0x0E2545), 1, Qt::SolidLine));
    hintwriter.setBrush(Qt::NoBrush);
    QMatrix matrix;
    if (Rotated) {
        matrix.rotate(-90);
    }
    matrix.scale(0.5, 0.5);

    MEMORYSTATUS ms;
    ms.dwLength = sizeof (MEMORYSTATUS);
    GlobalMemoryStatus(&ms);
    int freemem = ms.dwAvailPhys;

    hintwriter.setMatrix(matrix);
    QString speedtext = QString("%1 / %2 %3 %4 * %5 %6") \
        .arg(extrainfo.fileindex + 1).arg(extrainfo.allfilecount).arg(QString::fromUtf16((ushort*)ExtractFileName(extrainfo.filename).C_Str()))
        .arg(extrainfo.originalwidth).arg(extrainfo.originalheight).arg(QString::fromUtf16((ushort*)Num2Bytes(extrainfo.filesize).C_Str()));
    if (Rotated) {
        hintwriter.drawText(0 - (rect.bottom() - 5) * 2, (rect.left() + 13) * 2, speedtext);
    } else {
        hintwriter.drawText((rect.left() + 5) * 2, (rect.top() + 13) * 2, speedtext);
    }
    //hintwriter.drawText(rect, Qt::AlignTop || Qt::AlignLeft || Qt::TextSingleLine, speedtext);
    speedtext = QString("Mode: %1 (%2) Load: %3%4%5%6 FreeMem: %7") \
        .arg(fOverlayMode?"Overlay":"GDI") \
        .arg(QString::fromUtf16((ushort*)CodecNames[extrainfo.codecindex].C_Str())) \
        .arg(extrainfo.loadtime)
        .arg(extrainfo.decodetime==-1?QString(""):QString(" Decode: %1").arg(extrainfo.decodetime))
        .arg(extrainfo.scaletime==-1?QString(""):QString(" Scale: %1").arg(extrainfo.scaletime))
        .arg(extrainfo.drawtime==-1?QString(""):QString(" Draw: %1").arg(extrainfo.drawtime))
        .arg(QString::fromUtf16((ushort*)Num2Bytes(freemem).C_Str()));
    if (Rotated) {
        hintwriter.drawText(0 - (rect.bottom() - 5) * 2, (rect.left() + 26) * 2, speedtext);
    } else {
        hintwriter.drawText((rect.left() + 5) * 2, (rect.top() + 26) * 2, speedtext);
    }
}

void CMainWnd::DrawPageMarkOnQImage(QImage& qimage, int direction) {
    QImage arrow;
    LoadQImageFromResource(arrow, MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(direction==pmdLeft?IDR_PNG_PAGELEFT:IDR_PNG_PAGERIGHT));
    QPainter painter(&qimage);
    QRect rect;

    if (fOverlayMode) {
        if (direction == pmdLeft) {
            rect.setRect(40, 380, 630, 100);
        } else {
            rect.setRect(40, 0, 630, 100);
        }
    } else {
        if (direction == pmdLeft) {
            rect.setRect(0, 40, 100, 630);
        } else {
            rect.setRect(380, 40, 100, 630);
        }
    }
    painter.setPen(QPen(QColor(0x14C906), 2, Qt::SolidLine));
    //hintwriter.setBrush(QBrush((Qt::green, Qt::LinearGradientPattern)));
    QLinearGradient linearGradient(0, 0, 474, 36);
    linearGradient.setColorAt(0.0, Qt::white);
    linearGradient.setColorAt(0.2, Qt::lightGray);
    linearGradient.setColorAt(0.6, Qt::lightGray);
    linearGradient.setColorAt(1.0, Qt::darkGray);
    painter.setBrush(linearGradient);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setOpacity(0.5);
    painter.drawRoundedRect(rect, 4, 4, Qt::AbsoluteSize);
    if (arrow.format() != QImage::Format_Invalid) {
        // TODO: rotate
        if (fOverlayMode) {
            QMatrix matrix;
            matrix.rotate(-90);
            arrow = arrow.transformed(matrix);
        }
        painter.setOpacity(0.8);
        //painter.drawImage(int(qimage.width() - arrow.width()) / 2, int(qimage.height() - arrow.height()) / 2, arrow);
        painter.drawImage(rect.left() + (rect.width() - arrow.width()) / 2, rect.bottom() - (rect.height() + arrow.height()) / 2, arrow);
    }
}

bool CMainWnd::ShowPageMark( int direction )
{
    if (fJpegCodecIndex == jcmQJpeg) {
        fPageMark = true;
        fScrollDirection = direction;
        PaintStage();
        fPageMark = false;
    } else {
        ImagingHelper* helper = new ImagingHelper;
        helper->LoadImageFromRes(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(direction==pmdLeft?IDR_PNG_PAGELEFT:IDR_PNG_PAGERIGHT), false, true, true);

        RECT rect;
        rect.left = rect.top = 0;
        rect.right = rect.left + GetWidth();
        rect.bottom = rect.top + GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR;

        if (fOverlayMode) {
            stage1.Flip();
            HDC tempDC = stage1.BeginDraw();
            helper->Draw(tempDC, &rect, false, false);
            stage1.EndDraw();
        } else {
            HDC tempDC = GetWindowDC(m_hWnd);
            helper->Draw(tempDC, &rect, false, false);
            ReleaseDC(m_hWnd, tempDC);
        }

        delete helper;
    }
    return true;
}

void CMainWnd::DrawWidgetsOnQImage( QImage& qimage )
{
    if (fFakeWidgets.empty()) {
        return;
    }
    QPainter painter(&qimage);
    painter.setOpacity(0.2);
    for (QVector < TFakeWidgetRecItem >::iterator it = fFakeWidgets.begin(); it != fFakeWidgets.end(); it++ ) {
        if (it->NormalImage.format() != QImage::Format_Invalid) {
            painter.drawImage(it->TouchRect, it->NormalImage);
        }
    }
    painter.end();
}

bool CMainWnd::AddWidget( int ID, const QRect& rect, QImage normalimage )
{
    TFakeWidgetRecItem item;
    item.ID = ID;
    item.TouchRect = item.DrawRect = rect;
    item.NormalImage = normalimage;
    fFakeWidgets.append(item); // copy
    return true;
}
