#include <QtCore/QCoreApplication>
#include <QSettings>
#include <QFile>
#include <QXmlReader>
#include <QXmlSimpleReader>
#include "DbCentre.h"
#include <FvMzKOL.h>

TPathRecItem PathSetting; // imp
TStateRecItem StateSetting; // imp

void LoadAppSettings( void )
{
    // TODO: init from argv instead of INI when /override passed
    QSettings settings(QString::fromUtf16((ushort*)GetStartDir().C_Str()) + "/ConfMView.ini",
        QSettings::IniFormat);
    PathSetting.LastStageFile = settings.value("Path/LastStageFile", "").toString();
    PathSetting.StartupFolder = settings.value("Path/StartupFolder", "").toString();
    PathSetting.LastArchiveIndex = settings.value("Path/LastArchiveIndex", 0).toInt();

    StateSetting.ExtraInfo = settings.value("State/ExtraInfo", false).toBool();
    StateSetting.FullScreen = settings.value("State/FullScreen", true).toBool();
}

void SaveAppSettings( void )
{
    // maybe save from command line arguments
    QSettings settings(QString::fromUtf16((ushort*)GetStartDir().C_Str()) + "/ConfMView.ini",
        QSettings::IniFormat);
    settings.beginGroup("Path");
    settings.setValue("LastStageFile", PathSetting.LastStageFile);
    settings.setValue("StartupFolder", PathSetting.StartupFolder);
    settings.setValue("LastArchiveIndex", PathSetting.LastArchiveIndex);
    settings.endGroup();

    settings.beginGroup("State");
    settings.setValue("ExtraInfo", StateSetting.ExtraInfo);
    settings.setValue("FullScreen", StateSetting.FullScreen);
    settings.endGroup();
}
