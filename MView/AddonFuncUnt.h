#ifndef _ADDON_FUNC_UNIT
#define _ADDON_FUNC_UNIT

#include <string>
#include <QImage>
#include <QFile>

using std::string;
using std::wstring;

/*typedef enum {
    itUnknown = -1, itTarga, itBitmap, itPNG, itJPEG, itGif
} TImageType;*/
enum TImageType {
    itUnknown = -1, itTarga, itBitmap, itPNG, itJPEG, itGif, itHDPhoto, itPKArchive, itRarArchive
};

QImage NewQImageFromResource(HMODULE hModule, LPCTSTR lpType, LPCTSTR lpName);
bool LoadQImageFromResource(QImage& qimage, HMODULE hModule, LPCTSTR lpType, LPCTSTR lpName);
void NormalizeGrayScaleQImage( QImage& qimage );
void ReplaceAlphaWithChecker( QImage& qimage );
int GetFreePhysMemory();
quint16 readuint16(QFile& qfile, bool bigendian = false, bool peek = false);
quint32 readuint32(QFile& qfile, bool bigendian = false, bool peek = false);
QImage NewQImageFromEXIFThumbnail(QFile& qfile);
extern "C" {
    void armmemcpy(__out_bcount_full_opt(_Size) void * _Dst, __in_bcount_opt(_Size) const void * _Src, __in size_t _Size);
};
void* optmemcpy(__out_bcount_full_opt(_Size) void * _Dst, __in_bcount_opt(_Size) const void * _Src, __in size_t _Size);
TImageType GuessMIMEType(QIODevice* file);
TImageType GuessMIMEType(LPWSTR filename);
TImageType ExtToMIMEType(const QString& extname);
QString MIMETypeToExt(TImageType type);

void DrawSplashOnWorld();
bool RotateWorld(DWORD Orientation);
bool IsEmulator();
bool Check0619ROM();
wstring StringToWideString(const string& Source,unsigned int Codepage = CP_ACP);
QByteArray qUncompressEx(const uchar* data, int nbytes, int outputbytes);


#ifndef VERBOSE
#define VERBOSEMSG(cond,printf_exp) ((void)0)
#else
#define VERBOSEMSG(cond,printf_exp)   \
    ((cond)?(NKDbgPrintfW printf_exp),1:0)
#endif

#endif