#ifndef _DBCENTRE_H
#define _DBCENTRE_H

typedef struct tagPathRecItem {
    QString LastStageFile;
    QString StartupFolder;
    int LastArchiveIndex;
} TPathRecItem;

typedef struct tagStateRecItem {
    int ExtraInfo;
    bool FullScreen;
} TStateRecItem;

extern TPathRecItem PathSetting;
extern TStateRecItem StateSetting;

void LoadAppSettings( void );
void SaveAppSettings( void );


#endif
