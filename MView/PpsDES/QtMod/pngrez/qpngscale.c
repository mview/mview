/* We do all our arithmetic in integers.  In order not to get killed by the
rounding, we scale every number up by the factor SCALE, do the 
arithmetic, then scale it back down.
*/
#define SCALE 4096
#define HALFSCALE 2048

/* Macro for turning a format number into a type number. */

#define PPM_FORMAT_TYPE(f) ((f) == PPM_FORMAT || (f) == RPPM_FORMAT ? PPM_TYPE : PGM_FORMAT_TYPE(f))


#define PNM_FORMAT_TYPE(f) PPM_FORMAT_TYPE(f)


#define PPM_OVERALLMAXVAL PGM_OVERALLMAXVAL
#define PPM_MAXMAXVAL PGM_MAXMAXVAL
typedef struct
{
    pixval r, g, b;
} pixel;
#define PPM_GETR(p) ((p).r)
#define PPM_GETG(p) ((p).g)
#define PPM_GETB(p) ((p).b)

/************* added definitions *****************/
#define PPM_PUTR(p,red) ((p).r = (red))
#define PPM_PUTG(p,grn) ((p).g = (grn))
#define PPM_PUTB(p,blu) ((p).b = (blu))
/**************************************************/

#define PPM_ASSIGN(p,red,grn,blu) do { (p).r = (red); (p).g = (grn); (p).b = (blu); } while ( 0 )
#define PPM_EQUAL(p,q) ( (p).r == (q).r && (p).g == (q).g && (p).b == (q).b )

static void
horizontal_scale(const pixel inputxelrow[], pixel newxelrow[], 
                 const int cols, const int newcols, const long sxscale, 
                 const int format, const uint maxval,
                 int * stretchP) 
{
    /*----------------------------------------------------------------------------
    Take the input row inputxelrow[], which is 'cols' columns wide, and
    scale it by a factor of 'sxcale', which is in SCALEths to create
    the output row newxelrow[], which is 'newcols' columns wide.

    'format' and 'maxval' describe the Netpbm format of the both input and
    output rows.

    *stretchP is the number of columns (could be fractional) on the right 
    that we had to fill by stretching due to rounding problems.
    -----------------------------------------------------------------------------*/
    long r, g, b;
    long fraccoltofill, fraccolleft;
    unsigned int col;
    unsigned int newcol;

    newcol = 0;
    fraccoltofill = SCALE;  /* Output column is "empty" now */
    r = g = b = 0;          /* initial value */
    for (col = 0; col < cols; ++col) {
        /* Process one pixel from input ('inputxelrow') */
        fraccolleft = sxscale;
        /* Output all columns, if any, that can be filled using information
        from this input column, in addition what's already in the output
        column.
        */
        while (fraccolleft >= fraccoltofill) {
            /* Generate one output pixel in 'newxelrow'.  It will consist
            of anything accumulated from prior input pixels in 'r','g', 
            and 'b', plus a fraction of the current input pixel.
            */
            switch (PNM_FORMAT_TYPE(format)) {
            case PPM_TYPE:
                r += fraccoltofill * PPM_GETR(inputxelrow[col]);
                g += fraccoltofill * PPM_GETG(inputxelrow[col]);
                b += fraccoltofill * PPM_GETB(inputxelrow[col]);
                r /= SCALE;
                if ( r > maxval ) r = maxval;
                g /= SCALE;
                if ( g > maxval ) g = maxval;
                b /= SCALE;
                if ( b > maxval ) b = maxval;
                PPM_ASSIGN( newxelrow[newcol], r, g, b );
                break;

            default:
                g += fraccoltofill * PNM_GET1(inputxelrow[col]);
                g /= SCALE;
                if ( g > maxval ) g = maxval;
                PNM_ASSIGN1( newxelrow[newcol], g );
                break;
            }
            fraccolleft -= fraccoltofill;
            /* Set up to start filling next output column */
            newcol++;
            fraccoltofill = SCALE;
            r = g = b = 0;
        }
        /* There's not enough left in the current input pixel to fill up 
        a whole output column, so just accumulate the remainder of the
        pixel into the current output column.
        */
        if (fraccolleft > 0) {
            switch (PNM_FORMAT_TYPE(format)) {
            case PPM_TYPE:
                r += fraccolleft * PPM_GETR(inputxelrow[col]);
                g += fraccolleft * PPM_GETG(inputxelrow[col]);
                b += fraccolleft * PPM_GETB(inputxelrow[col]);
                break;

            default:
                g += fraccolleft * PNM_GET1(inputxelrow[col]);
                break;
            }
            fraccoltofill -= fraccolleft;
        }
    }

    *stretchP = 0;   /* initial value */
    while (newcol < newcols) {
        /* We ran out of input columns before we filled up the output
        columns.  This would be because of rounding down.  For small
        images, we're probably missing only a tiny fraction of a column, 
        but for large images, it could be multiple columns.

        So we fake the remaining output columns by copying the rightmost
        legitimate pixel.  We call this stretching.
        */

        *stretchP += fraccoltofill;

        switch (PNM_FORMAT_TYPE(format)) {
        case PPM_TYPE:
            r += fraccoltofill * PPM_GETR(inputxelrow[cols-1]);
            g += fraccoltofill * PPM_GETG(inputxelrow[cols-1]);
            b += fraccoltofill * PPM_GETB(inputxelrow[cols-1]);

            r += HALFSCALE;  /* for rounding */
            r /= SCALE;
            if ( r > maxval ) r = maxval;
            g += HALFSCALE;  /* for rounding */
            g /= SCALE;
            if ( g > maxval ) g = maxval;
            b += HALFSCALE;  /* for rounding */
            b /= SCALE;
            if ( b > maxval ) b = maxval;
            PPM_ASSIGN(newxelrow[newcol], r, g, b );
            break;

        default:
            g += fraccoltofill * PNM_GET1(inputxelrow[cols-1]);
            g += HALFSCALE;  /* for rounding */
            g /= SCALE;
            if ( g > maxval ) g = maxval;
            PNM_ASSIGN1(newxelrow[newcol], g );
            break;
        }
        newcol++;
        fraccoltofill = SCALE;
    }
}