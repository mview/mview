#ifndef _STRING_INTERNER
#define _STRING_INTERNER

#include <map>
#include <mzfc_inc.h>

typedef std::map < int, CMzStringW > StringMap;

CMzStringW LoadStr(HINSTANCE hInstance, int StringID);

class TStringStore {
public:
    TStringStore(HMODULE module = NULL): fSateModule(module) { };
    //~TStringStore();
public:
    CMzStringW RetroString(int StringID);
private:
    StringMap Strings;
    HMODULE fSateModule;
};

typedef TStringStore* PStringStore;

extern PStringStore stringstore;

#define _tr(x) stringstore->RetroString(x)

#endif
