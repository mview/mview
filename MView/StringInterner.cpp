#include "StringInterner.h"

using std::make_pair;

PStringStore stringstore; // imp

CMzStringW LoadStr(HINSTANCE hInstance, int StringID) {
    bool hitted = false;
    LPWSTR buf = LPWSTR(LoadStringW(hInstance, StringID, NULL, 0)); // cause the W, don't use LPCTSTR here
    if (buf != NULL) {
        // they must gather than 2
        if (*PWORD(--buf) > 0) {
            hitted = true;
        }
    }
    if (hitted) {
        CMzStringW Result;
        Result.SetBufferSize(*PWORD(buf) + 1); // for terminate (auto filled)
        wcsncpy(Result.C_Str(), ++buf, Result.GetBufferSize() - 1);
        return Result;
    } else {
        return L"";
    }
}

CMzStringW TStringStore::RetroString( int StringID )
{
    if (Strings.empty() == false){
        StringMap::iterator it = Strings.find(StringID);
        if (it != Strings.end()) {
            return CMzStringW(it->second);
        }
    }
    // try load from MUI satellite or native binary
    CMzStringW Result = LoadStr(fSateModule == NULL?MzGetInstanceHandle():fSateModule, StringID);
    // TODO: try load from native when satellite module haven't such an StringID
    if (Result.IsEmpty()) {
        Result = LoadStr(MzGetInstanceHandle(), StringID);
    }
    if (Result.IsEmpty() == false) {
        Strings.insert(make_pair(StringID, Result));
    }
    return Result;
}