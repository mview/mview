#ifndef _SELECTOR_UNIT
#define _SELECTOR_UNIT

#include <mzfc_inc.h>
#include <FvMzKOL.h>
#include "DirtyDefination.h"

class RadioList: public UiList {
public:
    // override the DrawItem member function to do your own drawing of the list
    void DrawItem(HDC hdcDst, int nIndex, RECT* prcItem, RECT *prcWin, RECT *prcUpdate);
    int OnLButtonDown(UINT fwKeys, int xPos, int yPos);
    //int OnLButtonUp(UINT fwKeys, int xPos, int yPos);
protected:
private:

};

class RadioWnd: public CMzWndEx {
    MZ_DECLARE_DYNAMIC(RadioWnd);
private:
    int* fResult;
    PKOLStrList fRadioList;
public:
    RadioWnd(int* result, PKOLStrList radiolist): fResult(result), fRadioList(radiolist) { };
protected:
    UiToolbar_Text m_Toolbar;
    RadioList m_List;

    // Initialization of the window (dialog)
    virtual BOOL OnInitDialog();

    // override the MZFC command handler
    virtual void OnMzCommand(WPARAM wParam, LPARAM lParam);

};

#endif