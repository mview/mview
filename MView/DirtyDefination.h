#ifndef _DIRTY_DEFINATIONS
#define _DIRTY_DEFINATIONS

#include <mzfc_inc.h>

#define MZ_IDC_TOOLBAR1     101

#define IDC_PPM_CAPTURE     102
#define IDC_PPM_BROWSE      103

#define IDT_SOMEMARKTIMEOUT 104
#define IDT_SOMEHINTTIMEOUT 105

#define WM_MZ_TOUCHNOTIFY   59369822

#endif
