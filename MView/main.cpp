#include <wingdi.h>
#include <mzfc_inc.h>
#include <ShellNotifyMsg.h>
#include <QApplication>
#include <QtGUI/QFont>
#include <QtGUI/QColorMap>
#include <FvMzKOL.h>
#include "MainUnt.h"
#include "AddonFuncUnt.h"
#include "resource.h"
#include "StringInterner.h"

#ifdef QT_NODLL
Q_IMPORT_PLUGIN(qjpeg)
Q_IMPORT_PLUGIN(qgif)
#endif

// Application class derived from CMzApp
class CBoxApp: public CMzApp
{
private:
#ifdef DEBUG
    QCoreApplication fakeQApp;
    int argc;
    char **argv;
#endif
public:
    CBoxApp();
    ~CBoxApp();
public:
    // The main window of the app.
    CMainWnd m_MainWnd;
    HANDLE fNukeMute;

    // Initialization of the application
    virtual BOOL Init();

    // Cleanup Resource
    virtual int Done();
};

char ansinull[2] = "";
char *null = &ansinull[0];

CBoxApp::CBoxApp()
#ifdef _DEBUG
    : argc(0), argv(&null), fakeQApp(argc, &null)
#endif
{
    //stringstore = new TStringStore;
}

CBoxApp::~CBoxApp()
{
    //delete stringstore;
#ifdef MVIEW_RETAIL
    // Should Call MainWnd~ctor now
    m_MainWnd.WaitForKillBoxFork();
#endif
}

BOOL CBoxApp::Init()
{
    // Init the COM relative library.
    CoInitializeEx(0, COINIT_MULTITHREADED);

    if (IsLockPhoneStatus() == TRUE) {
        if (IsEmulator()) {
            MzLeaveLockPhone();
        } else {
            return FALSE;
        }
    }

    fNukeMute = CreateMutex(NULL, TRUE, _T("16MView {20030319DEB090}"));
    if (GetLastError() == ERROR_ALREADY_EXISTS) {
        ShowMessage(_tr(IDS_STRING_ALREADYRUNNING));
        return FALSE;
    }

    

    QColormap::initialize(); // around with NDEBUG if no Qapp workaround
    QFont::initialize();

    // TODO: detect emulator
    DrawSplashOnWorld();

    // Create the main window
    RECT rcWork = MzGetWorkArea(); // 720 * 480? 690 * 480?
    //if (RECT_WIDTH(rcWork) <= 480 || RECT_HEIGHT(rcWork) > 480) {
    //    ShowMessage(_tr(IDS_STRING_NOM8LCD));
    //    return FALSE;
    //}
    if (m_MainWnd.Create(rcWork.left, rcWork.top, RECT_WIDTH(rcWork), RECT_HEIGHT(rcWork), 0, 0, 0) == FALSE) {
        ShowMessage(_tr(IDS_STRING_INITFAILED));
        return FALSE;
    }
    //m_MainWnd.SetBgColor(MzGetThemeColor(TCI_WINDOW_BG));
    m_MainWnd.EnableFillBgColor(false);
    m_MainWnd.SetSupportDShow(true);
    m_MainWnd.Show();

    // return TRUE means init success.
    return TRUE;
}

int CBoxApp::Done() {
    ReleaseMutex(fNukeMute);

    QFont::cleanup();
    QColormap::cleanup();

#ifdef DEBUG
    fakeQApp.exit(0);
#endif

    return CMzApp::Done();
}

// The global variable of the application.
CBoxApp theApp;
