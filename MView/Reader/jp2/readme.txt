 JPEG 2000 support for Qt based on JasPer lib
 Copyright (c) 2003 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>

 INSTALLATION
 
 Build and add Jasper lib into your project.
 
 WINDOWS (MSVC):
 You should build JasPer library, the MSVC project and simplified JasPer lib comes 
 with this distribution. Don't forget to build it using same STDC files as your
 project!!!
 
 Add macro to preprocessor definition: JAS_WIN_MSVC_BUILD
 And JasPer lib path to Additional Include Directories, smth like: XXX/libjasper/include

 LINUX: 
   Nothing here, just use it, that's all:-)
   
 HOW TO ADD SUPPORT TO QT:
   Add qjp2io.cpp to your project. Then simply call qInitJp2IO(); somewhere in your code.
   The best way is to put this call into the main before creating QApplication.

EXAMPLE (main.cpp):

//*****************************************
#include <qapplication.h> 
#include "formats/jp2/qjp2io.h"

int main( int argc, char** argv )
{
  qInitJp2IO();

  QApplication app( argc, argv );

  TControlForm window;
  app.setMainWidget(&window);
  window.show();
  
  return app.exec();
} 
//*****************************************