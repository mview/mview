/*****************************************************************************
 JPEG 2000 support for Qt based on JasPer lib
 Copyright (c) 2003 by Dmitry V. Fedorov <www.dimin.net>

 DEFINES

 Programmer: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

 History:
   13/06/2003 17:02 - First creation


 Ver : 1
*****************************************************************************/


#ifndef QJP2_H
#define QJP2_H

/* we better use jasper defined types
#ifndef int8
typedef	char int8;
#endif

#ifndef uint8
typedef	unsigned char uint8;
#endif

#ifndef int16
typedef	short int16;
#endif

#ifndef uint16
typedef	unsigned short uint16;
#endif

#ifndef int32
typedef	long int32;
#endif

#ifndef uint32
typedef	unsigned long uint32;	// sizeof (uint32) must == 4
#endif
*/

#ifndef max
#define max(a,b)      (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)      (((a) < (b)) ? (a) : (b))
#endif

#define MaxTextExtent 2053 

void qInitJp2IO();

#endif
