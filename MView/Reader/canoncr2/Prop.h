#ifndef _PROP_H_
#define _PROP_H_
/***********************************************************************
* File: 	Prop.h
* Change Info:
*   03/0407	HLS - Began.
* Description:	Properties header file for various photo image
*   	    	processing.
************************************************************************/

// Various constants

// Other Constants

// Type Definitions
#ifndef STD_TYPES
#define STD_TYPES
typedef char	    	tI8;
typedef unsigned char 	tU8;
typedef short	    	tI16;
typedef unsigned short	tU16;
typedef long	    	tI32;
typedef unsigned long	tU32;
#endif

typedef struct
{
    tU16    Type;
    tU32    Size;
    tU32    Ofst;
} tSEG;

typedef struct
{ // specific to CRW image files
    tSEG    Head; 	    	// CIFF header directory
    tSEG    HEAP;   	    	// CIFF Heap directory
    tSEG    RAW;    	    	// RAW compressed data directory
    tSEG    JPG;    	    	// JPG from RAW segment directory
    tSEG    THM;    	    	// Thumbnail image directory
    tSEG    Prop;   	    	// Image properties directory
    tU32    DecoderTableNum;	// which Huffmann table to use (1-3)
    tU16    CompressedLowBits;	// TRUE if the low bits are compressed
} tCRW;

#define PROP_MAX_HUF_LENGTHS        (16)
#define PROP_MAX_HUF_VALUES	    (256)
#define PROP_MAX_HUF_TABS	    (4)
typedef struct
{ // define Huffmann table
    tU8     TcTh;   	    	    	// table class, table ID
    tU8     nVals;  	    	    	// number values in Vij[]
    tU8     MaxVij; 	    	    	// largest value in Vij[]
    tU8     Li[PROP_MAX_HUF_LENGTHS];   // list of code lengths
    tU8     Vij[PROP_MAX_HUF_VALUES];   // list of values for each length Li[]
} tDHT;

typedef struct
{
    tU32    Width;
    tU32    Height;
} tSIZE;

#define PROP_FILE_TYPE_UNKNOWN      (0)
#define PROP_FILE_TYPE_TIF	    (1)
#define PROP_FILE_TYPE_CR2	    (2)
#define PROP_FILE_TYPE_CRW	    (3)
#define PROP_IMAGE_TYPE_RGB 	    (1)
#define PROP_IMAGE_TYPE_BAYER	    (2)
#define PROP_HYPER_UNITS_FEET	    (0)
#define PROP_HYPER_UNITS_METERS     (1)
typedef struct
{
    tSIZE   Size;   	    	    	// image size, raw (width x height)
    tSIZE   FinishedSize;  	    	// finished image size (width x height)
    int     HyperFDistanceUnits;    	// 0=Feet, 1=meters
    double  HyperFDistance; 	    	// lens to object distance
    double  ExposureTime;
    double  FNumber;
    double  FocalLength;
    double  FocalPlaneXResolution;
    double  FocalPlaneYResolution;
    tU16    FocalPlaneResolutionUnit;	// 1=none, 2=inch, 3=cm
    tU32    ISOSpeedRatings;
    tU32    Orientation;
    tU16    BitsPerSample[3];
    tU16    Precision;	    	    	// # bits precision, expect 12 for CR2
    int     FileType;	    	    	// 1=TIF, 2=CR2, 3=CRW
    int     Type;	    	    	// 1=RGB, 2=BAYER
} tIMG;

#define PROP_MAX_TEXT    	    (64)
#define PROP_MAX_PATH    	    (256)
typedef struct
{
    char    Filename[PROP_MAX_PATH];	// file name
    char    Make[PROP_MAX_TEXT]; 	// camera make (aka manufacturer)
    char    Model[PROP_MAX_TEXT];  	// camera model
    char    FwVersion[PROP_MAX_TEXT];   // camera F/W version
    char    DateTime[PROP_MAX_TEXT];    // date/time
    char    SerialNum[PROP_MAX_TEXT];   // camera serial number
    char    LensModel[PROP_MAX_TEXT];   // lens model
} tTXT;

typedef struct
{
    tU16    Top;    	    	    // # rows at top that are black
    tU16    Bottom;    	    	    // # rows at bottom that are black 
    tU16    Right;    	    	    // # columns at right that are black
    tU16    Left;    	    	    // # columns at left that are black
} tTRIM;

typedef struct
{
    tU32    *OffsetList;    	    // StripByteOffset[] list
    tU32    *CountList;    	    // StripByteCounts[] list
    tU32    nList;	    	    // # in OffsetList[] and CountList[]
    tU32    RowsPer;	    	    // RowsPerStrip
    tU32    OfstByteCounts; 	    // file offset of TIFF IFD StripByteCounts
    tU32    OfstStartOfScan;	    // file offset of JPG StartOfScan (for CR2)
} tSTRIP;

typedef struct
{
    tTXT    Txt;    	    	    // various text information
    tIMG    Image; 	    	    // image properties
    tSTRIP  Strip;  	    	    // TIFF StripByteOffset info
    tDHT    Huf[PROP_MAX_HUF_TABS]; // Huffmann tables descriptor
    tCRW    Crw;    	    	    // CRW file specific properties
    tTRIM   Trim;   	    	    // found 'black' border rows/columns
    tU8     *IB8;    	    	    // ImageBuffer[] for 8-bit images
    tU16    *IB16;    	    	    // ImageBuffer[] for 16-bit images
} tPROP;
    
#endif _PROP_H_
