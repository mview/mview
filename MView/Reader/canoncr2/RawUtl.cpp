/***************************************************************************
* File: 	RawUtl.cpp
* Functions:
*   	    	IntelMotoByteSwap()
*		HuffmannMakeCoderTable()
*		HuffmannMakeDecoderTable()
*   	    	FinishedSize()
*		FindTrim()
*		MakeSimpleHistogram()
*		MakeHistogram()
*		WriteTiff()
* Change Info:
*   08/14/07	HLS - Added DateTime to MakeSimpleHistogram() output.
*   03/15/07	HLS - Added FinishedSize().
*   03/14/07	HLS - Split off TiffXXX() to file TiffUtl.cpp.
*   03/10/07	HLS - Added TiffGetXmpItem().
*   03/09/07	HLS - Added IntelMotoByteSwap().
*   03/06/07	HLS - Incorporated TiffFirstIfd().
*   03/04/07	HLS - Began.
* Description:	Various RAW photo image utilities.
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Prop.h"
#include "RawUtl.h"

//#define DEBUG_PRINT

// Various Constants
#define FALSE	    	(0)
#define TRUE	    	(1)

// typedefs
#ifndef STD_TYPES
#define STD_TYPES
typedef char	    	tI8;
typedef unsigned char 	tU8;
typedef short	    	tI16;
typedef unsigned short	tU16;
typedef long	    	tI32;
typedef unsigned long	tU32;
#endif

// Global Variables

/***************************************************************************
* Function: 	HuffmannMakeCoderTable()
* Returns:  	0 if successful
* Description:	Builds a pair of Huffmann coder tables, one provides right
*   	    	justified Huffmann codes and the other provides the number
*   	    	of bits used by that code.  Memory is allocated from the
*   	    	heap for the tables.  Each table has 256 entries.  When
*   	    	used with CRW files, a two nibble index of HighNib(RptCnt)
*   	    	and LowNib(nDifBits) results in a right justified Huffmann
*   	    	code and number bits required by that code.  When used by
*   	    	conventional JPG files the whole index provides the code
*   	    	and bits information.  Unused codes return 0 for code and
*   	    	length.  Exits program upon failure to aquire memory or
*   	    	table  overflow.  The tables can be returned to the heap
*   	    	with free(HufCodes);
***************************************************************************/
int 	HuffmannMakeCoderTable(
tDHT	*Huf,	    	    // Huffmann table definition
tU16	**HufCodes,  	    // buffer, Huffmann codes
tU16 	**HufBits)  	    // buffer, # bits in Huffmann codes
{
    // allocate table memory
    tU16  *TabBuf;
    TabBuf = (tU16 *) calloc(2 * HUF_CODER_SIZE, sizeof(tU16));
    if (!TabBuf)
    {
    	fprintf(stderr, "can't calloc in MakeCoderTables()\n");
	exit(1);
    }

    tU16 *hCodes, *hBits;
    tU8  *Li, *Vij;
    tU8  Val;	    // value assigned to Huffmann code
    tU16 HufCode;   // right justified Huffmann code
    int  i; 	    // index into HufSiz[] table
    	    	    // and i+1 is length (nBits) of Huffmann code
    int  nSiz;	    // number of Huffmann codes of a particular code length
    
    Li  = Huf->Li;
    Vij = Huf->Vij;
    *HufCodes = hCodes = &TabBuf[0 * HUF_CODER_SIZE]; 
    *HufBits  = hBits  = &TabBuf[1 * HUF_CODER_SIZE];

    HufCode = 0;
    for (i = 0; i < 16; i++)
    {
    	HufCode <<= 1;
    	for (nSiz = Li[i]; nSiz; nSiz--)
    	{
    	    Val = *Vij++;
    	    hCodes[Val] = HufCode;
    	    hBits[Val]  = i + 1;
    	    HufCode += 1;
    	}
    }

#ifdef DEBUG_PRINT
    printf("\nTable # %02X\n", Huf->TcTh);
    for (i = 0; i < 256; i++)
    {
    	if (hBits[i] == 0) continue;
	printf("CoderTab[%02x]: Val=%02X, HufCode=%04X, HufBits=%d\n",
	    	Huf->TcTh, i, hCodes[i], hBits[i]);
    }
#endif
    
    return(0);
} /* end HuffmannMakeCoderTable() */

/***************************************************************************
* Function: 	HuffmannMakeDecoderTable()
* Returns:  	0 if successful
* Description:	Builds a set of Huffmann decoder tables, based on JPG 
*   	    	define Huffmann Table (DHT) specification and adapted for
*   	    	the Canon CRW specifition.  Two or three tables are created,
*   	    	memory is allocated from the heap for them.  If HufRpt is
*   	    	NULL then only two tables are created, otherwise the HufRpt
*   	    	table is also crated.  Each table has a size 0x10000, most
*   	    	is filled as redundant.  These tables are length of
*   	    	Huffmann codes and a value associated with the code.  If
*   	    	HufRpt table is requested, the the value is divided into
*   	    	two tables (HufVal and HufRpt) with the former being the
*   	    	lower nibble of the Huffmann Vij value and the latter
*   	    	being the upper nibber of the Huffmann Vij value.  The 
*   	    	index into these tables are left justified, thus don't
*   	    	care bits are to the right.  Thus if the 3-bit sequence
*   	    	010 is a valid Huffmann code, then 010X.XXXX.XXXX.XXXX
*   	    	used as an index will provide access to the value.  The
*   	    	tables may be returned to the heap with free(HufBits).
***************************************************************************/
int 	HuffmannMakeDecoderTable(
tDHT	*Huf,	    	    // Huffmann table definition
tU8 	**HufBits,  	    // buffer, length of Huffmann codes
tU8 	**HufVal,  	    // buffer, # bits to create 'difference'
tU8 	**HufRpt)  	    // buffer, length of repeat codes
{
    // allocate table memory
    int nTabs;
    tU8 *TabBuf;
    tU8 *hBits, *hVal, *hRpt;
    nTabs = (HufRpt == NULL) ? 2 : 3;
    TabBuf = (tU8 *) calloc(nTabs * HUF_DECODER_SIZE, sizeof(tU8));
    if (!TabBuf)
    {
    	fprintf(stderr, "can't calloc in MakeHuffmannDecoderTable()\n");
	exit(1);
    }
    *HufBits = hBits = &TabBuf[0 * HUF_DECODER_SIZE]; 
    *HufVal  = hVal  = &TabBuf[1 * HUF_DECODER_SIZE];
    if (HufRpt) *HufRpt = hRpt = &TabBuf[2 * HUF_DECODER_SIZE];
    
    tU8  *Li;	    // array of number Huffmann codes of length i
    tU8  *Vij;	    // value associated with each Huffmann code
    tU8  Val, Rpt;
    tU32 HufCode;   // left justified Huffmann code
    tU32 Delta;     // dual purpose: a) delta from previous Huffmann code
    	    	    // and b) the amount of duplicate fill for tables
    int  iTab;	    // which table: either 1 or 2
    int  i, j;
    int  nSiz;	    // number of Huffmann codes of a particular code length

    Li  = Huf->Li;
    Vij = Huf->Vij;
    HufCode = 0;
    for (Delta = HUF_DECODER_SIZE, i = j = 0; (Delta >>= 1); i++)
    {
    	for (nSiz = Li[i]; nSiz; nSiz--, HufCode += Delta, j++)
    	{
#ifdef DEBUG_PRINT
    	    printf("DecodeTab[%02X]: HufCode=%04X, Delta=%04X, HufVal=%02X, HufBits=%d\n",
    	    	    Huf->TcTh, HufCode, Delta, Vij[j], i+1);
#endif
    	    Val = Vij[j];
    	    if (HufRpt)
	    {
		Rpt  = (Val >> 4) & 0x0F;
	    	Val &= 0x0F;
		memset(&hRpt[HufCode], Rpt, Delta);
	    }
    	    memset(&hBits[HufCode], i + 1, Delta);
    	    memset(&hVal[HufCode], Val, Delta);
    	    if (HufCode >= HUF_DECODER_SIZE)
    	    {
    		fprintf(stderr, "HufTab overflow, aborting\n");
    		return(1);
    	    }
    	}
    }
#ifdef DEBUG_PRINT
    if (HufCode != HUF_DECODER_SIZE)
    {
    	fprintf(stderr, "Warning: HufTab underflow by 0x%X\n",
    		HUF_DECODER_SIZE - HufCode);
    }
#endif

    return(0);
} /* end HuffmannMakeDecoderTable() */

/***************************************************************************
* Function: 	FinishedSize()
* Returns:  	Finished size OR {0,0} if model not found
* Description:	Searches camera model against known list for FinishedSize.
* Note:     	Sizes are mostly Canon which use CRW file format.
***************************************************************************/
tSIZE 	FinishedSize(
char	*Model)
{
    typedef struct
    {
    	char	*Model;
	tSIZE	Size;
    } tSIZ_LIST;
    
    static tSIZ_LIST	SizeList[] =
    {// Model	    	    	Size{Width, Height}
    	"Canon PowerShot G2",	{2272, 1704},
	"Canon PowerShot G3",	{2272, 1704},
	"Canon PowerShot G5",	{2592, 1944},
	"Canon PowerShot G6",	{3072, 2304},
	"Canon PowerShot S30",	{2048, 1536},
	"Canon PowerShot S40",	{2272, 1704},
	"Canon PowerShot S45",	{2272, 1704},
	"Canon PowerShot S50",	{2592, 1944},
	"Canon PowerShot S60",	{2592, 1944},
	"Canon PowerShot S70",	{3072, 2304},
	"Canon EOS 10D",    	{3072, 2048},
	"Canon EOS 1D",     	{2464, 1648},
	"Canon EOS D30",    	{2160, 1440},
	"Canon EOS D60",    	{3072, 2048},
	"Canon EOS 300D",	{3072, 2048},
	"Canon EOS 20D",    	{3504, 2336},
	NULL, 	    	    	{0,    0}
    };
    
    int n;
    for (n = 0; SizeList[n].Model; n++)
    {
    	if (!strcmp(Model, SizeList[n].Model)) break;
    }
    
    return(SizeList[n].Size);
} /* end FinishedSize() */

/***************************************************************************
* Function: 	FindTrim()
* Returns:  	0 if successful
* Description:	Scans image buffer IB to find black rows and columns at
*   	    	top/bottom or right/left that are trimmed away.
***************************************************************************/
int 	FindTrim(
tPROP	*Prop)	    	    // pointer to properties list
{
    // check for a few known types first, these are all CR2 file types
    static tTRIM Cr2Trim3516 = {14, 0, 0, 42}; // {top, bottom, right, left}
    static tTRIM Cr2Trim3596 = {12, 0, 0, 74};
    static tTRIM Cr2Trim3948 = {18, 2, 0, 42};
    static tTRIM Cr2Trim4476 = {34, 0, 0, 90};
    static tTRIM Cr2Trim5108 = {13, 0, 0, 98};
    if ((Prop->Image.FileType == PROP_FILE_TYPE_CR2)
    	&& (!strcmp(Prop->Txt.Make, "Canon")))
    {
    	switch(Prop->Image.Size.Width)
	{
	    case 3516: Prop->Trim = Cr2Trim3516; return(0);
	    case 3596: Prop->Trim = Cr2Trim3596; return(0);
	    case 3948: Prop->Trim = Cr2Trim3948; return(0);
	    case 4476: Prop->Trim = Cr2Trim4476; return(0);
	    case 5108: Prop->Trim = Cr2Trim5108; return(0);
	    default: break; // known type not found
	}
    }

    tU16 *IB, *pIB;
    tU16 Rows, Cols;
    tU16 Top, Bottom, Left, Right;
    tU16 Black;
    int  Found;
    tU32 i;
    tU32 Pixels;
    
    IB   = Prop->IB16;
    Rows = Prop->Image.Size.Height;
    Cols = Prop->Image.Size.Width;
    Pixels = Rows * Cols;
    Black = IB[0] + 64;	    // assume trim border is less than Black
    if (Black < 192) Black = 192;
    
    // find Top rows trim
    for (i = 0, pIB = IB; i < Pixels; i++)
    {
    	if (*pIB++ > Black) break;
    }
    Prop->Trim.Top = i / Cols;
    
    // find Bottom rows trim
    for (i = 0, pIB = IB + Pixels - 1; i < Pixels; i++)
    {
    	if (*pIB-- > Black) break;
    }
    Prop->Trim.Bottom = i / Cols;
    
    // find Left columns trim
    for (i = 0; i < Pixels; i++)
    {
    	if ((i % Rows) == 0) pIB = IB + i / Rows;
    	if (*pIB > Black) break;
	pIB += Cols;
    }
    Prop->Trim.Left = i / Rows;
    
    // find Right columns trim
    for (i = 0; i < Pixels; i++)
    {
    	if ((i % Rows) == 0) pIB = IB + Pixels - 1 - i / Rows;
    	if (*pIB > Black) break;
	pIB -= Cols;
    }
    Prop->Trim.Right = i / Rows;

#ifdef DEBUG_PRINT
    printf("Trim: Top=%d, Bottom=%d, Left=%d, Right=%d\n",
    	    Prop->Trim.Top, Prop->Trim.Bottom, Prop->Trim.Left, Prop->Trim.Right);
#endif
   
    return(0);
} /* end FindTrim() */

/***************************************************************************
* Function: 	MakeSimpleHistogram()
* Returns:  	0 if successful
* Description:	Creates a simple histogram of the all colors in the
*   	    	image defined in Prop. The output is is a text bar chart.
***************************************************************************/
int 	MakeSimpleHistogram(
tPROP	*Prop)
{
    if ((Prop->Image.Precision > 16) || (Prop->Image.Precision < 8))
    {
    	fprintf(stderr, "Invalid precision (%d) for MakeSimpleHistogram()\n",
	    	Prop->Image.Precision);
	return(1);
    }
    
    int   Ret;
    tHIST Hist;
    tU32  *HB;
    tU32  HBextent;
    HBextent = 1 << Prop->Image.Precision;
    HB = (tU32 *) calloc(HBextent, sizeof(*HB));
    if (!HB)
    {
    	fprintf(stderr, "can't calloc in MakeSimpleHistogram()\n");
	exit(1);
    }
    
    Hist.HB = HB;
    Hist.HBextent = HBextent;
    
    Ret = MakeHistogram(Prop, 0xFFFF, &Hist);     	
    if (Ret) goto done;
    
#define HISTOGRAM_LINES     (32)
#define HISTOGRAM_WIDTH     (60)
    tU16 Rpt;
    double HBtext[HISTOGRAM_LINES];
    double HBmax;
    int  i, j, k;
    memset(HBtext, 0, HISTOGRAM_LINES * sizeof(*HBtext));
    Rpt = HBextent / HISTOGRAM_LINES;
    HBmax = 1;
    for (i = 0, j = 0; i < HISTOGRAM_LINES; i++)
    {
    	for (k = 0; k < Rpt; k++, j++)
	{
	    HBtext[i] += HB[j];
	}
	
	if (HBtext[i] > HBmax) HBmax = HBtext[i];
    }
    
    // normalize HBtext for text output
    double Total;
    Total = 0;
    for (i = 0; i < HISTOGRAM_LINES; i++)
    {
    	HBtext[i] *= HISTOGRAM_WIDTH / HBmax;
	Total += HBtext[i];
    }
    
    printf("Histogram of %s   DateTime=%s\n",
    	    Prop->Txt.Filename, Prop->Txt.DateTime);

    if (Prop->Txt.Model) printf("CameraModel=%s   ", Prop->Txt.Model);
    if (Prop->Txt.LensModel) printf("LensModel=%s", Prop->Txt.LensModel);
    if ((Prop->Txt.Model) || (Prop->Txt.LensModel)) printf("\n");
    
    if (Prop->Image.FocalLength)
    	printf("FocalLength=%g  ", Prop->Image.FocalLength);
    if (Prop->Image.ExposureTime)
    	printf("ExposureTime=1/%g  ", 1.0 / Prop->Image.ExposureTime);
    if (Prop->Image.FNumber)
    	printf("FNumber=f/%g  ", Prop->Image.FNumber);
    if (Prop->Image.ISOSpeedRatings)
    	printf("ISO=%d", Prop->Image.ISOSpeedRatings);
    if ((Prop->Image.FocalLength)
    	|| (Prop->Image.ExposureTime)
    	|| (Prop->Image.FNumber)
    	|| (Prop->Image.ISOSpeedRatings))
	printf("\n");

    printf("RawImageSize=(W=%d, H=%d)  ", 
    	    Prop->Image.Size.Width, Prop->Image.Size.Height);
    if (Prop->Image.FinishedSize.Width && Prop->Image.FinishedSize.Height)
    {
    	printf("FinishedSize=(W=%d, H=%d)",
    	    Prop->Image.FinishedSize.Width, Prop->Image.FinishedSize.Height);
    }
    printf("\n");

    printf("Trim: L=%d, R=%d, T=%d, B=%d\n",
    	    Prop->Trim.Left, Prop->Trim.Right,
	    Prop->Trim.Top, Prop->Trim.Bottom);

    printf("Range={0-%d}, Min=%d, Max=%d, Mean=%d\n", 
    	    HBextent-1, Hist.Min, Hist.Max, Hist.Mean);
    for (i = 0; i < HISTOGRAM_LINES; i++)
    {
    	j = HBtext[i];
    	printf("%6.1f - %5d |", 
	    	100.0 * HBtext[i] / Total, i * Rpt);
	while(j--)
	{
	    printf("*");
	}
	printf("\n");
    }
    printf(" 100.0%% %6d\n", i * Rpt - 1);
    
    done:
    free(HB);
    return(Ret);
} /* end MakeSimpleHistogram() */

/***************************************************************************
* Function: 	MakeHistogram()
* Returns:  	0 if successful
* Description:	Reads image buffer (except in trim area) to create a 
*   	    	histogram in buffer HB of the specified color for either
*   	    	RGB or BAYER grid images. Histogram range fills size of 
*   	    	buffer.  Returns largest image buffer value even if it's
*   	    	larger than buffer.
***************************************************************************/
int 	MakeHistogram(
tPROP	*Prop,	    	    // pointer to parameter list
tU16	Color,	    	    // bit map of which colors to histogram
tHIST	*Hist)     	    // pointer to histogram info
{
    tU32 *HB;
    tU16 Rows, Cols;
    tU16 iRow, iCol;
    tU16 Top, Bottom, Left, Right;
    tU32 HBextent = Hist->HBextent;
    tU32 iMax = 0;
    tU32 iMin = HBextent;
    tU32 i;
    tU32 nPix = 0;
    double MeanAve;
    
    HB = Hist->HB;
    memset(HB, 0, HBextent * sizeof(*HB));
    Rows = Prop->Image.Size.Height;
    Cols = Prop->Image.Size.Width;
    Top  = Prop->Trim.Top;
    Bottom = Rows - 1 - Prop->Trim.Bottom;
    Left = Prop->Trim.Left;
    Right = Cols - 1 - Prop->Trim.Right;
    MeanAve = 0;
    if ((Prop->Image.Type == PROP_IMAGE_TYPE_RGB) && (Prop->IB8))
    { // 8-bit per color channel RGB image
    	tU16 iRgb;
    	tU8  *IB;
	IB = Prop->IB8;
    	for (iRow = 0; iRow < Rows; iRow++)
    	{
    	    for (iCol = 0; iCol < Cols; iCol++, IB += 3)
    	    {
    	    	if (iCol < Left) continue;
    	    	if (iCol > Right) continue;
    	    	if (iRow < Top) continue;
    	    	if (iRow > Bottom) continue;
    	    	
    	    	for (iRgb = 0; iRgb < 3; iRgb++)
    	    	{
		    switch (iRgb)
		    {
		    	case 0: if (Color & RGB_COLOR_RED)   break; continue;
		    	case 1: if (Color & RGB_COLOR_GREEN) break; continue;
		    	case 2: if (Color & RGB_COLOR_BLUE ) break; continue;
		    }
		    
    	    	    i = IB[iRgb];
    	    	    MeanAve += i;
    	    	    if (i > iMax) iMax = i;
    	    	    if (i < iMin) iMin = i;
    	    	    if (i >= HBextent) i = HBextent - 1;
    	    	    HB[i]++;
    	    	    nPix++;
    	    	}
    	    }
    	}
    }
    else if ((Prop->Image.Type == PROP_IMAGE_TYPE_RGB) && (Prop->IB16))
    { // 16-bit per color channel RGB iamge
    	tU16 iRgb;
    	tU16 *IB;
	IB = Prop->IB16;
    	for (iRow = 0; iRow < Rows; iRow++)
    	{
    	    for (iCol = 0; iCol < Cols; iCol++, IB += 3)
    	    {
    	    	if (iCol < Left) continue;
    	    	if (iCol > Right) continue;
    	    	if (iRow < Top) continue;
    	    	if (iRow > Bottom) continue;
    	    	
    	    	for (iRgb = 0; iRgb < 3; iRgb++)
    	    	{
		    switch (iRgb)
		    {
		    	case 0: if (Color & RGB_COLOR_RED)   break; continue;
		    	case 1: if (Color & RGB_COLOR_GREEN) break; continue;
		    	case 2: if (Color & RGB_COLOR_BLUE ) break; continue;
		    }
		    
    	    	    i = IB[iRgb];
    	    	    MeanAve += i;
    	    	    if (i > iMax) iMax = i;
    	    	    if (i < iMin) iMin = i;
    	    	    if (i >= HBextent) i = HBextent - 1;
    	    	    HB[i]++;
    	    	    nPix++;
    	    	}
    	    }
    	}
    }
    else if ((Prop->Image.Type == PROP_IMAGE_TYPE_BAYER) && (Prop->IB16))
    {
    	tU16 eoRow, eoCol;  	// even/odd rows and columns
    	tU16 *IB;
	IB = Prop->IB16;
     	for (iRow = 0; iRow < Rows; iRow++)
     	{
     	    eoRow = iRow & 0x1;
     	    for (iCol = 0; iCol < Cols; iCol++, IB++)
     	    {
     		eoCol = iCol & 0x1;
     		if (!eoCol && !(Color & BAYER_GRID_RxC0)) continue;
     		if ( eoCol && !(Color & BAYER_GRID_RxC1)) continue;
     		if (!eoRow && !(Color & BAYER_GRID_R0Cx)) continue;
     		if ( eoRow && !(Color & BAYER_GRID_R1Cx)) continue;
     		
     		if (iCol < Left) continue;
     		if (iCol > Right) continue;
     		if (iRow < Top) continue;
     		if (iRow > Bottom) continue;
     		
     		i = *IB;
     		MeanAve += i;
     		if (i > iMax) iMax = i;
     		if (i < iMin) iMin = i;
     		if (i >= HBextent) i = HBextent - 1;
     		HB[i]++;
     		nPix++;
     	    }
     	}
    }
    else
    {
    	fprintf(stderr, "unknown image type in MakeHistogram()\n");
	return(1);
    }

    Hist->nPixels = nPix;
    Hist->Min  = iMin;
    Hist->Max  = iMax;
    Hist->Mean = MeanAve / nPix;
    return(0);
} /* end MakeHistogram() */

/***************************************************************************
* Function: 	WriteTiff()
* Returns:  	0 if successful
* Description:	Creates a 8 or 16 bit baseline Tiff of RGB or BAYER image
*   	    	defined by *Prop.  The RAW image is scaled to the 'mean'
*   	    	histogram intensity.
***************************************************************************/
int 	WriteTiff(
tPROP	*Prop, 	    	// image properties
tU16	TiffSize,   	// 8 or 16 bit Tiff
FILE	*ofp)	    	// output file pointer
{
    char head[] =
    {
    	/* signature */
	'I','I',    	    	    	    	/*  00 signature */
	42,0,	    	    	    	    	/*  02 magic number */
	8,0,0,0,	    	    	    	/*  04 offset to 1st IFD */
	
	12, 0,	    	    	    	    	/*  08 number IFDs=12 */
     /* tag(2), type(2), count(4), ValOfst(4) */
    	0,1,    4,0,     1,0,0,0,  0,0,0,0,     /*> 12 ImageWidth */
    	1,1,    4,0,     1,0,0,0,  0,0,0,0,     /*> 1e ImageHeight */
    	2,1,    3,0,     3,0,0,0,  0x9e,0,0,0,  /*  2a BitsPerSample */
    	3,1,    3,0,     1,0,0,0,  1,0,0,0,     /*  36 Compression */
    	6,1,    3,0,     1,0,0,0,  2,0,0,0,     /*  42 Photometric */
    	17,1,   4,0,     1,0,0,0,  0xb4,0,0,0,  /*  4e StripOffsets */
    	21,1,   3,0,     1,0,0,0,  3,0,0,0,     /*  5a SamplesPerPixel */
	22,1,   4,0,     1,0,0,0,  0,0,0,0,     /*> 66 RowsPerStrip */
	23,1,   4,0,     1,0,0,0,  0,0,0,0,     /*> 72 StripByteCounts*/
	26,1,   5,0,     1,0,0,0,  0xa4,0,0,0,  /*  7e xResolution*/
	27,1,   5,0,     1,0,0,0,  0xac,0,0,0,  /*  8a yResolution*/
	40,1,   3,0,     1,0,0,0,  1,0,0,0,     /*  96 ResolutionUnit*/
	0,0,0,0,     	    	    	    	/*  9a IFD terminator */
	
	16,0,16,0,16,0,	    	    	    	/*  9e data for BitsPerSample */
	1,0,0,0,1,0,0,0,    	    	    	/*  a4 data for xResolution */
	1,0,0,0,1,0,0,0    	    	    	/*  ac data for yResolution */
	    	    	    	    	    	/*  b4 start of image */
    };
#define OFST_IMAGE_WIDTH    	(0x12)
#define OFST_IMAGE_HEIGHT   	(0x1e)
#define OFST_ROWS_PER_STRIP 	(0x66)
#define OFST_STRIP_BYTE_COUNTS	(0x72)
#define OFST_BITS_PER_SAMPLE	(0x9e)
#define OFST_START_OF_IMAGE 	(0xb4)
    
    if ((Prop->Image.Precision > 16) || (Prop->Image.Precision < 8))
    {
    	fprintf(stderr, "Invalid precision (%d) for WriteTiff()\n",
	    	Prop->Image.Precision);
	return(1);
    }
    if (TiffSize == 8)
    {
    	head[OFST_BITS_PER_SAMPLE]     = 
	head[OFST_BITS_PER_SAMPLE + 2] = 
	head[OFST_BITS_PER_SAMPLE + 4] = 8;
    }
    
    tHIST Hist;
    tU32 HBextent;
    tU32 *HB;
    tU32 nPix;
    tU16 Min, Max;
    tU16 Rmean, Gmean, Bmean;
    tU32 Rmax,  Gmax,  Bmax;
    tU32 Rscale, Gscale, Bscale;
    tU16 Rcolor, Gcolor, Bcolor;
    int  Ret;
    HBextent = 1 << Prop->Image.Precision;
    HB = (tU32 *) calloc(HBextent, sizeof(*HB));
    if (!HB)
    {
    	fprintf(stderr, "can't calloc in RgbWriteTiff()\n");
	exit(1);
    }
    Hist.HB = HB;
    Hist.HBextent = HBextent;
    
    if (Prop->Image.Type == PROP_IMAGE_TYPE_BAYER)
    {
    	Rcolor = BAYER_GRID_RED;
	Gcolor = BAYER_GRID_GREEN;
	Bcolor = BAYER_GRID_BLUE;
    }
    else
    { // must be PROP_IMAGE_TYPE_RGB
    	Rcolor = RGB_COLOR_RED;
	Gcolor = RGB_COLOR_GREEN;
	Bcolor = RGB_COLOR_BLUE;
    }
    Ret  = MakeHistogram(Prop, Rcolor, &Hist);
    Rmean = Hist.Mean;
    Ret |= MakeHistogram(Prop, Gcolor, &Hist);
    Gmean = Hist.Mean;
    Ret |= MakeHistogram(Prop, Bcolor, &Hist);
    Bmean = Hist.Mean;
    free(HB);
    if (Ret) return(1);

    Rmax   = 2 * Rmean;
    Gmax   = 2 * Gmean;
    Bmax   = 2 * Bmean;
    Rscale = 0xFAE147AE / Rmax; // Scale maps max to 98% of range
    Gscale = 0xFAE147AE / Gmax; // Scale maps max to 98% of range
    Bscale = 0xFAE147AE / Bmax; // Scale maps max to 98% of range

    long iWidth, iHeight, RowsPerStrip, Pixels, ByteCount;
    iWidth  = Prop->Image.Size.Width ;
    iHeight = Prop->Image.Size.Height;
    if (Prop->Image.Type == PROP_IMAGE_TYPE_BAYER)
    {
    	iWidth  -= Prop->Trim.Left + Prop->Trim.Right;
    	iWidth  /= 2;
	iHeight -= Prop->Trim.Top + Prop->Trim.Bottom;
	iHeight /= 2;
    }
    RowsPerStrip = iHeight;
    Pixels = iWidth * iHeight;
    ByteCount = 3 * Pixels * ((TiffSize == 8) ? sizeof(tU8) : sizeof(tU16));
    *((long *) &head[OFST_IMAGE_WIDTH]) = iWidth;
    *((long *) &head[OFST_IMAGE_HEIGHT]) = iHeight;
    *((long *) &head[OFST_ROWS_PER_STRIP]) = RowsPerStrip;
    *((long *) &head[OFST_STRIP_BYTE_COUNTS]) = ByteCount;
    fwrite(head, sizeof(head), sizeof(*head), ofp);

    if ((Prop->Image.Type == PROP_IMAGE_TYPE_RGB) && (Prop->IB8) && (TiffSize == 8))
    {
    	tU8 *RGB, *pRGB;
	tU8 *IB;
	IB = Prop->IB8;
    	RGB = pRGB = (tU8 *) calloc (Pixels, 3 * sizeof(tU8));
    	if (!RGB)
    	{
    	    fprintf(stderr, "can't calloc in WriteTiff()\n");
    	    exit(1);
    	}
    	
    	tU32 Row, Rows, Col, Cols, RC, Idx;
    	tU32 R, G, B;
    	Cols = Prop->Image.Size.Width;
    	Rows = Prop->Image.Size.Height;
    	for (Row = 0; Row < Rows; Row++)
    	{
    	    RC = Row * Cols;
    	    for (Col = 0; Col < Prop->Image.Size.Width; Col++)
    	    {
    	    	Idx = 3 * (RC + Col);
    		R = IB[Idx];
    		G = IB[Idx + 1];
    		B = IB[Idx + 2];
    	    	
    	    	if (R > Rmax) R = Rmax;
    	    	if (G > Gmax) G = Gmax;
    	    	if (B > Bmax) B = Bmax;
    	    	
    		*pRGB++ = (R * Rscale) >> 24;
    		*pRGB++ = (G * Gscale) >> 24;
    		*pRGB++ = (B * Bscale) >> 24;
    	    }
    	}

    	fwrite(RGB, Pixels, 3 * sizeof(*RGB), ofp);
    	free(RGB);
    }
    else if ((Prop->Image.Type == PROP_IMAGE_TYPE_RGB) && (Prop->IB8) && (TiffSize == 16))
    {
    	tU16 *RGB, *pRGB;
	tU8 *IB;
	IB = Prop->IB8;
    	RGB = pRGB = (tU16 *) calloc (Pixels, 3 * sizeof(tU16));
    	if (!RGB)
    	{
    	    fprintf(stderr, "can't calloc in WriteTiff()\n");
    	    exit(1);
    	}
    	
    	tU32 Row, Rows, Col, Cols, RC, Idx;
    	tU32 R, G, B;
    	Cols = Prop->Image.Size.Width;
    	Rows = Prop->Image.Size.Height;
    	for (Row = 0; Row < Rows; Row++)
    	{
    	    RC = Row * Cols;
    	    for (Col = 0; Col < Prop->Image.Size.Width; Col++)
    	    {
    	    	Idx = 3 * (RC + Col);
    		R = IB[Idx];
    		G = IB[Idx + 1];
    		B = IB[Idx + 2];
    	    	
    	    	if (R > Rmax) R = Rmax;
    	    	if (G > Gmax) G = Gmax;
    	    	if (B > Bmax) B = Bmax;
    	    	
    		*pRGB++ = (R * Rscale) >> 16;
    		*pRGB++ = (G * Gscale) >> 16;
    		*pRGB++ = (B * Bscale) >> 16;
    	    }
    	}

    	fwrite(RGB, Pixels, 3 * sizeof(*RGB), ofp);
    	free(RGB);
    }
    else if ((Prop->Image.Type == PROP_IMAGE_TYPE_RGB) && (Prop->IB16) && (TiffSize == 8))
    {
    	tU8 *RGB, *pRGB;
	tU16 *IB;
	IB = Prop->IB16;
    	RGB = pRGB = (tU8 *) calloc (Pixels, 3 * sizeof(tU8));
    	if (!RGB)
    	{
    	    fprintf(stderr, "can't calloc in WriteTiff()\n");
    	    exit(1);
    	}
    	
    	tU32 Row, Rows, Col, Cols, RC, Idx;
    	tU32 R, G, B;
    	Cols = Prop->Image.Size.Width;
    	Rows = Prop->Image.Size.Height;
    	for (Row = 0; Row < Rows; Row++)
    	{
    	    RC = Row * Cols;
    	    for (Col = 0; Col < Prop->Image.Size.Width; Col++)
    	    {
    	    	Idx = 3 * (RC + Col);
    		R = IB[Idx];
    		G = IB[Idx + 1];
    		B = IB[Idx + 2];
    	    	
    	    	if (R > Rmax) R = Rmax;
    	    	if (G > Gmax) G = Gmax;
    	    	if (B > Bmax) B = Bmax;
    	    	
    		*pRGB++ = (R * Rscale) >> 24;
    		*pRGB++ = (G * Gscale) >> 24;
    		*pRGB++ = (B * Bscale) >> 24;
    	    }
    	}

    	fwrite(RGB, Pixels, 3 * sizeof(*RGB), ofp);
    	free(RGB);
    }
    else if ((Prop->Image.Type == PROP_IMAGE_TYPE_RGB) && (Prop->IB16) && (TiffSize == 16))
    {
    	tU16 *RGB, *pRGB;
	tU16 *IB;
	IB = Prop->IB16;
    	RGB = pRGB = (tU16 *) calloc (Pixels, 3 * sizeof(tU16));
    	if (!RGB)
    	{
    	    fprintf(stderr, "can't calloc in WriteTiff()\n");
    	    exit(1);
    	}
    	
    	tU32 Row, Rows, Col, Cols, RC, Idx;
    	tU32 R, G, B;
    	Cols = Prop->Image.Size.Width;
    	Rows = Prop->Image.Size.Height;
    	for (Row = 0; Row < Rows; Row++)
    	{
    	    RC = Row * Cols;
    	    for (Col = 0; Col < Prop->Image.Size.Width; Col++)
    	    {
    	    	Idx = 3 * (RC + Col);
    		R = IB[Idx];
    		G = IB[Idx + 1];
    		B = IB[Idx + 2];
    	    	
    	    	if (R > Rmax) R = Rmax;
    	    	if (G > Gmax) G = Gmax;
    	    	if (B > Bmax) B = Bmax;
    	    	
    		*pRGB++ = (R * Rscale) >> 16;
    		*pRGB++ = (G * Gscale) >> 16;
    		*pRGB++ = (B * Bscale) >> 16;
    	    }
    	}

    	fwrite(RGB, Pixels, 3 * sizeof(*RGB), ofp);
    	free(RGB);
    }
    else if ((Prop->Image.Type == PROP_IMAGE_TYPE_BAYER) && (TiffSize == 8))
    {
    	tU8 *RGB, *pRGB;
	tU16 *IB;
	IB = Prop->IB16;
    	RGB = pRGB = (tU8 *) calloc (Pixels, 3 * sizeof(tU8));
    	if (!RGB)
    	{
    	    fprintf(stderr, "can't calloc in WriteTiff()\n");
    	    exit(1);
    	}
    	
    	tU32 Row, Rows, Col, Cols, RC, Idx;
	tU32 TrimT, TrimB, TrimL, TrimR;
    	tU32 R, G, B;
    	Cols = Prop->Image.Size.Width;
    	Rows = Prop->Image.Size.Height;
	TrimL = Prop->Trim.Left;
	TrimR = Cols - Prop->Trim.Right;
	TrimT = Prop->Trim.Top;
	TrimB = Rows - Prop->Trim.Bottom;
    	for (Row = TrimT; Row < TrimB; Row += 2)
    	{
    	    //  Various BAYER Grids
    	    //      --------------------------------------------> Canon 5D
    	    //    0x16161616:	  0x61616161:	  0x49494949:	  0x94949494:
    	    //    0 1 2 3 4 5	  0 1 2 3 4 5	  0 1 2 3 4 5	  0 1 2 3 4 5
    	    //  0 B G B G B G	0 G R G R G R	0 G B G B G B	0 R G R G R G
    	    //  1 G R G R G R	1 B G B G B G	1 R G R G R G	1 G B G B G B
    	    //  2 B G B G B G	2 G R G R G R	2 G B G B G B	2 R G R G R G
    	    //  3 G R G R G R	3 B G B G B G	3 R G R G R G	3 G B G B G B
    	    RC = Row * Cols;
    	    for (Col = TrimL; Col < TrimR; Col += 2)
    	    {
    	    	Idx = RC + Col;
    		R = IB[Idx];
    		G = (IB[Idx + 1] / 2) + (IB[Idx + Cols] / 2);
    		B = IB[Idx + Cols + 1];
    	    	
    	    	if (R > Rmax) R = Rmax;
    	    	if (G > Gmax) G = Gmax;
    	    	if (B > Bmax) B = Bmax;
    	    	
    		*pRGB++ = (R * Rscale) >> 24;
    		*pRGB++ = (G * Gscale) >> 24;
    		*pRGB++ = (B * Bscale) >> 24;
    	    }
    	}

    	fwrite(RGB, Pixels, 3 * sizeof(*RGB), ofp);
    	free(RGB);
    }
    else if ((Prop->Image.Type == PROP_IMAGE_TYPE_BAYER) && (TiffSize == 16))
    {
    	tU16 *RGB, *pRGB;
	tU16 *IB;
	IB = Prop->IB16;
    	RGB = pRGB = (tU16 *) calloc (Pixels, 3 * sizeof(tU16));
    	if (!RGB)
    	{
    	    fprintf(stderr, "can't calloc in WriteTiff()\n");
    	    exit(1);
    	}
    	
    	tU32 Row, Rows, Col, Cols, RC, Idx;
	tU32 TrimT, TrimB, TrimL, TrimR;
    	tU32 R, G, B;
    	Cols = Prop->Image.Size.Width;
    	Rows = Prop->Image.Size.Height;
	TrimL = Prop->Trim.Left;
	TrimR = Cols - Prop->Trim.Right;
	TrimT = Prop->Trim.Top;
	TrimB = Rows - Prop->Trim.Bottom;
    	for (Row = TrimT; Row < TrimB; Row += 2)
    	{
    	    //  Various BAYER Grids
    	    //      --------------------------------------------> Canon 5D
    	    //    0x16161616:	  0x61616161:	  0x49494949:	  0x94949494:
    	    //    0 1 2 3 4 5	  0 1 2 3 4 5	  0 1 2 3 4 5	  0 1 2 3 4 5
    	    //  0 B G B G B G	0 G R G R G R	0 G B G B G B	0 R G R G R G
    	    //  1 G R G R G R	1 B G B G B G	1 R G R G R G	1 G B G B G B
    	    //  2 B G B G B G	2 G R G R G R	2 G B G B G B	2 R G R G R G
    	    //  3 G R G R G R	3 B G B G B G	3 R G R G R G	3 G B G B G B
    	    RC = Row * Cols;
    	    for (Col = TrimL; Col < TrimR; Col += 2)
    	    {
    	    	Idx = RC + Col;
    		R = IB[Idx];
    		G = (IB[Idx + 1] / 2) + (IB[Idx + Cols] / 2);
    		B = IB[Idx + Cols + 1];
    	    	
    	    	if (R > Rmax) R = Rmax;
    	    	if (G > Gmax) G = Gmax;
    	    	if (B > Bmax) B = Bmax;
    	    	
    		*pRGB++ = (R * Rscale) >> 16;
    		*pRGB++ = (G * Gscale) >> 16;
    		*pRGB++ = (B * Bscale) >> 16;
    	    }
    	}

    	fwrite(RGB, Pixels, 3 * sizeof(*RGB), ofp);
    	free(RGB);
    }
    else
    {
    	fprintf(stderr, "unknown image type in WriteTiff()\n");
	return(1);
    }
    
    return(0);
} /* end WriteTiff() */
