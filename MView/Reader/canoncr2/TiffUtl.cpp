/***************************************************************************
* File: 	TiffUtl.cpp
* Functions:
*   	    	TiffFirstIfd()
*   	    	TiffGetTagData()
*   	    	TiffGetXmpItem()
*       	TiffParseXMP()
*       	TiffParseMakerNote()
*       	TiffParseIFD()
* Change Info:
*   03/14/07	HLS - Began.
* Description:	Various Tiff photo image utilities.
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Prop.h"
#include "getput.h"
#include "MakerNote.h"
#include "TiffTags.h"

//#define DEBUG_PRINT

// Various Constants
#define FALSE	    	(0)
#define TRUE	    	(1)

// typedefs
#ifndef STD_TYPES
#define STD_TYPES
typedef char	    	tI8;
typedef unsigned char 	tU8;
typedef short	    	tI16;
typedef unsigned short	tU16;
typedef long	    	tI32;
typedef unsigned long	tU32;
#endif

typedef unsigned short * LPWORD;

// Global Variables

/***************************************************************************
* Function:	TiffFirstIfd()
* Returns:	Offset to 1st IFD, 0 if error
* Description:	Reads header of file to search for Tiff signature.  Sets up
*   	    	get() functions accordingly.  Returns 0 if TIFF signature
*   	    	not found, otherwise returns offset to 1st IFD.
***************************************************************************/
tU32	TiffFirstIfd(void)
{
    if (!TestIntelMotoFileSignature())
    { // oops, neither Intel or Moto signature
    	return(0);
    }
    
    tU16 TiffVersion;
    TiffVersion = GET2();
    if (TiffVersion != 0x2A)
    { // Tiff version s/b 42 = 0x002A
	Get2 = Get2F;
	Get4 = Get4F;
	Get8 = Get8F;
	Put2 = Put2F;
	Put4 = Put4F;
	Put8 = Put8F;
	return(0);
    }

    tU32 FirstIfdOfst;
    FirstIfdOfst = GET4();
    return(FirstIfdOfst);
} /* end TiffFirstIfd() */

/***************************************************************************
* Function: 	TiffGetTagData()
* Returns:  	0 if successful
* Description:	Gets the Tiff Tag data based on the TagType.  TagCnt and 
*   	    	DataCnt are both in units of TagType and not number of
*   	    	bytes (e.g. a single 16-bit word has a TagCnt of 1).
*   	    	Stored data will be truncated if DataCnt < TagCnt.  In a
*   	    	few cases, TIFF allows either SHORTs or LONGs, and for 
*   	    	these types DataSizeOf is used to properly cast pData.
***************************************************************************/
int	TiffGetTagData(
tU16	TagType,    // Tag type - e.g. BYTE, SHORT, etc.
tU32	TagCnt,     // Tag's data count
tU32	iTagOfst,   // index to Tag's value/ofst
tU32	DataSizeOf, // sizeof a single quantity of pData - e.g. 2 for tU16
tU32	DataCnt,    // data storage space count
void	*pData)     // place to store data
{
    switch (TagType)
    {
	case TIF_BYTE:
	case TIF_SBYTE:
	case TIF_UNDEFINED:
	case TIF_ASCII:
	if (TagCnt > 4)
	{
    	    fseek(ifp, iTagOfst, SEEK_SET);
    	    iTagOfst = GET4();
    	}
    	fseek(ifp, iTagOfst, SEEK_SET);
	if (TagCnt > DataCnt) TagCnt = DataCnt;
	fread(pData, 1, TagCnt, ifp);
	return(0);
	
	case TIF_SHORT:
	case TIF_SSHORT:
	if (TagCnt > 2)
	{
    	    fseek(ifp, iTagOfst, SEEK_SET);
    	    iTagOfst = GET4();
    	}
    	fseek(ifp, iTagOfst, SEEK_SET);
	if (TagCnt > DataCnt) TagCnt = DataCnt;
	while(TagCnt--)
	{
    	    if (DataSizeOf == sizeof(tU16))
	    {
    	    	*((tU16 *) pData) = GET2();
                pData = (void *)(unsigned(pData) + sizeof(tU16));
	    }
	    else
	    {
    	    	*((tU32 *) pData) = GET2();
                pData = (void *)(unsigned(pData) + sizeof(tU32));
	    }
	}
	return(0);
	
	case TIF_LONG:
	case TIF_SLONG:
	case TIF_FLOAT:
	if (TagCnt > 1)
	{
    	    fseek(ifp, iTagOfst, SEEK_SET);
    	    iTagOfst = GET4();
    	}
    	fseek(ifp, iTagOfst, SEEK_SET);
	if (TagCnt > DataCnt) TagCnt = DataCnt;
	while(TagCnt--)
	{
    	    if (DataSizeOf == sizeof(tU16))
	    {
    	    	*((tU16 *) pData) = GET4();
                pData = (void *)(unsigned(pData) + sizeof(tU16));
	    }
	    else
	    {
    	    	*((tU32 *) pData) = GET4();
                pData = (void *)(unsigned(pData) + sizeof(tU32));
	    }
	}
	return(0);

	case TIF_RATIONAL:  // save as double
    	fseek(ifp, iTagOfst, SEEK_SET);
    	iTagOfst = GET4();
    	fseek(ifp, iTagOfst, SEEK_SET);
	if (TagCnt > DataCnt) TagCnt = DataCnt;
	while(TagCnt--)
	{
	    double Val;
	    tU32   d2;
	    Val = GET4();
	    d2  = GET4();
	    if (d2 == 0) return(1);
	    *((double *) pData) = Val / d2;
        pData = (void *)(unsigned(pData) + sizeof(double));
	}
	return(0);
	
	case TIF_SRATIONAL: // save as double
    	fseek(ifp, iTagOfst, SEEK_SET);
    	iTagOfst = GET4();
    	fseek(ifp, iTagOfst, SEEK_SET);
	if (TagCnt > DataCnt) TagCnt = DataCnt;
	while(TagCnt--)
	{
	    double Val;
	    tI32   d2;
	    Val = (tI32) GET4();
	    d2  = (tI32) GET4();
	    if (d2 == 0) return(1);
	    *((double *) pData) = Val / d2;
        pData = (void *)(unsigned(pData) + sizeof(double));
    }
	return(0);
	
	case TIF_DOUBLE:
    	fseek(ifp, iTagOfst, SEEK_SET);
    	iTagOfst = GET4();
    	fseek(ifp, iTagOfst, SEEK_SET);
	if (TagCnt > DataCnt) TagCnt = DataCnt;
	while(TagCnt--)
	{
	    *((tU32 *) pData) = GET4();
        pData = (void *)(unsigned(pData) + sizeof(tU32));
	}
	return(0);
	
	default:  // Oops, invalid
	return(1);
    }
} /* end TiffGetTagData() */

/***********************************************************************
* Function:	TiffGetXmpItem()
* Returns:	0 if item not found, 1 if item found
* Description:	Scans XMP for string ItemDesc.  If found the ItemData is
*   	    	retrieved from the XMP to a maximum size of ItemCnt-1,
*   	    	and there is always a terminating NULL of ItemData
*   	    	string.
* Note:     	ItemDesc must end with delimiter '>'.  The ItemData is
*   	    	the string after delimiter '>' and before the following
*   	    	delimiter '<'.
* Example:  	If the XMP contains "<tiff:Make>Canon</tiff:Make>" and
*   	    	ItemDesc = "<tiff:Make>", then ItemData returns "Canon".
************************************************************************/
int 	TiffGetXmpItem(
char 	*XMP,	    	// pointer to XMP ASCII data
char	*ItemDesc,  	// ASCII string of search item ending in '>'
char	*ItemData,  	// pointer to place to save data of item
int 	ItemCnt)    	// sizeof(ItemData) buffer
{
    char *Found;
    int  n;
    Found = strstr(XMP, ItemDesc);
    if (!Found) return(0);
    
    Found += strlen(ItemDesc);
    for (n = 0; ; n++)
    {
    	if (Found[n] == '<') break;
	if (Found[n] == NULL) return(0);
    }
    
    // validate that item is ABSOLUTELY FULLY delimited
    if (strncmp(Found + n + 2, ItemDesc + 1, strlen(ItemDesc + 1))) return(0);
    if (Found[n + 1] != '/') return(0);
    
    if (n > (ItemCnt - 1)) n = ItemCnt - 1;
    
    memset(ItemData, 0, ItemCnt);
    memcpy(ItemData, Found, n);
    
    return(1);
} /* end TiffGetXmpItem() */

/***********************************************************************
* Function:	TiffParseXMP()
* Returns:	0 if successful
* Description:	Reads the Tiff XMP IFD that stores various exposure
*   	    	data, saves in Prop.
************************************************************************/
int 	TiffParseXMP(
tU32	iTagOfst,	    // offset in file of start of MakerNote
tU32	TagCnt,
tPROP	*Prop)      	    // pointer to properties
{
    // if (TagCnt == 0) there is no XMP buffer
    // if (TagCnt <= 4) the XMP buffer is immediate, but otherwise useless!
    if (TagCnt <= 4) return(0);
    
    char *XMP;
    XMP = (char *) calloc(TagCnt, 1);
    if (!XMP)
    {
    	fprintf(stderr, "can't calloc in TiffParseXMP()\n");
    	exit(1);
    }
    
    int Ret = 0;
    fseek(ifp, iTagOfst, SEEK_SET);
    if (!fread(XMP, 1, TagCnt, ifp))
    {
    	Ret = 1;
	goto done;
    }

    TiffGetXmpItem(XMP, 
    	    "<aux:SerialNumber>", 
	    Prop->Txt.SerialNum,
	    sizeof(Prop->Txt.SerialNum));

    TiffGetXmpItem(XMP, 
    	    "<aux:Lens>", 
	    Prop->Txt.LensModel,
	    sizeof(Prop->Txt.LensModel)); 

    TiffGetXmpItem(XMP, 
    	    "<aux:Firmware>", 
	    Prop->Txt.FwVersion,
	    sizeof(Prop->Txt.FwVersion));
    
    done:
    free(XMP);
    return(Ret);
} /* end TiffParseXMP() */

/***************************************************************************
* Function: 	TiffParseMakerNote()
* Returns:  	0 if successful
* Description:	Parses the Canon MakerNote segment within the TIFF file.
***************************************************************************/
int 	TiffParseMakerNote(
tU32	IfdOfst,	    // offset in file of start of MakerNote
tPROP	*Prop)      	    // pointer to properties
{
    if (strcmp(Prop->Txt.Make, "Canon"))
    { // unknown MakerNote
    	return(0);
    }
    
    tU32 TblOfst;
    tU16 nIfds;
    tU32 Save;
    tU16 TagID;
    tU32 TagCnt;
    tU32 iTagOfst;

    fseek (ifp, IfdOfst, SEEK_SET);
    nIfds = GET2(); 	    // number IFDs
    while (nIfds--)
    {
    	TagID    = GET2();   // get Tag ID
    	GET2();   // get Tag type, dispose
    	TagCnt   = GET4();   // get Tag's data count
    	iTagOfst = ftell(ifp);
	
	switch (TagID)
	{
	    case MN_CAMERA_FW_VERSION:
	    TiffGetTagData(TIF_ASCII, TagCnt, iTagOfst,
	    	    	   sizeof(*Prop->Txt.FwVersion),
			   sizeof(Prop->Txt.FwVersion),
			   &Prop->Txt.FwVersion);
	    break;
	    
	    case MN_CAMERA_SERIAL_NUM:
	    tU32 SerialNum;
	    TiffGetTagData(TIF_LONG, TagCnt, iTagOfst,
	    	    	   sizeof(SerialNum),
			   1,
			   &SerialNum);
    	    sprintf(Prop->Txt.SerialNum, "%d", SerialNum);
	    break;
	    
	    case MN_LENS_MODEL:
	    TiffGetTagData(TIF_ASCII, TagCnt, iTagOfst,
	    	    	   sizeof(*Prop->Txt.LensModel),
	    	    	   sizeof(Prop->Txt.LensModel),
			   &Prop->Txt.LensModel);
	    break;
	    
	}
    	fseek(ifp, iTagOfst + 4, SEEK_SET);
    }
    
    return(0);
} /* end TiffParseMakerNote() */

/***************************************************************************
* Function: 	TiffParseIFD()
* Returns:  	0 if successful
* Description:	Parses the IFD data segment within the TIFF file.
***************************************************************************/
int 	TiffParseIFD(
tU32	IfdOfst,    	    // offset in file of start of IFD
tPROP	*Prop)      	    // pointer to properties
{
    tU32 TblOfst;
    tU16 nIfds;
    tU32 Save;
    tU16 TagID;
    tU16 TagType;
    tU32 TagCnt;
    tU32 iTagOfst;
    int  Ret = 0;

    fseek (ifp, IfdOfst, SEEK_SET);
    nIfds = GET2(); 	    // number IFDs
    while (nIfds--)
    {
    	TagID    = GET2();   // get Tag ID
    	TagType  = GET2();   // get Tag type
    	TagCnt   = GET4();   // get Tag's data count
    	iTagOfst = ftell(ifp);

	switch (TagID)
	{
    	    case TIF_IMAGE_WIDTH:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.Size.Width),
			   1,
			   &Prop->Image.Size.Width);
	    break;
	    
    	    case TIF_IMAGE_LENGTH:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.Size.Height),
			   1,
			   &Prop->Image.Size.Height);
	    break;
	    
	    case TIF_BITS_PER_SAMPLE:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(*Prop->Image.BitsPerSample),
			   sizeof(Prop->Image.BitsPerSample) / sizeof(*Prop->Image.BitsPerSample),
			   &Prop->Image.BitsPerSample);
	    break;
	    
    	    case TIF_MAKE:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(*Prop->Txt.Make),
	    	    	   sizeof(Prop->Txt.Make),
			   &Prop->Txt.Make);
	    break;
	    
    	    case TIF_MODEL:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(*Prop->Txt.Model),
	    	    	   sizeof(Prop->Txt.Model),
			   &Prop->Txt.Model);
	    break;
	    
    	    case TIF_STRIP_OFFSETS:
	    Prop->Strip.nList = TagCnt;
	    if (Prop->Strip.OffsetList) free(Prop->Strip.OffsetList);
	    Prop->Strip.OffsetList = (tU32 *) calloc(TagCnt, sizeof(tU32));
	    if (!Prop->Strip.OffsetList)
	    {
	    	fprintf(stderr, "can't calloc in TiffParseIFD()\n");
		exit(1);
	    }
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(*Prop->Strip.OffsetList),
	    	    	   TagCnt,
			   Prop->Strip.OffsetList);
	    break;
	    
    	    case TIF_ORIENTATION:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.Orientation),
			   1,
			   &Prop->Image.Orientation);
	    break;
	    
    	    case TIF_ROWS_PER_STRIP:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Strip.RowsPer),
			   1,
			   &Prop->Strip.RowsPer);
	    break;
	    
    	    case TIF_STRIP_BYTE_COUNTS:
	    if (TagCnt != Prop->Strip.nList)
	    {
	    	fprintf(stderr, "conflict in # strip counts\n");
		Ret = 1;
		break;
	    }
	    Prop->Strip.OfstByteCounts = iTagOfst;
	    if (Prop->Strip.CountList) free(Prop->Strip.CountList);
	    Prop->Strip.CountList = (tU32 *) calloc(TagCnt, sizeof(tU32));
	    if (!Prop->Strip.CountList)
	    {
	    	fprintf(stderr, "can't calloc in TiffParseIFD()\n");
		exit(1);
	    }
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(*Prop->Strip.CountList),
	    	    	   TagCnt,
			   Prop->Strip.CountList);
	    break;

    	    case TIF_DATE_TIME:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(*Prop->Txt.DateTime),
	    	    	   sizeof(Prop->Txt.DateTime),
			   &Prop->Txt.DateTime);
	    break;
	    
    	    case TIF_EXIF:  // EXIF subdirectory
    	    Ret = TiffParseIFD(GET4(), Prop);
	    break;
	    
    	    case TIF_EXPOSURE_TIME:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.ExposureTime),
			   1,
			   &Prop->Image.ExposureTime);
	    break;
	    
    	    case TIF_FNUMBER:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.FNumber),
			   1,
			   &Prop->Image.FNumber);
	    break;
	    
    	    case TIF_ISO_SPEED_RATINGS:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.ISOSpeedRatings),
			   1,
			   &Prop->Image.ISOSpeedRatings);
	    break;
	    
    	    case TIF_FOCAL_LENGTH:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.FocalLength),
			   1,
			   &Prop->Image.FocalLength);
	    break;
	    
    	    case TIF_XMP:  // Parse TIFF xmp-meta data
	    Ret = TiffParseXMP(GET4(), TagCnt, Prop);
	    break;
	    
    	    case TIF_MAKER_NOTE:  // Parse TIFF MakerNote for Canon
	    Ret = TiffParseMakerNote(GET4(), Prop);
	    break;
	    
    	    case TIF_FOCAL_PLANE_X_RESOLUTION:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.FocalPlaneXResolution),
			   1,
			   &Prop->Image.FocalPlaneXResolution);
	    break;
	    
    	    case TIF_FOCAL_PLANE_Y_RESOLUTION:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.FocalPlaneYResolution),
			   1,
			   &Prop->Image.FocalPlaneYResolution);
	    break;
	    
    	    case TIF_FOCAL_PLANE_RESOLUTION_UNIT:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.FocalPlaneResolutionUnit),
			   1,
			   &Prop->Image.FocalPlaneResolutionUnit);
	    break;
	    
    	    case TIF_PIXEL_X_DIMENSION:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.FinishedSize.Width),
			   1,
			   &Prop->Image.FinishedSize.Width);
	    break;
	    
    	    case TIF_PIXEL_Y_DIMENSION:
	    Ret = TiffGetTagData(TagType, TagCnt, iTagOfst,
	    	    	   sizeof(Prop->Image.FinishedSize.Height),
			   1,
			   &Prop->Image.FinishedSize.Height);
	    break;
	    
	}
    	if (Ret)
	{
	    fprintf(stderr, "corrupt IFD, TagID=0x%04X\n", TagID);
	    return(1);
	}
    	fseek(ifp, iTagOfst + 4, SEEK_SET);
    }
    
    return(0);
} /* end TiffParseIFD() */


