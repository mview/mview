/***************************************************************************
* File: 	Cr2Codec.cpp
* Change Info:
*   11/10/08	HLS - New versoin to reflect NO runtime lib on compile.
*   09/21/08	HLS - Recompiled after fix of module getput.cpp.
*   03/30/07	HLS - Added Cr2ExtractJpg().
*   02/28/07	HLS - Initial version tested and done. Hot Stuff!!!
*   02/15/07	HLS - Began.
* Description:	Canon *.CR2 raw bit coder/decoder.
***************************************************************************/

/***************************************************************************
* The Canon CR2 file format is an encapsulated TIFF shell having 4 IFD sets.
* These IFDs are different versions of the same image.
*
*	  +=====================================+ Start of TIFF/CR2 file
*	  | TIFF Header 			|
*	  | Size = 8				|
*	  +=====================================+
*	  | Various TIFF Tags describing File	| IFD #1 Segment
*	  |   EXIF (TIFF subdirectory)		| Canon 5D image size 2496x1664
*	  |- - - - - - - - - - - - - - - - - - -|
*	  | JPEG data (baseline compression)	|
*	  +=====================================+
*	  | JpegInterchangeFormat		| IFD #2 Segment
*	  |					| unknown image size
*	  |- - - - - - - - - - - - - - - - - - -|
*	  | JPEG Compressed data		|
*	  +=====================================+
*	  | Few TIFF Tags describing segment	| IFD #3 Segment
*	  |					| Canon 5D image size 384x256
*	  |- - - - - - - - - - - - - - - - - - -|
*	  | JPEG data (unknown compression)	|
*	  +=====================================+
*	  | Few TIFF Tags describing segment	| IFD #4 Segment - RAW image
*	  |					| Canon 5D image size 4476x2954
*	  |- - - - - - - - - - - - - - - - - - -|
*	  | JPEG data (lossless compression)	|
*	  +=====================================+
*
* A sample parsing of these IFDs are:
* sample.cr2: FileID=II, Ver=2A, IFD #1 w/ 14 Tags
*  TagName(TagID,Len,DataOfst) =Value
*  ImageWidth(0100,1,001A)     =2496
*  ImageLength(0101,1,0026)    =1664
*  BitsPerSample(0102,3,00BE)  =8 8 8
*  Compression(0103,1,003E)    =6  {JpegCompression}
*  Make(010F,6,00C4)	       ="Canon"
*  Model(0110,D,00CA)	       ="Canon EOS 5D"
*  StripOffsets(0111,1,0062)   =82349
*  Orientation(0112,1,006E)    =1  {TopLeft, Normal}
*  StripByteCounts(0117,1,007A)=1030590
*  XResolution(011A,1,00EA)    =72/1 (=72)
*  YResolution(011B,1,00F2)    =72/1 (=72)
*  ResolutionUnit(0128,1,009E) =2  {InchUnits}
*  DateTime(0132,14,00FA)      ="2006:09:24 07:14:52"
*  Exif(8769,1,00B6)	       =270
*   TagName(TagID,Len,DataOfst) 	 =Value
*   ExposureTime(829A,1,0264)		 =1/80 (=0.0125)
*   FNumber(829D,1,026C)		 =4/1 (=4)
*   ExposureProgram(8822,1,0130)	 =3
*   ISOSpeedRatings(8827,1,013C)	 =100
*   ExifVersion(9000,4,0148)		 =48 50 50 49
*   DateTimeOriginal(9003,14,0274)	 ="2006:09:24 07:14:52"
*   DateTimeDigitized(9004,14,0288)	 ="2006:09:24 07:14:52"
*   ComponentsConfiguration(9101,4,016C) =1 2 3 0
*   ShutterSpeedValue(9201,1,029C)	 =417792/65536 (=6.375)
*   ApertureValue(9202,1,02A4)		 =262144/65536 (=4)
*   ExposureBiasValue(9204,1,02AC)	 =0/1 (=0)
*   MeteringMode(9207,1,019C)		 =5
*   Flash(9209,1,01A8)			 =16
*   FocalLength(920A,1,02B4)		 =24/1 (=24)
*   MakerNote(927C,12548,02BC)		 =29 0 1 0 3 0 46 0 0 0 30 4 0 0 2 ...
*   UserComment(9286,108,12804) 	 =0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ...
*   FlashpixVersion(A000,4,01D8)	 =48 49 48 48
*   ColorSpace(A001,1,01E4)		 =1
*   PixelXDimension(A002,1,01F0)	 =4368
*   PixelYDimension(A003,1,01FC)	 =2912
*   Interoperability(A005,1,0208)	 =76044
*   FocalPlaneXResolution(A20E,1,1292A)  =4368000/1415 (=3086.93)
*   FocalPlaneYResolution(A20F,1,12932)  =2912000/942 (=3091.3)
*   FocalPlaneResolutionUnit(A210,1,022C)=2  {InchUnits}
*   CustomRendered(A401,1,0238) 	 =0
*   ExposureMode(A402,1,0244)		 =0
*   WhiteBalance(A403,1,0250)		 =0
*   SceneCaptureType(A406,1,025C)	 =0
* sample.cr2: FileID=II, Ver=2A, IFD #2 w/ 2 Tags
*  TagName(TagID,Len,DataOfst)	      =Value
*  JPEGInterchangeFmt(0201,1,12944)   =76348
*  JPEGInterchangeFmtLen(0202,1,12950)=6001
* sample.cr2: FileID=II, Ver=2A, IFD #3 w/ 11 Tags
*  TagName(TagID,Len,DataOfst)	=Value
*  ImageWidth(0100,1,12962)	=384
*  ImageLength(0101,1,1296E)	=256
*  BitsPerSample(0102,3,129E2)	=8 8 8
*  Compression(0103,1,12986)	=6  {JpegCompression}
*  Photometric(0106,1,12992)	=2  {RGB}
*  StripOffsets(0111,1,1299E)	=1112939
*  SamplesPerPixel(0115,1,129AA)=3
*  RowsPerStrip(0116,1,129B6)	=256
*  StripByteCounts(0117,1,129C2)=294912
*  PlanarConfig(011C,1,129CE)	=1  {ChunkyFormat}
*  UndefinedTag(C5D9,1,129DA)	=2
* sample.cr2: FileID=II, Ver=2A, IFD #4 w/ 6 Tags
*  TagName(TagID,Len,DataOfst)	=Value
*  Compression(0103,1,129F2)	=6  {JpegCompression}
*  StripOffsets(0111,1,129FE)	=1407851
*  StripByteCounts(0117,1,12A0A)=9920742
*  UndefinedTag(C5D8,1,12A16)	=1
*  UndefinedTag(C5E0,1,12A22)	=1
*  UndefinedTag(C640,3,12A36)	=1 2238 2238
*
* The first IFD is a low pixel count JPEG image, and for the Canon 5D its image
* size is 2496x1664.  Also included as part of the first IFD are an assortment
* of image and shot data, such as Make, Model, and abundant EXIF information.
* The EXIF information is itself an IFD subdirectory, and within the EXIF
* directory is the Tag 'MakerNote' which contains more shot information such a
* lens type and serial numbers.  MakerNote is has a TIFF like IFD structure,
* but the "TagType" field follows different rules, see below.
*
* The second IFD is another JPEG rendered image, and it is described by two
* TIFF Tags.  It is not clear what the purpose of this image, but noting that
* the section size is very small (6001 bytes in the sample), then the image
* dimensions must also be small.  It is likely a thumbnail.  Peeking into this
* section one finds four JPEG markers:	SOI (start of image), DHT (define
* Huffmann tables), DQT (define quantization tables), and SOS (start of scan).
* There is no SOFn marker to identify the image dimensions.
*
* The third IFD is another JPEG rendered image.  In this case there are several
* TIFF tags, and these give the size of the image as 384x256.  Certainly a
* small image.	Although the compression is tagged as JPEG, it is believed this
* is not the case since (1) the image data contains no JPEG markers and (2) the
* size of the image data is exactly three times the rows times columns (294912
* = 3x384x256).  Thus, it is believed the image data is uncompressed RGB.
*
* The forth IFD is the sweet spot of the CR2 file.  It is a lossless JPEG
* compressed image of dimensions the size of the camera's photo sensor.  Canon
* adds three proprietary TIFF tags which are of unknown meaning, except the Tag
* 0xC640 with values for the sample file {1, 2238, 2238} are described by Dave
* Coffin's as being 'slice' information. Peeking into the JPEG code of this
* section one finds the following markers:  SOI (start of image), DHT (define 2
* Huffmann tables), SOF3 (start of frame for lossless, sequential, non-
* differential, Huffman coding), and SOS (start of scan).  There are no RSTm
* (reset modulo-8) markers.  The section is located at the end of the file, and
* it is this location which makes it easy to replace a 'valued added' version
* of the image with the original.
*
* Decoding lossless JPEG is similar (and simpler) to decoding CRW images.  Like
* CRW images, the compressed data is organized as a concatenation of a Huffmann
* code (i.e. HufCode or codeword) followed by a variable length difference code
* (i.e. Diff) bit stream as shown below.  Unlike CRW images the HufCode only
* conveys the number of bits required by Diff.
*
*	  +----------------+-------------+
*     ... | HufCode[nBits] | Diff[nBits] | ...
*	  +----------------+-------------+
*
* Like CRW images, the compressed data is row organized with interleaved BAYER
* grid array data for that row.  So with a BAYER grid of RG/GB, the even rows
* has interleaved HuffCode/Diff data for ...RGRGRG..., while the odd rows it is
* ...GBGBGB...
*
* Unlike CRW images, there are no 64 pixel blocks, rather it is the width of a
* row.	The initial values at the beginning of each row is the RG/GB value of
* its nearest previous row beginning.  For the first row, the initial row
* values are 1/2 the bit range defined by the precision.  Thus for 12-bit
* precision:
*     Pix[Row, Col] = Val
*     Pix[0,0] = (1 << (Precision - 1)) + Diff
*     Pix[0,1] = (1 << (Precision - 1)) + Diff
* and for n >= 1
*     Pix[n,0] = Pix[n-2,0] + Diff
*     Pix[n,1] = Pix[n-2,1] + Diff
* while for any other Row/Column
*     Pix[R,C] = Pix[R,C-2] + Diff
*
* Also unlike CRW images the mapping of decoded rows into the image buffer is
* divided into four quadrants.	It is unclear why Canon chose this method, but
* nonetheless it's there.  The RAW image is divided into 4 equal sized
* quandrants:
*     +------------------------+------------------------+ RAW Image
*     | 		       |			|
*     |        Quad 0	       |	Quad 1		|
*     | 		       |			|
*     +------------------------+------------------------+
*     | 		       |			|
*     |        Quad 2	       |	Quad 3		|
*     | 		       |			|
*     +------------------------+------------------------+
* The finished image inteleaves quadrants row segments 0/1 and interleaves
* quandrant rows segments 2/3 while combined quandrants 0/1 is located to the
* left and combined quadrants 2/3 are concatenated to the right:
*     +------------------------+------------------------+ Finished Image
*     |      Quad0.Row0        |      Quad2.Row0	|
*     |      Quad1.Row0        |      Quad3.Row0	|
*     |      Quad0.Row1        |      Quad2.Row1	|
*     |      Quad1.Row1        |      Quad3.Row1	|
*     | 	 etc	       |	  etc		|
*     |      Quad0.RowN        |      Quad2.RowN	|
*     |      Quad1.RowN        |      Quad3.RowN	|
*     +------------------------+------------------------+
* So it can be said that Quads 0 and 2 contain the same two BAYER grid colors
* and Quads 1 and 3 contain the other two BAYER grid colors.
*
* Like CRW images, the actual difference value contained in the Diff[nBits] is
* organized loosely as a signed magnitude number, but has its own specific
* rules.  First, it should be noted that the value of nBits determines the
* range of the number.	As an example if nBits = 3, then the magnitude (of a
* DiffVal that can be +/- this magnitude) is 4 <= magnitude <= 7, or more
* specifically:
*
*      (1 << (nBits - 1)) <= DiffVal <=  ((1 << nBits) - 1)    // + DiffVal
*     -(1 << (nBits - 1)) >= DiffVal >= -((1 << nBits) - 1)    // - DiffVal
*
* If the most significant bit (MSB) of Diff[nBits] is one, then DiffVal is
* positive and has a magnitude directly represented by binary value of these
* bits.  If however, the MSB is zero, then DiffVal is negative and has a
* magnitude that is the compliment of these bits.  As an example if
* Diff[nBits=3] = 101, then DiffVal is 5, whereas if Diff[nBits=3] = 001, then
* DiffVal is -6.
*
* The size of the decompressed image is slightly larger than the final image
* size.   A few extra 'black' rows added on top and bottom and a few extra
* 'black' columns may be added to the left and right (but none have been
* observed).  These 'black' rows/columns along with a few additional RG/GB
* rows/columns make up a set of Trim rows/columns which are ultimately deleted
* to get the final 'as advertised' RAW image size.  Other than noting that some
* of the Trim is 'black,' it is beyond the scope of this description to
* determine the method to find the optimum Trim rows/columns.  As an
* observation, customized Trim may (or it may not) be encoded in the Firmware
* of camera so that that the final calibration of the image sensor reflects
* this Trim in a manner that places the center of the image sensor at the lens
* center.
*
* TIFF MakerNote:
*
* The TIFF Tag called MakerNote may be used by a manufacturer to embed any type
* of information.  Canon uses this segment.  Parsing of this proprietary
* segment follows the following data structures:
*
* typedef struct
* {
*     tU16  TagID;
*     tU16  TagType;
*     tU32  TagCnt;
*     tU32  DataOfst;
* } tREC;
*
* typedef struct
* {
*     tU16  numRecords;
*     tREC  RecordDir[numRecords];
*     tU8   Heap[];
* } tMAKERNOTE;
*
* The ordering of bytes is 'little endian' (Intel), but this is probably a
* function of the overall TIFF byte ordering.  The first two bytes identifies
* the number of records (numRecords), and this is followed by the record
* directory (RecordDir), and this if followed by the Heap of various data.
* Each record directory consists of 12 bytes with 4 fields.  The first field is
* the tag identification (TagID), followed by the tag type (TagType), followed
* by the tag count (TagCnt), and finally followed by data offset (DataOfst) or
* if the TagType is 4 then this field is immediate data.  This format closely,
* but not exactly, follows standard TIFF.
*
* The TagType fields may have the following meaning:
* Type = 2:  ASCII data
* Type = 3:  tU16 data
* Type = 4:  immediate data
* Type = 7:  unknown
*
* The TagID fields may have the following meaning:
* TagType = 0x01: Camera settings 1
* TagType = 0x04: Camera settings 2
* TagType = 0x06: Camera model (string)
* TagType = 0x07: Camera F/W version (string)
* TagType = 0x08: Image number ???
* TagType = 0x09: Owner name (string)
* TagType = 0x0C: Camera serial number (immediate data number)
* TagType = 0x0F: Custom functions
* TagType = 0x95: Lens model (string)
*
* The following is an sample extraction:
*
* 02BC: 1D 00  =NumRecord
*	-ID-- -Type  ---Cnt---	 --Ofst---    #
* 02BE: 01 00 03 00 2E 00 00 00 1E 04 00 00   1 - CameraSettings1
* 02CA: 02 00 03 00 04 00 00 00 7A 04 00 00   2
* 02D6: 03 00 03 00 04 00 00 00 82 04 00 00   3
* 02E2: 04 00 03 00 22 00 00 00 8A 04 00 00   4 - CameraSettings2
* 02EE: 06 00 02 00 0D 00 00 00 CE 04 00 00   5 - "Canon EOS 5D"
* 02FA: 07 00 02 00 18 00 00 00 EE 04 00 00   6 - "Firmware Version 1.1.0"
* 0306: 09 00 02 00 20 00 00 00 06 05 00 00   7 - OwnerName
* 0312: 0C 00 04 00 01 00 00 00 1F 15 CE 42   8 - CameraSerialNum = 0x42CE151F
* 031E: 0D 00 07 00 00 04 00 00 26 05 00 00   9
* 032A: 0F 00 03 00 17 00 00 00 76 09 00 00   a - CustomFunctions
* 0336: 10 00 04 00 01 00 00 00 13 02 00 80   b
* 0342: 12 00 03 00 28 00 00 00 26 09 00 00   c
* 034E: 13 00 03 00 04 00 00 00 A4 09 00 00   d
* 035A: 15 00 04 00 01 00 00 00 00 00 00 A0   e
* 0366: 19 00 03 00 01 00 00 00 01 00 00 00   f
* 0372: 83 00 04 00 01 00 00 00 00 00 00 00  10
* 037E: 93 00 03 00 10 00 00 00 AC 09 00 00  11
* 038A: 95 00 02 00 40 00 00 00 CC 09 00 00  12 = "EF24-105mm f/4L IS USM"
* 0396: 96 00 02 00 10 00 00 00 0C 0A 00 00  13
* 03A2: A0 00 03 00 0E 00 00 00 1C 0A 00 00  14
* 03AE: AA 00 03 00 05 00 00 00 38 0A 00 00  15
* 03BA: B4 00 03 00 01 00 00 00 01 00 00 00  16
* 03C6: E0 00 03 00 11 00 00 00 42 0A 00 00  17
* 03D2: D0 00 04 00 01 00 00 00 00 00 00 00  18
* 03DE: 01 40 03 00 1C 03 00 00 64 0A 00 00  19
* 03EA: 02 40 03 00 66 2B 00 00 9C 10 00 00  1a
* 03F6: 05 40 07 00 88 C0 00 00 68 67 00 00  1b
* 0402: 08 40 03 00 03 00 00 00 F0 27 01 00  1c
* 040E: 09 40 03 00 03 00 00 00 F6 27 01 00  1d
*
* Heap
* 0410: 			   00 00 00 00 00 5C 00 	   ....\.
* 0420: 02 00 00 00 04 00 00 00 00 00 00 00 00 00 00 00  ................
* 0430: 07 00 00 00 01 00 00 00 00 00 00 00 FF 7F FF 7F  ................
* 0440: 03 00 02 00 00 00 03 00 FF FF 00 00 69 00 18 00  ............i...
* 0450: 01 00 80 00 20 01 00 00 00 00 00 00 00 00 FF FF  .... ...........
* 0460: FF FF FF FF 00 00 00 00 00 00 00 00 FF FF FF FF  ................
* 0470: 00 00 00 00 FF 7F FF FF FF FF 02 00 69 00 C9 05  ............i...
* 0480: D2 03 00 00 00 00 00 00 00 00 44 00 00 00 A0 00  ..........D.....
* 0490: E4 00 B4 00 D4 00 00 00 00 00 03 00 00 00 08 00  ................
* 04A0: 08 00 95 00 00 00 00 00 00 00 00 00 00 00 01 00  ................
* 04B0: 00 00 00 00 B4 00 D0 00 91 00 00 00 00 00 F8 00  ................
* 04C0: FF FF FF FF FF FF FF FF 00 00 00 00 00 00 43 61  ..............Ca
* 04D0: 6E 6F 6E 20 45 4F 53 20 35 44 00 00 00 00 00 00  non EOS 5D......
* 04E0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 46 69  ..............Fi
* 04F0: 72 6D 77 61 72 65 20 56 65 72 73 69 6F 6E 20 31  rmware Version 1
* 0500: 2E 31 2E 30 00 00 00 00 00 00 00 00 00 00 00 00  .1.0............
* 0510: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
***************************************************************************/

#define  VERSION    "Version 1.02"
#define  COPYRIGHT   \
"Wildtramper.com, Copyright (c), All rights reserved"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Prop.h"
#include "getput.h"
#include "TiffTags.h"
#include "jpeg.h"
#include "MakerNote.h"
#include "RawUtl.h"
#include "TiffUtl.h"
#include "Cr2Codec.h"

//#define DEBUG_PRINT

// Various Constants
#define FALSE	    	(0)
#define TRUE	    	(1)

// typedefs
#ifndef STD_TYPES
#define STD_TYPES
typedef char	    	tI8;
typedef unsigned char 	tU8;
typedef short	    	tI16;
typedef unsigned short	tU16;
typedef long	    	tI32;
typedef unsigned long	tU32;
#endif

// Global Variables

#if 1
/***************************************************************************
* Function: 	main()
* Returns:  	0 if successful
* Description:	The main entry point.
***************************************************************************/
int 	main(
int 	argc,
char	**argv)
{
    int  Arg, Args;
    int  Recode = 0;
    int  Binary = 0;
    int  Histogram = 0;
    int  Tiff = 0;
    int  JpegImage = 0;
    int  ArgsRequired = 2;  // assume
    
    Args = argc;
    for (Arg = 1; Arg < Args; Arg++)
    {
	char	c;

	c = *argv[Arg];
	if (c != '-') break;
	for (argv[Arg]++; (c = *(argv[Arg]++)) != NULL; )
	{
	    switch (c)
	    {
		case 'b': Binary = 1; break;
		case 'c': Recode = 1; break;
		case 'h': Histogram = 1; ArgsRequired = 1; break;
		case 'j': JpegImage = 1; break;
		case 't': Tiff = 8; break;
		case 'T': Tiff = 16; break;
		
		default:
		fprintf(stderr, "Unknown option %c\n", c);
		return(1);
	    }
	}
    }
    if (!Recode && !Tiff && !Histogram) Binary = 1;	// default
    
    if ((Args - Arg) < ArgsRequired)
    {
    	fprintf(stderr, "Usage:  %s [options] file.cr2 outfile\n",argv[0]);
    	fprintf(stderr, "Decompress raw image data of file.cr2\n");
	fprintf(stderr, "  -b: decode then save binary file (default)\n");
	fprintf(stderr, "  -c: decode then recode file\n");
	fprintf(stderr, "  -h: decode then create (text) histogram\n");
	fprintf(stderr, "  -j: extract JPG image\n");
	fprintf(stderr, "  -t: decode then save as 8-bit TIFF file\n");
	fprintf(stderr, "  -T: decode then save as 16-bit TIFF file\n");
	fprintf(stderr, "%s\n", VERSION);
	fprintf(stderr, "%s\n", COPYRIGHT);
	return(0);
    }
    

    char *InFilename, *OutFilename;
    InFilename  = argv[Arg];
    OutFilename = argv[Arg+1];
    
    ifp = fopen(InFilename, "rb");
    if (!ifp)
    {
    	fprintf(stderr, "can't open input file %s\n", InFilename);
    	return(1);
    }
    
    if (JpegImage)
    {
    	ofp = fopen(OutFilename, "wb");
	if (!ofp)
	{
	    fprintf(stderr, "can't open output file %s\n", OutFilename);
	    return(1);
	}
	
	int Ret;
	Ret = Cr2ExtractJpg();
	return(Ret);
    }
    
    tPROP Prop;
    memset(&Prop, 0, sizeof(Prop));
    if (strlen(InFilename) > (sizeof(Prop.Txt.Filename)-1))
    {
    	fprintf(stderr, "input filename too long, aborting\n");
	return(1);
    }
    strcpy(Prop.Txt.Filename, InFilename);
    
    int Ret;
    Ret = Cr2Decoder(&Prop);
    if ((Ret) || (Prop.IB16 == NULL))
    {
    	fprintf(stderr, "can't decompress CR2 file %s\n", InFilename);
	return(1);
    }
    
    if (Tiff)
    {
     	ofp = fopen(OutFilename, "wb");
     	if (!ofp)
     	{
     	    fprintf(stderr, "can't open output file %s\n", OutFilename);
     	    return(1);
     	}
	
    	WriteTiff(&Prop, Tiff, ofp);
    	fclose(ofp);
    }
    else if (Recode)
    {
     	ofp = fopen(OutFilename, "wb");
     	if (!ofp)
     	{
     	    fprintf(stderr, "can't open output file %s\n", OutFilename);
     	    return(1);
     	}
	
	Cr2Coder(&Prop);
    	fclose(ofp);
    }
    else if (Histogram)
    {
	MakeSimpleHistogram(&Prop);
    }
    else if (Binary)
    { // default, just write image to out file
     	ofp = fopen(OutFilename, "wb");
     	if (!ofp)
     	{
     	    fprintf(stderr, "can't open output file %s\n", OutFilename);
     	    return(1);
     	}
	
    	tU32 Size;
	Size = Prop.Image.Size.Width * Prop.Image.Size.Height;
    	fwrite(Prop.IB16, sizeof(*Prop.IB16), Size, ofp);
    	fclose(ofp);
    }
    
    Cr2DistroyImage(&Prop);
    fclose(ifp);

    return(0);
} /* end main() */
#endif

/***************************************************************************
* Function: 	Cr2DistroyImage()
* Returns:  	0 if successful
* Description:	Returns various data segments back to the heap, and then
*   	    	clears out all properties.
***************************************************************************/
int 	Cr2DistroyImage(
tPROP	*Prop)	    	    // pointer to properties
{
    if (Prop->IB16) free(Prop->IB16);
    if (Prop->Strip.OffsetList) free(Prop->Strip.OffsetList);
    if (Prop->Strip.CountList) free(Prop->Strip.CountList);
    memset(Prop, 0, sizeof(*Prop));
    return(0);
} // end Cr2DistroyImage() */

/***************************************************************************
* Function: 	Cr2Coder()
* Returns:  	0 if successful
* Description:	Converts and outputs to file the non-compressed image in
*   	    	buffer Prop->IB16[] to a CR2 compressed RAW file segment.
*   	    	The other segments of input file pointed to by ifp are 
*   	    	written to output file pointed to by ofp with a new and
*   	    	replaced compressed RAW image.
***************************************************************************/
int 	Cr2Coder(
tPROP	*Prop)	    	    // pointer to parameter list
{
#define OBUF_SIZE   	(0x10000)
#define MAX_PRECISION	(16)

    // get the largest number bits
    tU8  MaxVij;
    MaxVij = Prop->Huf[0].MaxVij;
    if (Prop->Huf[1].MaxVij > MaxVij) MaxVij = Prop->Huf[1].MaxVij;
    if (MaxVij > MAX_PRECISION)
    {
    	fprintf(stderr, "Invalid MaxVij (%d), aborting\n", MaxVij);
	return(1);
    }

    
    int  Ret = 0;
    tU8  *oBuf;
    tU16 *RB, *pRB;
    tU16 *DV, *DifVal;
    tU16 *DB, *DifBits;
    oBuf = (tU8 *)  calloc(OBUF_SIZE, sizeof(*oBuf));
    RB   = (tU16 *) calloc(Prop->Image.Size.Width, sizeof(*RB));
    DV   = (tU16 *) calloc(2 << MaxVij, sizeof(*DifVal));
    DB   = (tU16 *) calloc(2 << MaxVij, sizeof(*DifBits));
    if (!oBuf || !RB || !DV || !DB)
    {
    	fprintf(stderr, "can't get memory in Cr2Coder()\n");
    	exit(1); 
    }
    
    // create Huffmann tables
    tU16 *TC0;	// buffer, table 0, Huffmann code
    tU16 *TL0;	// buffer, table 0, length of Huffmann codes
    tU16 *TC1;	// buffer, table 1, Huffmann code
    tU16 *TL1;	// buffer, table 1, length of Huffmann codes
    Ret |= HuffmannMakeCoderTable(&Prop->Huf[0], &TC0, &TL0);
    Ret |= HuffmannMakeCoderTable(&Prop->Huf[1], &TC1, &TL1);
    if (Ret) goto done;

    // build Diff lookup tables
    // DifVal[] is right justified Diff bits
    // DifBits[] is number bits significant bits in DifVal[]
    int i, nBits, iBits;
    DifVal  = DV + (1 << MaxVij);	// index for +/-
    DifBits = DB + (1 << MaxVij);	// index for +/-
    for (i = 0, iBits = 0, nBits = 0; i < (1 << MaxVij); i++)
    { // build Diff tables over max range of precision
	if      (i == (1 << 0))  { nBits = 1;  iBits = (1 << 1) - 1; }
	else if (i == (1 << 1))  { nBits = 2;  iBits = (1 << 2) - 1; }
	else if (i == (1 << 2))  { nBits = 3;  iBits = (1 << 3) - 1; }
	else if (i == (1 << 3))  { nBits = 4;  iBits = (1 << 4) - 1; }
	else if (i == (1 << 4))  { nBits = 5;  iBits = (1 << 5) - 1; }
	else if (i == (1 << 5))  { nBits = 6;  iBits = (1 << 6) - 1; }
	else if (i == (1 << 6))  { nBits = 7;  iBits = (1 << 7) - 1; }
	else if (i == (1 << 7))  { nBits = 8;  iBits = (1 << 8) - 1; }
	else if (i == (1 << 8))  { nBits = 9;  iBits = (1 << 9) - 1; }
	else if (i == (1 << 9))  { nBits = 10; iBits = (1 << 10) - 1; }
	else if (i == (1 << 10)) { nBits = 11; iBits = (1 << 11) - 1; }
	else if (i == (1 << 11)) { nBits = 12; iBits = (1 << 12) - 1; }
	else if (i == (1 << 12)) { nBits = 13; iBits = (1 << 13) - 1; }
	else if (i == (1 << 13)) { nBits = 14; iBits = (1 << 14) - 1; }
	else if (i == (1 << 14)) { nBits = 15; iBits = (1 << 15) - 1; }
	else if (i == (1 << 15)) { nBits = 16; iBits = (1 << 16) - 1; }
	
	DifVal[i]   = i;
	DifVal[-i]  = i ^ iBits;
	DifBits[i]  = nBits;
	DifBits[-i] = nBits;
    }

    if (ifp != ofp)
    { // copy common IFDs and other stuff if input/output files different
     	fseek(ifp, 0, SEEK_SET);    // go to start of files
     	fseek(ofp, 0, SEEK_SET);
     	for (i = Prop->Strip.OfstStartOfScan; ; i -= OBUF_SIZE)
     	{
     	    if (i <= OBUF_SIZE)
     	    {
     		fread(oBuf, i, 1, ifp);
     		fwrite(oBuf, i, 1, ofp);
     		break;
     	    }
     	    fread(oBuf, OBUF_SIZE, 1, ifp);
     	    fwrite(oBuf, OBUF_SIZE, 1, ofp);
     	}
    }
    
    // initialize PutBits()
    PutBits((tU32) ofp, PUTBITS_INIT_OFP);
    PutBits((tU32) oBuf, PUTBITS_INIT_BUF);
    PutBits((tU32) OBUF_SIZE, PUTBITS_BUF_SIZE);
    
    tU32 HufCode;   	    	    // Huffmann code for HufVal
    int  HufBits;   	    	    // number bits in Huffmann code
    tU32 DiffVal;
    int  DiffBits;
    tI16 Diff0, Diff1;
    tU32 Row, Rows;
    tU32 Col, Cols;
    tU16 *IB;
    tU16 Predictor[2];
    IB   = Prop->IB16;
    Rows = Prop->Image.Size.Height;
    Cols = Prop->Image.Size.Width;
    Predictor[0] = Predictor[1] = 1 << (Prop->Image.Precision - 1);
    for (Row = 0; Row < Rows; Row++)
    {
    	// get image row, scramble it
    	Cr2GetScrambleRowSlice(RB, IB, Row, Prop);

	for (Col = 0, pRB = RB; Col < Cols; Col += 2, pRB += 2)
	{
	    if (Col == 0)
	    {
	    	Diff0 = pRB[0] - Predictor[0];
		Diff1 = pRB[1] - Predictor[1];
		Predictor[0] = pRB[0];
		Predictor[1] = pRB[1];
	    }
	    else
	    {
	    	Diff0 = pRB[0] - pRB[-2];
		Diff1 = pRB[1] - pRB[-1];
	    }
	    
	    // even columns
	    DiffBits = DifBits[Diff0];
    	    DiffVal  = DifVal[Diff0];
	    HufCode  = TC0[DiffBits];
	    HufBits  = TL0[DiffBits];
	    PutBits(HufCode, HufBits);
	    PutBits(DiffVal, DiffBits);
	    
	    // odd columns
	    DiffBits = DifBits[Diff1];
    	    DiffVal  = DifVal[Diff1];
	    HufCode  = TC0[DiffBits];
	    HufBits  = TL0[DiffBits];
	    PutBits(HufCode, HufBits);
	    PutBits(DiffVal, DiffBits);
	}
    }
    
    // terminate JPG segment with EOF
    if (!PutBits(NULL, PUTBITS_EOF)) Ret = 1;
    
    // write the size of lossless JPG segment
    tU32 StripByteCounts;
    StripByteCounts = ftell(ofp) - Prop->Strip.OffsetList[0];
    fseek(ofp, Prop->Strip.OfstByteCounts, SEEK_SET);
    PUT4(StripByteCounts);
    
    done:
    if (TC0)  free(TC0);
    if (TC1)  free(TC1);
    if (DB)   free(DB);
    if (DV)   free(DV);
    if (RB)   free(RB);
    if (oBuf) free(oBuf);
    return(Ret);
} /* end Cr2Coder() */

/***************************************************************************
* Function: 	Cr2Decoder()
* Returns:  	0 if successful
* Description:	Gets the compressed RAW data from file.  The caller provides
*   	    	a pointer to the image properties which will be filled in.
*   	    	Memory for the image buffer is allocated and it may later
*   	    	be returned to the heap by calling Cr2DistroyImage().
***************************************************************************/
int 	Cr2Decoder(
tPROP	*Prop)	    	    // pointer to parameter list
{
    // get image properties
    if (Cr2GetProperties(Prop)) return(1);
    
    // decode lossless JPG
    if (Cr2LosslessJpgDecoder(Prop)) return(1);

    FindTrim(Prop);
    
    return(0);
} /* end Cr2Decoder() */

/***************************************************************************
* Function: 	Cr2GetProperties()
* Returns:  	0 if successful
* Description:	Gets the image properties for the compressed RAW data from
*   	    	file.  Allocated may be returned to the heap by calling
*   	    	Cr2DistroyImage().
***************************************************************************/
int 	Cr2GetProperties(
tPROP	*Prop)	    	    // pointer to parameter list
{
    TestPlatformType();     // Determine if platform is Intel or Moto
    
    Prop->Image.Type = PROP_IMAGE_TYPE_BAYER;
    Prop->Image.FileType = PROP_FILE_TYPE_CR2;
    
    tU32 IfdOfst;
    IfdOfst = TiffFirstIfd();
    if (IfdOfst == NULL)
    { // Oops, not a TIFF file
    	fprintf(stderr, "TIFF signature not found in %s\n", Prop->Txt.Filename);
    	return(1); 
    }
    
    // get primary file/image information
    if (TiffParseIFD(IfdOfst, Prop)) return(1);
    
    // skip the first 3 IFDs, go to the Lossless JPG IFD segment
    int  IfdNum;
    tU16 NumIfds;
    for (IfdNum = 1; IfdNum < 4; IfdNum++)
    { // skip the first 3 IFDs, goto the 4th
    	fseek(ifp, IfdOfst, SEEK_SET);
	NumIfds = GET2();
	fseek(ifp, NumIfds * 12, SEEK_CUR);
	IfdOfst = GET4();
	if (IfdOfst == NULL)
	{
	    fprintf(stderr, "can't find Lossless JPG IFD segment\n");
	    return(1);
	}
    }

    // get Lossless JPG IFD information
    if (TiffParseIFD(IfdOfst, Prop)) return(1);
    
    // Now get properties embedded in Lossless JPG
    fseek(ifp, Prop->Strip.OffsetList[0], SEEK_SET);	// goto start of JPG
    if (GET2M() != ((JPG_MARK_FF << 8) | JPG_MARK_SOI))
    {
    	fprintf(stderr, "SOI marker not found\n");
	return(1);
    }

#define FOUND_SOF3  	(1 << 0)
#define FOUND_DHT   	(1 << 1)
#define FOUND_ALL   	(FOUND_SOF3 | FOUND_DHT)
    tU16 Markers = 0;
    int Done;
    int Ret = 0;
    for (Done = FALSE; !Done; )
    {
    	if (GET1() != JPG_MARK_FF)
	{
	    fprintf(stderr, "JPG marker not found\n");
	    Ret = 1;
	    goto done;
	}
	
	switch(GET1())
	{
	    case JPG_MARK_SOF3: // start of frame, lossless, Huffmann coding
	    if (Cr2HandleStartOfFrame(Prop))
	    {
	    	Ret = 1;
		goto done;
	    }
	    Markers |= FOUND_SOF3;
    	    break;
	    
	    case JPG_MARK_DHT:	// define Huffmann tables
	    if (Cr2GetHuffmannTableProperties(Prop))
	    {
	    	Ret = 1;
		goto done;
	    }
	    Markers |= FOUND_DHT;
	    break;
	    
	    case JPG_MARK_SOS:	// start of scan
     	    if (Markers != FOUND_ALL)
     	    {
     		fprintf(stderr, "insufficient JPG markers\n");
		Ret = 1;
     		goto done;
     	    }
	    Done = TRUE;
	    
	    // skip SOS header data, then save ofst to start of compressed data
    	    fseek(ifp, GET2M() - 2, SEEK_CUR);	// skip SOS header
    	    Prop->Strip.OfstStartOfScan = ftell(ifp);
	    break;

    	    default:
	    fprintf(stderr, "unexpected JPG marker\n");
	    Ret = 1;
	    goto done;
	}
    }

    done:    
    return(Ret);
} /* end Cr2GetProperties() */

/***************************************************************************
* Function: 	Cr2GetScrambleRowSlice() - a CR2 coder function
* Returns:  	0 if successful
* Description:	Reorders the image components in the image buffer (IB[])
*   	    	into quaduant slices of the row buffer (RB[]).
***************************************************************************/
int 	Cr2GetScrambleRowSlice(
tU16	*RB,    	// pointer to Row[] buffer
tU16 	*IB,	    	// pointer ImageBuffer[]
tU32 	Row,	    	// Row of compressed image
tPROP	*Prop)	    	// pointer to properties
{
// Yet another wierd Canon software decision.  I don't understand the
// need for this, but here it is.  The RAW image is divided into 4 equal
// sized quadrants:
//     +---------------+---------------+ RAW image
//     |	       |	       |
//     |    Quad 0     |    Quad 1     |
//     |	       |	       |
//     +---------------+---------------+
//     |	       |	       |
//     |    Quad 2     |    Quad 3     |
//     |	       |	       |
//     +---------------+---------------+
// The finished image interleaves quadrants row segments 0/1 and interleaves
// quadrants row segments 2/3 while combined quandrants 0/1 is located to
// the left and combined quadrants 2/3 are concatented to the right:
//     +---------------+---------------+ Finished image
//     |  Quad0.Row0   |  Quad2.Row0   |
//     |  Quad1.Row0   |  Quad3.Row0   |
//     |  Quad0.Row1   |  Quad2.Row1   |
//     |  Quad1.Row1   |  Quad3.Row1   |
//     |      etc      |      etc      |
//     |  Quad0.RowN   |  Quad2.RowN   |
//     |  Quad1.RowN   |  Quad3.RowN   |
//     +---------------+---------------+
// So it can be said that Quads 0 and 2 contain the same two BAYER grid 
// colors and Quads 1 and 3 contain the other two BAYER grid colors.

    tU32 ColSlice, RowSlice;
    tU32 Cols;
    tU32 MemCpySize;
    
    Cols  = Prop->Image.Size.Width;
    ColSlice = Prop->Image.Size.Width / 2;
    RowSlice = Prop->Image.Size.Height / 2;
    MemCpySize = ColSlice * sizeof(*RB);
    if (Row < RowSlice)
    {
    	IB += 2 * Row * Cols;
    }
    else
    {
    	IB += 2 * (Row - RowSlice) * Cols  +  ColSlice;
    }
    
    memcpy(RB, IB, MemCpySize);
    memcpy(RB + ColSlice, IB + Cols, MemCpySize);

    return(0);    
} /* end Cr2GetScrambleRowSlice() */

/***************************************************************************
* Function: 	Cr2LosslessJpgDecoder() - a CR2 decoder function
* Returns:  	0 if successful
* Description:	Extracts the lossless JPG image from the file (ifp) which
*   	    	has an offset/count based on TIFF StripOffsets and
*   	    	StripByteCounts.  The image width/height is saved in Prop
*   	    	and a pointer to the RGB image buffer (IB[]) is returned
*   	    	upon successful completion, else a NULL pointer is returned.
* Bugs:     	This decoder is designed for CR2 files and may not be
*   	    	sufficiently general for all Lossless JPG.
* Note:     	JPG is BIG ENDIAN byte ordering
***************************************************************************/
int 	Cr2LosslessJpgDecoder(
tPROP	*Prop)      	    // pointer to properties
{
    Prop->IB16 = NULL;

    int  Ret;
    tU8  *TL0 = NULL;	    // buffer, table 0, length of Huffmann codes
    tU8  *TB0 = NULL;	    // buffer, table 0, # bits to create 'difference'
    tU8  *TL1 = NULL;       // buffer, table 1, length of Huffmann codes
    tU8  *TB1 = NULL;       // buffer, table 1, # bits to create 'difference'
    tU16 *IB  = NULL;	    // ImageBuffer[] pointer
    tU8  *RAW = NULL;	    // Raw[] input data buffer pointer
    tU16 *RowBuf = NULL;    // RowBuffer[] pointer
    
    // build decoder Huffmann tables
    Ret  = HuffmannMakeDecoderTable(&Prop->Huf[0], &TL0, &TB0, NULL);
    Ret |= HuffmannMakeDecoderTable(&Prop->Huf[1], &TL1, &TB1, NULL);
    if (Ret) goto done;
    
    // go to start of lossless JPG scan data
    fseek(ifp, Prop->Strip.OfstStartOfScan, SEEK_SET);

    // set up buffers
    tU32 IbSize, RawSize;
    IbSize = Prop->Image.Size.Width * Prop->Image.Size.Height;
    IB = (tU16 *) calloc(IbSize, sizeof(tU16));
    RawSize  = Prop->Strip.CountList[0]
    	     - Prop->Strip.OfstStartOfScan
	     + Prop->Strip.OffsetList[0];
    RAW = (tU8 *) calloc(RawSize, sizeof(tU8));
    RowBuf = (tU16 *) calloc(Prop->Image.Size.Width, sizeof(tU16));
    if (!IB || !RAW || !RowBuf)
    {
    	fprintf(stderr, "can't calloc in Cr2LosslessJpgDecoder()\n");
	exit(1);
    }

    // get the RAW data, save it in its buffer for faster access
    Ret = !fread(RAW, RawSize, 1, ifp);
    if (Ret) goto done;
    
    // setup GetBits()
    GetBits(RAW, GETBITS_INIT);
    GetBits(&RAW[RawSize], GETBITS_FILE_END);
    
    // initialize Cr2GetLosslessJpgRow()
    Ret = Cr2GetLosslessJpgRow(NULL, RAW, TB0, TL0, TB1, TL1, Prop);
    if (Ret) goto done;
    
    tU32  ImageHeight;	    // image height
    tU32  iRow;	    	    // row counter
    ImageHeight = Prop->Image.Size.Height;
    for (iRow = 0; iRow < ImageHeight; iRow++)
    {
    	// decode a row of lossless JPG
    	Ret = Cr2GetLosslessJpgRow(RowBuf, RAW, TL0, TB0, TL1, TB1, Prop);
    	if (Ret) goto done;

	// Put the decoded row in its proper image buffer locations
	Ret = Cr2PutUnscrambleRowSlice(RowBuf, IB, iRow, Prop); 
    	if (Ret) goto done;
    }
    
    Prop->IB16 = IB;
    
    done:
    if (!Prop->IB16 && IB) free(IB);
    if (RowBuf) free(RowBuf);
    if (RAW) free(RAW);
    if (TL0) free(TL0);
    if (TL1) free(TL1);
    return(Ret);
} /* end Cr2LosslessJpgDecoder() */

/***************************************************************************
* Function: 	Cr2GetHuffmannTableProperties() - a CR2 decoder function
* Returns:  	0 if successful
* Description:	Gets the 2 Huffmann table properties from the JPG stream
*   	    	for a CR2 image.
***************************************************************************/
int 	Cr2GetHuffmannTableProperties(
tPROP 	*Prop)  	    // pointer to properties
{
    
    int  Lh;	    // length of DHT marker segment
    tU8  *Li;	    // pointer to lengths table
    tU8  *Vij;	    // pointer to values table
    tU8  MaxVij;    // largest Vij
    int  iTab;	    // which table: either 1 or 2
    int  i, j, k;
    
    MaxVij = 0;
    Lh = GET2M() - 2;
    for (iTab = 0; ; iTab++)
    { // The following code fills two table sets (Val and Siz)
    	switch(iTab)
	{
	    case 0: // table 0
	    case 1: // table 1
    	    Prop->Huf[iTab].TcTh = GET1();	// get table class / table ID
	    if (Prop->Huf[iTab].TcTh != iTab)
	    {
	    	fprintf(stderr, "unexpected DQT class/ID, is=%02X, sb=%02X\n",
	    	    	Prop->Huf[iTab].TcTh, iTab);
	    	return(1);
	    }
	    Li  = Prop->Huf[iTab].Li;
	    Vij = Prop->Huf[iTab].Vij;
	    break;
	    
	    default:
	    if (Lh == 0) return(0);
	    fprintf(stderr, "overrun in Cr2GetHuffmannTableProperties()\n");
	    return(1);
	}
	
    	fread(Li, 1, 16, ifp);	    // get Li[] values
	Lh -= 17;
	for (i = j = 0; i < 16; i++)
	{
	    for (k = Li[i]; k--; j++)
	    {
	    	Vij[j] = GET1();
		if (Vij[j] > MaxVij) MaxVij = Vij[j];
		Lh--;
	    }
	}
	Prop->Huf[iTab].nVals  = j;
	Prop->Huf[iTab].MaxVij = MaxVij;
    } 
} /* end Cr2GetHuffmannTableProperties() */

/***************************************************************************
* Function: 	Cr2HandleStartOfFrame() - a CR2 decoder function
* Returns:  	0 if successful
* Description:	Reads JPG start of frame information, saves as needed.
***************************************************************************/
int 	Cr2HandleStartOfFrame(
tPROP	*Prop)      	    // pointer to properties
{
    int  Lf;	    	    // length of SOFn segment
    tU8  Precision; 	    // number bits precision
    tU16 nLines;    	    // number lines in scan
    tU16 SamplesPerLine;    // number samples per line
    tU8  Nf;	    	    // number image components in frame
    Lf = GET2M();
    if (Lf != 14)
    {
    	fprintf(stderr, "unexpected record length for SOF3\n");
	return(1);
    }
    
    Precision = GET1();
    nLines = GET2M();
    SamplesPerLine = GET2M();
    Nf = GET1();
    if (Nf != 2)
    {
    	fprintf(stderr, "unexpected number image components in frame\n");
	return(1);
    }
    
    for (Lf -= 8; Lf; Lf -= 3)
    {
    	fseek(ifp, 1, SEEK_CUR);    // dispose of component ID
	if (GET1() != 0x11)
	{
	    fprintf(stderr, "unexpected horz/vert sampling factor\n");
	    return(1);
	}
	fseek(ifp, 1, SEEK_CUR);    // dispose of quantization table selector
    }
    
    Prop->Image.Precision = Precision;
    Prop->Image.Size.Width  = SamplesPerLine * Nf;
    Prop->Image.Size.Height = nLines;
    
#ifdef DEBUG_PRINT
    printf("Precision=%d, Samples/Line=%d, nLines=%d, nFrames=%d\n",
    	    Precision, SamplesPerLine, nLines, Nf);
#endif
  
    return(0);
} /* end Cr2HandleStartOfFrame() */

/***************************************************************************
* Function: 	Cr2PutUnscrambleRowSlice() - a CR2 decoder function
* Returns:  	0 if successful
* Description:	Reorders the image components in RowBuf[] into image buffer
*   	    	IB for the given JpgRow for the specified Prop->Cr2.Slice.
*   	    	If no slice is given, then the RowBuf[] is copied to IB[]
*   	    	at the given JpgRow.
***************************************************************************/
int 	Cr2PutUnscrambleRowSlice(
tU16	*RowBuf,    	// pointer to Row[] buffer
tU16 	*IB,	    	// pointer ImageBuffer[]
tU32 	Cr2Row,	    	// Row of RowBuf[]
tPROP	*Prop)	    	// pointer to properties
{
// Yet another wierd Canon software decision.  I don't understand the
// need for this, but here it is.  The RAW image is divided into 4 equal
// sized quadrants:
//     +---------------+---------------+ RAW image
//     |	       |	       |
//     |    Quad 0     |    Quad 1     |
//     |	       |	       |
//     +---------------+---------------+
//     |	       |	       |
//     |    Quad 2     |    Quad 3     |
//     |	       |	       |
//     +---------------+---------------+
// The finished image interleaves quadrants row segments 0/1 and interleaves
// quadrants row segments 2/3 while combined quandrants 0/1 is located to
// the left and combined quadrants 2/3 are concatented to the right:
//     +---------------+---------------+ Finished image
//     |  Quad0.Row0   |  Quad2.Row0   |
//     |  Quad1.Row0   |  Quad3.Row0   |
//     |  Quad0.Row1   |  Quad2.Row1   |
//     |  Quad1.Row1   |  Quad3.Row1   |
//     |      etc      |      etc      |
//     |  Quad0.RowN   |  Quad2.RowN   |
//     |  Quad1.RowN   |  Quad3.RowN   |
//     +---------------+---------------+
// So it can be said that Quads 0 and 2 contain the same two BAYER grid 
// colors and Quads 1 and 3 contain the other two BAYER grid colors.

    tU32  Cr2Slice;
    tU32  Cr2Cols, Cr2QuadRows;
    
    Cr2Cols  = Prop->Image.Size.Width;
    Cr2Slice = Cr2Cols / 2;
    Cr2QuadRows = Prop->Image.Size.Height / 2;
    if (Cr2Row < Cr2QuadRows)
    {
    	IB += 2 * Cr2Row * Prop->Image.Size.Width;
    }
    else
    {
    	IB += 2 * (Cr2Row - Cr2QuadRows) * Prop->Image.Size.Width  +  Cr2Slice;
    }
    
    memcpy(IB, RowBuf, Cr2Slice * sizeof(*IB));
    memcpy(IB + Cr2Cols, RowBuf + Cr2Slice, Cr2Slice * sizeof(*IB));

    return(0);    
} /* end Cr2PutUnscrambleRowSlice() */

/***************************************************************************
* Function: 	Cr2GetLosslessJpgRow() - a CR2 decoder function
* Returns:  	0 if successful
* Description:	Gets one row of lossless JPG image data.  If RowBuf == NULL,
*   	    	then the process is initialized.
***************************************************************************/
int 	Cr2GetLosslessJpgRow(
tU16	*RowBuf,    	// pointer to Row[] buffer
tU8 	*RAW,	    	// RAW lossess JPG data
tU8 	*TL0,	    	// buffer, table 0, length of Huffmann codes
tU8 	*TB0,	    	// buffer, table 0, # bits to create 'difference'
tU8 	*TL1,	    	// buffer, table 1, length of Huffmann codes
tU8 	*TB1,	    	// buffer, table 1, # bits to create 'difference'
tPROP	*Prop)	    	// pointer to properties
{
    static tU16 Predictor[2];
    static int  DifBits;    // # difference bits needed
    
    if (RowBuf == NULL)
    { // initialize process
    	Predictor[0] =
	Predictor[1] = 1 << Prop->Image.Precision - 1;
	DifBits = 0;
	return(0);
    }
    
    tU16 HufCode;   	    // Huffmann code
    int  HufBits;   	    // # Huff bits used
    tI16 Dif0, Dif1;	    // difference values for even/odd columns
    int  iCol;
    int  ImageWidth;
    ImageWidth = Prop->Image.Size.Width;
    for (iCol = 0; iCol < ImageWidth; iCol += 2, RowBuf += 2)
    {
    	// first get the Dif value for the even columns
    	HufCode = GetBits(RAW, DifBits);
	HufBits = TL0[HufCode];
	DifBits = TB0[HufCode];
	if (HufBits == 0)
	{ // oops, either end of file or just plain lost
	    fprintf(stderr, "lost in space decoding lossless JPG\n");
	    return(1);
	}
    	// Get the 'Dif' bits.  These bits represent a pseudo signed
    	// magnitude of the difference.  It should be noted that the
    	// bits have perfect entropy in that a 2 bit field represents
    	// exactly 2 positive values and 2 negative values, a 3 bit
    	// field 8 positive and 8 negative, a 4 bit field 16 and 16, 
    	// etc.  The most significant bit of positive values are one,
    	// while the most significant bit of negative values are zero
    	// and to extract the negative value the bits are inverted.  
    	// e.g. The 4 2-bit fields and values are:  00=>-3, 01->-2,
    	// 10->+2, and 11->+3.
    	register tU32 DifCode;
    	// Note:  DiffCode is a tU32 (rather than tU16) to resolve
    	// the nasty problem of right shifting without sign extension
    	// with some compliers.  The Borland C++ complier falls into
    	// this category.  The code should work across all compliers.
    	DifCode = GetBits(RAW, HufBits);
    	if (DifCode & 0x8000)
    	{ // msb is 1, thus decoded DifCode is positive
    	    Dif0 = DifCode >> (16 - DifBits);
    	}
    	else
    	{ // msb is 0, thus DifCode is negative
    	    Dif0 = -((tI32) ((DifCode ^ 0xFFFF) >> (16 - DifBits)));
    	}

    	// now get the Dif value for the odd columns
    	HufCode = GetBits(RAW, DifBits);
	HufBits = TL1[HufCode];
	DifBits = TB1[HufCode];
	if (HufBits == 0)
	{ // oops, either end of file or just plain lost
	    fprintf(stderr, "lost in space decoding lossless JPG\n");
	    return(1);
	}
    	DifCode = GetBits(RAW, HufBits);
    	if (DifCode & 0x8000)
    	{ // msb is 1, thus decoded DifCode is positive
    	    Dif1 = DifCode >> (16 - DifBits);
    	}
    	else
    	{ // msb is 0, thus DifCode is negative
    	    Dif1 = -((tI32) ((DifCode ^ 0xFFFF) >> (16 - DifBits)));
    	}
    
    	if (iCol == 0)
	{
    	    RowBuf[0] = Predictor[0] += Dif0;
    	    RowBuf[1] = Predictor[1] += Dif1;
    	}
	else
	{
    	    RowBuf[0] = RowBuf[-2] + Dif0;
    	    RowBuf[1] = RowBuf[-1] + Dif1;
	}
    }
    
    return(0);
} /* Cr2GetLosslessJpgRow() */

/***************************************************************************
* Function: 	Cr2ExtractJpg()
* Returns:  	0 if successful
* Description:	Extracts the JPG image from the first TIFF IFD of input
*   	    	(previously opened) file ifp and writes to (previously
*   	    	opened) output file ofp.
***************************************************************************/
int 	Cr2ExtractJpg(void)
{
    static char JpgHeader[] = 
    {
    	0xff, 0xd8, 	    	    	// (00) JPG signature
	0xff, 0xe0, 	    	    	// (02) APPO segment
	0x00, 0x10, 	    	    	// (04) length of APPO segment
	'J', 'F', 'I', 'F', 0,	    	// (06) JFIF identifier
	0x01, 0x02, 	    	    	// (0b) Version
	0x01, 0x01, 0x2c, 0x01, 0x2c,	// (0d) units & x/y density
	0x00, 0x00  	    	    	// (12) thumbnail dimensions
	    	    	    	    	// (14)
    };
    
    tPROP Prop;
    memset(&Prop, 0, sizeof(Prop));
    
    TestPlatformType();     // Determine if platform is Intel or Moto
    
    tU32 IfdOfst;
    IfdOfst = TiffFirstIfd();
    if (IfdOfst == NULL)
    { // Oops, not a TIFF file
    	fprintf(stderr, "TIFF signature not found\n");
    	return(1); 
    }
    
    // get primary file/image information
    if (TiffParseIFD(IfdOfst, &Prop)) return(1);
    
    // write JPG Header
    if (!fwrite(JpgHeader, 1, sizeof(JpgHeader), ofp))
    {
    	fprintf(stderr, "can't write output file\n");
	return(1);
    }
    
    char Buf[256];
    int  Ret = 0;
    tU32 Start, Size;
    // For a reason only Canon knows, there is an extra JPG_MARK_SOI
    // at the start of this segment, skip these 2 bytes.
    Start = Prop.Strip.OffsetList[0] + 2;
    Size  = Prop.Strip.CountList[0]  - 2;
    fseek(ifp, Start, SEEK_SET);
    for ( ; ; Size -= sizeof(Buf))
    {
    	if (Size <= sizeof(Buf))
	{
	    Ret |= !fread(Buf, 1, Size, ifp);
	    Ret |= !fwrite(Buf, 1, Size, ofp);
	    break;
	}
	else
	{
	    Ret |= !fread(Buf, 1, sizeof(Buf), ifp);
	    Ret |= !fwrite(Buf, 1, sizeof(Buf), ofp);
	    if (Ret) break;
	}
    }
    
    if (Ret)
    {
    	fprintf(stderr, "can't read input file OR write output file\n");
	return(1);
    }

    return(0);
} /* end Cr2ExtractJpg() */
