#ifndef _JPEG_H_
#define _JPEG_H_
/***********************************************************************
* File: 	jpeg.h
* Change Info:
*   02/17/07	HLS - Re-created.
* Description:	Basic JPEG cConstants and structures.
************************************************************************/

//***********************************************************************
// JPEG Marker Constants
//***********************************************************************
// Marker Code Assignments
// Note - all markes are 16 bits, of form 0xFFxx, only LSBs defined
// Start of Frame Makers, non-differential, Huffman Coding
#define JPG_MARK_SOF0       (0xC0)	// baseline DCT
#define JPG_MARK_SOF1       (0xC1)	// Extended sequential DCT
#define JPG_MARK_SOF2       (0xC2)	// Progressive DCT
#define JPG_MARK_SOF3	    (0xC3)	// Lossless (sequential)

// Start of Frame Markers, differential, Huffman coding
#define JPG_MARK_SOF5       (0xC5)	// Diferential sequential DCT
#define JPG_MARK_SOF6       (0xC6)	// Differential progressive DCT
#define JPG_MARK_SOF7       (0xC7)	// Differential lossless (sequential)

// Start of Fram Markers, non-differential, arithmetic coding
#define JPG_MARK_SOF9       (0xC9)	// Extended sequential DCT
#define JPG_MARK_SOF10      (0xCA)	// Progressive DCT
#define JPG_MARK_SOF11      (0xCB)	// Lossless (sequential)

// Start of Frame Markers, differential, arithmetic coding
#define JPG_MARK_SOF13      (0xCD)	// Differential sequential DCT
#define JPG_MARK_SOF14      (0xCE)	// Differential progressive DCT
#define JPG_MARK_SOF15      (0xCF)	// Differential Lossless (sequential)

// Huffman Table Specification
#define JPG_MARK_DHT        (0xC4)	// Define Huffman Table(s)

// Arithmetic Coding Conditioning Specification
#define JPG_MARK_DAC	    (0xCC)  	// Define Arithmetic Coding Conditioning(s)

// Restart Interval Termination - these markers have no data segment
#define JPG_MARK_RST0       (0xD0)	//$Restart with modulo 8 count 0
#define JPG_MARK_RST1       (0xD1)	//$Restart with modulo 8 count 1
#define JPG_MARK_RST2       (0xD2)	//$Restart with modulo 8 count 2
#define JPG_MARK_RST3       (0xD3)	//$Restart with modulo 8 count 3
#define JPG_MARK_RST4       (0xD4)	//$Restart with modulo 8 count 4
#define JPG_MARK_RST5       (0xD5)	//$Restart with modulo 8 count 5
#define JPG_MARK_RST6       (0xD6)	//$Restart with modulo 8 count 6
#define JPG_MARK_RST7       (0xD7)	//$Restart with modulo 8 count 7

// Other Markers
#define JPG_MARK_SOI        (0xD8)	//$Start of Image - w/o data segment
#define JPG_MARK_EOI        (0xD9)	//$End of Image - w/o data segment
#define JPG_MARK_SOS        (0xDA)	// Start of Scan
#define JPG_MARK_DQT        (0xDB)	// Define Quantization Table(s)
#define JPG_MARK_DNL        (0xDC)	// Define Number of Lines
#define JPG_MARK_DRI        (0xDD)	// Define Restart Interval
#define JPG_MARK_DHP        (0xDE)	// Define Hierarchical Progression
#define JPG_MARK_EXP        (0xDF)	// Expand Reference Component(s)
#define JPG_MARK_COM        (0xFE)	// Comment

// Reserved Application Segments
// note - APP-2 has been found to contain "ICC_PROFILE" string discriptor
#define JPG_MARK_APP0       (0xE0)	// Reserved Application Segment 0
#define JPG_MARK_APP1       (0xE1)	// Reserved Application Segment 1
#define JPG_MARK_APP2       (0xE2)	// Reserved Application Segment 2
#define JPG_MARK_APP3       (0xE3)	// Reserved Application Segment 3
#define JPG_MARK_APP4       (0xE4)	// Reserved Application Segment 4
#define JPG_MARK_APP5       (0xE5)	// Reserved Application Segment 5
#define JPG_MARK_APP6       (0xE6)	// Reserved Application Segment 6
#define JPG_MARK_APP7       (0xE7)	// Reserved Application Segment 7
#define JPG_MARK_APP8       (0xE8)	// Reserved Application Segment 8
#define JPG_MARK_APP9       (0xE9)	// Reserved Application Segment 9
#define JPG_MARK_APP10      (0xEA)	// Reserved Application Segment 10
#define JPG_MARK_APP11      (0xEB)	// Reserved Application Segment 11
#define JPG_MARK_APP12      (0xEC)	// Reserved Application Segment 12
#define JPG_MARK_APP13      (0xED)	// Reserved Application Segment 13
#define JPG_MARK_APP14      (0xEE)	// Reserved Application Segment 14
#define JPG_MARK_APP15      (0xEF)	// Reserved Application Segment 15

// Reserved for JPEG Extensions
#define JPG_MARK_EXT        (0xC8)	// Reserved for JPEG extensions
#define JPG_MARK_EXT0       (0xF0)	// Reserved for JPEG extensions 0
#define JPG_MARK_EXT1       (0xF1)	// Reserved for JPEG extensions 1
#define JPG_MARK_EXT2       (0xF2)	// Reserved for JPEG extensions 2
#define JPG_MARK_EXT3       (0xF3)	// Reserved for JPEG extensions 3
#define JPG_MARK_EXT4       (0xF4)	// Reserved for JPEG extensions 4
#define JPG_MARK_EXT5       (0xF5)	// Reserved for JPEG extensions 5
#define JPG_MARK_EXT6       (0xF6)	// Reserved for JPEG extensions 6
#define JPG_MARK_EXT7       (0xF7)	// Reserved for JPEG extensions 7
#define JPG_MARK_EXT8       (0xF8)	// Reserved for JPEG extensions 8
#define JPG_MARK_EXT9       (0xF9)	// Reserved for JPEG extensions 9
#define JPG_MARK_EXT10      (0xFA)	// Reserved for JPEG extensions 10
#define JPG_MARK_EXT11      (0xFB)	// Reserved for JPEG extensions 11
#define JPG_MARK_EXT12      (0xFC)	// Reserved for JPEG extensions 12
#define JPG_MARK_EXT13      (0xFD)	// Reserved for JPEG extensions 13

// Reserved Markers
#define JPG_MARK_TEM        (0x01)	//$Temporary private use in arithmetic coding
#define JPG_MARK_RES_START  (0x02)	// Reserved, 1st of 0xBD markers
#define JPG_MARK_RES_END    (0xBF)	// Reserved, last of 0xBD markers

// Special stream marker values
#define JPG_MARK_FF 	    (0xFF)  	// Starts marker or white space
#define JPG_MARK_ZERO       (0x00)  	// Ignore this mark

// APP0 Mandatory (JFIF) constants
#define JFIF_VERSION	    (0x0102)    // required JFIF version
#define JFIF_UNITS_NONE     (0)
#define JFIF_UNITS_INCH     (1)
#define JFIF_UNITS_CM	    (2)

// other JPEG constants
#define JPG_ZIG_ZAG_SIZE    (64)    // 8x8 array
#define JPG_ZIG_ZAG_MAP     { 0, 1, 5, 6,14,15,27,28,   \
                              2, 4, 7,13,16,26,29,42,   \
                              3, 8,12,17,25,30,41,43,   \
                              9,11,18,24,31,40,44,53,   \
                             10,19,23,32,39,45,52,54,   \
                             20,22,33,38,46,51,55,60,   \
                             21,34,37,47,50,56,59,61,   \
                             35,36,48,49,57,58,62,63 }

// Type Definitions
#ifndef STD_TYPES
#define STD_TYPES
typedef char	    	tI8;
typedef unsigned char 	tU8;
typedef short	    	tI16;
typedef unsigned short	tU16;
typedef long	    	tI32;
typedef unsigned long	tU32;
#endif

typedef struct sAPP
{
    tU16    AppNum;  	    	    // APPn segment number, 0-15
    tU16    AppType;   	    	    // Appn type, 0=unknown, else see list
    char    *Name;    	    	    // name ID of APPn
    void    *Data;  	    	    // pointer to data of APPn
    sAPP    *Prev;  	    	    // previous APP data in link list
    sAPP    *Next;  	    	    // next APP data in link list
} tAPP;

typedef struct
{ // APP0 JFIF segment
    tU16    Version;	    	    // version 0x0102
    tU8     Units;  	    	    // 0=no units, 1=dots/inch, 2=dots/cm
    tU16    xDensity;	    	    // horizontal pixel density
    tU16    yDensity;	    	    // vertical pixel density
    tU8     xThumbnail;     	    // thumbnail horizontal pixel count
    tU8     yThumbnail;     	    // thumbnail vertical pixel count
    void    *Thumbnail;     	    // thumbnail (rgb) image ptr - NULL if none
} tJFIF;

// SOS:  Start of Scan, aka scan header
//  	    ----SequentialDCT---
// Param    Baseline	Extended    ProgressiveDCT  Lossless
//  lf	    ----------------- 6  +  2 * ns -----------------
//  ns	    -------------------- 1-4 -----------------------
//  cs[j]   ------------------- 0-255 ----------------------
//  td[j]      0-1  	  0-3	    	0-3 	      0-3
//  ta[j]      0-1  	  0-3	    	0-3 	      0
//  ss	       0   	  0	    	0-63	      1-7
//  se	       63  	  63	    	ss-63	      0
//  ah	       0    	  0 	    	0-13	      0
//  al	       0    	  0 	    	0-13	      0-15
#define MAX_SCAN_IMAGES     (4)
typedef struct
{
    tU8     Valid;  	    	    // set TRUE when valid
    tU8     Cs;	    	    	    // Scan component selector, 0-255
    tU8     Tdc;   	     	    // DC entropy coding table dest sel, 0-3
    tU8     Tac;   	    	    // AC entropy coding table dest sel, 0-3
} tSOSSEL;

typedef struct
{
    tU8     Ns;  	    	    // Number image components in scan, 1-4
    tU8     Ss;     	    	    // start of spectral/predictor sel
    tU8     Se;     	    	    // end of spectral/predictor sel
    tU8     Ah;    	    	    // successive approx bit high, 0-13
    tU8     Al;    	    	    // successive approx bit low, 0-13
    tSOSSEL Sel[MAX_SCAN_IMAGES];   // various selectors
} tSOS;

// Quantization Tables
// DQT:  Define Quantization Table
//  	    ----SequentialDCT---
// Param    Baseline	Extended    ProgressiveDCT  Lossless
//  lq	    2 + SUM{t=1 to t=64}[65 +  64 * pq(t)]  undefined
//  pq	       0    	   0,1	    	0,1 	    undefined
//  tq	    --------------- 1-3 ------------------  undefined
//  qk	    ---------- 1-255, 1-65535 ------------  undefined
// - up to 4 tables
// - table size is 8x8, 64 elements
// - Staggered jpeg ordering
// - quantization values represent step size
// - step size is 1-255 for baseline, can be 1-65535 for others
// - void** is tU8 for 8-bit precision, tU16 for 16-bit precision
// - often tables are "luminance" and "chrominance"
#define QT_SIZE     	    (8*8)   // one value per 8x8 segment
#define QT_PRECISION_8	    (0)     // 8-bit precision step size
#define QT_PRECISION_16	    (1)     // 16-bit precision step size
#define MAX_Q_TABLES	    (4)     // up to 4 tables
typedef struct
{ // Quantization tables
    int     Valid;	    	    // set TRUE when valid
    int     Precision;    	    // 8-bit or 16-bit element size precision
    union
    {
    	tU8 	t8[JPG_ZIG_ZAG_SIZE];	// 8-bit precision table
	tU16	t16[JPG_ZIG_ZAG_SIZE];	// 16-bit precision table
    } qt;		    	    // 64-element zig-zag table
} tQT;

// Huffman Tables
// DHT:  Define Huffman Table
//  	    ----SequentialDCT---
// Param    Baseline	Extended    ProgressiveDCT  Lossless
//  lh	    -- 2 + SUM{t=1 to n}[
//  tc	    ------------------ 0,1----------------  	0
//  th	       0,1  	---------------- 0-3 ---------------
//  li	    ------------------- 0-255 ----------------------
//  vij     ------------------- 0-255 ----------------------
// - up to 4 tables
// - each table set has DC and AC sections
// - symbol map codes support both JPEG baseline and others
#define MAX_H_TABLES	    (4)     // up to 4 tables
#define HT_DC	    	    (0)     // DC table
#define HT_AC	    	    (1)     // AC table
#define BITS_LEN   	    (16)    // Bits is an array of 16 elements
typedef struct
{
    int     Valid;  	    	    // set TRUE when valid
    int     NumCodes;	    	    // number of unique Huffman symbol codes
    int     MaxCodeSize;    	    // bit width of longest Huffman code (1-16)
    long    SizeOf;  	    	    // size of Codes and Lens arrays
    tU8     *Map; 	    	    // Map[i] maps Huffman symbol to Code
    tU8     *Len;  	    	    // Len[i] is bit length of symbol (1-16)
} tHUF;

typedef struct
{
    tHUF    Dc;     	    	    // Dc Huffman translation table
    tHUF    Ac;     	    	    // Ac Huffman translation table
} tHT;

// SOFn
// Non-differential frame markers: SOF0, SOF1, SOF2, SOF3, SOF9, SOF10, SOF11
// Differential frame markers: SOF5, SOF6, SOF7, SOF13, SOF14, SOF15
//  	    ----SequentialDCT---
// Param    Baseline	Extended    ProgressiveDCT  Lossless
//  lf	    ----------------- 8  +  3 * nf -----------------
//  p	     	8   	  8,12	    	8,12	      2-16
//  y	    ----------------- 0-65535 ----------------------
//  x	    ----------------- 1-65535 ----------------------
//  nf	      1-255 	  1-255     	1-4 	      1-255
//  c[i]    -----------------  0-255 -----------------------
//  h[i]    -----------------  1-4   -----------------------
//  v[i]    -----------------  1-4   -----------------------
//  tq[i]      0-3  	  0-3	    	0-3 	    	0
#define MAX_SOF_COMP_IDS    (256)
typedef struct
{
    tU8     Valid;  	    	    // TRUE if valid
    tU8     Horz;  	  	    // horizontal sampling factor
    tU8     Vert;   	    	    // vertical sampling factor
    tU8     qtSel; 	    	    // quantization table selector
} tFI;

typedef struct
{
    tU8     SOFn;   	    	    // which SOFn (0xC0, C1,C2,C3,C9,CA,CB)
    tU8     Prec;	    	    // 8 or 12 bit precision
    tU16    x;	    	    	    // number of columns in scan
    tU16    y;	    	    	    // number of rows in scan
    tFI     fi[MAX_SOF_COMP_IDS];   // frame information indexed by CompID
} tSOF;

#endif /* _JPEG_H_ */
