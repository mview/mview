#ifndef _TIFF_UTL_H_
#define _TIFF_UTL_H_
/***********************************************************************
* File: 	TiffUtl.h
* Change Info:
*   03/14/07	HLS - Began.
* Description:	Header file for TiffUtl.cpp.
************************************************************************/

// Various constants

// external variables

// Function Prototypes
tU32	TiffFirstIfd(void);
int	TiffGetTagData(tU16, tU32, tU32, tU32, tU32, void *);
int 	TiffGetXmpItem(char *, char *, char *, int);
int 	TiffParseXMP(tU32, tU32, tPROP *);
int 	TiffParseMakerNote(tU32, tPROP *);
int 	TiffParseIFD(tU32, tPROP *);

#endif _TIFF_UTL_H_
