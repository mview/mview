#ifndef _GETPUT_H_
#define _GETPUT_H_
/***********************************************************************
* File: 	getput.h
* Change Info:
*   02/17/07	HLS - Began.
* Description:	Header file for get.cpp.
************************************************************************/

#include <stdio.h>

// Various constants
#define INTEL_SIGNATURE     (('I' << 8) + 'I')
#define MOTO_SIGNATURE	    (('M' << 8) + 'M')

//***********************************************************************
// GetBits() Constants
//***********************************************************************
#define GETBITS_INIT	    (-1)
#define GETBITS_FILE_END    (-2)
#define GETBITS_LOW_INDEX   (-3)
#define GETBITS_HIGH_INDEX  (-4)
#define GETBITS_BIT_STATUS  (-5)
#define GETBITS_EOF_STATUS  (-6)

//***********************************************************************
// PutBits() Constants
//***********************************************************************
#define PUTBITS_INIT_OFP    (-1)
#define PUTBITS_INIT_BUF    (-2)
#define PUTBITS_BUF_SIZE    (-3)
#define PUTBITS_EOF 	    (-4)
#define PUTBITS_STATUS 	    (-5)

// Type Definitions
#ifndef STD_TYPES
#define STD_TYPES
typedef char	    	tI8;
typedef unsigned char 	tU8;
typedef short	    	tI16;
typedef unsigned short	tU16;
typedef long	    	tI32;
typedef unsigned long	tU32;
#endif

// external variables
extern	tU16	PlatformType;	    	// Intel or Moto
extern	tU16	IntelMotoFileSignature; // Intel or Moto
extern	FILE	*ifp;	    	    	// input file pointer
extern	FILE	*ofp;	    	    	// output file pointer

extern	tU16    (*Get2)();
extern	tU32    (*Get4)();
extern	double  (*Get8)();
extern	tU16    (*Get2I)();
extern	tU32    (*Get4I)();
extern	double  (*Get8I)();
extern	tU16    (*Get2M)();
extern	tU32    (*Get4M)();
extern	double  (*Get8M)();
extern	void    (*Put2)(tU16);
extern	void    (*Put4)(tU32);
extern	void    (*Put8)(double);
extern	void    (*Put2I)(tU16);
extern	void    (*Put4I)(tU32);
extern	void    (*Put8I)(double);
extern	void    (*Put2M)(tU16);
extern	void    (*Put4M)(tU32);
extern	void    (*Put8M)(double);

#define GET1()	    Get1()
#define GET2()	    (*Get2)()
#define GET4()	    (*Get4)()
#define GET8()	    (*Get8)()
#define GET2I()	    (*Get2I)()
#define GET4I()	    (*Get4I)()
#define GET8I()	    (*Get8I)()
#define GET2M()	    (*Get2M)()
#define GET4M()	    (*Get4M)()
#define GET8M()	    (*Get8M)()
#define PUT1(d)	    Put1(d)
#define PUT2(d)	    (*Put2)(d)
#define PUT4(d)	    (*Put4)(d)
#define PUT8(d)     (*Put8)(d)
#define PUT2I(d)    (*Put2I)(d)
#define PUT4I(d)    (*Put4I)(d)
#define PUT8I(d)    (*Put8I)(d)
#define PUT2M(d)    (*Put2M)(d)
#define PUT4M(d)    (*Put4M)(d)
#define PUT8M(d)    (*Put8M)(d)

// Function Prototypes
tU16 	GetBits(tU8 *, int);
tI32	PutBits(tU32, int);
int	IntelMotoByteSwap(tU16 *, tU32);
tU16	TestPlatformType(void);
tU16	TestIntelMotoFileSignature(void);
tU8	Get1(void);
tU16	Get2F(void);
tU32	Get4F(void);
double	Get8F(void);
tU16	Get2R(void);
tU32	Get4R(void);
double	Get8R(void);
void	Put1(tU8);
void	Put2F(tU16);
void	Put4F(tU32);
void	Put8F(double);
void	Put2R(tU16);
void	Put4R(tU32);
void	Put8R(double);
void	Oops(char *);

#endif _GETPUT_H_
