#ifndef _RAW_UTL_H_
#define _RAW_UTL_H_
/***********************************************************************
* File: 	RawUtl.h
* Change Info:
*   03/04/07	HLS - Began.
* Description:	Header file for RawUtl.cpp.
************************************************************************/

// Various constants

//***********************************************************************
// Huffmann Constants
//***********************************************************************
#define HUF_CODER_SIZE	    (256)   	// # entiies in HUF coder tables
#define HUF_DECODER_SIZE    (0x10000)   // # entries in HUF decoder tables
#define HUF_MAX_BITS	    (16)        // maximum 16 bits

//***********************************************************************
// Bayer grid Constants
//***********************************************************************
#define BAYER_GRID_R0C0	    (0x01)  	// colors starting from row 0, col 0
#define BAYER_GRID_R0C1	    (0x02)  	// colors starting from row 0, col 1
#define BAYER_GRID_R1C0	    (0x04)  	// colors starting from row 1, col 0
#define BAYER_GRID_R1C1	    (0x08)  	// colors starting from row 1, col 1
#define BAYER_GRID_RxC0	    (BAYER_GRID_R0C0 | BAYER_GRID_R1C0)
#define BAYER_GRID_RxC1	    (BAYER_GRID_R0C1 | BAYER_GRID_R1C1)
#define BAYER_GRID_R0Cx	    (BAYER_GRID_R0C0 | BAYER_GRID_R0C1)
#define BAYER_GRID_R1Cx	    (BAYER_GRID_R1C0 | BAYER_GRID_R1C1)
#define BAYER_GRID_ALL	    (BAYER_GRID_RxC0 | BAYER_GRID_RxC1)

#define BAYER_GRID_RED	    (BAYER_GRID_R0C0)
#define BAYER_GRID_GREEN1   (BAYER_GRID_R0C1)
#define BAYER_GRID_GREEN2   (BAYER_GRID_R1C0)
#define BAYER_GRID_GREEN    (BAYER_GRID_GREEN1 | BAYER_GRID_GREEN2)
#define BAYER_GRID_BLUE	    (BAYER_GRID_R1C1)

//***********************************************************************
// RGB color Constants
//***********************************************************************
#define RGB_COLOR_RED	    (0x01)
#define RGB_COLOR_GREEN	    (0x02)
#define RGB_COLOR_BLUE	    (0x04)
#define RGB_COLOR_ALL	    (0x07)

// Typedefs
typedef struct
{
    tU32    *HB;    	    // histogram buffer of size HB[HBsize]
    tU32    HBextent;	    // histogram value maximum, e.g. 4096 for 12-bit
    tU32    nPixels;	    // number pixels represented in HB
    tU16    Min;	    // minimum pixel value
    tU16    Max;	    // maximum pixel value
    tU16    Mean;	    // mean pixel value
} tHIST;

// external variables

// Function Prototypes
tU32	TiffFirstIfd(void);
int	TiffGetTagData(tU16, tU32, tU32, tU32, tU32, void *);
int 	TiffGetXmpItem(char *, char *, char *, int);
int 	HuffmannMakeCoderTable(tDHT *, tU16 **, tU16 **);
int 	HuffmannMakeDecoderTable(tDHT *, tU8 **, tU8**, tU8**);
tSIZE 	FinishedSize(char *);
int 	FindTrim(tPROP *);
int 	MakeSimpleHistogram(tPROP *);
int 	MakeHistogram(tPROP *, tU16, tHIST *);
int 	WriteTiff(tPROP *, tU16, FILE *);

#endif _RAW_UTL_H_
