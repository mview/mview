#ifndef _CR2_CODEC_H_
#define _CR2_CODEC_H_
/***********************************************************************
* File: 	CrwCodec.h
* Change Info:
*   02/19/07	HLS - Began.
* Description:	Header file for Cr2COdec.cpp.
************************************************************************/

// Various constants

// Other Constants

// Type Definitions
#ifndef STD_TYPES
#define STD_TYPES
typedef char	    	tI8;
typedef unsigned char 	tU8;
typedef short	    	tI16;
typedef unsigned short	tU16;
typedef long	    	tI32;
typedef unsigned long	tU32;
#endif
    
// external variables

// Function Prototypes
int 	Cr2DistroyImage(tPROP *);
int 	Cr2Coder(tPROP *);
int 	Cr2Decoder(tPROP *);
int 	Cr2GetProperties(tPROP *);
int 	Cr2GetScrambleRowSlice(tU16 *, tU16 *, tU32, tPROP *);
int 	Cr2LosslessJpgDecoder(tPROP *);
int 	Cr2GetHuffmannTableProperties(tPROP *);
int 	Cr2HandleStartOfFrame(tPROP *);
int 	Cr2PutUnscrambleRowSlice(tU16 *, tU16 *, tU32, tPROP *);
int 	Cr2GetLosslessJpgRow(tU16 *, tU8 *, tU8 *, tU8 *, tU8 *, tU8 *, tPROP *);
int 	Cr2ParseMakerNote(tU32, tPROP *);
int 	Cr2ParseIFD(tU32, tPROP *);
int 	Cr2ExtractJpg(void);

#endif _CR2_CODEC_H_
