#ifndef _MAKER_NOTE_H_
#define _MAKER_NOTE_H_
/***********************************************************************
* File: 	MakerNote.h
* Change Info:
*   02/20/07	HLS - Began.
* Description:	Header file for TIFF MakerNote for Canon.
* Reference:	Various web sites, search "MakerNote+Canon"
************************************************************************/

// nearly known Canon MakerNote tags
#define MN_CAMERA_SETTINGS_1	    (0x0001)	// array of tU16
#define MN_CAMERA_SETTINGS_2	    (0x0004)	// array of tU16
#define MN_CAMERA_MODEL	    	    (0x0006)	// string
#define MN_CAMERA_FW_VERSION	    (0x0007)	// string
#define MN_OWNER_NAME	    	    (0x0009)	// string
#define MN_CAMERA_SERIAL_NUM	    (0x000C)	// immediate data
#define MN_CUSTOM_FUNCTIONS	    (0x000F)	// array of tU16
#define MN_LENS_MODEL	    	    (0x0095)	// string

#endif _MAKER_NOTE_H_
