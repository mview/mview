#ifndef _TIFF_TAGS_H_
#define _TIFF_TAGS_H_
/***********************************************************************
* File: 	TiffTags.h
* Change Info:
*   02/19/07	HLS - Began.
* Description:	Header file containing various TIFF tag IDs.
* Reference:	http://www.awaresystems.be/imaging/tiff/tifftags
************************************************************************/

//***********************************************************************
// TIFF Data Type Constants
//***********************************************************************
#define TIF_BYTE	    (0x01)	// 8-bit byte
#define TIF_ASCII	    (0x02)	// 8-bit ASCII code
#define TIF_SHORT	    (0x03)	// 16-bit unsigned int
#define TIF_LONG	    (0x04)	// 32-bit unsigned int
#define TIF_RATIONAL	    (0x05)	// 2 LONGs - numerator then denomonator
#define TIF_SBYTE	    (0x06)	// 8-bit signed int
#define TIF_UNDEFINED	    (0x07)	// 8-bit contain anything
#define TIF_SSHORT	    (0x08)	// 16-bit signed int
#define TIF_SLONG	    (0x09)	// 32-bit signed int
#define TIF_SRATIONAL	    (0x0A)	// 2 SLONGs - numerator then denomonator
#define TIF_FLOAT	    (0x0B)	// 4-byte IEEE float
#define TIF_DOUBLE	    (0x0C)	// 8-byte IEEE float

//***********************************************************************
// Tiff Tag IDs
//***********************************************************************
// Baseline TIFF Tags
#define TIF_NEW_SUBFILETYPE		(0x00FE)    // A general indication of the kind of data contained in this subfile.
#define TIF_SUB_FILETYPE		(0x00FF)    // A general indication of the kind of data contained in this subfile.
#define TIF_IMAGE_WIDTH 		(0x0100)    // The number of columns in the image.
#define TIF_IMAGE_LENGTH		(0x0101)    // The number of rows in the image.
#define TIF_BITS_PER_SAMPLE		(0x0102)    // Number of bits per component.
#define TIF_COMPRESSION 		(0x0103)    // Compression scheme used on the image data.
#define TIF_PHOTOMETRIC 		(0x0106)    // The color space of the image data.
#define TIF_THRESHHOLDING		(0x0107)    // For black and white TIFF files that represent shades of gray, the technique used to convert from gray to black and white pixels.
#define TIF_CELL_WIDTH			(0x0108)    // The width of the dithering or halftoning matrix used to create a dithered or halftoned bilevel file.
#define TIF_CELL_LENGTH 		(0x0109)    // The length of the dithering or halftoning matrix used to create a dithered or halftoned bilevel file.
#define TIF_FILL_ORDER			(0x010A)    // The logical order of bits within a byte.
#define TIF_IMAGE_DESC			(0x010E)    // A string that describes the subject of the image.
#define TIF_MAKE			(0x010F)    // The scanner manufacturer.
#define TIF_MODEL			(0x0110)    // The scanner model name or number.
#define TIF_STRIP_OFFSETS		(0x0111)    // For each strip, the byte offset of that strip.
#define TIF_ORIENTATION 		(0x0112)    // The orientation of the image with respect to the rows and columns.
#define TIF_SAMPLES_PER_PIXEL		(0x0115)    // The number of components per pixel.
#define TIF_ROWS_PER_STRIP		(0x0116)    // The number of rows per strip.
#define TIF_STRIP_BYTE_COUNTS		(0x0117)    // For each strip, the number of bytes in the strip after compression.
#define TIF_MIN_SAMPLE_VALUE		(0x0118)    // The minimum component value used.
#define TIF_MAX_SAMPLE_VALUE		(0x0119)    // The maximum component value used.
#define TIF_X_RESOLUTION		(0x011A)    // The number of pixels per ResolutionUnit in the ImageWidth direction.
#define TIF_Y_RESOLUTION		(0x011B)    // The number of pixels per ResolutionUnit in the ImageLength direction.
#define TIF_PLANAR_CONFIG		(0x011C)    // How the components of each pixel are stored.
#define TIF_FREE_OFFSETS		(0x0120)    // For each string of contiguous unused bytes in a TIFF file, the byte offset of the string.
#define TIF_FREE_BYTE_COUNTS		(0x0121)    // For each string of contiguous unused bytes in a TIFF file, the number of bytes in the string.
#define TIF_GRAY_RESPONSE_UNIT		(0x0122)    // The precision of the information contained in the GrayResponseCurve.
#define TIF_GRAY_RESPONSE_CURVE 	(0x0123)    // For grayscale data, the optical density of each possible pixel value.
#define TIF_RESOLUTION_UNIT		(0x0128)    // The unit of measurement for XResolution and YResolution.
#define TIF_SOFTWARE			(0x0131)    // Name and version number of the software package(s) used to create the image.
#define TIF_DATE_TIME			(0x0132)    // Date and time of image creation.
#define TIF_ARTIST			(0x013B)    // Person who created the image.
#define TIF_HOST_COMPUTER		(0x013C)    // The computer and/or operating system in use at the time of image creation.
#define TIF_COLOR_MAP			(0x0140)    // A color map for palette color images.
#define TIF_EXTRA_SAMPLES		(0x0152)    // Description of extra components.
#define TIF_COPYRIGHT			(0x8298)    // Copyright notice.

// Extension TIFF tags
#define TIF_DOCUMENT_NAME		(0x010D)    // The name of the document from which this image was scanned.
#define TIF_PAGE_NAME			(0x011D)    // The name of the page from which this image was scanned.
#define TIF_X_POSITION			(0x011E)    // X position of the image.
#define TIF_Y_POSITION			(0x011F)    // Y position of the image.
#define TIF_T4_OPTIONS			(0x0124)    // Options for Group 3 Fax compression
#define TIF_T6_OPTIONS			(0x0125)    // Options for Group 4 Fax compression
#define TIF_PAGE_NUMBER 		(0x0129)    // The page number of the page from which this image was scanned.
#define TIF_TRANSFER_FUNCTION		(0x012D)    // Describes a transfer function for the image in tabular style.
#define TIF_PREDICTOR			(0x013D)    // A mathematical operator that is applied to the image data before an encoding scheme is applied.
#define TIF_WHITE_POINT 		(0x013E)    // The chromaticity of the white point of the image.
#define TIF_PRIMARY_CHROMATICITIES	(0x013F)    // The chromaticities of the primaries of the image.
#define TIF_HALFTONE_HINTS		(0x0141)    // Conveys to the halftone function the range of gray levels within a colorimetrically-specified image that should retain tonal detail.
#define TIF_TILE_WIDTH			(0x0142)    // The tile width in pixels., This is the number of columns in each tile.
#define TIF_TILE_LENGTH 		(0x0143)    // The tile length (height) in pixels., This is the number of rows in each tile.
#define TIF_TILE_OFFSETS		(0x0144)    // For each tile, the byte offset of that tile, as compressed and stored on disk.
#define TIF_TILE_BYTE_COUNTS		(0x0145)    // For each tile, the number of (compressed) bytes in that tile.
#define TIF_BAD_FAX_LINES		(0x0146)    // Used in the TIFF-F standard, denotes the number of 'bad' scan lines encountered by the facsimile device.
#define TIF_CLEAN_FAX_DATA		(0x0147)    // Used in the TIFF-F standard, indicates if 'bad' lines encountered during reception are stored in the data, or if 'bad' lines have been replaced by the receiver.
#define TIF_CONSECUTIVE_BAD_FAX_LINES	(0x0148)    // Used in the TIFF-F standard, denotes the maximum number of consecutive 'bad' scanlines received.
#define TIF_SUB_IFDS			(0x014A)    // Offset to child IFDs.
#define TIF_INK_SET			(0x014C)    // The set of inks used in a separated (PhotometricInterpretation=5) image.
#define TIF_INK_NAMES			(0x014D)    // The name of each ink used in a separated image.
#define TIF_NUMBER_INKS 		(0x014E)    // The number of inks.
#define TIF_DOT_RANGE			(0x0150)    // The component values that correspond to a 0% dot and 100% dot.
#define TIF_TARGET_PRINTER		(0x0151)    // A description of the printing environment for which this separation is intended.
#define TIF_SAMPLE_FORMAT		(0x0153)    // Specifies how to interpret each data sample in a pixel.
#define TIF_SMIN_SAMPLE_VALUE		(0x0154)    // Specifies the minimum sample value.
#define TIF_SMAX_SAMPLE_VALUE		(0x0155)    // Specifies the maximum sample value.
#define TIF_TRANSFER_RANGE		(0x0156)    // Expands the range of the TransferFunction.
#define TIF_CLIP_PATH			(0x0157)    // Mirrors the essentials of PostScript's path creation functionality.
#define TIF_X_CLIP_PATH_UNITS		(0x0158)    // The number of units that span the width of the image, in terms of integer ClipPath coordinates.
#define TIF_Y_CLIP_PATH_UNITS		(0x0159)    // The number of units that span the height of the image, in terms of integer ClipPath coordinates.
#define TIF_INDEXED			(0x015A)    // Aims to broaden the support for indexed images to include support for any color space.
#define TIF_JPEG_TABLES 		(0x015B)    // JPEG quantization and/or Huffman tables.
#define TIF_OPI_PROXY			(0x015F)    // OPI-related.
#define TIF_GLOBAL_PARAMETERS_IFD	(0x0190)    // Used in the TIFF-FX standard to point to an IFD containing tags that are globally applicable to the complete TIFF file.
#define TIF_PROFILE_TYPE		(0x0191)    // Used in the TIFF-FX standard, denotes the type of data stored in this file or IFD.
#define TIF_FAX_PROFILE 		(0x0192)    // Used in the TIFF-FX standard, denotes the 'profile' that applies to this file.
#define TIF_CODIN_GMETHODS		(0x0193)    // Used in the TIFF-FX standard, indicates which coding methods are used in the file.
#define TIF_VERSION_YEAR		(0x0194)    // Used in the TIFF-FX standard, denotes the year of the standard specified by the FaxProfile field.
#define TIF_MODE_NUMBER 		(0x0195)    // Used in the TIFF-FX standard, denotes the mode of the standard specified by the FaxProfile field.
#define TIF_DECODE			(0x01B1)    // Used in the TIFF-F and TIFF-FX standards, holds information about the ITULAB (PhotometricInterpretation = 10) encoding.
#define TIF_DEFAULT_IMAGE_COLOR 	(0x01B2)    // Defined in the Mixed Raster Content part of RFC 2301, is the default color needed in areas where no image is available.
#define TIF_JPEG_PROC			(0x0200)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_JPEG_INTERCHANGE_FMT	(0x0201)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_JPEG_INTERCHANGE_FMT_LEN	(0x0202)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_JPEG_RESTART_INTERVAL	(0x0203)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_JPEG_LOSSLESS_PREDICTORS	(0x0205)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_JPEG_POINT_TRANSFORMS	(0x0206)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_JPEGQ_TABLES		(0x0207)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_JPEG_DC_TABLES		(0x0208)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_JPEG_AC_TABLES		(0x0209)    // Old-style JPEG, TechNote2 invalidates this.
#define TIF_YCBCR_COEFFICIENTS		(0x0211)    // The transformation from RGB to YCbCr image data.
#define TIF_YCBCR_SUBSAMPLING		(0x0212)    // Specifies the subsampling factors used for the chrominance components of a YCbCr image.
#define TIF_YCBCR_POSITIONING		(0x0213)    // Specifies the positioning of subsampled chrominance components relative to luminance samples.
#define TIF_REFERENCE_BLACK_WHITE	(0x0214)    // Specifies a pair of headroom and footroom image data values (codes) for each pixel component.
#define TIF_STRIP_ROW_COUNTS		(0x022F)    // Defined in the Mixed Raster Content part of RFC 2301, used to replace RowsPerStrip for IFDs with variable-sized strips.
#define TIF_XMP 			(0x02BC)    // XML packet containing XMP metadata
#define TIF_IMAGE_ID			(0x800D)    // OPI-related.
#define TIF_IMAGE_LAYER 		(0x87AC)    // Defined in the Mixed Raster Content part of RFC 2301, used to denote the particular function of this Image in the mixed raster scheme.

// Private TIFF tags
#define TIF_WANG			(0x80A4)    // Annotation Annotation data, as used in 'Imaging for Windows'.
#define TIF_MD1				(0x82A5)    // FileTag, Specifies the pixel data format encoding in the Molecular Dynamics GEL file format.
#define TIF_MD2				(0x82A6)    // ScalePixel, Specifies a scale factor in the Molecular Dynamics GEL file format.
#define TIF_MD3				(0x82A7)    // ColorTable, Used to specify the conversion from 16bit to 8bit in the Molecular Dynamics GEL file format.
#define TIF_MD4				(0x82A8)    // LabName, Name of the lab that scanned this file, as used in the Molecular Dynamics GEL file format.
#define TIF_MD5				(0x82A9)    // SampleInfo, Information about the sample, as used in the Molecular Dynamics GEL file format.
#define TIF_MD6				(0x82AA)    // PrepDate, Date the sample was prepared, as used in the Molecular Dynamics GEL file format.
#define TIF_MD7				(0x82AB)    // PrepTime, Time the sample was prepared, as used in the Molecular Dynamics GEL file format.
#define TIF_MD8				(0x82AC)    // FileUnits, Units for data in this file, as used in the Molecular Dynamics GEL file format.
#define TIF_MODEL_PIXEL_SCALE_TAG	(0x830E)    // Used in interchangeable GeoTIFF files.
#define TIF_IPTC			(0x83BB)    // IPTC (International Press Telecommunications Council) metadata.
#define TIF_INGR1			(0x847E)    // Packet Data Tag, Intergraph Application specific storage.
#define TIF_INGR2			(0x847F)    // Flag Registers, Intergraph Application specific flags.
#define TIF_IRASB			(0x8480)    // Transformation Matrix	 Originally part of Intergraph's GeoTIFF tags, but likely understood by IrasB only.
#define TIF_MODEL_TIEPOINT_TAG		(0x8482)    // Originally part of Intergraph's GeoTIFF tags, but now used in interchangeable GeoTIFF files.
#define TIF_MODEL_TRANSFORMATION_TAG	(0x85D8)    // Used in interchangeable GeoTIFF files.
#define TIF_PHOTOSHOP			(0x8649)    // Collection of Photoshop 'Image Resource Blocks'.
#define TIF_EXIF			(0x8769)    // IFD - A pointer to the Exif IFD.
#define TIF_ICC 			(0x8773)    // Profile - ICC profile data.
#define TIF_GEO_KEY_DIRECTORY_TAG	(0x87AF)    // Used in interchangeable GeoTIFF files.
#define TIF_GEO_DOUBLE_PARAMS_TAG	(0x87B0)    // Used in interchangeable GeoTIFF files.
#define TIF_GEO_ASCII_PARAMS_TAG	(0x87B1)    // Used in interchangeable GeoTIFF files.
#define TIF_GPS 			(0x8825)    // IFD A pointer to the Exif-related GPS Info IFD.
#define TIF_HYLA_FAX1			(0x885C)    // FaxRecvParams   Used by HylaFAX.
#define TIF_HYLA_FAX2			(0x885D)    // FaxSubAddress   Used by HylaFAX.
#define TIF_HYLA_FAX3			(0x885E)    // FaxRecvTime     Used by HylaFAX.
#define TIF_IMAGE_SOURCE_DATA		(0x935C)    // Used by Adobe Photoshop.
#define TIF_INTEROPERABILITY		(0xA005)    // IFD    A pointer to the Exif-related Interoperability IFD.
#define TIF_GDAL_METADATA		(0xA480)    // Used by the GDAL library, holds an XML list of name=value 'metadata' values about the image as a whole, and about specific samples.
#define TIF_GDAL_NODATA 		(0xA481)    // Used by the GDAL library, contains an ASCII encoded nodata or background pixel value.
#define TIF_OCE1 			(0xC427)    // Scanjob Description Used in the Oce scanning process.
#define TIF_OCE2 			(0xC428)    // Application Selector	   Used in the Oce scanning process.
#define TIF_OCE3 			(0xC429)    // Identification Number	   Used in the Oce scanning process.
#define TIF_OCE4 			(0xC42A)    // ImageLogic Characteristics  Used in the Oce scanning process.
#define TIF_DNG_VERSION 		(0xC612)    // Used in IFD 0 of DNG files.
#define TIF_DNG_BACKWAR_DVERSION	(0xC613)    // Used in IFD 0 of DNG files.
#define TIF_UNIQUE_CAMERA_MODEL 	(0xC614)    // Used in IFD 0 of DNG files.
#define TIF_LOCALIZED_CAMERA_MODEL	(0xC615)    // Used in IFD 0 of DNG files.
#define TIF_CFA_PLANE_COLOR		(0xC616)    // Used in Raw IFD of DNG files.
#define TIF_CFA_LAYOUT			(0xC617)    // Used in Raw IFD of DNG files.
#define TIF_LINEARIZATION_TABLE 	(0xC618)    // Used in Raw IFD of DNG files.
#define TIF_BLACK_LEVEL_REPEAT_DIM	(0xC619)    // Used in Raw IFD of DNG files.
#define TIF_BLACK_LEVEL 		(0xC61A)    // Used in Raw IFD of DNG files.
#define TIF_BLACK_LEVEL_DELTA_H 	(0xC61B)    // Used in Raw IFD of DNG files.
#define TIF_BLACK_LEVEL_DELTA_V 	(0xC61C)    // Used in Raw IFD of DNG files.
#define TIF_WHITE_LEVEL 		(0xC61D)    // Used in Raw IFD of DNG files.
#define TIF_DEFAULT_SCALE		(0xC61E)    // Used in Raw IFD of DNG files.
#define TIF_DEFAULT_CROP_ORIGIN 	(0xC61F)    // Used in Raw IFD of DNG files.
#define TIF_DEFAULT_CROP_SIZE		(0xC620)    // Used in Raw IFD of DNG files.
#define TIF_COLOR_MATRIX1		(0xC621)    // Used in IFD 0 of DNG files.
#define TIF_COLOR_MATRIX2		(0xC622)    // Used in IFD 0 of DNG files.
#define TIF_CAMERA_CALIBRATION1 	(0xC623)    // Used in IFD 0 of DNG files.
#define TIF_CAMERA_CALIBRATION2 	(0xC624)    // Used in IFD 0 of DNG files.
#define TIF_REDUCTION_MATRIX1		(0xC625)    // Used in IFD 0 of DNG files.
#define TIF_REDUCTION_MATRIX2		(0xC626)    // Used in IFD 0 of DNG files.
#define TIF_ANALOG_BALANCE		(0xC627)    // Used in IFD 0 of DNG files.
#define TIF_ASSHOT_NEUTRAL		(0xC628)    // Used in IFD 0 of DNG files.
#define TIF_AS_SHOT_WHITE_XY		(0xC629)    // Used in IFD 0 of DNG files.
#define TIF_BASELINE_EXPOSURE		(0xC62A)    // Used in IFD 0 of DNG files.
#define TIF_BASELINE_NOISE		(0xC62B)    // Used in IFD 0 of DNG files.
#define TIF_BASELINE_SHARPNESS		(0xC62C)    // Used in IFD 0 of DNG files.
#define TIF_BAYER_GREEN_SPLIT		(0xC62D)    // Used in Raw IFD of DNG files.
#define TIF_LINEAR_RESPONSE_LIMIT	(0xC62E)    // Used in IFD 0 of DNG files.
#define TIF_CAMERA_SERIAL_NUMBER	(0xC62F)    // Used in IFD 0 of DNG files.
#define TIF_LENS_INFO			(0xC630)    // Used in IFD 0 of DNG files.
#define TIF_CHROMA_BLUR_RADIUS		(0xC631)    // Used in Raw IFD of DNG files.
#define TIF_ANTI_ALIAS_STRENGTH 	(0xC632)    // Used in Raw IFD of DNG files.
#define TIF_DNG_PRIVATE_DATA		(0xC634)    // Used in IFD 0 of DNG files.
#define TIF_MAKER_NOTE_SAFETY		(0xC635)    // Used in IFD 0 of DNG files.
#define TIF_CALIBRATION_ILLUMINANT1	(0xC65A)    // Used in IFD 0 of DNG files.
#define TIF_CALIBRATION_ILLUMINANT2	(0xC65B)    // Used in IFD 0 of DNG files.
#define TIF_BESTQUALITY_SCALE		(0xC65C)    // Used in Raw IFD of DNG files.
#define TIF_ALIAS			(0xC660)    // Layer Metadata	 Alias Sketchbook Pro layer usage description.
#define TIF_CR2_SLICE	    	    	(0xC640)    // Used in Canon CR2 files

// TIFF Tag Reference, Exif Tags - this is a subdirectory
#define TIF_EXPOSURE_TIME		(0x829A)    // Exposure time, given in seconds.
#define TIF_FNUMBER			(0x829D)    // The F number.
#define TIF_EXPOSURE_PROGRAM		(0x8822)    // The class of the program used by the camera to set exposure when the picture\n is taken.
#define TIF_SPECTRAL_SENSITIVITY	(0x8824)    // Indicates the spectral sensitivity of each channel of the camera used.
#define TIF_ISO_SPEED_RATINGS		(0x8827)    // Indicates the ISO Speed and ISO Latitude of the camera or input device as\n specified in ISO 12232.
#define TIF_OECF			(0x8828)    // Indicates the Opto-Electric Conversion Function (OECF) specified in ISO 14524.
#define TIF_EXIF_VERSION		(0x9000)    // The version of the supported Exif standard.
#define TIF_DATE_TIME_ORIGINAL		(0x9003)    // The date and time when the original image data was generated.
#define TIF_DATE_TIME_DIGITIZED 	(0x9004)    // The date and time when the image was stored as digital data.
#define TIF_COMPONENTS_CONFIGURATION	(0x9101)    // Specific to compressed data; specifies the channels and complements\n PhotometricInterpretation
#define TIF_COMPRESSED_BITS_PER_PIXEL	(0x9102)    // Specific to compressed data; states the compressed bits per pixel.
#define TIF_SHUTTER_SPEED_VALUE 	(0x9201)    // Shutter speed.
#define TIF_APERTURE_VALUE		(0x9202)    // The lens aperture.
#define TIF_BRIGHTNESS_VALUE		(0x9203)    // The value of brightness.
#define TIF_EXPOSURE_BIAS_VALUE 	(0x9204)    // The exposure bias.
#define TIF_MAXAPERTURE_VALUE		(0x9205)    // The smallest F number of the lens.
#define TIF_SUBJECT_DISTANCE		(0x9206)    // The distance to the subject, given in meters.
#define TIF_METERING_MODE		(0x9207)    // The metering mode.
#define TIF_LIGHT_SOURCE		(0x9208)    // The kind of light source.
#define TIF_FLASH			(0x9209)    // Indicates the status of flash when the image was shot.
#define TIF_FOCAL_LENGTH		(0x920A)    // The actual focal length of the lens, in mm.
#define TIF_SUBJECT_AREA		(0x9214)    // Indicates the location and area of the main subject in the overall scene.
#define TIF_MAKER_NOTE			(0x927C)    // Manufacturer specific information.
#define TIF_USER_COMMENT		(0x9286)    // Keywords or comments on the image; complements ImageDescription.
#define TIF_SUBSEC_TIME 		(0x9290)    // A tag used to record fractions of seconds for the DateTime tag.
#define TIF_SUBSEC_TIME_ORIGINAL	(0x9291)    // A tag used to record fractions of seconds for the DateTimeOriginal tag.
#define TIF_SUBSEC_TIME_DIGITIZED	(0x9292)    // A tag used to record fractions of seconds for the DateTimeDigitized tag.
#define TIF_FLASHPIX_VERSION		(0xA000)    // The Flashpix format version supported by a FPXR file.
#define TIF_COLOR_SPACE 		(0xA001)    // The color space information tag is always recorded as the color space\n specifier.
#define TIF_PIXEL_X_DIMENSION		(0xA002)    // Specific to compressed data; the valid width of the meaningful image.
#define TIF_PIXEL_Y_DIMENSION		(0xA003)    // Specific to compressed data; the valid height of the meaningful image.
#define TIF_RELATED_SOUND_FILE		(0xA004)    // Used to record the name of an audio file related to the image data.
#define TIF_FLASH_ENERGY		(0xA20B)    // Indicates the strobe energy at the time the image is captured, as measured\n in Beam Candle Power Seconds
#define TIF_SPATIAL_FREQUENCY_RESPONSE	(0xA20C)    // Records the camera or input device spatial frequency table and SFR values in\n the direction of image width, image height, and diagonal direction, as\n specified in ISO 12233.
#define TIF_FOCAL_PLANE_X_RESOLUTION	(0xA20E)    // Indicates the number of pixels in the image width (X) direction per\n FocalPlaneResolutionUnit on the camera focal plane.
#define TIF_FOCAL_PLANE_Y_RESOLUTION	(0xA20F)    // Indicates the number of pixels in the image height (Y) direction per\n FocalPlaneResolutionUnit on the camera focal plane.
#define TIF_FOCAL_PLANE_RESOLUTION_UNIT (0xA210)    // Indicates the unit for measuring FocalPlaneXResolution and\n FocalPlaneYResolution.
#define TIF_SUBJECT_LOCATION		(0xA214)    // Indicates the location of the main subject in the scene.
#define TIF_EXPOSURE_INDEX		(0xA215)    // Indicates the exposure index selected on the camera or input device at the\n time the image is captured.
#define TIF_SENSING_METHOD		(0xA217)    // Indicates the image sensor type on the camera or input device.
#define TIF_FILE_SOURCE 		(0xA300)    // Indicates the image source.
#define TIF_SCENE_TYPE			(0xA301)    // Indicates the type of scene.
#define TIF_CFA_PATTERN 		(0xA302)    // Indicates the color filter array (CFA) geometric pattern of the image sensor\n when a one-chip color area sensor is used.
#define TIF_CUSTOM_RENDERED		(0xA401)    // Indicates the use of special processing on image data, such as rendering\n geared to output.
#define TIF_EXPOSURE_MODE		(0xA402)    // Indicates the exposure mode set when the image was shot.
#define TIF_WHITE_BALANCE		(0xA403)    // Indicates the white balance mode set when the image was shot.
#define TIF_DIGITAL_ZOOM_RATIO		(0xA404)    // Indicates the digital zoom ratio when the image was shot.
#define TIF_FOCAL_LENGTH_IN_35MM_FILM	(0xA405)    // Indicates the equivalent focal length assuming a 35mm film camera, in mm.
#define TIF_SCENE_CAPTURE_TYPE		(0xA406)    // Indicates the type of scene that was shot.
#define TIF_GAIN_CONTROL		(0xA407)    // Indicates the degree of overall image gain adjustment.
#define TIF_CONTRAST			(0xA408)    // Indicates the direction of contrast processing applied by the camera when the\n image was shot.
#define TIF_SATURATION			(0xA409)    // Indicates the direction of saturation processing applied by the camera when\n the image was shot.
#define TIF_SHARPNESS			(0xA40A)    // Indicates the direction of sharpness processing applied by the camera when\n the image was shot.
#define TIF_DEVICE_SETTING_DESCRIPTION	(0xA40B)    // This tag indicates information on the picture-taking conditions of a\n particular camera model.
#define TIF_SUBJECT_DISTANCE_RANGE	(0xA40C)	// blockIndicates the distance to the subject.
#define TIF_IMAGE_UNIQUE_ID		(0xA420)    // Indicates an identifier assigned uniquely to each image.

// TIFF Tag Reference, GPS Tags - this is probably a subdirectory
#define TIF_GPS_VERSION_ID		(0x0000)    // Indicates the version of GPSInfoIFD.
#define TIF_GPS_LATITUDE_REF		(0x0001)    // Indicates whether the latitude is north or south latitude.
#define TIF_GPS_LATITUDE		(0x0002)    // Indicates the latitude.
#define TIF_GPS_LONGITUDE_REF		(0x0003)    // Indicates whether the longitude is east or west longitude.
#define TIF_GPS_LONGITUDE		(0x0004)    // Indicates the longitude.
#define TIF_GPS_ALTITUDE_REF		(0x0005)    // Indicates the altitude used as the reference altitude.
#define TIF_GPS_ALTITUDE		(0x0006)    // Indicates the altitude based on the reference in GPSAltitudeRef.
#define TIF_GPS_TIME_STAMP		(0x0007)    // Indicates the time as UTC (Coordinated Universal Time).
#define TIF_GPS_SATELLITES		(0x0008)    // Indicates the GPS satellites used for measurements.
#define TIF_GPS_STATUS			(0x0009)    // Indicates the status of the GPS receiver when the image is recorded.
#define TIF_GPS_MEASURE_MODE		(0x000A)    // Indicates the GPS measurement mode.
#define TIF_GPS_DOP			(0x000B)    // Indicates the GPS DOP (data degree of precision).
#define TIF_GPS_SPEED_REF		(0x000C)    // Indicates the unit used to express the GPS receiver speed of movement.
#define TIF_GPS_SPEED			(0x000D)    // Indicates the speed of GPS receiver movement.
#define TIF_GPS_TRACK_REF		(0x000E)    // Indicates the reference for giving the direction of GPS receiver movement.
#define TIF_GPS_TRACK			(0x000F)    // Indicates the direction of GPS receiver movement.
#define TIF_GPS_IMG_DIRECTION_REF	(0x0010)    // Indicates the reference for giving the direction of the image when it is\n captured.
#define TIF_GPS_IMG_DIRECTION		(0x0011)    // Indicates the direction of the image when it was captured.
#define TIF_GPS_MAP_DATUM		(0x0012)    // Indicates the geodetic survey data used by the GPS receiver.
#define TIF_GPS_DEST_LATITUDE_REF	(0x0013)    // Indicates whether the latitude of the destination point is north or south\n latitude.
#define TIF_GPS_DEST_LATITUDE		(0x0014)    // Indicates the latitude of the destination point.
#define TIF_GPS_DEST_LONGITUDE_REF	(0x0015)    // Indicates whether the longitude of the destination point is east or west\n longitude.
#define TIF_GPS_DEST_LONGITUDE		(0x0016)    // Indicates the longitude of the destination point.
#define TIF_GPS_DEST_BEARING_REF	(0x0017)    // Indicates the reference used for giving the bearing to the destination point.
#define TIF_GPS_DEST_BEARING		(0x0018)    // Indicates the bearing to the destination point.
#define TIF_GPS_DEST_DISTANCE_REF	(0x0019)    // Indicates the unit used to express the distance to the destination point.
#define TIF_GPS_DEST_DISTANCE		(0x001A)    // Indicates the distance to the destination point.
#define TIF_GPS_PROCESSING_METHOD	(0x001B)    // A character string recording the name of the method used for location finding.
#define TIF_GPS_AREA_INFORMATION	(0x001C)    // A character string recording the name of the GPS area.
#define TIF_GPS_DATE_STAMP		(0x001D)    // A character string recording date and time information relative to UTC\n (Coordinated Universal Time).
#define TIF_GPS_DIFFERENTIAL		(0x001E)    // Indicates whether differential correction is applied to the GPS receiver.

//***********************************************************************
// Tiff Tag Enumeration
//***********************************************************************
// TAG NEW_SUBFILE
#define TIF_IMAGE_REDUCED_RESOLUTION		    0x0001
#define TIF_IMAGE_PART_OF_MULTI_PAGE		    0x0002
#define TIF_IMAGE_TRANSPARENCY_MASK		    0x0004

// TAG SUBFILE
#define TIF_FULL_RESOLUTION_IMAGE		    1
#define TIF_REDUCED_RESOLUTION_IMAGE		    2
#define TIF_MULTIPAGE_IMAGE			    3

// TAG COMPRESSION
#define TIF_NO_COMPRESSION			    1
#define TIF_HUFFMAN_COMPRESSION 		    2
#define TIF_GROUP_3FAX_COMPRESSION  	    	    3
#define TIF_GROUP_4FAX_COMPRESSION  	    	    4
#define TIF_LZW_COMPRESSION 	  	    	    5
#define TIF_JPG_COMPRESSION 	  	    	    6
#define TIF_PACK_BITS_COMPRESSION		    32773

// TAG PHOTOMETRIC
#define TIF_WHITE_IS_ZERO			    0
#define TIF_BLACK_IS_ZERO			    1
#define TIF_RGB 				    2
#define TIF_RGB_PALETTE 			    3
#define TIF_TRANSPARENCY_MASK			    4
#define TIF_CMYK				    5
#define TIF_Y_CB_CR				    6
#define TIF_CIE_LAB				    8

// TAG THRESHOLDING
#define TIF_NO_DITHERING			    1	// default
#define TIF_DITHERED_IMAGE			    2
#define TIF_RANDOMIZED_IMAGE			    3

// TAG FILL_ORDER
#define TIF_LOW_PIXEL_COLUMNS_AT_HIGH_ORDER_BITS    1
#define TIF_LOW_PIXEL_COLUMNS_AT_LOW_ORDER_BITS     2

// TAG ORIENTATION
// RRR_CCC = where 0th row(RRR) and 0th column(CCC) are located
#define TIF_TOP_LEFT				    1	// default
#define TIF_ORIENT_NORMAL			    1
#define TIF_TOP_RIGHT				    2
#define TIF_ORIENT_FLIP_VERT			    2
#define TIF_BOTTOM_RIGHT			    3
#define TIF_ORIENT_ROTATE_180			    3
#define TIF_BOTTOM_LEFT 			    4
#define TIF_ORIENT_FLIP_HORIZ			    4
#define TIF_LEFT_TOP				    5
#define TIF_ORIENT_ROTATE_RIGHT_90_FLIP_VERT	    5
#define TIF_RIGHT_TOP				    6
#define TIF_ORIENT_ROTATE_RIGHT_90		    6
#define TIF_RIGHT_BOTTOM			    7
#define TIF_ORIENT_ROTATE_LEFT_90_FLIP_VERT	    7
#define TIF_LEFT_BOTTOM 			    8
#define TIF_ORIENT_ROTATE_LEFT_90		    8

// TAG PLANAR_CONFIG
#define TIF_CHUNKY_FORMAT			    1	// default
#define TIF_PLANAR_FORMAT			    2

// TAG GRAY_RESPONSE_UNIT
#define TIF_TENTHS_OF_UNIT			    1
#define TIF_HUNDREDTHS_OF_UNIT			    2
#define TIF_THOUSANDTHS_OF_UNIT 		    3
#define TIF_TEN_THOUSANDTHS_OF_UNIT		    4
#define TIF_HUNDRED_THOUSANDTHS_OF_UNIT 	    5

// TAG RESOLUTION_UNIT
#define TIF_INVALID_UNITS   	    	    	    0
#define TIF_NO_UNITS				    1
#define TIF_INCH_UNITS				    2	// default
#define TIF_CENTIMETER_UNITS			    3

#endif _TIFF_TAGS_H_
