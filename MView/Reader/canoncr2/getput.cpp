/***********************************************************************
* File: 	getput.cpp
* Functions:
*   	    	GetBits()
*   	    	PutBits()
*   	    	IntelMotoByteSwap() 
*   	    	TestPlatformType()
*   	    	TestIntelMotoFileSignature()
*   	    	Get1()
*   	    	Get2F()
*   	    	Get4F()
*   	    	Get8F()
*   	    	Get2R()
*   	    	Get4R()
*   	    	Get8R()
*   	    	Put1()
*   	    	Put2F()
*   	    	Put4F()
*   	    	Put8F()
*   	    	Put2R()
*   	    	Put4R()
*   	    	Put8R()
*   	    	Oops()
* Change Info:
*   09/21/08	HLS - Oops, s/b "if (!fwrite())" but was "if (fwrite())" for
*   	    	PUTBITS_EOF write of "file size would be odd, pad with NULL
*   	    	to make even."
*   03/06/07	HLS - Added TestIntelMotoFileSignature().
*   02/27/07	HLS - Fixed PutBits() for code of PUTBITS_EOF.
*   02/17/03	HLS - Created, GetBits() & PutBits() from elsewhere
* Description:	Primary get() and put() functions.
************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "jpeg.h"
#include "getput.h"

/* Global data */
tU16	PlatformType;	    	// Intel or Moto
tU16	IntelMotoFileSignature; // Intel or Moto
FILE	*ifp = NULL;	    	// input file pointer
FILE	*ofp = NULL;	    	// output file pointer

/* Intel & Motorola byte handling, and shortcuts */
tU16	(*Get2)()   	 = Get2F;
tU32	(*Get4)()   	 = Get4F;
double	(*Get8)()   	 = Get8F;
tU16	(*Get2I)()  	 = Get2F;
tU32	(*Get4I)()  	 = Get4F;
double	(*Get8I)()  	 = Get8F;
tU16	(*Get2M)()  	 = Get2R;
tU32	(*Get4M)()  	 = Get4R;
double	(*Get8M)()  	 = Get8R;
void	(*Put2)(tU16)    = Put2F;
void	(*Put4)(tU32)    = Put4F;
void	(*Put8)(double)  = Put8F;
void	(*Put2I)(tU16)   = Put2F;
void	(*Put4I)(tU32)   = Put4F;
void	(*Put8I)(double) = Put8F;
void	(*Put2M)(tU16)   = Put2R;
void	(*Put4M)(tU32)   = Put4R;
void	(*Put8M)(double) = Put8R;

/***************************************************************************
* Function: 	GetBits()
* Returns:  	left justified bits from stream p[] OR see description
* Description:	Up shifts the bit stream of file ifp by nBits.  New bits are
*   	    	always left justified.  Returns 0xFFFF if the input buffer
*   	    	overflows. Various negative values for nBits provide
*		initialization and status.  If nBits is GETBITS_INIT, this>> 
*		engine is initialized and p must point to the buffer start.  
*		If nBits is GETBITS_FILE_END, p is the address of the buffer
*		end so that buffer overflow checking can be done - note that
*		GETBITS_INIT must preceed GETBITS_FILE_END.  If nBits is
*		GETBITS_LOW_INDEX it returns the low 16 bits of its current
*		file index, if nBits is GETBITS_HIGH_INDEX it returns the
*		high 16 bits of its current file index.  If nBits is
*		GETBITS_BIT_STATUS it returns the number valid ledft
*   	    	justified bits in the previous normal call to GetBits(), 
*   	    	that is 16 means all bits, whereas 1 means only the msb if
*   	    	valid and 0 means none are valid.  If nBits is
*   	    	GETBITS_EOF_STATUS it returns 1 if the buffer has reached
*   	    	its end or has encountered a JPG_MARK_EOI flag.  If nBits
*   	    	is any other negative value it returns 0.
* Bugs:     	Unpredictable results if nBits > 16.
***************************************************************************/
tU16 	GetBits(
tU8	*p,
int	nBits)
{
    static tU32 Bits;
    static int  ValidResidue;
    static tU32 i = 0;
    static tU32 BufSize = 0xFFFFFFFF;
    static tU8  *BufStart = NULL;
    int  j;
    tU32 NewBits;

    if (nBits < 0)
    { // various initialization
    	if (nBits == GETBITS_INIT)
    	{ // initialize, prime the pump!
    	    BufStart = p;
    	    i = 0;
    	    for (j = 4; j; j--)
    	    {
    	    	Bits <<= 8;
		register tU8 c;
    	    	Bits |= c = p[i++];
		if (c == JPG_MARK_FF)
		{
		    c = p[i++];
		    if (c != JPG_MARK_ZERO)
		    {
		    	fprintf(stderr, "corrupt RAW bits file\n");
			return(0);
		    }
		}
    	    }
    	    ValidResidue = 8;
    	    return(Bits >> 16);
    	}
	else if (nBits == GETBITS_FILE_END)
	{ // initizlize buffer size for overflow checking
	    BufSize = p - BufStart;
	    return(0);
	}
	else if (nBits == GETBITS_LOW_INDEX)
	{ // return lower 16 bits of index
	    return(i);
	}
	else if (nBits == GETBITS_HIGH_INDEX)
	{ // return upper 16 bits of index
	    return(i >> 16);
	}
	else if (nBits == GETBITS_BIT_STATUS)
	{ // return number valid left justaified bits in (upper word of) Bits
	    int ValidBits;
	    ValidBits = 24 + ValidResidue;
	    if (ValidBits >= 16) return(16);
	    else if (ValidBits <= 0)  return(0);
	    else return(ValidBits);
	}
	else if (nBits == GETBITS_EOF_STATUS)
	{ // if at or past end of image buffer, return 1, else 0
	    return((i >= BufSize) ? 1 : 0);
	}
	else
	{ 
	    return(0);
	}
    }
    
    Bits <<= nBits;
    ValidResidue -= nBits;
    if (i >= BufSize)
    { // near or past end of buffer, special handling
    	Bits |= (1 << (8 - ValidResidue)) - 1;
    	return(Bits >> 16);
    }
    
    for (NewBits = 0; ValidResidue <= 0; ValidResidue += 8)
    {
    	NewBits <<= 8;
	register tU8 c;
    	NewBits |= c = p[i++];
	if (c == JPG_MARK_FF)
	{
	    c = p[i++];
	    if (c == JPG_MARK_ZERO)
	    { // its the expected 0xff 0x00 marker pattern, just dispose of 0x00
	    	continue;
	    }
	    else if (c == JPG_MARK_EOI)
	    { // end of image mark encountered, force buffer size to this
	    	i -= 2;     	// put index at end of image marker start
	    	BufSize = i;
		Bits |= (1 << (8 - ValidResidue)) - 1;
		return(Bits >> 16);
	    }
	    else
	    { // oops, unexpected, should not occur!
	    	fprintf(stderr, 
		    "unexpected marker (0x%02X) @ ofst=0x%X in RAW data, aborting\n",
		    p[i-1], i-2);
		return(0);
	    }
	}
    }
    Bits |= NewBits << (8 - ValidResidue);
    
    return(Bits >> 16);
} /* end GetBits() */

/***************************************************************************
* Function: 	PutBits()
* Returns:  	bytes put to FILE *ofp, OR see description
* Description:	Upshifts Bits by nBits into a temporary working buffer WB[]
*   	    	inserts JPEG markers as required, and outputs WB[] contents
*   	    	to FILE *ofp either when buffer fills or when an end of file
*   	    	command PUTBITS_EOF is given.  Returns number bytes put to
*   	    	FILE *ofp when nBits command is either PUTBITS_EOF or 
*   	    	PUTBITS_STATUS, otherwise returns 0.
*   	    	When nBits is negative, special action is
*   	    	taken.  When nBits = PUTBITS_INIT_OFP, Bits is a pointer to
*   	    	an alredy open write binary "wb" FILE *ofp.  When nBits =
*   	    	PUTBITS_INIT_BUF, Bits is a pointer to a temporary working
*   	    	buffer WB[], and the size of this buffer is conveyed when
*   	    	nBits = PUTBITS_BUF_SIZE with Bit giving the size. When
*   	    	nBits = PUTBITS_EOF, the buffer is formally purged by first
*   	    	padding one bits to the current byte, then purging WB[] to
*   	    	FILE *ofp, then appending FILE *ofp with the JPEG marker
*   	    	JPG_MARK_EOI, and finally appending an optional NULL byte
*   	    	if necessary to make the bytes put to FILE *ofp even.
* Bugs:     	Unpredictable results if nBits > 16.
***************************************************************************/
tI32	PutBits(
tU32 	Bits,
int	nBits)
{
    static int  WriteError = 0;
    static FILE *ofp = NULL;
    static tU8  *WB = NULL;
    static int  ResidueBits = 0;
    static tU32 Residue;
    static tI32 iWB = 0;
    static tI32 iFile = 0;
    static tI32 BufSize = 0;
    static tU8  EndOfFile[4] =
    	    	{ JPG_MARK_ZERO, JPG_MARK_FF, JPG_MARK_EOI, JPG_MARK_ZERO };

    if (nBits < 0)
    { // various initialization
    	if (nBits == PUTBITS_INIT_OFP)
    	{ // initialize
	    ofp = (FILE *) Bits;
	    WriteError = 0;
	    iWB = 0;
	    iFile = 0;
	    ResidueBits = 0;
	    return(iFile);
    	}
    	if (nBits == PUTBITS_INIT_BUF)
    	{ // initialize
	    WB = (tU8 *) Bits;
	    WriteError = 0;
	    iWB = 0;
	    iFile = 0;
	    ResidueBits = 0;
	    return(iFile);
    	}
	else if (nBits == PUTBITS_BUF_SIZE)
	{ // initialize max buffer size
	    BufSize = (tI32) Bits;
	    WriteError = 0;
	    iWB = 0;
	    iFile = 0;
	    ResidueBits = 0;
	    return(iFile);
	}
	else if (nBits == PUTBITS_EOF)
	{ // handle JPEG_MARK_EOI
	    tU8 c = 0;
	    if (ResidueBits)
	    {
	    	WB[iWB++] = 
	    	c = (Residue << (8 - ResidueBits))
		  | ((1 << (8 - ResidueBits)) - 1);
	    	iFile++;
	    }
    	    if (!fwrite(WB, 1, iWB, ofp)) WriteError = 1;
	
	    if (c == JPG_MARK_FF)
	    { // don't forget to put a JPG_MARK_ZERO
	    	if (iFile & 0x1)
	    	{ // file size will be even, no need to pad with NULL
	    	    if (!fwrite(&EndOfFile[0], 3, 1, ofp)) WriteError = 1;
	    	    iFile += 3;
	    	}
	    	else
	    	{ // file size would be odd, pad with NULL to make even
	    	    if (!fwrite(&EndOfFile[0], 4, 1, ofp)) WriteError = 1;
	    	    iFile += 4;
	    	}
	    }
	    else
	    {
	    	if (iFile & 0x1)
	    	{ // file size would be odd, pad with NULL to make even
	    	    if (!fwrite(&EndOfFile[1], 3, 1, ofp)) WriteError = 1;
	    	    iFile += 3;
	    	}
	    	else
	    	{ // file size will be even, no need to pad with NULL
	    	    if (!fwrite(&EndOfFile[1], 2, 1, ofp)) WriteError = 1;
	    	    iFile += 2;
	    	}
	    }
	    goto done;
	}
	else
	{ // must be PUTBITS_STATUS
	    goto done;
	}
    }
    
    Residue    <<= nBits;
    Residue     |= Bits & ((1 << nBits) - 1);
    ResidueBits += nBits;

    int nBytes;
    for (nBytes = ResidueBits / 8; nBytes--; ResidueBits -= 8)
    {
    	tU8 c;
	WB[iWB++] = c = Residue >> (8 * nBytes  +  ResidueBits % 8);
	if (iWB >= BufSize)
	{
	    if (!fwrite(WB, iWB, 1, ofp)) WriteError = 1;
	    iWB = 0;
	}
	iFile++;
	if (c == JPG_MARK_FF)
	{
	    WB[iWB++] = JPG_MARK_ZERO;
	    if (iWB >= BufSize)
	    {
	    	if (!fwrite(WB, iWB, 1, ofp)) WriteError = 1;
	    	iWB = 0;
	    }
	    iFile++;
	}
    }
    
    done:
    return((WriteError) ? 0 : iFile);
} /* end PutBits() */

/***************************************************************************
* Function:	IntelMotoByteSwap()
* Returns:	0 if successful
* Description:	Swaps the low and high bytes of Cnt tU16 words in Buf.
***************************************************************************/
int	IntelMotoByteSwap(
tU16	*Buf, 
tU32	Cnt)
{
    register tU16 *rBuf = Buf;
    register tU32 rCnt  = Cnt;
    while(rCnt--)
    {
    	register tU32 Swap;
	Swap   = *rBuf;
	*rBuf++ = ((Swap << 16) + Swap) >> 8;
    }
    
    return(0);
} /* end IntelMotoByteSwap() */

/***************************************************************************
* Function:	TestPlatformType()
* Returns:	'I' for Intel, 'M' for Moto
* Description:	Tests the memory organization of the platform, and set the
*   	    	Get?I() and Get?M() functions pointers accordingly.
***************************************************************************/
tU16	TestPlatformType(void)
{
    tU16 PlatformTest;
    PlatformTest = 'I';
    if (*((tU8 *) &PlatformTest) == 'I')
    {
    	PlatformType = INTEL_SIGNATURE;
    	Get2I = Get2F;
    	Get4I = Get4F;
    	Get8I = Get8F;
    	Get2M = Get2R;
    	Get4M = Get4R;
    	Get8M = Get8R;
    	Put2I = Put2F;
    	Put4I = Put4F;
    	Put8I = Put8F;
    	Put2M = Put2R;
    	Put4M = Put4R;
    	Put8M = Put8R;
    }
    else
    {
    	PlatformType = MOTO_SIGNATURE;
    	Get2I = Get2R;
    	Get4I = Get4R;
    	Get8I = Get8R;
    	Get2M = Get2F;
    	Get4M = Get4F;
    	Get8M = Get8F;
    	Put2I = Put2R;
    	Put4I = Put4R;
    	Put8I = Put8R;
    	Put2M = Put2F;
    	Put4M = Put4F;
    	Put8M = Put8F;
    }
    
    return(PlatformType);
} /* end TestPlatformType() */

/***************************************************************************
* Function:	TestIntelMotoFileSignature()
* Returns:	'II' or 'MM' for Intel/Moto, or 0 if neither
* Description:	Reads the file first two characters for Tiff/Ciff style
*   	    	signature.
***************************************************************************/
tU16	TestIntelMotoFileSignature(void)
{
    TestPlatformType();     	    // set platform to Intel or Moto
        
    fseek(ifp, 0, SEEK_SET);
    IntelMotoFileSignature = Get2F();
    if (IntelMotoFileSignature == INTEL_SIGNATURE)
    {
    	if (PlatformType == INTEL_SIGNATURE)
	{ // Intel file and Intel Platform
	    Get2 = Get2F;
	    Get4 = Get4F;
	    Get8 = Get8F;
	    Put2 = Put2F;
	    Put4 = Put4F;
	    Put8 = Put8F;
	}
	else
	{ // Intel file and Moto Platform
	    Get2 = Get2R;
	    Get4 = Get4R;
	    Get8 = Get8R;
	    Put2 = Put2R;
	    Put4 = Put4R;
	    Put8 = Put8R;
	}
    }
    else if (IntelMotoFileSignature == MOTO_SIGNATURE)
    {
    	if (PlatformType == MOTO_SIGNATURE)
	{ // Moto file and Moto Platform
	    Get2 = Get2F;
	    Get4 = Get4F;
	    Get8 = Get8F;
	    Put2 = Put2F;
	    Put4 = Put4F;
	    Put8 = Put8F;
	}
	else
	{ // Moto file and Intel Platform
	    Get2 = Get2R;
	    Get4 = Get4R;
	    Get8 = Get8R;
	    Put2 = Put2R;
	    Put4 = Put4R;
	    Put8 = Put8R;
	}
    }
    else
    { // not Tiff, use foward get() functions as default
	Get2 = Get2F;
	Get4 = Get4F;
	Get8 = Get8F;
	Put2 = Put2F;
	Put4 = Put4F;
	Put8 = Put8F;
	IntelMotoFileSignature = 0;
    }

    return(IntelMotoFileSignature);
} /* end TestIntelMotoFileSignature() */

/***************************************************************************
* Function:	Get1()
* Returns:	1 byte
* Description:	Gets 1 bytes from File.  Exits upon error.
***************************************************************************/
tU8	Get1(void)
{
    tU8 d;
    if (fread(&d, 1, 1, ifp) != 1) Oops("can't Get1()");
    return (d);
} /* end Get1() */

/***************************************************************************
* Function:	Get2F()
* Returns:	2 bytes
* Description:	Gets 2 bytes from File, does not reverse.  Exits upon error.
***************************************************************************/
tU16	Get2F(void)
{
    tU8 d[2];
    if (fread(&d, 1, 2, ifp) != 2) Oops("can't Get2F()");
    return (*((tU16 *) d));
} /* end Get2F() */

/***************************************************************************
* Function:	Get4F()
* Returns:	4 bytes
* Description:	Gets 4 bytes from File, does not reverse.  Exits upon error.
***************************************************************************/
tU32	Get4F(void)
{
    tU8 d[4];
    if (fread(&d, 1, 4, ifp) != 4) Oops("can't Get4F()");
    return (*((tU32 *) d));
} /* end Get4F() */

/***************************************************************************
* Function:	Get8F()
* Returns:	8 bytes
* Description:	Gets 8 bytes from File, does not reverse.  Exits upon error.
***************************************************************************/
double	Get8F(void)
{
    tU8 d[8];
    if (fread(&d, 1, 8, ifp) != 8) Oops("can't Get8F()");
    return (*((double *) d));
} /* end Get8F() */

/***************************************************************************
* Function:	Get2R()
* Returns:	2 bytes
* Description:	Gets 2 bytes from File, reverses order.  Exits upon error.
***************************************************************************/
tU16	Get2R(void)
{
    tU8 d[2], r[2];
    if (fread(&r, 1, 2, ifp) != 2) Oops("can't Get2R()");
    d[0] = r[1];
    d[1] = r[0];
    return (*((tU16 *) d));
} /* end Get2R() */

/***************************************************************************
* Function:	Get4R()
* Returns:	4 bytes
* Description:	Gets 4 bytes from File, reverses order.  Exits upon error.
***************************************************************************/
tU32	Get4R(void)
{
    tU8 d[4], r[4];
    if (fread(&r, 1, 4, ifp) != 4) Oops("can't Get4R()");
    d[0] = r[3];
    d[1] = r[2];
    d[2] = r[1];
    d[3] = r[0];
    return (*((tU32 *) d));
} /* end Get4R() */

/***************************************************************************
* Function:	Get8R()
* Returns:	8 bytes
* Description:	Gets 8 bytes from File, reverses order.  Exits upon error.
***************************************************************************/
double	Get8R(void)
{
    tU8 d[8], r[8];
    if (fread(&r, 1, 8, ifp) != 8) Oops("can't Get8R()");
    d[0] = r[7];
    d[1] = r[6];
    d[2] = r[5];
    d[3] = r[4];
    d[4] = r[3];
    d[5] = r[2];
    d[6] = r[1];
    d[7] = r[0];
    return (*((double *) d));
} /* end Get8R() */

/***************************************************************************
* Function:	Put1()
* Returns:	none
* Description:	Puts 1 bytes to File.  Exits upon error.
***************************************************************************/
void	Put1(
tU8 	d)
{
    if (!fwrite(&d, 1, 1, ofp)) Oops("can't Get1()");
} /* end Put1() */

/***************************************************************************
* Function:	Put2F()
* Returns:	none
* Description:	Puts 2 bytes to File, does not reverse.  Exits upon error.
***************************************************************************/
void	Put2F(
tU16	d)
{
    if (!fwrite(&d, 2, 1, ofp)) Oops("can't Get2F()");
} /* end Put2F() */

/***************************************************************************
* Function:	Put4F()
* Returns:	none
* Description:	Puts 4 bytes to File, does not reverse.  Exits upon error.
***************************************************************************/
void	Put4F(
tU32	d)
{
    if (!fwrite(&d, 4, 1, ofp)) Oops("can't Get4F()");
} /* end Put4F() */

/***************************************************************************
* Function:	Put8F()
* Returns:	none
* Description:	Puts 8 bytes to File, does not reverse.  Exits upon error.
***************************************************************************/
void	Put8F(
double	d)
{
    if (!fwrite(&d, 8, 1, ofp)) Oops("can't Get8F()");
} /* end Put8F() */

/***************************************************************************
* Function:	Put2R()
* Returns:	none
* Description:	Puts 2 bytes to File, reverse order.  Exits upon error.
***************************************************************************/
void	Put2R(
tU16	d)
{
    tU8 r[2];
    r[0] = ((tU8 *) &d)[1];
    r[1] = ((tU8 *) &d)[0];
    if (!fwrite(&r, 2, 1, ofp)) Oops("can't Get2F()");
} /* end Put2R() */

/***************************************************************************
* Function:	Put4R()
* Returns:	none
* Description:	Puts 4 bytes to File, reverse order.  Exits upon error.
***************************************************************************/
void	Put4R(
tU32	d)
{
    tU8 r[4];
    r[0] = ((tU8 *) &d)[3];
    r[1] = ((tU8 *) &d)[2];
    r[2] = ((tU8 *) &d)[1];
    r[3] = ((tU8 *) &d)[0];
    if (!fwrite(&r, 4, 1, ofp) != 4) Oops("can't Get4F()");
} /* end Put4R() */

/***************************************************************************
* Function:	Put8R()
* Returns:	none
* Description:	Puts 8 bytes to File, reverse order.  Exits upon error.
***************************************************************************/
void	Put8R(
double	d)
{
    tU8 r[4];
    r[0] = ((tU8 *) &d)[7];
    r[1] = ((tU8 *) &d)[6];
    r[2] = ((tU8 *) &d)[5];
    r[3] = ((tU8 *) &d)[4];
    r[4] = ((tU8 *) &d)[3];
    r[5] = ((tU8 *) &d)[2];
    r[6] = ((tU8 *) &d)[1];
    r[7] = ((tU8 *) &d)[0];
    if (!fwrite(&r, 8, 1, ofp)) Oops("can't Get8F()");
} /* end Put8R() */

/***************************************************************************
* Function:	Oops()
* Returns:	doesn't
* Description:	Prints error message and exits program.
***************************************************************************/
void	Oops(
char	*Msg)
{
    static int Count = 0;
    if (Count++ < 8) fprintf(stderr, "%s\n", Msg);
} /* end Oops() */
