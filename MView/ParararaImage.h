#ifndef _PARARAR_IMAGE_H
#define _PARARAR_IMAGE_H

#include <QImage>

class QRenderImage: public QPaintDevice {
public:
    QRenderImage(LPBYTE surface, int bpp, int width, int height);
    QRenderImage();
    ~QRenderImage();
};

#endif