﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Source of source of stream
// share MainUnit.h between Walker, Painter Stocker
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <mzfc_inc.h>
#include <mzfc/ImagingHelper.h>
#include <acc_api.h>
#include <BackLightApi.h>
#include <CallNotifyApi.h>
#include <FlyModeSetting.h>
#include <SettingApi.h>
#include <TouchNotifyApi.h>
#include <wingdi.h>
#include <ShellNotifyMsg.h>
#include <IFileBrowser.h>
#include <IFileBrowser_GUID.h>
#include <IMzUnknown.h>
#include <IMzUnknown_IID.h>

#include <FvMzKOL.h>
#include "MainUnt.h"
#include "resource.h"
#include "StringInterner.h"
#include "SelectorUnt.h"
#include "DbCentre.h"
#include "AddonFuncUnt.h"

#include <QApplication>


MZ_IMPLEMENT_DYNAMIC(CMainWnd);

BOOL CMainWnd::OnInitDialog()
{
    // Must all the Init of parent class first!
    if (!CMzWndEx::OnInitDialog())
    {
        return FALSE;
    }


    stage0.SetPos(0, 0, GetWidth(), GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR);
    stage0.EnableNotifyMessage(true);
    //stage0.SetID(MZ_IDC_UNLOCK);
    AddUiWin(&stage0); // touch licker

    LoadAppSettings(); // TODO: move to constructor
    fWindowedHonz = false;
    fOverlayMode = true;
    fDrawToolbar = true;

    fEmulatorDetected = IsEmulator();
    f0619Detected = Check0619ROM();

    if (fEmulatorDetected) {
        fOverlayMode = false;
    }
    // Check 90*90 Desktop Icon
    QImageReader clockreader("\\Windows\\ClockIcon.png");
    CMzStringW IconBase = GetStartDir();
    bool IconMissed = false;
    if (FileExists(L"\\Windows\\Icons\\MnViewIkon.png") && FileExists(L"\\Windows\\Icons\\MnViewIkon0619.png")) {
        IconBase = L"\\Windows\\Icons\\"; // Prefer an Standard one
    } else if (!FileExists(IconBase + L"MnViewIkon.png") && !FileExists(IconBase + L"MnViewIkon0619.png")) {
        IconMissed = true;
    }
    if (IconMissed == false) {
        if (clockreader.size().width() == 90) {
            HKEY IKonKey = RegKeyOpenWrite(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Meizu\\MiniOneShell\\Main\\MView");
            if (IKonKey > 0) {
                if (AnsiCompareStrNoCase(RegKeyGetStr(IKonKey, L"DefaultIcon"), IconBase + L"MnViewIkon.png") == 0) {
                    RegKeySetStr(IKonKey, L"DefaultIcon", IconBase + L"MnViewIkon0619.png");
                    //ReloadDesktopIcons();
                    Sleep(2048);
                }
                RegKeyClose(IKonKey);
            }
        } else if (clockreader.size().width() == 96) {
            HKEY IKonKey = RegKeyOpenWrite(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Meizu\\MiniOneShell\\Main\\MView");
            if (IKonKey > 0) {
                if (AnsiCompareStrNoCase(RegKeyGetStr(IKonKey, L"DefaultIcon"), IconBase + L"MnViewIkon0619.png") == 0) {
                    RegKeySetStr(IKonKey, L"DefaultIcon", IconBase + L"MnViewIkon.png");
                    //ReloadDesktopIcons();
                    Sleep(2048);
                }
                RegKeyClose(IKonKey);
            }
        }
    }
    if (f0619Detected) {
        MzAutoMsgBoxEx(m_hWnd, L"Unstable 0910_0619 ROM Detected.\nSwitch to slowly workaround Mode.", 800);
        DrawSplashOnWorld();
    }

    RegisterShellMessage(m_hWnd, WM_MZSH_ENTRY_LOCKPHONE | WM_MZSH_LEAVE_LOCKPHONE | WM_MZSH_ENTRY_SHUTDOWN| WM_MZSH_LEAVE_SHUTDOWN);
    RegisterTouchNotify(m_hWnd, WM_MZ_TOUCHNOTIFY);
    stage0.EnablePressedHoldSupport(true);
    stage0.SetPressedHoldTime(420);

    fFullScreen = StateSetting.FullScreen = true; // Wait for stable?
    fExtraInfo = ExtraInfoLevel(StateSetting.ExtraInfo);
    if (fExtraInfo > eilBench) {
        fExtraInfo = eilPageOnly;
    }
    fPaperBorder = true;
    fStageShowMode = ssmFit;

    fDisacardOverlayShow = true;
    RenderMaintainStage();

    if (!PathSetting.LastStageFile.isEmpty() && FileExists(LPWSTR(PathSetting.LastStageFile.utf16()))) {
        fStagefile = LPWSTR(PathSetting.LastStageFile.utf16());
    } else {
        fStagefile = GetStartDir() + L"plants-on-sunsine.jpg";
        PathSetting.StartupFolder = ""; // workaround
    }
    if (!PathSetting.StartupFolder.isEmpty()) {
        fLastBrowsePath = LPWSTR(PathSetting.StartupFolder.utf16());
    } else {
        fLastBrowsePath = ExtractFilePath(fStagefile);
    }
    RebuildScrollList(); // Format fCurrentScrollIndex
    if (fStagefile.Compare(fLastBrowsePath) == 0) {
        // TODO: fBrowsingArchive
        fCurrentScrollIndex = NormalizeScrollIndex(PathSetting.LastArchiveIndex);
    }

    // make same widgets as toolbar?
    AddWidget(0, QRect(646, 400, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_TOOLBAR_EXIT)));
    AddWidget(1, QRect(646, 305, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_TOOLBAR_REFRESH)));
    AddWidget(2, QRect(646, 210, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_TOOLBAR_FULLSCREEN)));
    AddWidget(3, QRect(646, 110, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_TOOLBAR_RENDER)));
    AddWidget(4, QRect(646, 15, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_TOOLBAR_MENU)));
    AddWidget(5, QRect(242, 15, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_M_BROWSE)), "Browse");
    AddWidget(6, QRect(342, 15, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_M_MANGA)), "1 to 2");
    AddWidget(7, QRect(442, 15, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_M_CAPTURE)), "Capture");
    AddWidget(8, QRect(542, 15, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_M_WALLPAPER)), "Wallpaper");
    AddWidget(9,  QRect(342, 115, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_MANGA_NIPPON)), "right to left");
    AddWidget(10, QRect(342, 215, 0, 0), NewQImageFromResource(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_MANGA_NATIVE)), "left to right");
    SetWidgetParent(5, 4);
    SetWidgetParent(6, 4);
    SetWidgetParent(7, 4);
    SetWidgetParent(8, 4);
    SetWidgetParent(9, 6);
    SetWidgetParent(10, 6);

#ifdef DEBUG
    stage1.SetTransparency(12);
#endif

    SetScreenAlwaysOn(m_hWnd);

    fDrawToolbar = true;
    fLastToolbarDrawTick = GetTickCount();
    SetTimer(m_hWnd, IDT_SOMEMARKTIMEOUT, 8080, NULL);
    SetTimer(m_hWnd, IDT_SOMEHINTTIMEOUT, 2080, NULL);
    EnableWindow(m_hWnd, true);
    PaintStage();
    fDisacardOverlayShow = false;
    stage1.ShowOverlay();

    //RefreshWallpaper(WALLPAPER_DESKTOP);
    //InvalidateRect(GetDesktopWindow(), NULL, FALSE);
    //::UpdateWindow(GetDesktopWindow());

    return TRUE;
}


void CMainWnd::OnMzCommand( WPARAM wParam, LPARAM lParam )
{
    UINT_PTR id = LOWORD(wParam);
    switch(id)
    {
    case MZ_IDC_TOOLBAR1:
        {
            int nIndex = lParam;
            if (nIndex == 0) {
                // Exit
                if (fSettingsSaved == false) {
                    FlushSettings();
                    fSettingsSaved = true;
                    SaveAppSettings();
                }
                PostQuitMessage(0);
                return;
            }
            if (nIndex == 1) {
                // Refresh
                fPauseAni = false;
                PaintStage();
            }
            if (nIndex == 2) {
                // FullScreen
                ToggleFullScreen();
                PaintStage();
                CheckPrefetchHealthy();
            }
            if (nIndex == 3) {
                // Overlay
                /*if (fOverlayMode) {
                    stage1.HideOverlay();
                } else {
                    stage1.ShowOverlay();
                }*/
                fOverlayMode = !fOverlayMode;
                RenderMaintainStage();
                PaintStage();
                QImage exitimage;
                LoadQImageFromResource(exitimage, MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(fOverlayMode?IDR_PNG_TOOLBAR_RENDER:IDR_PNG_TOOLBAR_RENDER_64K));
                SetWidgetImage(3, exitimage);
                return;
            }
            if (nIndex == 4) {
                 // Introduced new widget based submenu
            }
            if (nIndex == 5) {
                // Browse
                if (fOverlayMode && stage1.IsVisible()) {
                    stage1.HideOverlay();
                    fDisacardOverlayShow = true;
                }

                fInShellUsage = true;
                if (BrowseFile()) {
                    fPauseAni = false;
                    PaintStage();
                }
                fInShellUsage = false;

                fDisacardOverlayShow = false;
                if (fOverlayMode && !stage1.IsVisible() && !fDisacardOverlayShow) {
                    stage1.ShowOverlay();
                }
                return;
            }
            if (nIndex == 6) {
                // divide
                return;
            }
            if (nIndex == 7) {
                // Capture
                fDoSingleCapture = true;
                PaintStage();
                fDoSingleCapture = false; // no needed because unsupported image will cached
                return;
            }
            if (nIndex == 8) {
                // Desktop
                fDoSingleCapture = true;
                fCaptureForDesktop = true;
                PaintStage();
                // save to desktop
                HKEY DesktopKey = RegKeyOpenWrite(HKEY_CURRENT_USER, L"ControlPanel\\Desktop");
                if (DesktopKey) {
                    if (fCaptureForDesktop && FileExists(L"\\Windows\\MyViewWallpaper.png")) {
                        // use as success flag
                        if (RegKeyGetStr(DesktopKey, L"Wallpaper").Compare(L"\\Windows\\MyViewWallpaper.png") != 0) {
                            RegKeySetStr(DesktopKey, L"Wallpaper", L"\\Windows\\MyViewWallpaper.png");
                        }
                    } else {
                        // save Failed, or file missed
                        if (RegKeyGetStr(DesktopKey, L"Wallpaper").Compare(L"\\Windows\\MyViewWallpaper.png") == 0) {
                            RegKeySetStr(DesktopKey, L"Wallpaper", L"\\Windows\\\\Windows\\DesktopBg.jpg");
                        }
                    }

                    RefreshWallpaper(WALLPAPER_DESKTOP);
                    RegKeyClose(DesktopKey);
                }
                fDoSingleCapture = false; // no needed because unsupported image will cached
                fCaptureForDesktop = false;

                PaintStage(); // flush screen

                return;
            }
        }
        break;
    }
}

bool CMainWnd::BrowseFile()
{
    // TODO: use a Widget Thumbnail Browser instaed GDI one.
    bool Result = false;
    IMzSelect *pSelect = NULL; 
    IFileBrowser *pFile = NULL;                      
    CoInitializeEx(NULL, COINIT_MULTITHREADED );
    if ( SUCCEEDED( CoCreateInstance( CLSID_FileBrowser, NULL, CLSCTX_INPROC_SERVER, IID_MZ_FileBrowser, (LPVOID*)&pFile ) ) ) {     
        if( SUCCEEDED( pFile->QueryInterface( IID_MZ_Select, (LPVOID*)&pSelect ) ) ) {
            TCHAR file[ MAX_PATH ] = { 0 };
            pFile->SetParentWnd( m_hWnd );
            if (fLastBrowsePath.IsEmpty()) {
                fLastBrowsePath = GetStartDir();
            }
            if (fStagefile.Compare(fLastBrowsePath) == 0 && FileExists(fLastBrowsePath)) {
                pFile->SetOpenDirectoryPath( ExcludeTrailingPathDelimiter(ExtractFilePath(fLastBrowsePath)) ); // Archive
            } else {
                pFile->SetOpenDirectoryPath( ExcludeTrailingPathDelimiter(fLastBrowsePath) ); //如果不调用此函数则默认为根目录
            }
#ifdef MVIEW_RETAIL
            pFile->SetExtFilter( L"*.png;*.jpg;*.gif;*.bmp;*.zip" );
#else
            pFile->SetExtFilter( L"*.png;*.jpg;*.gif;*.bmp;*.cr2;*.hdp;*.wdp;*.zip;*.rar" );                                      
#endif
            pFile->SetOpenDocumentType(DOCUMENT_SELECT_SINGLE_FILE); //应用根据需求进行文档打开方式的设置
            if( pSelect->Invoke() ) {
                //各应用根据自己需求获取文档的返回值						
                _tcscpy( file, pFile->GetSelectedFileName() );
                fStagefile = file;
                Result = true;
                CMzStringW NewBrowsePath = ExtractFilePath(fStagefile);
                if (NewBrowsePath.Compare(fLastBrowsePath) != 0) {
                    // Rebuild filelist
                    fLastBrowsePath = NewBrowsePath;
                    RebuildScrollList();
                    CheckPrefetchHealthy(true);
                } else {
                    // Same path
                    int oldScrollIndex = fCurrentScrollIndex;
                    RebuildScrollList();
                    CheckPrefetchHealthy(oldScrollIndex != fCurrentScrollIndex);
                }
            }
            pSelect->Release();
        }     
        pFile->Release();
    }
    CoUninitialize();
    ::InvalidateRect( m_hWnd, NULL, FALSE );
    ::UpdateWindow( m_hWnd );

    return Result;
}

LRESULT CMainWnd::MzDefWndProc( UINT message, WPARAM wParam, LPARAM lParam )
{
    int Result = 0;
    if (message == fShuttingDownMessageID) {
        stage1.SetTransparency(0);
        return Result;
    }
    switch(message)
    {
    case MZ_WM_MOUSE_NOTIFY:
        {
            if (fDiscardClick) return Result;
            int nID = LOWORD(wParam);
            int nNotify = HIWORD(wParam);
            int x = LOWORD(lParam);
            int y = HIWORD(lParam);

            RenderFormatLocalPos(x, y);

            // process the mouse left button down notification
            switch (nNotify)
            {
            case MZ_MN_LBUTTONDOWN:
                if (OnMouseDown(x, y, Result)) return Result;
                break;
            case MZ_MN_LBUTTONUP:
                if (OnMouseUp(x, y)) return 0;
                break;
            case MZ_MN_LBUTTONDBLCLK:
                if (OnMouseDblClick(x, y)) return Result;
                break;
            case MZ_MN_MOUSEMOVE:
                if (OnMouseMove(x, y)) return 0;
                break;
            case MZ_MN_PRESSEDHOLD_START:
                if (OnLongPressStart(x, y)) return 0;
                break;
            case MZ_MN_PRESSEDHOLD_TIMEUP:
                if (OnLongPressTimeout(x, y)) return 0;
                break;
            }
        }
        return Result;
    case WM_MZ_TOUCHNOTIFY:
        {
           int NotifyEvent = wParam;
           int x = HIWORD(lParam);
           int y = LOWORD(lParam);
           if (NotifyEvent == TCH_EVENT_ZOOM_IN) {
               fDiscardClick = true;
               if (OnZoomIn(x, y)) return 0;
           }
           if (NotifyEvent == TCH_EVENT_ZOOM_OUT) {
               fDiscardClick = true;
               if (OnZoomOut(x, y)) return 0;
           }
           if (NotifyEvent == TCH_EVENT_ZOOM_END) {
               fDiscardClick = false;
               if (OnZoomEnd(x, y)) return 0;
           }
        }
        return 0;
    case WM_ACTIVATE:
        {
            // TODO: Startup flag here
            int action = LOWORD(wParam);
            switch (action)
            {
            case WA_ACTIVE:
            case WA_CLICKACTIVE:
                {
                    fDiscardClick = false;
                    stage0.EnableNotifyMessage(true);
                    //RegisterTouchNotify(m_hWnd, MZ_WM_MOUSE_NOTIFY);

                    if (fOverlayMode) {
                        if (!stage1.IsVisible() && !fDisacardOverlayShow) {
                            stage1.ShowOverlay();
                        }
                        //PaintStage();
                    }
                }
                break;
            case WA_INACTIVE:
                {
                    fDiscardClick = true;
                    stage0.EnableNotifyMessage(false);
                    //UnRegisterTouchNotify(m_hWnd, MZ_WM_MOUSE_NOTIFY);
                    CleanupTouchNotify();

                    TryStopAnimation();

                    if (fOverlayMode) {
                        if (stage1.IsVisible()) {
                            stage1.HideOverlay();
                        }
                    }
                }
                break;
            }
        }
        // An application should return zero if it processes this message. 
        return 0;
    case WM_TIMER:
        {
            int TimerID = wParam;
            if (TimerID == IDT_SOMEMARKTIMEOUT) {
                if (GetTickCount() - fLastToolbarDrawTick > 8000) {
                    if (fDrawToolbar == true) {
                        fDrawToolbar = false;
                        TryRefreshStageOnFit();
                    }
                }
                KillTimer(m_hWnd, TimerID);
                return 0;
            }
            if (TimerID == IDT_SOMEHINTTIMEOUT) {
                if (GetTickCount() - fLastExtraInfoLevelHintTick > 2000) {
                    TryRefreshStageOnFit();
                }
                if (GetTickCount() - fLastZoomLevelHintTick > 2000) {

                }
                KillTimer(m_hWnd, TimerID);
                return 0;
            }
        }
        break;
    }
#ifndef CALL_BEGIN
    if (message == fCallMessageID) {
            if (wParam == PAL_BC_NOTIFY_CALL_STATUS && lParam == PAL_CALLSTAT_BIT_INCOMING) {
                stage1.HideOverlay();
            } else if (wParam == PAL_BC_NOTIFY_CALL_STATUS && lParam == PAL_CALLSTAT_BIT_DISCONNECT) {
                stage1.ShowOverlay();
                //PaintStage();
            }
    } else if (message == fSmsMessageID) {
        if (wParam == SMS_BEGIN) {
            stage1.HideOverlay();
        } else if (wParam == SMS_END) {
            stage1.ShowOverlay();
            //PaintStage();
        }
#else
    if (message == fCallMessageID || message == fSmsMessageID) {
        if (fOverlayMode) {
            if (wParam == CALL_BEGIN) {
                stage1.HideOverlay();
            } else if (wParam == CALL_END) {
                stage1.ShowOverlay();
                //PaintStage();
            }
            return 0;
        }
#endif
    } else if (message == fLockScreenStartMessageID) {
        if (fOverlayMode) {
            //stage1.HideOverlay();
            stage1.SetTransparency(0);
            return 0;
        }
    } else if (message == fLockScreenFinishMessageID) {
        if (fOverlayMode) {
            //stage1.ShowOverlay();
            stage1.SetTransparency(15);
            return 0;
        }
    } else if (message == fShutdownMenuMessageID) {
        if (fOverlayMode) {
            stage1.HideOverlay();
            return 0;
        }
    } else if (message == fShutdownCancelMessageID) {
        if (fOverlayMode) {
            stage1.ShowOverlay();
            return 0;
        }
    } else if (message == fACCMessageID) {
        bool rotated = (wParam == SCREEN_LANDSCAPE_P || wParam == SCREEN_LANDSCAPE_N);
        if (rotated !=  fWorldRoted) {
            fWorldRoted = rotated;
            if (fDrawToolbar) {
                TryRefreshStageOnFit();
            }
        }

    }
    return CMzWndEx::MzDefWndProc(message, wParam, lParam);
}

bool CMainWnd::ToggleFullScreen()
{
    fFullScreen = !fFullScreen;
    return RenderMaintainStage();
}

bool CMainWnd::RenderSwitchStage( StageRenderMode mode ) {
    bool Result = false;

    bool originalvisible = stage1.IsVisible();
    bool targetgdi = mode == srmFullscreenGDIHorizontal || mode == srmFullscreenGDIVertical ||
        mode == srmWindowedGDIHorizontal || mode == srmWindowedGDIVertical;
    bool targetfullscreen = mode == srmFullscreenGDIHorizontal || mode == srmFullscreenGDIVertical ||
        mode == srmFullscreenOverlayHorizontal || mode == srmFullscreenOverlayVertical;

    if (originalvisible) {
        stage1.HideOverlay();
        if (!targetgdi) {
            stage1.Clear(); // prepare next init
        } else {
            stage1.Clear();
        }
    }

    if (targetfullscreen) {
        // hide bars n toolbar (TODO: Widget)
        HideMzTopBar();
    } else {
        ShowMzTopBar();
    }

    if (targetgdi){
        if (mode == srmFullscreenGDIHorizontal) {
            if (fEmulatorDetected) {
                RotateWorld(DMDO_270);
            } else {
                RotateWorld(DMDO_0);
            }
            fLocalRoted = true;
            this->SetWindowPos(0, 0, 0, 720, 480);
            fStageSize.stage1width = 720;
            fStageSize.stage1height = 480;
            stage0.SetPos(0, 0, GetWidth(), GetHeight()); // move touch licker
        } else if (mode == srmFullscreenGDIVertical) {
            // out of design
        } else if (mode == srmWindowedGDIHorizontal) {
            // out of design
        } else if (mode == srmWindowedGDIVertical) {
            // for FileBrowser, menu, etc
            if (fEmulatorDetected) {
                RotateWorld(DMDO_0);
            } else {
                RotateWorld(DMDO_90);
            }
            fLocalRoted = false;
            this->SetWindowPos(0, 0, 40, 480, 720 - 40);
            fStageSize.stage1width = 480;
            fStageSize.stage1height = 720 - 40;
            stage0.SetPos(0, 0, GetWidth(), GetHeight()); // move touch licker
        }
    } else {
        bool canflip = !f0619Detected;
        if (mode == srmFullscreenOverlayHorizontal) {
            // out of design
            // for MyStage Sync
            RotateWorld(DMDO_0);
            fLocalRoted = true;
            this->SetWindowPos(0, 0, 0, 720, 480);
            stage1.Init(m_hWnd, 0, 0, 720, 480, 0xC0FF, 15, 15, canflip, PixFmtRGB24);
            fStageSize.stage1width = 720; // for Mixer
            fStageSize.stage1height = 480; // we can soft rotate surface just like landscape
            stage0.SetPos(0, 0, GetWidth(), GetHeight()); // move touch licker
        } else if (mode == srmFullscreenOverlayVertical) {
            RotateWorld(DMDO_90);
            fLocalRoted = false;
            this->SetWindowPos(0, 0, 0, 480, 720);
            stage1.Init(m_hWnd, 0, 0, 480, 720, 0xC0FF, 15, 15, canflip, PixFmtRGB24);
            fStageSize.stage1width = 720; // for Mixer
            fStageSize.stage1height = 480; // we can soft rotate surface just like landscape
            stage0.SetPos(0, 0, GetWidth(), GetHeight()); // move touch licker
        } else if (mode == srmWindowedOverlayHorizontal) {
            // out of design
        } else if (mode == srmWindowedOverlayVertical) {
            RotateWorld(DMDO_90);
            fLocalRoted = false;
            this->SetWindowPos(0, 0, 40, 480, 720 - 40);
            stage1.Init(m_hWnd, 0, 40, 480, 720 - 40, 0xC0FF, 15, 15, canflip, PixFmtRGB24);
            fStageSize.stage1width = 720 - 40; // for Mixer
            fStageSize.stage1height = 480;
            stage0.SetPos(0, 0, GetWidth(), GetHeight()); // move touch licker
        }
    }
    if (originalvisible) {
        // from overlay to overlay
        if (!targetgdi && !fDisacardOverlayShow) {
            stage1.ShowOverlay();
        }
    } else {
        // from GDI to overlay
        if (!targetgdi && !fDisacardOverlayShow) {
            stage1.ShowOverlay();
        }
    }

    return Result;
}

bool CMainWnd::RenderMaintainStage() {
    bool Result = false;


    if (fFullScreen) {
        if (fOverlayMode) {
            /*if (fWindowedHonz) {
                RenderSwitchStage(srmFullscreenOverlayHorizontal);
            } else {
                RenderSwitchStage(srmFullscreenOverlayVertical);
            }*/
            RenderSwitchStage(srmFullscreenOverlayVertical);
        } else {
            // fGDIMode
            /*if (fWindowedHonz) {
                RenderSwitchStage(srmFullscreenGDIHorizontal);
            } else {
                RenderSwitchStage(srmFullscreenGDIVertical);
            }*/
            RenderSwitchStage(srmFullscreenGDIHorizontal);
        }
    } else {
        if (fOverlayMode) {
            if (fWindowedHonz) {
                RenderSwitchStage(srmWindowedOverlayHorizontal);
            } else {
                RenderSwitchStage(srmWindowedOverlayVertical);
            }
        } else {
            // fGDIMode
            if (fWindowedHonz) {
                RenderSwitchStage(srmWindowedGDIHorizontal);
            } else {
                RenderSwitchStage(srmWindowedGDIVertical);
            }
        }
    }

    return Result;
}


bool CMainWnd::RenderPrepareShellUsage()
{
    return RenderSwitchStage(srmWindowedGDIVertical);
}

bool CMainWnd::RenderFormatLocalPos(int& x, int& y)
{
    int newx, newy;
    if (fLocalRoted) {
        // untouch
    } else {
        newx = y;
        newy = 480 - x;

        x = newx;
        y = newy;
    }
    return true;
}

CMainWnd::CMainWnd()
{
    // may 1st function invoked during startup
    SateHandle = LoadLibraryExW(GetStartDir() + L"Res" + GetLanguageCode() + L".dll", NULL, LOAD_LIBRARY_AS_DATAFILE);
    stringstore = new TStringStore(SateHandle);

    // some variable
    fExtraInfo = eilPageOnly;
    fDrawToolbar = true;
    fDoSingleCapture = false;
    fAutoPlayMode = false;
    fBuildingPrefetch = false;
    fPaintingStage = 0;
    fDiscardClick = false;
    fSettingsSaved = false;
    fPrefetchBuilderThread = fFlowingKeeperThread = fAnimationKeeperThread = fStagePainterThread = NULL;
    fBoxKillerThread = NULL;
    fLastMouseX = fLastMouseY = -1;
    fLastMouseDownTick = fLastZoomTick = fLastToolbarDrawTick = fLastExtraInfoLevelHintTick = fLastZoomLevelHintTick = 0;
    LeftMovingCount = RightMovingCount = UpMovingCount = DownMovingCount = 0;
    LeftUpMovingCount = RightUpMovingCount = LeftDownMovingCount = RightDownMovingCount = 0;
    fPrevScollDepth = fNextScrollDepth = 2; // 5 image

    // TODO: check ACC
    fACCOpened = MzAccOpen();
    // Register critical messages
    fCallMessageID = GetCallRegisterMessage();
    fSmsMessageID = GetSmsRegisterMessage();
    fLockScreenStartMessageID   = GetShellNotifyMsg_EntryLockPhone();
    fLockScreenFinishMessageID  = GetShellNotifyMsg_LeaveLockPhone();
    fShutdownMenuMessageID      = GetShellNotifyMsg_EntryShutDown();
    fShutdownCancelMessageID    = GetShellNotifyMsg_LeaveShutDown();
    fShuttingDownMessageID      = GetShellNotifyMsg_ReadyPowerOFF();
    fACCMessageID = MzAccGetMessage();
    // m_hWnd busy now

#ifndef QT_NODLL
    // for imageformats
    QApplication::addLibraryPath(QString::fromUtf16((ushort*)GetStartDir().C_Str()));
#endif

    /*QString qjpegpath = QString::fromUtf16((ushort*)GetStartDir().C_Str()) + "qjpegd4.dll";
    fQJpegplugins.setFileName(qjpegpath);
    fQJpegplugins.load();*/

    // Load back light settings
    HKEY PhotoKey = RegKeyOpenRead(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Meizu\\PhotoViewer");
    int BacklightLevel = RegKeyGetDw(PhotoKey, L"BlackLightRate");
    int BacklightKeep = RegKeyGetDw(PhotoKey, L"BlackLightTime");
    bool AutoBacklight = RegKeyGetDw(PhotoKey, L"LuxSensor") != 0;

    // backup original arguments
    if (GetBackLightLevel(&fOriginalBacklightLevel) == false) {
        fOriginalBacklightLevel = -1;
    } else {
        SetBackLightLevel(BacklightLevel * 255 / 100);
    }
    fOriginalBacklightKeep = GetSystemIdleTimeout();
    if (GetLuxSensorState(&fOriginalAutoBacklight)) {
        SetLuxSensorState(AutoBacklight);
        fAutoBacklightChanged = true; // TODO: fAutoBacklightChanged = GeLux;
    } else {
        fAutoBacklightChanged = false;
    }

    RegKeyClose(PhotoKey);

    //LoadAppSettings();
    fRotisLoaded = AddFontResource(GetStartDir() + L"fonts\\RotisSansSerifStd-Light.otf") > 0;

    InitializeCriticalSection(&fPreqMapcs);
    InitializeCriticalSection(&fFlowingcs);
    InitializeCriticalSection(&fBuildingcs);
    InitializeCriticalSection(&fAnimatecs);
}

CMainWnd::~CMainWnd()
{
    if (fFullScreen) {
        RenderPrepareShellUsage(); // DONE: RenderPrepareShellUsage
    }
    //fQJpegplugins.unload();
    stage1.HideOverlay();
    stage1.Clear();
    fDiscardClick = true;
    fDisacardOverlayShow = true;
    UnRegisterTouchNotify(m_hWnd, WM_MZ_TOUCHNOTIFY);
    CleanupTouchNotify();
    //ShowMzTopBar();

    if (fOriginalBacklightLevel != -1) {
        SetBackLightLevel(fOriginalBacklightLevel);
    }
    if (fAutoBacklightChanged) {
        SetLuxSensorState(fOriginalAutoBacklight);
    }

    if (fACCOpened) {
        MzAccClose();
    }

    //UnRegisterShellMessage();

    delete stringstore;
    if (SateHandle) {
        FreeLibrary(SateHandle);
    }
    if (fRotisLoaded) {
        RemoveFontResource(GetStartDir() + L"fonts\\RotisSansSerifStd-Light.otf");
    }
    CheckPrefetchHealthy(true);
    fFetchedQImages.clear();
    fFakeWidgets.clear();
    fBuildingFitIndexes.clear();
    fScrollFiles.clear();

    DeleteCriticalSection(&fBuildingcs);
    DeleteCriticalSection(&fFlowingcs);
    DeleteCriticalSection(&fPreqMapcs);
    DeleteCriticalSection(&fAnimatecs);

    if (fPrefetchBuilderThread != NULL) {
        //fPrefetchBuilderThread->Terminate();
        delete fPrefetchBuilderThread;
    }
    if (fFlowingKeeperThread != NULL) {
        //fFlowingKeeperThread->Terminate();
        delete fFlowingKeeperThread;
    }
    if (fAnimationKeeperThread != NULL) {
        fCurrentScrollIndex = -1;
        delete fAnimationKeeperThread;
    }

    SetScreenAutoOff();

    if (fBoxKillerThread != NULL) {
        delete fBoxKillerThread;
        fBoxKillerThread = NULL;
    }
}

bool CMainWnd::ProcessMessage()
{
    bool Result = false;
    MSG Msg;
    if (PeekMessage( &Msg, 0, 0, 0, PM_REMOVE )){
        Result = Msg.message != 0;
        if (Msg.message == WM_QUIT) {
            //AppletTerminated = True;
            //PostQuitMessage(0);
        } else {  
            if (/*not(Assigned( fExMsgProc ) and fExMsgProc( @Self, Msg )*/ true)
            {
                TranslateMessage( &Msg );
                DispatchMessage( &Msg );
            }
        }
    }
    return Result;
}

bool CMainWnd::CleanupTouchNotify()
{
    int i = 0;
    MSG Msg;
    while ( i < 1000 && PeekMessage( &Msg, m_hWnd, MZ_WM_MOUSE_NOTIFY, MZ_WM_MOUSE_NOTIFY, PM_REMOVE ) == TRUE) {
        i++;
    }
    if (i > 0) {
        NKDbgPrintfW(L"Removed %d TouchNotify after UnRegisterTouchNotify.\n", i);
    }
    return i > 0;
}

void CMainWnd::WaitForKillBoxFork() {
    UFUNCTION func;
    func.Data = this;
    func.Code = &CMainWnd::OnBoxKillerThreadExecute;
    if (fBoxKillerThread != 0) {
        delete fBoxKillerThread;
        fBoxKillerThread = 0;
    }
    fBoxKillerThread = new TThread(func, THREAD_PRIORITY_ABOVE_NORMAL);
}

int CMainWnd::OnBoxKillerThreadExecute(TThread* Sender) {
#ifdef QT_NODLL
    int Result = 4200;
#else
    int Result = 2400;
#endif
    Sleep(Result);
    if (true /*fBoxKillerThread != NULL*/) {
        // havn't be terminated
        if (fSettingsSaved == false) {
            FlushSettings();
            fSettingsSaved = true;
            SaveAppSettings();
        }

        TerminateProcess(GetCurrentProcess(), Result);
    }
    return Result;
}

void CMainWnd::FlushSettings()
{
    if (fCurrentScrollIndex >= 0 && fCurrentScrollIndex < fScrollFiles.size()) {
        if (fScrollFiles[fCurrentScrollIndex].fromarchive) {
            fStagefile = fScrollFiles[fCurrentScrollIndex].archivename;
        } else {
            fStagefile = fScrollFiles[fCurrentScrollIndex].filename;
        }
    }
    PathSetting.LastStageFile = QString::fromUtf16((ushort*)fStagefile.C_Str());
    PathSetting.StartupFolder = QString::fromUtf16((ushort*)fLastBrowsePath.C_Str());
    PathSetting.LastArchiveIndex = fCurrentScrollIndex;
    StateSetting.ExtraInfo = fExtraInfo;
    StateSetting.FullScreen = fFullScreen;
}