#include "WincePlaceHolder.h"

void MZFC_DBG( const wchar_t *format, ... )
{
    wchar_t __outputStr[2048];
    va_list argptr;
    va_start(argptr, format);
    _vsnwprintf_s(__outputStr, 2048, format, argptr);
    va_end(argptr);
    OutputDebugStringW(__outputStr);
}

BOOL MzLeaveLockPhone() {
    return TRUE;
}

BOOL IsLockPhoneStatus() {
    return FALSE;
}

BOOL SetScreenAlwaysOn(HWND hWnd) {
    return TRUE;
}

BOOL SetScreenAutoOff() {
    return FALSE;
}

BOOL RegisterShellMessage(HWND hWnd, UINT uMsg) {
    return TRUE;
}

BOOL UnRegisterShellMessage(HWND hWnd, UINT uMsg) {
    return TRUE;
}

UINT GetShellNotifyMsg_EntryLockPhone() {
    return 0;
}

UINT GetShellNotifyMsg_LeaveLockPhone() {
    return 0;
}

UINT GetShellNotifyMsg_EntryShutDown() {
    return 0;
}

UINT GetShellNotifyMsg_LeaveShutDown() {
    return 0;
}

UINT GetShellNotifyMsg_ReadyPowerOFF() {
    return 0;
}

BOOL RegisterTouchNotify(HWND hWnd, INT iNotifyMsg) {
    return TRUE;
}

BOOL UnRegisterTouchNotify(HWND hWnd, INT iNotifyMsg) {
    return TRUE;
}

BOOL RefreshWallpaper(int nPaper) {
    return TRUE;
}

BOOL HideMzTopBar() {
    return TRUE;
}

BOOL ShowMzTopBar() {
    return TRUE;
}

DWORD GetCallRegisterMessage() {
    return 0;
}

DWORD GetSmsRegisterMessage() {
    return 0;
}

BOOL SetBackLightLevel(DWORD level/*0-255*/) {
    return TRUE;
}

BOOL GetBackLightLevel(DWORD * pLevel) {
    return TRUE;
}

DWORD GetSystemIdleTimeout(VOID) {
    return TRUE;
}

BOOL SetBackLightState(BOOL bOpened) {
    return TRUE;
}

BOOL SetLuxSensorState(BOOL bOpened) {
    return TRUE;
}

BOOL GetLuxSensorState(BOOL * bOpened) {
    return TRUE;
}

BOOL MzAccOpen() {
    return TRUE;
}

void  MzAccClose() {

}

DWORD  MzAccGetMessage() {
    return 0;
}
