#ifndef _MAIN_UNIT
#define _MAIN_UNIT

#include <mzfc_inc.h>
#include <vector>
#include <set>
#include <QtCore/QMap>
#ifdef UNDER_CE
#include <mzfc/MzDDrawOverlay.h>
#else
#include "MzDDrawOverlay86.h"
#endif
#include <QtCore/QPluginLoader>
#include <QtGui/QImage>
#include <QtGui/QMovie>
#include <QtGui/QImageReader>
#include "DirtyDefination.h"

using std::vector;
using std::set;

enum PageMarkDirection {
    pmdLeft,
    pmdRight,
    pmdUp,
    pmdDown
};

enum ZoomMarkAction {
    zmaOriginal,
    zmaBestFit,
    zmaIn,
    zmaOut
};

enum StageShowMode {
    ssmFit,
    ssmOriginal,
    ssmZoom,
    ssmAlumb
};

enum FingerMovingAction {
    fmaLeft,
    fmaRight,
    fmaUp,
    fmaDown,
    fmaLeftUp,
    fmaRightUp,
    fmaLeftDown,
    fmaRightDown
};

enum StageRenderMode {
    srmFullscreenOverlayHorizontal,
    srmFullscreenOverlayVertical,
    srmFullscreenGDIHorizontal,
    srmFullscreenGDIVertical,
    srmWindowedOverlayHorizontal,
    srmWindowedOverlayVertical,
    srmWindowedGDIHorizontal,
    srmWindowedGDIVertical
};

enum StreamCodec {
    scUnknown = -1,
    scStored = 0,
    scSHRUNK,
    scReduced1,
    scReduced2,
    scReduced3,
    scReduced4,
    scImploded,
    scToken,
    scDeflate,
    scDeflate64
};

enum ExtraInfoLevel {
    eilSilent,
    eilPageOnly,
    eilPageExif,
    eilSLRChart,
    eilBench
};

enum MangaDoublePageMode {
    mdpmSingle,
    mdpmAutoNippon,
    mdpmForceNippon,
    mdpmAutoNative,
    mdpmForceNative
};

enum ScaleFilter {
    sfAuto,
    sfQt,
    sfLanczos3,
    sfQuadRatic
};

typedef struct tagExtraInfoRec {
    CMzStringW filename;
    int fileindex, allfilecount;
    int loadtime, decodetime, scaletime, drawtime;
    int codecindex;
    int filesize, originalwidth, originalheight;
    bool fitrotated;
} TExtraInfoRec;

typedef struct tagPrefetchRecItem {
    tagPrefetchRecItem();
    int FileIndex, RightGuideLineX, LeftGuideLineX;
    bool FitExpired, FitRotated, FromGrayscale, FromAnimate, DoublePage, Sharpened, Corrupted;
    QImage *FitImage, *LargeImage, *EmbThumbImage, *AlumbThumbImage;
    QImageReader *AnimateStore;
    TExtraInfoRec ExtraInfo;
    MangaDoublePageMode DoublePageMode;
} TPrefetchRecItem;

typedef struct tagScrollFileRecItem {
    CMzStringW filename, archivename;
    long long filesize;
    FILETIME filetimestamp;
    bool fromarchive;
    long long streamoffset, streamsize, sourcesize;
    StreamCodec streamcodec;

    tagScrollFileRecItem();
} TScrollFileRecItem;
typedef vector < TScrollFileRecItem > TScrollFileList;


typedef struct tagMovingRecItem {
    long long ticket;
    int srcx, srcy;
    int destx, desty;
} TMovingRecItem;

typedef struct tagFakeWidgetRecItem {
    tagFakeWidgetRecItem() {
        Rotation = 0;
        DrawHoverRect = true;
        Hovered = false;
        ParentID = -1;
        SingleToggle = false;
    }
    QRect TouchRect, DrawRect, CaptionRect;
    QImage NormalImage, HoverImage, DisableImage;
    int Rotation;
    QString Caption;
    bool DrawHoverRect;
    bool Hovered, SingleToggle; // TODO: state
    int ID, ParentID;
} TFakeWidgetRecItem;

typedef struct tagStageMetric {
    int stage1width, stage1height; // 720 * 480 in MViewLite
    int stage1left;
} TStageMetric;

class TThread;
class CMainWnd;
typedef int (CMainWnd::*TOnThreadExecute)(TThread *); // fake the functor

#pragma pack(push, 1)
typedef union _UFUNCTION {
    struct {
        CMainWnd* Data;
        TOnThreadExecute Code;
    };
    DWORDLONG Quad;
} UFUNCTION;
#pragma pack(pop)

// Main window derived from CMzWndEx
class CMainWnd: public CMzWndEx
{
    MZ_DECLARE_DYNAMIC(CMainWnd);
private:
    HMODULE SateHandle;
    bool fOverlayMode, fFullScreen, fPaperBorder, fPageMark, fZoomMark, fDrawToolbar, fInShellUsage;
    bool fBasePageLoaded, fRotisLoaded, fEmulatorDetected, f0619Detected, fSettingsSaved;
    bool fWorldRoted, fLocalRoted, fWindowedHonz;
    CMzStringW fStagefile, fLastBrowsePath;
    TScrollFileList fScrollFiles;
    int fCurrentScrollIndex, fInternelPageIndex, fAlumbMarkedIndex, fAlumbMarkedCount;
    PageMarkDirection fScrollDirection, fLastPushDirection;
    ZoomMarkAction fZoomAction;
    StageShowMode fStageShowMode;
    ExtraInfoLevel fExtraInfo;
    QImage fZoomMarkBestFitCache, fZoomMarkOriginalCache, fMovingOriginalPiece, fAlumbPiece, fAlumbFilmStripe, fAlumbCursor, fMarkBaseCache;
    DWORD fOriginalBacklightLevel, fOriginalBacklightKeep;
    BOOL fOriginalAutoBacklight, fAutoBacklightChanged, fACCOpened;
    DWORD fCallMessageID, fSmsMessageID, fACCMessageID;
    DWORD fLockScreenStartMessageID, fLockScreenFinishMessageID, fShutdownMenuMessageID, fShutdownCancelMessageID, fShuttingDownMessageID, fAlarmStartMessageID;
    QMap < int, TPrefetchRecItem > fFetchedQImages; // fileindex, resource
    QMap < int, TFakeWidgetRecItem > fFakeWidgets; // ID, Widgets
    bool fBuildingPrefetch, fDiscardClick, fDiscardLongPress, fDisacardOverlayShow, fKeepAni, fPauseAni; // TODO: nuke Keep/Pause
    long fPaintingStage;
    int fPrefetchWidth, fPrefetchHeight, fPrefetchBuildingIndex;
    set < int > fBuildingFitIndexes;
    TThread *fPrefetchBuilderThread, *fFlowingKeeperThread, *fAnimationKeeperThread, *fToolbarKeeperThread, *fStagePainterThread;
    TThread *fBoxKillerThread;
    CRITICAL_SECTION fPreqMapcs, fFlowingcs, fBuildingcs, fAnimatecs;
    double fZoomLevel;
    int fLastMouseX, fLastMouseY, fOriginalOffsetX, fOriginalOffsetY;
    int LeftMovingCount, RightMovingCount, UpMovingCount, DownMovingCount;
    int LeftUpMovingCount, RightUpMovingCount, LeftDownMovingCount, RightDownMovingCount;
    long long fLastZoomTick, fLastMouseDownTick, fLastToolbarDrawTick, fLastExtraInfoLevelHintTick, fLastZoomLevelHintTick;
    int fPrevScollDepth, fNextScrollDepth;
    TStageMetric fStageSize;
    bool fDoSingleCapture, fCaptureForDesktop, fAutoPlayMode, fMangaOptimizeMode;
    MangaDoublePageMode fDoublePageMode;
    ScaleFilter fScaleFilter;
public:
    CMainWnd();
    ~CMainWnd();
public:
    // Demo
#ifdef UNDER_CE
    MzDDrawOverlay stage1;
#else
    MzDDrawOverlay86 stage1;
#endif
    UiStatic stage0;
protected:
    // Initialization of the window (dialog)
    virtual BOOL OnInitDialog();

    // override the MZFC command handler
    virtual void OnMzCommand(WPARAM wParam, LPARAM lParam);

    // override the MZFC window messages handler
    LRESULT MzDefWndProc(UINT message, WPARAM wParam, LPARAM lParam);

    // nuke repaint
    virtual void OnPaint(HDC hdc, LPPAINTSTRUCT ps);

    virtual int OnShellHomeKey(UINT message, WPARAM wParam, LPARAM lParam);
protected:
    int OnPrefetchBuilderThreadExecute(TThread* Sender);
    int OnHealthyKeeperThreadExecute(TThread* Sender);
    int OnFlowingKeeperThreadExecute(TThread* Sender);
    int OnAnimationKeeperThreadExecute(TThread* Sender);
    int OnWaitingMarkKeeperThreadExecute(TThread* Sender);

    int OnStagePainterThreadExecute(TThread* Sender);
    int OnBoxKillerThreadExecute(TThread* Sender);

    bool OnMouseDown( int x, int y, int& Rslt );
    bool OnMouseUp( int x, int y );

    bool OnMouseDblClick( int x, int y );
    bool OnMouseMove(int x, int y);
    bool OnZoomIn(int x, int y);
    bool OnZoomOut(int x, int y);
    bool OnZoomEnd(int x, int y);
    bool OnLongPressStart(int x, int y);
    bool OnLongPressTimeout(int x, int y);
private:
    // UI
    void PaintStage(bool fork = false);
    void PaintStage(int index);

    bool CheckStageMatchDesign( DDSURFACEDESC &surface, LPBYTE &dest, bool &blockable );
    void PaintFlushStage( QImage &qimage, LPBYTE dest, DDSURFACEDESC &surface, int *drawtime = NULL );
    void PaintStage0(HDC hdc, RECT* prcUpdate = NULL);
    void MixPhotoOnStage(const QImage& groundimage, const QImage& coverimage, int offsetx, int offsety, qreal rotation, qreal alpha);

    bool RebuildScrollList();
    void ScrollPage();
    void ClickToNextPage(bool next);
    bool ShowPageMark(PageMarkDirection direction);
    bool ShowZoomMark(ZoomMarkAction action);
    void ShowFlowingOnStageFork( PageMarkDirection direction );
    void ShowAnimationOnStageFork();
    void ShowWaitingMarkOnStageFork();
    void TryStopAnimation();
    void TryRefreshStageOnFit();
    void PaintStageFork();
    //bool PrepareScaledQPicture(QImage& qimage, int destwidth, int destheight, int& scaletime);
    static bool PrepareScaledQPicture( QImage& qimage, int destwidth, int destheight, ScaleFilter filter, int& scaletime );
    static void OptimizeMangaImage(QImage& qimage);

    void DrawExtraInfoOnQImage( QImage& qimage, int stagewidth, int stageheight, TExtraInfoRec& extrainfo ); // TODO: Mixer
    void DrawPageMarkOnQImage( QImage& qimage, PageMarkDirection direction ); // TODO: Mixer
    void DrawZoomMarkOnQImage( QImage& qimage, int action ); // TODO: Mixer
    void CheckPrefetchHealthy( bool flush = false );
    void BuildExtraPrefetch( int destwidth, int destheight );
    void BuildSinglePrefetch( int index, int destwidth, int destheight, bool force = false );
    void BuildSingleAlumbnail( int index, int destwidth, int destheight, bool force = false);
    void BuildExtraPrefetchFork(int destwidth, int destheight);
    bool MixdownAlumbPiece();

    int PastleSingleAlumbThumb( int iworkingindex, QPainter &painter, QPen linepen, QPen fontpen, bool checked = false );

    void FlushToolbarIconState();

    // critical
    bool ProcessMessage();
    bool CleanupTouchNotify();
    bool ToggleFullScreen();

    // Render
    // TODO: move Render functions to Render Object after 2010
    bool RenderMaintainStage();
    bool RenderSwitchStage(StageRenderMode mode);
    bool RenderPrepareShellUsage(); // leave flags un-touch for maintain
    bool RenderPrepareMZFCUsage(); // TODO: pure Widget instead MzWnd
    bool RenderFormatLocalPos(int& x, int& y);

    // Misc
    bool BrowseFile();
    void exitmessage(const TCHAR* msg, bool autohide = false); // TODO: remove tiny jpeg job 
    int NormalizeScrollIndex( int index );
    int CalculateIndexDistance( int baseindex, int sideindex );
    void UpdateScrollDepthLevel( PageMarkDirection direction );
    static bool NiceToRotate( int width, int height, int destwidth, int destheight );
    static bool NicetoScaleOnFly( QSize &orgsize, bool ispng, bool isjpeg );
    void FlushSettings();
    void CalculateMovingStep( int deltax, int deltay );
    int GuessFingerAction();
    QString ExtraLevel2Str( ExtraInfoLevel ExtraInfo );

    // Widget Emulator
    bool AddWidget( int ID, const QRect& rect, QImage normalimage, QString caption = "" );
    bool SetWidgetImage( int ID, QImage normalimage );
    bool SetWidgetParent( int ID, int ParentID );
    void DrawWidgetsOnQImage( QImage& qimage );
public:
    void WaitForKillBoxFork();
};

class TThread {
public:
    explicit TThread(UFUNCTION Proc, int Priority = THREAD_PRIORITY_NORMAL);
    ~TThread();
public:
    int virtual Execute();
    void Resume(void);
    void Suspend(void);
    void Terminate(void);
    int WaitFor(int tick = INFINITE);
protected:
    bool FSuspended,
        FTerminated;
    DWORD FThreadID;
    HANDLE FHandle;
    int FResult;
    UFUNCTION fOnExecute;
public:
    HANDLE GetHandle();
    int GetThreadId();
    bool GetTerminated();
};

#endif
