#ifndef _PARARERU_IMAGE_H
#define _PARARERU_IMAGE_H

#include <QtGui/QImage>

class QRenderImage: public QPaintDevice {
public:
    QRenderImage(char* surface, int bpp, int width, int height);
    QRenderImage();
    ~QRenderImage();
};

#endif