#include <QtGui/QImage>
#include <math.h>
#include "QBaseFilter.h"

template<class T>
inline const T& kClamp( const T& x, const T& low, const T& high )
{
    if ( x < low )       return low;
    else if ( high < x ) return high;
    else                 return x;
}

inline
int changeBrightness( int value, int brightness )
{
    return kClamp( value + brightness * 255 / 100, 0, 255 );
}

inline
int changeContrast( int value, int contrast )
{
    return kClamp((( value - 127 ) * contrast / 100 ) + 127, 0, 255 );
}

inline
int changeGamma( int value, int gamma )
{
    return kClamp( int( pow( value / 255.0, 100.0 / gamma ) * 255 ), 0, 255 );
}

inline
int changeUsingTable( int value, const int table[] )
{
    return table[ value ];
}

template< int operation( int, int ) >
static
void/*QImage*/ changeImage( QImage& image, int value )
{
    //QImage image = image;
    //image.detach();
    if( image.numColors() == 0 ) // 8bit grayscale, or 16/24/32 bit image
    {
        if( image.format() != QImage::Format_RGB32 ) /* just in case */
            image = image.convertToFormat( QImage::Format_RGB32 );
        int table[ 256 ];
        for( int i = 0;
            i < 256;
            ++i )
            table[ i ] = operation( i, value );
        if( image.hasAlphaChannel() )
        {
            for( int y = 0;
                y < image.height();
                ++y )
            {
                QRgb* line = reinterpret_cast< QRgb* >( image.scanLine( y ));
                for( int x = 0;
                    x < image.width();
                    ++x )
                    line[ x ] = qRgba( changeUsingTable( qRed( line[ x ] ), table ),
                    changeUsingTable( qGreen( line[ x ] ), table ),
                    changeUsingTable( qBlue( line[ x ] ), table ),
                    changeUsingTable( qAlpha( line[ x ] ), table ));
            }
        }
        else
        {
            for( int y = 0;
                y < image.height();
                ++y )
            {
                QRgb* line = reinterpret_cast< QRgb* >( image.scanLine( y ));
                for( int x = 0;
                    x < image.width();
                    ++x )
                    line[ x ] = qRgb( changeUsingTable( qRed( line[ x ] ), table ),
                    changeUsingTable( qGreen( line[ x ] ), table ),
                    changeUsingTable( qBlue( line[ x ] ), table ));
            }
        }
    }
    else
    {
        QVector<QRgb> colors = image.colorTable();
        for( int i = 0;
            i < image.numColors();
            ++i )
            colors[ i ] = qRgb( operation( qRed( colors[ i ] ), value ),
            operation( qGreen( colors[ i ] ), value ),
            operation( qBlue( colors[ i ] ), value ));
    }
    //return image;
}


// brightness is multiplied by 100 in order to avoid floating point numbers
void changeBrightness( QImage& image, int brightness )
{
    if( brightness == 0 ) // no change
        return;
    changeImage< changeBrightness >( image, brightness );
}


// contrast is multiplied by 100 in order to avoid floating point numbers
void changeContrast( QImage& image, int contrast )
{
    if( contrast == 100 ) // no change
        return;
    changeImage< changeContrast >( image, contrast );
}

// gamma is multiplied by 100 in order to avoid floating point numbers
void changeGamma( QImage& image, int gamma )
{
    if( gamma == 100 ) // no change
        return;
    changeImage< changeGamma >( image, gamma );
}