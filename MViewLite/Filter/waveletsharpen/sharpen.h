#ifndef __SHARPEN_H__
#define __SHARPEN_H__

#include <QtGui/QImage>

void sharpen(QImage& qimage, double amount, double radius, bool luminanceonly);
QImage sharpenex(const QImage& qimage, double amount, double radius, bool luminanceonly);

#endif