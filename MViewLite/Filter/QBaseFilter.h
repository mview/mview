#ifndef _QBASEFILTER_H
#define _QBASEFILTER_H

// brightness is multiplied by 100 in order to avoid floating point numbers
void changeBrightness( QImage& image, int brightness );
// contrast is multiplied by 100 in order to avoid floating point numbers
void changeContrast( QImage& image, int contrast );
// gamma is multiplied by 100 in order to avoid floating point numbers
void changeGamma( QImage& image, int gamma );

#endif
