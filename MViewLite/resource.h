//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Addon.rc
//
#define IDR_PNG_CHECKON                 104
#define IDS_STRING_OK                   105
#define IDS_STRING_CANCEL               106
#define IDR_PNG_PAGELEFT                106
#define IDS_STRING_APPLY                107
#define IDS_STRING_EXIT                 108
#define IDR_PNG_TOOLBAR_EXIT            108
#define IDS_STRING_GDI                  109
#define IDR_PNG_TOOLBAR_MENU            109
#define IDS_STRING_MENU                 110
#define IDR_PNG_TOOLBAR_REFRESH         110
#define IDS_STRING_WALLPAPER            111
#define IDR_PNG_TOOLBAR_FULLSCREEN      112
#define IDS_STRING_1TO2                 112
#define IDR_PNG_ZOOMBESTFIT             113
#define IDS_STRING_NIPPON               113
#define IDR_PNG_ZOOMORIGINAL            114
#define IDS_STRING_NATIVE               114
#define IDR_PNG_FILESTRIPE_VECBG        115
#define IDS_STRING_SINGLE               115
#define IDS_STRING_QJPEGLOADFAILED      116
#define IDR_PNG_M_BROWSE                117
#define IDS_STRING_ALREADYRUNNING       118
#define IDR_PNG_M_CAPTURE               118
#define IDS_STRING_OPTION               119
#define IDR_PNG_M_MANGA                 119
#define IDS_STRING_SETTING              120
#define IDS_STRING_INITFAILED           121
#define IDR_PNG_MANGA_NIPPON            121
#define IDS_STRING_NOM8LCD              122
#define IDR_PNG_MANGA_NATIVE            122
#define IDS_STRING_INITOVERLAYFAILED    123
#define IDR_PNG_XFREESELECT             123
#define IDS_STRING_CAPTURE              124
#define IDR_PNG_WAITINGSTRIPE           124
#define IDR_PNG_MANGA_SINGAL            125
#define IDS_STRING_PHOTO                125
#define IDR_PNG_MANGA_SINGLE            125
#define IDR_PNG_OPT_FANTA               126
#define IDR_PNG_OPT_ORIGIN              127
#define IDS_STRING_HINT                 128
#define IDR_PNG_OPT_MANGA               128
#define IDS_STRING_WARNING              130
#define IDS_STRING_RETURN               134
#define IDS_STRING_OVERLAY              139
#define IDS_STRING_BROWSE               140
#define IDR_PNG_M_WALLPAPER             141

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
