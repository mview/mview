#ifndef _WINCE_PLACEHOLDER_
#define _WINCE_PLACEHOLDER_

#ifdef MZFC_X86

#include <stdio.h>
#include <tchar.h>
#include <objbase.h>

void MZFC_DBG(const wchar_t *format, ...);

#ifdef _DEBUG
#define DEBUGMSG(cond, printf_exp)   ((cond) ? (MZFC_DBG printf_exp), 1 : 0)
#endif
#ifndef VERBOSE
#define VERBOSEMSG(cond,printf_exp) ((void)0)
#else
#define VERBOSEMSG(cond,printf_exp)   \
    ((cond)?(MZFC_DBG printf_exp),1:0)
#endif

#define NKDbgPrintfW(printf_exp, ...) MZFC_DBG(printf_exp, __VA_ARGS__)

#else
#ifndef VERBOSE
#define VERBOSEMSG(cond,printf_exp) ((void)0)
#else
#define VERBOSEMSG(cond,printf_exp)   \
    ((cond)?(NKDbgPrintfW printf_exp),1:0)
#endif
#endif // MZFC_X86


#endif
