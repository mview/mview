////////////////////////////////////////////////////////////////////////////////////////////
// Helper for MainUnt.
// Some collection for non UI related worker
////////////////////////////////////////////////////////////////////////////////////////////
#include <mzfc_inc.h>
#include <mzfc/ImagingHelper.h>
#include <ShellNotifyMsg.h>
#include "WincePlaceHolder.h"


#include <FvMzKOL.h>
#include "MainUnt.h"
#include "resource.h"
#include "StringInterner.h"
#include "DbCentre.h"
#include "AddonFuncUnt.h"

#include <QtGui/QImage>
#include <QtGui/QLabel>
#include <QtGui/QImageReader>
#include <QtGui/QImageWriter>
#include <QtGui/QPainter>
#include <QtGui/QApplication>
#include <QtCore/QDateTime>
#include <QtCore/QtEndian>
#include <QtCore/QBuffer>
#include "Filter/QBaseFilter.h"
#include "Filter/waveletsharpen/sharpen.h"
#include "Filter/QResample.h"


void CMainWnd::exitmessage(const TCHAR* msg, bool autohide) {
    stage1.HideOverlay();
    if (autohide) {
        MzAutoMsgBoxEx(m_hWnd, CMzStringW(msg), 1600);
    } else {
        MzMessageBoxEx(m_hWnd, CMzStringW(msg), _T(""), MZ_OK, 0);
    }
    stage1.ShowOverlay();
}



#ifdef TESTNUKED
void CMainWnd::PaintWinNuked( HDC hdc, RECT* prcUpdate /*= NULL*/ ) {
    RETAILMSG(fFullScreen, (L"PaintWin trigged under overlay\n"));
    return;
}
#endif


void CMainWnd::OnPaint(HDC hdc, LPPAINTSTRUCT ps) {
    RETAILMSG(fFullScreen, (L"OnPaint trigged under overlay\n"));
    return;
}


void CMainWnd::PaintStage(int index)
{
    if (fPaintingStage) {
        return;
    }
    InterlockedIncrement(&fPaintingStage); // TODO: InterlockedExchange
    int workingindex = NormalizeScrollIndex(index);
    TScrollFileRecItem& workingitem = fScrollFiles[workingindex];
    CMzStringW filename = workingitem.filename;
    if (workingitem.fromarchive) {
        if (!FileExists(workingitem.archivename)) {
            exitmessage(L"Cannot open archive", true);
        }
    } else {
        if (!FileExists(filename) && false) {
            exitmessage(L"Cannot open filename", true);
        }
    }
    int loadtime = -1, decodetime = -1, drawtime = -1, scaletime = -1;
    // Dual Entry for both GDI/Overlay render in QJpeg Mode
    // we assume QJpeg can decode jpeg/png/gif/bitmap

    TImageType mimetype;
    if (workingitem.fromarchive) {
        mimetype = itPKArchive;
    } else {
        mimetype = GuessMIMEType(filename);
    }

    // Overlay, QImage
    int width, height, orgwidth, orgheight;
    int eatedmem = -1;
    bool isanimation = false;

    // TODO: check index
    QImage qimage;
    if (fBuildingPrefetch && fPrefetchBuildingIndex == workingindex) {
        // last building running now
        EnterCriticalSection(&fBuildingcs); // wait for signal
        LeaveCriticalSection(&fBuildingcs); // fake leave
    } else {
        if (fStageShowMode == ssmFit) {
            // Try build current Prefetch
            if (fPrefetchBuilderThread != NULL && !fPrefetchBuilderThread->GetTerminated()) {
                // TODO: remove suspend using fBuildingIndexes
                //fPrefetchBuilderThread->Suspend();
                BuildSinglePrefetch(workingindex, fStageSize.stage1width, fStageSize.stage1height, true);
                //fPrefetchBuilderThread->Resume();
            } else {
                EnterCriticalSection(&fPreqMapcs);
                BuildSinglePrefetch(workingindex, fStageSize.stage1width, fStageSize.stage1height, true);
                LeaveCriticalSection(&fPreqMapcs);
            }
        }
    }
    EnterCriticalSection(&fPreqMapcs);
    bool insertion = !fFetchedQImages.contains(workingindex);
    TPrefetchRecItem& preqimage = fFetchedQImages[workingindex]; // silent insert?
    if (insertion) {
        preqimage.FitImage = NULL;
        preqimage.LargeImage = NULL;
        preqimage.EmbThumbImage = NULL;
        preqimage.AlumbThumbImage = NULL;
        preqimage.AnimateStore = NULL;
        preqimage.FromAnimate = false;
        preqimage.FitExpired = true;
        preqimage.DoublePageMode = mdpmSingle;
    }
    LeaveCriticalSection(&fPreqMapcs);
    if (insertion 
        || ( fStageShowMode == ssmFit && (preqimage.FitImage == NULL || preqimage.FitExpired || preqimage.DoublePageMode != fDoublePageMode) ) 
        || ( fStageShowMode == ssmOriginal && preqimage.LargeImage == NULL)
        ) {
        // Try to generate new fit/large/store image
        if (fStageShowMode == ssmFit) {
            RETAILMSG(!insertion, (L"Regenerate FitImage for %s in PaintStage.\n", filename.C_Str()));
            RETAILMSG(insertion, (L"Generate new FitImage for %s in PaintStage.\n", filename.C_Str()));
            //int lastmem = GetFreePhysMemory();
            QImageReader* reader;
            QBuffer uncompressbuf; // shouldn't delete before reader read
            if (workingitem.fromarchive) {
                QFile pkfile(QString::fromUtf16((ushort*)(workingitem.archivename.C_Str())));
                pkfile.open(QIODevice::ReadOnly);
                pkfile.seek(workingitem.streamoffset);
                quint32 sourcesize = workingitem.sourcesize;
                QByteArray packedstream = pkfile.read(workingitem.streamsize);
                pkfile.close();
                if (workingitem.streamcodec == scStored) {
                    uncompressbuf.setData(packedstream);
                } else if (workingitem.streamcodec == scDeflate) {
                    uncompressbuf.setData(qUncompressEx((uchar*)packedstream.constData(), packedstream.size(), sourcesize));
                }
                mimetype = GuessMIMEType(&uncompressbuf);
                uncompressbuf.open(QBuffer::ReadOnly);

                reader = new QImageReader(&uncompressbuf);
            } else {
                reader = new QImageReader(QString::fromUtf16((ushort*)(filename.C_Str())), MIMETypeToExt(mimetype).toAscii());
            }

            //eatedmem = lastmem - GetFreePhysMemory();
            QSize orgsize = reader->size();
            orgwidth = orgsize.width();
            orgheight = orgsize.height();
            if (mimetype == itJPEG) {
                // Try to load thumbnail from EXIF embedded field
                //QFile qfile(QString::fromUtf16((ushort*)(filename.C_Str())));
                //qfile.open(QIODevice::ReadOnly);
                //QImage embedded = NewQImageFromEXIFThumbnail(qfile);
                //if (!embedded.isNull()) {
                //    QImage *smart = (QImage*)InterlockedExchange(((long*)&preqimage.EmbThumbImage), long(new QImage(embedded)));
                //    if (smart && insertion == false) {
                //        delete smart;
                //    }
                //}
            }
            
            if (mimetype == itPNG || mimetype == itJPEG) {
                if (NicetoScaleOnFly(orgsize, mimetype == itPNG, mimetype == itJPEG)) {
                    reader->setScaledSize(orgsize);
                    reader->setQuality(49);
                }
            }
            int starttime = GetTickCount();
            int lastmem = GetFreePhysMemory();
            if (reader->read(&qimage) == false) {
                eatedmem = lastmem - GetFreePhysMemory();
                qimage = QImage();
            } else {
                // TODO: GIF rewind
                // DONE: Processed in Animate fork
            }
            //qimage.load(QString::fromUtf16((ushort*)(filename.C_Str()))); // Large one
            loadtime = GetTickCount() - starttime;
            if (reader->supportsAnimation() && reader->canRead()/* nextImageDelay() > 0*/) {
                // TODO: size detection
                isanimation = true;
                preqimage.FromAnimate = true;
                preqimage.AnimateStore = reader;
            } else {
                delete reader;
            }

            // drop last fit
            QImage *smart = (QImage*)InterlockedExchange(((long*)&preqimage.FitImage), 0);
            if (smart && insertion == false) {
                delete smart;
            }
        } else if (fStageShowMode == ssmOriginal) {
            int starttime = GetTickCount();
            //preqimage.LargeImage = new QImage(QString::fromUtf16((ushort*)(filename.C_Str())));
            if (workingitem.fromarchive) {
                QFile pkfile(QString::fromUtf16((ushort*)(workingitem.archivename.C_Str())));
                pkfile.open(QIODevice::ReadOnly);
                pkfile.seek(workingitem.streamoffset);
                quint32 sourcesize = workingitem.sourcesize;
                QByteArray packedstream = pkfile.read(workingitem.streamsize);
                pkfile.close();
                if (workingitem.streamcodec == scStored) {
                    preqimage.LargeImage = new QImage;
                    preqimage.LargeImage->loadFromData(packedstream);
                } else if (workingitem.streamcodec == scDeflate) {
                    preqimage.LargeImage = new QImage;
                    preqimage.LargeImage->loadFromData(qUncompressEx((uchar*)packedstream.constData(), packedstream.size(), sourcesize));
                }
            } else {
                // TODO: Scaled Size
                preqimage.LargeImage = new QImage(QString::fromUtf16((ushort*)(filename.C_Str())));
            }

            loadtime = GetTickCount() - starttime;
            // no need to drop NULL original large one
        }

        //orgwidth = qimage.width();
        //orgheight = qimage.height();
    } else {
        // not insertion, can use
        if (fStageShowMode == ssmFit) {
            qimage = *preqimage.FitImage; // @CopyRecord
        } else if (fStageShowMode == ssmOriginal) {
            //qimage = *preqimage.LargeImage; // Dangerous?
        }
        loadtime = preqimage.ExtraInfo.loadtime;
        scaletime = preqimage.ExtraInfo.scaletime;
        orgwidth = preqimage.ExtraInfo.originalwidth;
        orgheight = preqimage.ExtraInfo.originalheight;
        isanimation = preqimage.FromAnimate;
    }
    // we get image though prefetch (720*480, 720*960) or inplace decode (may scale on fly) now
    // orgwidth, orgheight ready
    bool isvalid = false;
    if (fStageShowMode == ssmFit) {
        isvalid = !qimage.isNull();
    } else if (fStageShowMode == ssmOriginal) {
        isvalid = !preqimage.LargeImage->isNull();
    } else if (fStageShowMode == ssmAlumb){
        isvalid = !fAlumbPiece.isNull();
    }

    if (IsLockPhoneStatus() == TRUE) {
        InterlockedDecrement(&fPaintingStage);
        RETAILMSG(true, (L"Cancel Draw on Shutdown."));
        return;
    }

    LPBYTE dest;
    bool blockable = false;
    DDSURFACEDESC surface;
    bool stagesizematched = CheckStageMatchDesign(surface, dest, blockable);

    if (stagesizematched) {
        if (isvalid) {
            if (fStageShowMode == ssmFit) {
                width = qimage.width();
                height = qimage.height();
            }
            // Time to scale
            if (fStageShowMode == ssmOriginal) {
                //// Store for next 
                //if (preqimage.LargeImage == NULL) {
                //    QImage *smart = new QImage(qimage); // @CopyRecord
                //    InterlockedExchange((long*)&preqimage.LargeImage, (long)smart);
                //}
                // only copy
                int combinex, combiney;
                if (fZoomLevel == 1) {
                    if (preqimage.FitRotated) {
                        combinex = (preqimage.LargeImage->height() - int(fStageSize.stage1width)) / 2 + fOriginalOffsetY;
                        combiney = (preqimage.LargeImage->width() - int(fStageSize.stage1height)) / 2 - fOriginalOffsetX;

                        // TODO: more clear transform
                        QMatrix maxtrix;
                        maxtrix.rotate(-90);
                        qimage = preqimage.LargeImage->copy(combinex, combiney,  fStageSize.stage1height, fStageSize.stage1width ).transformed(maxtrix);
                    } else {
                        combinex = (preqimage.LargeImage->width() - int(fStageSize.stage1width)) / 2 - fOriginalOffsetX;
                        combiney = (preqimage.LargeImage->height() - int(fStageSize.stage1height)) / 2 - fOriginalOffsetY;

                        qimage = preqimage.LargeImage->copy(combinex, combiney, fStageSize.stage1width, fStageSize.stage1height );
                    }
                } else {
                    // TODO: Roted
                    int smallw = fStageSize.stage1width / fZoomLevel;
                    int smallh = fStageSize.stage1height / fZoomLevel;
                    if (preqimage.FitRotated) {
                        combinex = (preqimage.LargeImage->width() - int(fStageSize.stage1width)) / 2 + fOriginalOffsetY / fZoomLevel;
                        combiney = (preqimage.LargeImage->height() - int(fStageSize.stage1height)) / 2 - fOriginalOffsetX / fZoomLevel;

                        QMatrix maxtrix;
                        maxtrix.rotate(-90);
                        qimage = preqimage.LargeImage->copy(combinex, combiney, smallh, smallw).transformed(maxtrix);
                    } else {
                        combinex = (preqimage.LargeImage->width() - int(fStageSize.stage1width)) / 2 - fOriginalOffsetX / fZoomLevel;
                        combiney = (preqimage.LargeImage->height() - int(fStageSize.stage1height)) / 2 - fOriginalOffsetY / fZoomLevel;
                        qimage = preqimage.LargeImage->copy(combinex, combiney, smallw, smallh);
                    }
                    qimage = qimage.scaled(fStageSize.stage1width, fStageSize.stage1height);
                }

                // TODO: an Fixed store QImage for ARGB32 to RGB32 Check
                //if (qimage.hasAlphaChannel()) {
                //    if (blockable) {
                //        ReplaceAlphaWithChecker(qimage);
                //    }
                //}
            } else if (fStageShowMode == ssmFit) {
                // qimage may fit in 720*480 or 720*960 if in prefetch
                // qimage may have wild size in insertion mode
                if (fDoublePageMode != mdpmSingle) {
                    preqimage.DoublePage = ((float)orgwidth / orgheight) > 1.2;
                }
                if (preqimage.FitImage == NULL) {
                    preqimage.FitRotated = false;
                }
                if (width > height){
                    if (preqimage.DoublePage && fDoublePageMode != mdpmSingle) {
                        // 721 * 961
                        if (width > fStageSize.stage1height * 2 || height > fStageSize.stage1width) {
                            // we wanna an 720 * 960 90 rotated picture without any border optimize
                            preqimage.FitRotated = NiceToRotate(qimage.width(), qimage.height(), fStageSize.stage1width, fStageSize.stage1height * 2);
                            PrepareScaledQPicture(qimage, fStageSize.stage1width, fStageSize.stage1height * 2, fScaleFilter, scaletime); // auto rotate
                        }
                        // else only keep and wait for black border.
                    } else if (height - fStageSize.stage1height > 80 || width - fStageSize.stage1width > 120) {
                        // Crop Scale 800x600 -> 720x480 may not rotated
                        preqimage.FitRotated = NiceToRotate(qimage.width(), qimage.height(), fStageSize.stage1width, fStageSize.stage1height);
                        PrepareScaledQPicture(qimage, fStageSize.stage1width, fStageSize.stage1height, fScaleFilter, scaletime); // auto rotate
                    }
                } else {
                    // width <= height
                    // if we get qimage from prefetch and less than 720*960, do nothing
                    if (preqimage.DoublePage && fDoublePageMode != mdpmSingle) {
                        if (width <= fStageSize.stage1width && height <= fStageSize.stage1height * 2) {
                            // less than 720 * 960, ok
                        } else {
                            preqimage.FitRotated = NiceToRotate(qimage.width(), qimage.height(), fStageSize.stage1width, fStageSize.stage1height * 2);
                            PrepareScaledQPicture(qimage, fStageSize.stage1width, fStageSize.stage1height * 2, fScaleFilter, scaletime); // auto rotate
                        }
                    } else if (width - fStageSize.stage1height > 80 || height - fStageSize.stage1width > 120) {
                        // Crop Scale 600x800 -> 720x480, sometimes rotated
                        preqimage.FitRotated = NiceToRotate(qimage.width(), qimage.height(), fStageSize.stage1width, fStageSize.stage1height);
                        PrepareScaledQPicture(qimage, fStageSize.stage1height,  fStageSize.stage1width, fScaleFilter, scaletime); // auto rotate
                    } else {
                        if (width != height) {
                            // Safe for manual Rotate?
                            // Cute image , 200x400 -> 480x720
                            QMatrix matrix;
                            matrix.rotate(-90);
                            qimage = qimage.transformed(matrix, Qt::FastTransformation);
                            preqimage.FitRotated = true;
                        } else {
                            // 96x96
                            preqimage.FitRotated = false;
                        }
                    }
                }
            } else if (fStageShowMode == ssmAlumb) {
                int combinex = (fAlumbPiece.width() - int(fStageSize.stage1width)) / 2 - fOriginalOffsetX;
                int combiney = (fAlumbPiece.height() - int(fStageSize.stage1height)) / 2 - fOriginalOffsetY;

                /*if (combinex < -10 || combinex > fAlumbPiece.width() - fStageSize.stage1width + 10) {
                    if (combinex < 0) {
                        fOriginalOffsetX = (fAlumbPiece.width() - int(fStageSize.stage1width)) / 2;
                    } else {
                        // must > width
                        fOriginalOffsetX = (fAlumbPiece.width() - int(fStageSize.stage1width)) / 2 - fAlumbPiece.width() + fStageSize.stage1width;
                    }
                    fLastMouseX = fLastMouseY = -1;
                }
                if (combiney < -10 || combiney > fAlumbPiece.height() - fStageSize.stage1height + 10) {
                    if (combiney < 0) {
                        fOriginalOffsetY = fAlumbPiece.height() - fStageSize.stage1height - (fAlumbPiece.height() - int(fStageSize.stage1height)) / 2;
                    } else {
                        fOriginalOffsetY = -(fAlumbPiece.height() - int(fStageSize.stage1height)) / 2;
                    }
                    fLastMouseX = fLastMouseY = -1;
                }*/
                if (combinex < 0 || combinex > fAlumbPiece.width() - fStageSize.stage1width) {
                    if (combinex < 0) {
                        fOriginalOffsetX = (fAlumbPiece.width() - int(fStageSize.stage1width)) / 2 - 20;
                        fCurrentScrollIndex--;
                    } else {
                        // must > width
                        fOriginalOffsetX = (fAlumbPiece.width() - int(fStageSize.stage1width)) / 2 - fAlumbPiece.width() + fStageSize.stage1width + 20;
                        fCurrentScrollIndex++;
                    }
                    fLastMouseX = fLastMouseY = -1;
                    fCurrentScrollIndex = NormalizeScrollIndex(fCurrentScrollIndex);
                    MixdownAlumbPiece();
                }
                if (combiney < 0 || combiney > fAlumbPiece.height() - fStageSize.stage1height) {
                    if (combiney < 0) {
                        fOriginalOffsetY = fAlumbPiece.height() - fStageSize.stage1height - (fAlumbPiece.height() - int(fStageSize.stage1height)) / 2 - 20;
                        fCurrentScrollIndex -= 5;
                    } else {
                        fOriginalOffsetY = -(fAlumbPiece.height() - int(fStageSize.stage1height)) / 2 + 35;
                        fCurrentScrollIndex += 5;
                    }
                    fLastMouseX = fLastMouseY = -1;
                    fCurrentScrollIndex = NormalizeScrollIndex(fCurrentScrollIndex);
                    MixdownAlumbPiece();
                }
                // Regeneration
                combinex = (fAlumbPiece.width() - int(fStageSize.stage1width)) / 2 - fOriginalOffsetX;
                combiney = (fAlumbPiece.height() - int(fStageSize.stage1height)) / 2 - fOriginalOffsetY;

                qimage = fAlumbPiece.copy(combinex, combiney, fStageSize.stage1width, fStageSize.stage1height );
            }
            // either Original n Fit get qimage here
            // try normalize 8bit GrayScale Un-cropped manga jpeg
            if (blockable && (qimage.depth() == 8 || qimage.depth() == 1)) {
                //stage1.SetTransparency(12);
                if (qimage.colorTable().empty() && qimage.depth() == 8) {
                    NormalizeGrayScaleQImage(qimage);
                    preqimage.FromGrayscale = true;
                } else {
                    // 8bpp + color table, 1bpp
                    qimage = qimage.convertToFormat(QImage::Format_RGB32, Qt::AutoColor); // ColorTable
                }
            }

            bool expired = false;
            if (fMangaOptimizeMode && preqimage.Sharpened == false) {
                // TODO: AutoLevel, SurfaceBlure, AWarpSharpen
                OptimizeMangaImage(qimage);
                expired = true;
            }
            // Store fitimage for next. we only store uncrop unchecker qimage
            if (preqimage.FitImage == NULL || insertion || expired) {
                if (fStageShowMode == ssmFit) {
                    EnterCriticalSection(&fPreqMapcs);
                    QImage *smart = new QImage(qimage); // @CopyRecord
                    InterlockedExchange((long*)&preqimage.FitImage, (long)smart);
                    preqimage.FitExpired = false; // interlocked?
                    preqimage.FileIndex = workingindex; // DONE: add in argument
                    preqimage.DoublePageMode = fDoublePageMode;
                    preqimage.Sharpened = fMangaOptimizeMode;
                    LeaveCriticalSection(&fPreqMapcs);
                }

                //preqimage.FitRotated = (rotation == 90 || rotation == 270);
                preqimage.ExtraInfo.loadtime = loadtime;
                preqimage.ExtraInfo.scaletime = scaletime;
                preqimage.ExtraInfo.originalheight = orgheight;
                preqimage.ExtraInfo.originalwidth = orgwidth;
            }

            // Add Checker Board for Alpha Image
            // TODO: 90, 270, 0
            // TODO: NULL Alpha Channel Check
            if (qimage.hasAlphaChannel()) {
                if (blockable) {
                    // checker
                    if (fStageShowMode == ssmFit) {
                        // refuse original n album
                        RETAILMSG(true, (L"AlphaChannel for %s\n", workingitem.filename.C_Str()));
                        ReplaceAlphaWithChecker(qimage);
                    }
                }
            }

            if (fStageShowMode == ssmFit) {
                // TODO: check Manga DoublePageMode
                if (fDoublePageMode != mdpmSingle && preqimage.DoublePage == true) {
                    // Splite it!
                    QMatrix matrix;
                    matrix.rotate(-90);
                    if ((fDoublePageMode == mdpmAutoNippon && fInternelPageIndex == 0) || (fDoublePageMode == mdpmAutoNative && fInternelPageIndex == 1)) {
                        // Right
                        qimage = qimage.copy(0, 0, qimage.width(), qimage.height() / 2); // TODO: overcopy
                    }
                    if ((fDoublePageMode == mdpmAutoNippon && fInternelPageIndex == 1) || (fDoublePageMode == mdpmAutoNative && fInternelPageIndex == 0)) {
                        // Left
                        qimage = qimage.copy(0, qimage.height() / 2, qimage.width(),  qimage.height() / 2); // TODO: overcopy
                    }
                }
            }

            // check if mismatch?
            if (qimage.width() != fStageSize.stage1width || qimage.height() != fStageSize.stage1height) {
                qimage = qimage.copy((qimage.width() - int(fStageSize.stage1width)) / 2, (qimage.height() - int(fStageSize.stage1height)) / 2, fStageSize.stage1width, fStageSize.stage1height );
            }
            // we can replace qimage to QRenderImage here to save memcpy time?
            // we will get 480*730 or 480*615 here

            width = qimage.width();
            height = qimage.height();


            QImage *MaybeCapImage = NULL;
            if (fDoSingleCapture && fCaptureForDesktop) {
                if (qimage.isNull() == false) {
                    if (qimage.width() > qimage.height()) {
                        QMatrix matrix;
                        matrix.rotate(90);
                        MaybeCapImage = new QImage(qimage.transformed(matrix));
                    } else {
                        MaybeCapImage = new QImage(qimage);
                    }
                }
            }

            // better to make sure 32bit here
            // Draw text on screen / image
            if (fPageMark) {
                //if (fFullScreen) {
                //    DrawWidgetsOnQImage(qimage); // TODO: auto hide on idle
                //}
                DrawPageMarkOnQImage(qimage, fScrollDirection);
            } else if (fZoomMark) {
                DrawZoomMarkOnQImage(qimage, fZoomAction);
            } else if (fExtraInfo != eilSilent && fStageShowMode == ssmFit) { // TODO: fix huge font
                TExtraInfoRec info;
                info.filename = filename;
                info.filesize = FileSize(filename);
                info.originalwidth = orgwidth;
                info.originalheight = orgheight;
                info.drawtime = drawtime;
                info.loadtime = loadtime;
                info.scaletime = scaletime;
                info.decodetime = decodetime;
                info.allfilecount = fScrollFiles.size();
                info.fileindex = workingindex;
                info.fitrotated = preqimage.FitRotated;

                DrawExtraInfoOnQImage(qimage, fStageSize.stage1width, fStageSize.stage1height, info);
            }
            int CurrentTick = GetTickCount();
            if (!fPageMark && !fZoomMark && fDrawToolbar && fStageShowMode == ssmFit) {
                if (CurrentTick - fLastToolbarDrawTick > 8000) {
                    fDrawToolbar = false;
                } else {
                    DrawWidgetsOnQImage(qimage); // TODO: auto hide on idle
                }
            }
            if (CurrentTick - fLastExtraInfoLevelHintTick <= 2000) {
                // Hint
                QRect ExtraLevelRect(20 * 2, 400* 2, 200 * 2, 60 * 2);
                QPainter ExtraHintPainter(&qimage);
                ExtraHintPainter.scale(0.5, 0.5);
                QFont litefont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 60);
                litefont.setWeight(QFont::DemiBold - 3);
                litefont.setStyleStrategy(QFont::PreferAntialias);
                ExtraHintPainter.setFont(litefont);
                ExtraHintPainter.setBrush(QBrush(QColor(0x17AD17)));
                ExtraHintPainter.setPen(QPen(Qt::white, 2));
                ExtraHintPainter.drawRoundedRect(ExtraLevelRect, 8, 8);
                QString ExtraLevelStr = ExtraLevel2Str(fExtraInfo);
                ExtraHintPainter.drawText(ExtraLevelRect, Qt::TextWrapAnywhere | Qt::AlignCenter, ExtraLevelStr);
            }
            if (CurrentTick - fLastZoomLevelHintTick <= 2000) {
                // Hint
            }
            //width = qimage.width();
            //height = qimage.height();

            if (fDoSingleCapture) {
                if (isvalid && fCaptureForDesktop == false) {
                    MaybeCapImage = new QImage(qimage);
                }
                fMarkBaseCache = qimage; // Copy
            }

            PaintFlushStage(qimage, dest, surface, &drawtime);

            preqimage.ExtraInfo.drawtime = drawtime;

            //wprintf(L"drawtime:%d\n", drawtime); // instead of RETAILMSG?
            RETAILMSG(drawtime > 0, (L"drawtime: %d\n", drawtime)); 

            if (fDoSingleCapture) {
                fDoSingleCapture = false; // Single
                if (MaybeCapImage != NULL) {
                    if (fCaptureForDesktop) {
                        ShowWaitingMarkOnStageFork();
                        MaybeCapImage->save("\\Windows\\MyViewWallpaper.png", "png", 80);
                        delete MaybeCapImage;
                        MaybeCapImage = NULL;
                    } else {
                        ShowWaitingMarkOnStageFork();
                        //MaybeCapImage->save("\\Disk\\Photo\\Camera Album\\MyView " + QDateTime::currentDateTime().toString("yyyyMMdd hhmmss") + ".png", "png", 60);
                        qimage.save("\\Disk\\Photo\\Camera Album\\MyView " + QDateTime::currentDateTime().toString("yyyyMMdd hhmmss") + ".png", "png", 60);
                        // TODO: copy after using parararaImage
                    }
                    fMarkBaseCache = QImage();

                    TryStopAnimation();
                } else {
                    fCaptureForDesktop = false;
                }
            }

            // nuke drag mode
            if (fStageShowMode == ssmFit) {
                BuildExtraPrefetchFork(fStageSize.stage1width, fStageSize.stage1height);
            }

            // Animation
            if (isanimation &&/* fOverlayMode &&*/ !fPauseAni) {
                // TODO: Animation fork
                ShowAnimationOnStageFork();
            }
        } else {
            // isvalied == false
            // Your picture shamed. we can only print some hint on stage
            if (fStageShowMode == ssmFit) {
                // DONE: drop fit image
                qimage = QImage(); // a null one
                //delete &qimage;
            } else if (fStageShowMode == ssmOriginal) {
                // TODO: drop original image
                //preqimage.LargeImage->detach();
                //delete preqimage.LargeImage;
                //preqimage.LargeImage = 0;
            }
            QList<QByteArray> formats = QImageReader::supportedImageFormats();
            QString unsupportHint = QString::fromUtf16((ushort*)filename.C_Str()) + "\n" + _trq(IDS_STRING_QJPEGLOADFAILED) + "\n";
            for (QList<QByteArray>::iterator it = formats.begin(); it != formats.end(); it++)
            {
                unsupportHint += it->data();
                if (it->data() != formats.last()) {
                    unsupportHint += " ";
                };
            }
            unsupportHint += eatedmem < 0?QString(""):QString("\n Lostmem: %1").arg(QString::fromUtf16((ushort*)Num2Bytes(eatedmem).C_Str()));
            if (GetFreePhysMemory() > 8192*1024) {
                // try generate error picture
                // Time to scale
                qimage = QImage(fStageSize.stage1width, fStageSize.stage1height, QImage::Format_ARGB32);
                QPainter painter(&qimage);
                painter.setPen(QPen(QColor(0xFFC000), 6, Qt::SolidLine)); // slight orange
                QLinearGradient linearGradient(0, 0, 474, 36);
                linearGradient.setColorAt(0.0, QColor(0x555555));
                linearGradient.setColorAt(0.1, QColor(0x6B6B6B));
                linearGradient.setColorAt(0.9, QColor(0x6B6B6B));
                linearGradient.setColorAt(1.0, QColor(0x555555));
                painter.setBrush(linearGradient);
                painter.setRenderHint(QPainter::Antialiasing);
                painter.setOpacity(1.0);
                QFont litefont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 18);
                litefont.setWeight(QFont::DemiBold);
                litefont.setStyleStrategy(QFont::PreferAntialias);
                painter.setFont(litefont);
                QRect rect = painter.boundingRect(0, 0, fStageSize.stage1width / 2, fStageSize.stage1height / 2, Qt::AlignHCenter | Qt::TextWrapAnywhere, unsupportHint);
                rect.adjust(-12, -12, 12, 12);
                rect.moveTo((fStageSize.stage1width - rect.width()) / 2, (fStageSize.stage1height - rect.height()) / 2);
                painter.drawRoundedRect(rect, 8, 8, Qt::AbsoluteSize);
                painter.setOpacity(1.0);
                painter.setPen(QPen(QColor(0xFFF8E2), 1, Qt::SolidLine)); // young yellow
                painter.setBrush(Qt::NoBrush);
                painter.setRenderHint(QPainter::TextAntialiasing);
                painter.drawText(rect.adjusted(8, 8, -8, -8), Qt::AlignHCenter | Qt::TextWrapAnywhere, unsupportHint);
                DEBUGMSG(!unsupportHint.isEmpty(), (_L("%s\n"), (WCHAR*)unsupportHint.utf16()));
                // Store for next
                if (preqimage.FitImage == NULL) {
                    if (fStageShowMode == ssmFit) {
                        EnterCriticalSection(&fPreqMapcs);
                        QImage *smart = new QImage(qimage); // @CopyRecord
                        InterlockedExchange((long*)&preqimage.FitImage, (long)smart);
                        preqimage.FitExpired = false; // interlocked?
                        preqimage.FileIndex = workingindex;
                        preqimage.DoublePageMode = fDoublePageMode;
                        LeaveCriticalSection(&fPreqMapcs);
                    }
                    //preqimage.FitRotated = (rotation == 90 || rotation == 270);
                    preqimage.ExtraInfo.loadtime = loadtime;
                    preqimage.ExtraInfo.scaletime = -1;
                    preqimage.ExtraInfo.originalheight = -1;
                    preqimage.ExtraInfo.originalwidth = -1;
                }

                PaintFlushStage(qimage, dest, surface, &drawtime);
            } else {
                exitmessage(LPTSTR(unsupportHint.utf16()));
            }
        }
    } else {
        // un matched
        exitmessage(_T("stagesizematched"));
        PostQuitMessage(-1);
    }

    InterlockedDecrement(&fPaintingStage); // fPaintingStage = 0
}

void CMainWnd::PaintStage(bool fork)
{
    if (fork) {
        PaintStageFork();
    } else {
        PaintStage(fCurrentScrollIndex);
    }
}

void CMainWnd::PaintFlushStage( QImage &qimage, LPBYTE dest, DDSURFACEDESC &surface, int *drawtime )
{
    // TODO: support crop on fly
    int width = qimage.width();
    int height = qimage.height();
#if defined(UNDER_CE) || defined(MVIEW_X86OVERLAY)
    // qimage supports 1, 8, 16, 32
    LPBYTE src = qimage.bits();
    if (qimage.depth() == 32) {
        int starttime;
        if (drawtime != NULL) {
            starttime = GetTickCount();
        }
        memcpy(dest, src, min(surface.dwWidth * surface.dwHeight * 4, width * height * 4));
        if (drawtime != NULL) {
            *drawtime = GetTickCount() - starttime;
        }
    } else if (qimage.depth() == 16) {

    }
    if (stage1.UnLockData(true) == false) {
        exitmessage(L"stage1.UnLockData(true) == false");
    }
#else
    // GDIMode
    HDC tempDC = GetDC(m_hWnd);

    // Extract from QImage
    // Define the header
    BITMAPINFO bmi;
    memset(&bmi, 0, sizeof(bmi));
    bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
    bmi.bmiHeader.biWidth       = width;
    bmi.bmiHeader.biHeight      = -height;
    bmi.bmiHeader.biPlanes      = 1;
    bmi.bmiHeader.biBitCount    = 32;
    bmi.bmiHeader.biCompression = BI_RGB;
    bmi.bmiHeader.biSizeImage   = width * height * 4;

    // Create the pixmap
    uchar *pixels = 0;
    HBITMAP bitmap = CreateDIBSection(tempDC, &bmi, DIB_RGB_COLORS, (void **) &pixels, 0, 0);

    // Copy over the data
    QImage::Format imageFormat = QImage::Format_RGB32;
    if (qimage.format() != imageFormat && qimage.format() != QImage::Format_ARGB32) {
        qimage = qimage.convertToFormat(imageFormat);
    }
    //const QImage image = qimage.convertToFormat(imageFormat);
    int bytes_per_line = width * 4;
    for (int y = 0; y < height; ++y) {
        memcpy(pixels + y * bytes_per_line, qimage.scanLine(y), bytes_per_line);
    }
    // end of code sniper

    HBITMAP HB1 = bitmap;//QPixmap::fromImage(qimage).toWinHBITMAP();
    HDC MemDC = CreateCompatibleDC(tempDC);
    HBITMAP oldBitmap = HBITMAP(SelectObject(MemDC, HB1));

    int starttime;
    if (drawtime != NULL) {
        starttime = GetTickCount();
    }
    bool failed = BitBlt(tempDC, max(0, (width - fStageSize.stage1width) / 2), max(0, (height - fStageSize.stage1height) / 2), min(fStageSize.stage1width, width), min(fStageSize.stage1height, height),
            MemDC, max(0, (fStageSize.stage1width - width) / 2), max(0, (fStageSize.stage1height - height) / 2), SRCCOPY) == FALSE;
    if (drawtime != NULL) {
        *drawtime = GetTickCount() - starttime;
    }
    RETAILMSG(failed, (_T("draw failed in PaintStage.\n")));
    DeleteObject(SelectObject(MemDC, HGDIOBJ(oldBitmap)));
    DeleteDC(MemDC);

    ReleaseDC(m_hWnd, tempDC);
#endif
}

//bool CMainWnd::PrepareScaledQPicture( QImage& qimage, int destwidth, int destheight, int& scaletime )
//{
//    int width = qimage.width();
//    int height = qimage.height();

//    // Better Quality than in viaimage 
//    if (qimage.depth() == 8) {
//        if (qimage.colorTable().empty()) {
//            NormalizeGrayScaleQImage(qimage);
//        } else {
//            // 8bpp + color table, 1bpp
//            qimage = qimage.convertToFormat(QImage::Format_RGB32);
//        }
//    }

//    Qt::TransformationMode scaleQ = double(width * height) / (destwidth * destheight) > 4? Qt::FastTransformation:Qt::SmoothTransformation;
//    if (NiceToRotate(width, height, destwidth, destheight)) {
//        int starttime = GetTickCount();
//        if (scaleQ == Qt::FastTransformation && double(width) / destheight > 2 && double(height) / destwidth > 2) {
//            qimage = qimage.scaled(width / 2, height / 2, Qt::KeepAspectRatio, scaleQ).scaled(destheight, destwidth, Qt::KeepAspectRatio, Qt::SmoothTransformation);
//        } else {
//            qimage = qimage.scaled(destheight, destwidth, Qt::KeepAspectRatio, scaleQ);
//        }
//        scaletime = GetTickCount() - starttime;
//        QMatrix matrix;
//        matrix.rotate(-90); 
//        qimage = qimage.transformed(matrix, Qt::FastTransformation);
//    } else {
//        int starttime = GetTickCount();
//        qimage = qimage.scaled(destwidth, destheight, Qt::KeepAspectRatio, scaleQ);
//        scaletime = GetTickCount() - starttime;
//    }
//    return true;
//}

bool CMainWnd::PrepareScaledQPicture( QImage& qimage, int destwidth, int destheight, ScaleFilter filter, int& scaletime )
{
    int width = qimage.width();
    int height = qimage.height();

    // Better Quality than in viaimage 
    if (qimage.depth() == 8) {
        if (qimage.colorTable().empty()) {
            NormalizeGrayScaleQImage(qimage);
        } else {
            // 8bpp + color table, 1bpp
            qimage = qimage.convertToFormat(QImage::Format_RGB32);
        }
    }

    bool rotate = NiceToRotate(width, height, destwidth, destheight);
    if (rotate) {
        qSwap(destwidth, destheight);
    }
    int starttime = GetTickCount();
    if (filter == sfLanczos3 || filter == sfQuadRatic) {
        // suite for manga
        QSize targetsize(qimage.size());
        targetsize.scale(destwidth, destheight, Qt::KeepAspectRatio);
        qimage = CreateResampledBitmap(qimage, targetsize.width(), targetsize.height(), filter == sfLanczos3?STOCK_FILTER_LANCZOS3:STOCK_FILTER_QUADRATIC);

    } else {
        // Qt == Auto
        Qt::TransformationMode scaleQ = double(width * height) / (destwidth * destheight) > 4? Qt::FastTransformation:Qt::SmoothTransformation;
        if (scaleQ == Qt::FastTransformation && double(width) / destheight > 2 && double(height) / destwidth > 2) {
            // never here
            qimage = qimage.scaled(width / 2, height / 2, Qt::KeepAspectRatio, scaleQ).scaled(destwidth, destheight, Qt::KeepAspectRatio, Qt::SmoothTransformation);
        } else {
            qimage = qimage.scaled( destwidth, destheight,Qt::KeepAspectRatio, scaleQ);
        }
    }
    scaletime = GetTickCount() - starttime;

    if (rotate) {
        QMatrix matrix;
        matrix.rotate(-90); 
        qimage = qimage.transformed(matrix, Qt::FastTransformation);
    }
    return true;
}


bool CMainWnd::NiceToRotate( int width, int height, int destwidth, int destheight )
{
    return fabs(double(width) / height - double(destheight) / destwidth) < 0.25 || double(height) / width - double(destwidth) / height > 0.5;
}


bool CMainWnd::NicetoScaleOnFly( QSize &orgsize, bool ispng, bool isjpeg  )
{
    bool cheated = false;

    int orgwidth = orgsize.width();
    int orgheight = orgsize.height();
    if ((orgwidth > orgheight) && (orgwidth > 1440) && (orgheight >= 16)) {
        // 6000 * 2400?
        if (isjpeg) {
            // libjpeg8 + qjpegmod
            // 7/8 6/8 5/8 4/8 3/8 2/8 1/8
            for (int i = 7; i >= 1; i--) {
                if (greedydiv(orgwidth * i, 8) <= 1440) {
                    cheated = true;
                    orgsize.setWidth(greedydiv(orgwidth * i, 8));
                    orgsize.setHeight(greedydiv(orgheight * i, 8));
                    break;
                }
            }
        }
        if (ispng) {
            // please apply pngrez patch
            // 1/2 1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10
            for (int i = 2; i <= 16; i++) {
                if (greedydiv(orgwidth, i) <= 1440) {
                    cheated = true;
                    orgsize.setWidth(greedydiv(orgwidth, i));
                    orgsize.setHeight(greedydiv(orgheight, i));
                    break;
                }
            }
        }
    }
    if ((orgwidth < orgheight) && (orgwidth >=16) && (orgheight >= 1440)) {
        //  2400 * 6000?
        if (isjpeg) {
            // libjpeg7only
            // 7/8 6/8 5/8 4/8 3/8 2/8 1/8
            for (int i = 7; i >= 1; i--) {
                if (greedydiv(orgheight * i, 8) <= 1440) {
                    cheated = true;
                    orgsize.setWidth(greedydiv(orgwidth * i, 8));
                    orgsize.setHeight(greedydiv(orgheight * i, 8));
                    break;
                }
            }
        }
        if (ispng) {
            // please apply pngrez patch
            // 1/2 1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10
            for (int i = 2; i <= 16; i++) {
                if (greedydiv(orgheight, i) <= 1440) {
                    cheated = true;
                    orgsize.setWidth(greedydiv(orgwidth, i));
                    orgsize.setHeight(greedydiv(orgheight, i));
                    break;
                }
            }
        }
    }

    return cheated;
}

void CMainWnd::DrawExtraInfoOnQImage(QImage& qimage, int stagewidth, int stageheight, TExtraInfoRec& extrainfo)
{
    QPainter hintwriter(&qimage);
    QRect rect;
    if (fExtraInfo == eilBench) {
        bool Rotated = stagewidth > stageheight;
        if (Rotated) {
            rect.setRect(min(qimage.width(), stagewidth) - 31, -2, 36, min(qimage.height(), stageheight) + 4);
        } else {
            rect.setRect(-2, min(qimage.height(), stageheight) - 31, min(qimage.width(), stagewidth) + 4, 36);
        }
        hintwriter.setPen(QPen(QColor(0x14C906), 2, Qt::SolidLine));
        //hintwriter.setBrush(QBrush((Qt::green, Qt::LinearGradientPattern)));
        QLinearGradient linearGradient(0, 0, 474, 36);
        linearGradient.setColorAt(0.0, Qt::white);
        linearGradient.setColorAt(0.2, Qt::lightGray);
        linearGradient.setColorAt(0.6, Qt::lightGray);
        linearGradient.setColorAt(1.0, Qt::darkGray);
        hintwriter.setBrush(linearGradient);
        hintwriter.setRenderHint(QPainter::Antialiasing);
        hintwriter.setOpacity(0.5);
        hintwriter.drawRoundedRect(rect, 4, 4, Qt::AbsoluteSize);

        hintwriter.setOpacity(0.75);
        QFont litefont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 25);
        litefont.setWeight(QFont::DemiBold - 3);
        litefont.setStyleStrategy(QFont::PreferAntialias);
        hintwriter.setFont(litefont);
        hintwriter.setPen(QPen(QColor(0x0E2545), 1, Qt::SolidLine));
        hintwriter.setBrush(Qt::NoBrush);
        hintwriter.setRenderHint(QPainter::TextAntialiasing);
        QMatrix matrix;
        if (Rotated) {
            matrix.rotate(-90);
        }
        matrix.scale(0.5, 0.5);

        int freemem = GetFreePhysMemory();

        hintwriter.setMatrix(matrix);
        QString speedtext = QString("%1 / %2 %3 %4 * %5 %6") \
            .arg(extrainfo.fileindex + 1).arg(extrainfo.allfilecount).arg(QString::fromUtf16((ushort*)ExtractFileName(extrainfo.filename).C_Str()))
            .arg(extrainfo.originalwidth).arg(extrainfo.originalheight).arg(QString::fromUtf16((ushort*)Num2Bytes(extrainfo.filesize).C_Str()));
        if (Rotated) {
            hintwriter.drawText(0 - (rect.bottom() - 5) * 2, (rect.left() + 13) * 2, speedtext);
        } else {
            hintwriter.drawText((rect.left() + 5) * 2, (rect.top() + 13) * 2, speedtext);
        }
        //hintwriter.drawText(rect, Qt::AlignTop || Qt::AlignLeft || Qt::TextSingleLine, speedtext);
        DEBUGMSG(!speedtext.isEmpty(), (_L("%s\n"), (WCHAR*)speedtext.utf16()));

        speedtext = QString("Load: %1%2%3%4 FreeMem: %5") \
            .arg(extrainfo.loadtime)
            .arg(extrainfo.decodetime==-1?QString(""):QString(" Decode: %1").arg(extrainfo.decodetime))
            .arg(extrainfo.scaletime==-1?QString(""):QString(" Scale: %1").arg(extrainfo.scaletime))
            .arg(extrainfo.drawtime==-1?QString(""):QString(" Draw: %1").arg(extrainfo.drawtime))
            .arg(QString::fromUtf16((ushort*)Num2Bytes(freemem).C_Str()));

        if (Rotated) {
            hintwriter.drawText(0 - (rect.bottom() - 5) * 2, (rect.left() + 26) * 2, speedtext);
        } else {
            hintwriter.drawText((rect.left() + 5) * 2, (rect.top() + 26) * 2, speedtext);
        }

        DEBUGMSG(!speedtext.isEmpty(), (_L("%s\n"), (WCHAR*)speedtext.utf16()));
    } else if (fExtraInfo == eilPageOnly) {
        QFont litefont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 50);
        litefont.setWeight(QFont::Bold);
        litefont.setStyleStrategy(QFont::PreferAntialias);
        hintwriter.setFont(litefont);
        hintwriter.setPen(QPen(QColor(0x0E2545), 1, Qt::SolidLine));
        hintwriter.setBrush(Qt::NoBrush);
        hintwriter.setRenderHint(QPainter::TextAntialiasing);
        hintwriter.scale(0.5, 0.5);
        QString pagetext = QString("%1 / %2").arg(extrainfo.fileindex + 1).arg(extrainfo.allfilecount);
        if (extrainfo.fitrotated) {
            hintwriter.rotate(-90);
            hintwriter.translate(-180*2, 20*2);

            hintwriter.drawText(0, 0, 160*2, 40*2, Qt::TextWrapAnywhere | Qt::AlignRight, pagetext);
            hintwriter.setPen(QPen(QColor(0xD2F3C9), 1, Qt::SolidLine));
            hintwriter.translate(-2, -2);
            hintwriter.drawText(0, 0, 160*2, 40*2, Qt::TextWrapAnywhere | Qt::AlignRight,  pagetext);
        } else {
            QRect PageRect(20*2, 20*2, 160*2, 40*2);
            hintwriter.drawText(PageRect, Qt::TextWrapAnywhere | Qt::AlignLeft | Qt::AlignVCenter,  pagetext);
            hintwriter.setPen(QPen(QColor(0xD2F3C9), 1, Qt::SolidLine));
            hintwriter.translate(-2, -2);
            hintwriter.drawText(PageRect, Qt::TextWrapAnywhere | Qt::AlignLeft | Qt::AlignVCenter,  pagetext);
        }
    } else if (fExtraInfo == eilPageExif) {
        // TODO: add EXIF tags in ExtraInfo
        QFont litefont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 50);
        litefont.setWeight(QFont::Bold);
        litefont.setStyleStrategy(QFont::PreferAntialias);
        hintwriter.setFont(litefont);
        hintwriter.setPen(QPen(QColor(0x0E2545), 1, Qt::SolidLine));
        hintwriter.setBrush(Qt::NoBrush);
        hintwriter.setRenderHint(QPainter::TextAntialiasing);
        hintwriter.scale(0.5, 0.5);
        QRect PageRect(20*2, 20*2, 240*2, 40*2);
        QString pagetext = QString("%1 / %2 ISO: 100").arg(extrainfo.fileindex + 1).arg(extrainfo.allfilecount);
        hintwriter.drawText(PageRect, Qt::TextWrapAnywhere | Qt::AlignLeft | Qt::AlignVCenter,  pagetext);
        hintwriter.setPen(QPen(QColor(0xE1EEFF), 1, Qt::SolidLine));
        hintwriter.translate(-2, -2);
        hintwriter.drawText(PageRect, Qt::TextWrapAnywhere | Qt::AlignLeft | Qt::AlignVCenter,  pagetext);

    } else if (fExtraInfo == eilSLRChart) {
        // TODO: Build Chart
        QFont litefont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 50);
        litefont.setWeight(QFont::Bold);
        litefont.setStyleStrategy(QFont::PreferAntialias);
        hintwriter.setFont(litefont);
        hintwriter.setPen(QPen(QColor(0x0E2545), 1, Qt::SolidLine));
        hintwriter.setBrush(Qt::NoBrush);
        hintwriter.setRenderHint(QPainter::TextAntialiasing);
        hintwriter.scale(0.5, 0.5);
        QRect chartrect(240*2, 20*2, 100*2, 60*2);
        hintwriter.drawRect(chartrect);
        QRect PageRect(20*2, 20*2, 280*2, 40*2);
        QString pagetext = QString("%1 / %2 ISO: 1600").arg(extrainfo.fileindex + 1).arg(extrainfo.allfilecount);
        hintwriter.drawText(PageRect, Qt::TextWrapAnywhere | Qt::AlignLeft | Qt::AlignVCenter, pagetext);
        hintwriter.setPen(QPen(QColor(0xE1EEFF), 1, Qt::SolidLine));
        hintwriter.translate(-2, -2);
        hintwriter.drawText(PageRect, Qt::TextWrapAnywhere | Qt::AlignLeft | Qt::AlignVCenter, pagetext);
    }
}

void CMainWnd::DrawPageMarkOnQImage( QImage& qimage, PageMarkDirection direction )
{
    QRect rect;

    switch (direction) {
        case pmdLeft:
            rect.setRect(40, 380, 630, 100);
            break;
        case pmdRight:
            rect.setRect(40, 0, 630, 100);
            break;
        case pmdUp:
            rect.setRect(0, 40, 100, 400);
            break;
        case pmdDown:
            rect.setRect(620, 40, 100, 400);
            break;
    }

    QImage arrow = NewQImageFromRCDATA(MAKEINTRESOURCE(IDR_PNG_PAGELEFT));
    QPainter painter(&qimage);
    painter.setPen(QPen(QColor(0x14C906), 2, Qt::SolidLine));
    //hintwriter.setBrush(QBrush((Qt::green, Qt::LinearGradientPattern)));
    QLinearGradient linearGradient(0, 0, 474, 36);
    linearGradient.setColorAt(0.0, Qt::white);
    linearGradient.setColorAt(0.2, Qt::lightGray);
    linearGradient.setColorAt(0.6, Qt::lightGray);
    linearGradient.setColorAt(1.0, Qt::darkGray);
    painter.setBrush(linearGradient);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setOpacity(0.5);
    painter.drawRoundedRect(rect, 4, 4, Qt::AbsoluteSize);
    if (arrow.isNull() == false) {
        // TODO: rotate
        if (direction == pmdRight || direction == pmdDown) {
            arrow = arrow.mirrored(true, false);
        }
        if (direction == pmdLeft || direction == pmdRight) {
            QMatrix matrix;
            matrix.rotate(-90);
            arrow = arrow.transformed(matrix);
        }
        painter.setOpacity(0.8);
        painter.drawImage(rect.left() + (rect.width() - arrow.width()) / 2, rect.bottom() - (rect.height() + arrow.height()) / 2, arrow);
    }
}

bool CMainWnd::ShowPageMark( PageMarkDirection direction )
{
    fPageMark = true;
    fScrollDirection = direction;
    TryStopAnimation();
    PaintStage();
    fPageMark = false;
    return true;
}

bool CMainWnd::ShowZoomMark(ZoomMarkAction action) {
    fZoomMark = true;
    fZoomAction = action;
    TryStopAnimation();
    PaintStage();
    fZoomMark = false;
    return true;
}

void CMainWnd::DrawZoomMarkOnQImage(QImage& qimage, int action) {
    QImage mark;
    if (action == zmaOriginal) {
        if (fZoomMarkOriginalCache.isNull()) {
            LoadQImageFromResource(fZoomMarkOriginalCache, MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_ZOOMORIGINAL));
        }
        mark = fZoomMarkOriginalCache; // @CopyInstance
    } else if (action == zmaBestFit) {
        if (fZoomMarkBestFitCache.isNull()) {
            LoadQImageFromResource(fZoomMarkBestFitCache, MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_ZOOMBESTFIT));
        }
        mark = fZoomMarkBestFitCache; // @CopyInstance
    }
    if (mark.isNull()) {
        return;
    }
    QPainter painter(&qimage);
    QRect rect(0, 0, mark.width(), mark.height());
    if (fWorldRoted) {
        rect.moveCenter(QPoint(qimage.width() / 2, qimage.height() / 2));
    } else {
        painter.rotate(-90);
        rect.moveTo(QPoint(-qimage.width() / 2, qimage.height() / 2));
    }
    painter.drawImage(rect, mark);
    painter.end();
}

void CMainWnd::DrawWidgetsOnQImage( QImage& qimage )
{
    if (fFakeWidgets.empty()) {
        return;
    }
    QPainter painter(&qimage);
    // Sub Area
    bool pathfine = false;
    QPainterPath combinepath;
    for (QMap < int, TFakeWidgetRecItem >::iterator it = fFakeWidgets.begin(); it != fFakeWidgets.end(); it++) {
        if (it->Hovered) {
            int subleft, subright, subtop, subbottom;
            bool subhit = false;
            for (QMap < int, TFakeWidgetRecItem >::iterator subit = fFakeWidgets.begin(); subit != fFakeWidgets.end(); subit++) {
                if (subit->ParentID == it->ID) {
                    if (subhit == false) {
                        subhit = true;
                        subleft = subit->DrawRect.left();
                        subright = subit->DrawRect.right();
                        subtop = subit->DrawRect.top();
                        subbottom = subit->DrawRect.bottom();
                    } else {
                        if (subleft > subit->DrawRect.left()) {
                            subleft = subit->DrawRect.left();
                        }
                        if (subright < subit->DrawRect.right()) {
                            subright = subit->DrawRect.right();
                        }
                        if (subtop > subit->DrawRect.top()) {
                            subtop = subit->DrawRect.top();
                        }
                        if (subbottom < subit->DrawRect.bottom()) {
                            subbottom = subit->DrawRect.bottom();
                        }
                    }
                }
            }
            if (subhit) {
                pathfine = true;
                QRect subrect(QPoint(subleft, subtop), QPoint(subright, subbottom));
                if (fWorldRoted) {
                    subrect.adjust(-10, -10, 18, 20);
                } else {
                    subrect.adjust(-10, -10, 18, 10);
                }
               
                QPainterPath subpath;
                subpath.addRoundedRect(subrect, 8, 8);
                combinepath = combinepath.united(subpath);
                subpath = QPainterPath();
                subpath.addRoundedRect(it->DrawRect.adjusted(-20, -10, 10, 10), 4, 4);
                combinepath = combinepath.united(subpath);
            }
        }
    }
    if (pathfine) {
        painter.setBrush(QBrush(QColor(0xF1E9D0)));
        painter.setOpacity(0.40);
        combinepath.setFillRule(Qt::WindingFill);
        painter.drawPath(combinepath);
    }
    for (QMap < int, TFakeWidgetRecItem >::iterator it = fFakeWidgets.begin(); it != fFakeWidgets.end(); it++ ) {
        if (it->NormalImage.isNull() == false) {
            if (it->ParentID != -1) {
                if (fFakeWidgets.contains(it->ParentID) == false) {
                    // wrong
                    NKDbgPrintfW(L"you place wrong parent %d for %d widgets.\n", it->ParentID, it->ID);
                    continue;
                } else {
                    if (fFakeWidgets[it->ParentID].Hovered == false) {
                        continue; // Hide
                    }
                }
            }
            if (it->Hovered) {
                painter.setPen(QPen(QColor(0x14C906), 2, Qt::SolidLine));
                //hintwriter.setBrush(QBrush((Qt::green, Qt::LinearGradientPattern)));
                QLinearGradient linearGradient(0, 0, 128, 128);
                linearGradient.setColorAt(0.0, Qt::white);
                linearGradient.setColorAt(0.2, Qt::lightGray);
                linearGradient.setColorAt(0.6, Qt::lightGray);
                linearGradient.setColorAt(1.0, Qt::white);
                painter.setBrush(linearGradient);
                painter.setRenderHint(QPainter::Antialiasing);
                painter.setOpacity(0.75);

                QRect HintRect = it->CaptionRect.adjusted(-8, -4, 8, 4);
                HintRect.moveTo(it->DrawRect.left() + 8, it->DrawRect.top() - 75);
                QPolygon conn;
                conn << QPoint(HintRect.left() + 8, HintRect.bottom() - 2) 
                     << QPoint(HintRect.left() + 28, HintRect.bottom() - 2)
                     << QPoint(HintRect.left() + 18, it->DrawRect.top() + 2)
                     << QPoint(HintRect.left() + 8, HintRect.bottom() - 2); // close
                if (HintRect.right() >= 718) {
                    HintRect.moveRight(it->DrawRect.right() + 8);
                    conn.translate(HintRect.width() - it->DrawRect.width() - 16, 0);
                } else {
                    // Keep
                }
                if (HintRect.top() <= 2) {
                    // Invert
                    HintRect.moveTop(it->DrawRect.bottom() + 82);
                    conn.clear();
                    conn << QPoint(HintRect.right() - 8, HintRect.top() + 2) 
                        << QPoint(HintRect.right() - 28, HintRect.top() + 2)
                        << QPoint(HintRect.right() - 18, it->DrawRect.bottom() - 2)
                        << QPoint(HintRect.right() - 8, HintRect.top() + 2); // close
                } else {
                }

                //painter.drawRoundedRect((fFullScreen?HintRect:HintRect.adjusted(-40, 0, -40, 0)), 4, 4, Qt::AbsoluteSize);
                //painter.drawRoundedRect((fFullScreen?it->TouchRect:it->TouchRect.adjusted(-40, 0, -40, 0)), 8, 8, Qt::AbsoluteSize);
                QPainterPath HintPath;
                HintPath.addRoundedRect(it->TouchRect, 8, 8, Qt::AbsoluteSize); // for all hover icon
                if (it->Caption.isEmpty() == false && it->ParentID == -1) {
                    HintPath.addRoundedRect(HintRect, 4, 4, Qt::AbsoluteSize); // caption rect
                    QPainterPath ConnPath;
                    ConnPath.addPolygon(conn);
                    HintPath = HintPath.united(ConnPath); // hover with an hint balloon
                }

                painter.drawPath(HintPath);

                if (it->Caption.isEmpty() == false && it->ParentID == -1) {
                    QFont captionfont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 40);
                    painter.setFont(captionfont);
                    painter.setPen(QPen(QColor(0x0E2545), 1, Qt::SolidLine));
                    painter.setBrush(Qt::NoBrush);
                    painter.setRenderHint(QPainter::TextAntialiasing);

                    QMatrix aamatrix;
                    aamatrix.scale(0.5, 0.5);
                    painter.setMatrix(aamatrix);

                    painter.drawText(HintRect.left() * 2, HintRect.top() * 2, HintRect.width() *2, HintRect.height() *2,
                                     Qt::AlignCenter | Qt::AlignHCenter | Qt::TextWrapAnywhere, it->Caption);
                    painter.resetMatrix();
                }

                painter.setOpacity(1.2); // for hover icon
            } else {
                painter.setOpacity(0.95); // normal
            }

            if (fWorldRoted) {
                painter.drawImage((it->DrawRect), it->NormalImage);
            } else {
                QMatrix matrix;
                matrix.rotate(-90);
                painter.drawImage((it->DrawRect), it->NormalImage.transformed(matrix));
            }

            if (it->ParentID != -1 && fFakeWidgets.contains(it->ParentID) && it->Caption.isEmpty() == false && it->Hovered == false) {
                // with caption, have valid parent widget (sure hidden one continued upon beginning)
                QFont captionfont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 28);
                painter.setFont(captionfont);
                painter.setPen(QPen(QColor(0x324469), 1, Qt::SolidLine));
                painter.setBrush(Qt::NoBrush);
                painter.setRenderHint(QPainter::TextAntialiasing);

                if (fWorldRoted) {
                    QMatrix aamatrix;
                    aamatrix.scale(0.5, 0.5);
                    painter.setMatrix(aamatrix);
                    
                    QRect CaptionRect = it->DrawRect;
                    CaptionRect.moveTop(CaptionRect.bottom());
                    CaptionRect.setHeight(25);

                    painter.drawText(CaptionRect.left() * 2, CaptionRect.top() * 2, CaptionRect.width() *2, CaptionRect.height() *2,
                                     Qt::AlignCenter | Qt::AlignHCenter | Qt::TextWrapAnywhere, it->Caption);

                } else {
                    painter.scale(0.5, 0.5);
                    painter.rotate(-90);
                    painter.translate(-it->DrawRect.bottom() * 2, it->DrawRect.right() * 2);

                    painter.drawText(0, -8, it->DrawRect.width() * 2, 50,
                                     Qt::AlignCenter | Qt::TextWrapAnywhere, it->Caption);
                }
                painter.resetMatrix();
            }
        }
    }
    painter.end();
}


bool CMainWnd::CheckStageMatchDesign( DDSURFACEDESC &surface, LPBYTE &dest, bool &blockable )
{
    bool stagesizematched = false;
#ifdef UNDER_CE
    if (stage1.LockData(&surface)) {
        if (surface.dwWidth == fStageSize.stage1width && surface.dwHeight == fStageSize.stage1height) {
            //rotation = 0; // MayHonz
            stagesizematched = true;
            dest = LPBYTE(surface.lpSurface);
            blockable = true;
        }
        if (surface.dwWidth == fStageSize.stage1height && surface.dwHeight == fStageSize.stage1width && surface.lXPitch < -4 && surface.lPitch == 4) {
            //rotation = 0; // soft Rotate
            stagesizematched = true;
            dest = LPBYTE(surface.lpSurface) + (surface.dwWidth - 1) * surface.lXPitch;
            blockable = true;
        }
    }
    if (!stagesizematched) {
        stage1.UnLockData(false);
    }
#else
    // GDI
    stagesizematched = true; // we assume GDI may draw all size
    blockable = true; // TODO: implement CropDIBSection 720*480
#endif
    return stagesizematched;
}


void CMainWnd::TryRefreshStageOnFit() {
    if (fStageShowMode == ssmFit) {
        if (fAnimationKeeperThread == NULL) {
            // FIXME: GDIMode Detection
            if (fInShellUsage == false) {
                PaintStage();
            }
        } else if (fAnimationKeeperThread->GetTerminated()) {
            if (fInShellUsage == false) {
                PaintStage();
            }
        }
    }
}

bool CMainWnd::AddWidget( int ID, const QRect& rect, QImage normalimage, QString caption )
{
    TFakeWidgetRecItem item;
    item.DrawRect = rect;
    if (rect.size().width() == 0 && rect.size().height() == 0) {
        item.DrawRect.setSize(normalimage.size());
    }
    item.ID = ID;
    //item.DrawRect = rect;
    item.TouchRect = item.DrawRect.adjusted(-8, -8, 8, 8);
    item.NormalImage = normalimage;
    item.Caption = caption;
    if (item.Caption.isEmpty() == false) {
        QPainter painter(&item.NormalImage);
        QFont captionfont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 24);
        painter.setFont(captionfont);
        item.CaptionRect = painter.boundingRect(0, 0, 300, 20, Qt::AlignLeft | Qt::AlignTop | Qt::TextWrapAnywhere, item.Caption);
    }
    fFakeWidgets.insert(ID, item); // copy
    return true;
}

bool CMainWnd::SetWidgetImage( int ID, QImage normalimage ) {
    if (fFakeWidgets.contains(ID)) {
        fFakeWidgets[ID].NormalImage = normalimage;
        return true;
    }
    return false;
}

bool CMainWnd::SetWidgetParent( int ID, int ParentID ) {
    if (fFakeWidgets.contains(ID)) {
        fFakeWidgets[ID].ParentID = ParentID;
        if (fFakeWidgets.contains(ParentID)) {
            fFakeWidgets[ParentID].SingleToggle = true;
        }
        return true;
    }
    return false;
}

void CMainWnd::OptimizeMangaImage( QImage& qimage )
{
    changeContrast(qimage, 108);
    changeBrightness(qimage, -2);
    changeGamma(qimage, 120);
    //QGaussFilter sharper;
    //qimage = sharper.GetUnsharpMask(qimage, 0.5, 0.3);
    sharpen(qimage, 0.4, 0.2, true);
}
