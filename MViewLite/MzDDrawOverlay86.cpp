/************************************************************************/
/*  																	*/
/* 		Copyright (c) 2003-2007 MeiZu Technologies Co.,Ltd.				*/
/*  	珠海市魅族电子科技有限公司 版权所有 2003-2007  	  		  		*/

/************************************************************************
	created:	2008-05-13	   									  
	filename: 	MzDDrawOverlay86.cpp									 
	author:		周详													 
	purpose:																 
*************************************************************************/

#include "MzDDrawOverlay86.h"

#define RGB565(r,g,b)            (((r << 8) & 0xF800) | ((g << 3) & 0x07E0) | ((b >> 3) & 0x001F))

static DDPIXELFORMAT g_ddpfOverlayFormats[] =
{
	{sizeof(DDPIXELFORMAT), DDPF_FOURCC, MAKEFOURCC('Y', 'V', '1', '2'), 0, 0, 0, 0, 0},
	{sizeof(DDPIXELFORMAT), DDPF_FOURCC, MAKEFOURCC('Y','U','Y','V'),0,0,0,0,0},  // YUYV
	{sizeof(DDPIXELFORMAT), DDPF_FOURCC, MAKEFOURCC('U','Y','V','Y'),0,0,0,0,0},  // UYVY
	{sizeof(DDPIXELFORMAT), DDPF_RGB, 0, 16,  0x7C00, 0x03e0, 0x001F, 0},         // 16-bit RGB 5:5:5
	{sizeof(DDPIXELFORMAT), DDPF_RGB, 0, 16,  0xF800, 0x07e0, 0x001F, 0},         // 16-bit RGB 5:6:5
	{sizeof(DDPIXELFORMAT), DDPF_RGB, 0, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0},
	{sizeof(DDPIXELFORMAT), DDPF_FOURCC, MAKEFOURCC('I','4','2','0'),0,0,0,0,0} // YUV420
};

#define PF_TABLE_SIZE (sizeof(g_ddpfOverlayFormats) / sizeof(g_ddpfOverlayFormats[0]))

#ifdef MZFC_X86
#define	DDLOCK_WAITNOTBUSY	DDLOCK_WAIT
#endif

MzDDrawOverlay86::MzDDrawOverlay86()
:	m_lpEP_MzDDrawOverlay(0)
{
	m_lpDDraw = NULL;
	m_lpDDSPrimary = NULL;
	m_lpDDSOverlay = NULL;

	m_lpDDSBack = NULL;

	m_visible = false;
}

MzDDrawOverlay86::~MzDDrawOverlay86()
{
	Clear();
}

static HRESULT WINAPI EnumSurfacesCallback(LPDIRECTDRAWSURFACE lpDDSurface, LPDDSURFACEDESC lpDDSurfaceDesc, LPVOID lpContext)
{
	LPDIRECTDRAWSURFACE *pSurface = (LPDIRECTDRAWSURFACE*)lpContext;
	HRESULT hr = (HRESULT)DDENUMRET_OK;

	*pSurface = lpDDSurface;

	return hr;
}

bool MzDDrawOverlay86::Init(HWND hWnd, int x, int y, int width, int height, COLORREF colorKey, BYTE foreAlpha, BYTE backAlpha, bool needBackBuffer, OverlayPixelFormat pixFmt)
{
	Clear();

	bool succeeded = false;
	
	while (1)
	{
		if (DD_OK != DirectDrawCreate(NULL, &m_lpDDraw, NULL))
		{
			break;
		}

		if (DD_OK != m_lpDDraw->SetCooperativeLevel(hWnd, DDSCL_NORMAL))
		{
			break;
		}

		DDSURFACEDESC ddsd;
		memset(&ddsd, 0, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS ;
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;

		if (DD_OK != m_lpDDraw->CreateSurface(&ddsd, &m_lpDDSPrimary, NULL))
		{
			break;
		}

		DDCAPS 	ddcaps;
		memset(&ddcaps, 0, sizeof(ddcaps));
		ddcaps.dwSize = sizeof(ddcaps);
		if (DD_OK != m_lpDDraw->GetCaps(&ddcaps, NULL))
		{
			break;
		}
#ifndef MZFC_X86
		if (ddcaps.dwOverlayCaps == 0)
		{
			break;
		}
#endif

		m_rcSrc.left = 0;
		m_rcSrc.top = 0;
		m_rcSrc.right = width;
		m_rcSrc.bottom = height;

		if (ddcaps.dwAlignSizeSrc != 0)
		{
			m_rcSrc.right += m_rcSrc.right % ddcaps.dwAlignSizeSrc;
		}

		memset(&ddsd, 0, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.ddsCaps.dwCaps = DDSCAPS_OVERLAY;
		ddsd.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT;
		if (needBackBuffer)
		{
			ddsd.ddsCaps.dwCaps |= DDSCAPS_FLIP;
			ddsd.dwFlags |= DDSD_BACKBUFFERCOUNT;
			ddsd.dwBackBufferCount = 1;
		}
		ddsd.dwWidth = m_rcSrc.right;
		ddsd.dwHeight = m_rcSrc.bottom;		

		int i = 0;
		HRESULT hRet;

		ddsd.ddpfPixelFormat = g_ddpfOverlayFormats[pixFmt];
		hRet = m_lpDDraw->CreateSurface(&ddsd, &m_lpDDSOverlay, NULL);

		if (hRet != DD_OK)
		{
			break;
		}

		m_lpDDSOverlay->EnumAttachedSurfaces(&m_lpDDSBack, EnumSurfacesCallback);
		if (m_lpDDSBack)
		{
			m_lpDDSForDC = m_lpDDSBack;
		}
		else
		{
			m_lpDDSForDC = m_lpDDSOverlay;
		}

		int stretchFactor = ddcaps.dwMinOverlayStretch > 1000 ? ddcaps.dwMinOverlayStretch : 1000;
		m_rcDst.left = 0; 
		m_rcDst.top = 0;
		m_rcDst.right  = (m_rcSrc.right * stretchFactor + 999) / 1000;
		m_rcDst.bottom = m_rcSrc.bottom * stretchFactor / 1000;
		if (ddcaps.dwAlignSizeDest != 0)
		{
			m_rcDst.right = (int)((m_rcDst.right + ddcaps.dwAlignSizeDest - 1) / ddcaps.dwAlignSizeDest) * ddcaps.dwAlignSizeDest;
		}

		DWORD dwUpdateFlags = 0;	//DDOVER_SHOW

		memset(&ovfx, 0, sizeof(ovfx));
		ovfx.dwSize = sizeof(ovfx);

#ifndef MZFC_X86
		BYTE r, g, b;
		r = GetRValue(colorKey);
		g = GetGValue(colorKey);
		b = GetBValue(colorKey);
		DWORD colorKey565 = RGB565(r, g, b);
		if (ddcaps.dwOverlayCaps & DDOVERLAYCAPS_CKEYSRC)
		{
			dwUpdateFlags |= DDOVER_KEYSRCOVERRIDE;

			ovfx.dckSrcColorkey.dwColorSpaceLowValue = colorKey565; // black as the color key
			ovfx.dckSrcColorkey.dwColorSpaceHighValue = colorKey565;
			//ovfx.dwAlphaConst = ((foreAlpha & 0xF) << 4) | (backAlpha & 0xF);
		}

		if (DD_OK != m_lpDDSOverlay->UpdateOverlay(&m_rcSrc, m_lpDDSPrimary, &m_rcDst, dwUpdateFlags, &ovfx))
		{
			break;
		}

		ovfx.dwAlphaConst = ((backAlpha & 0xF) << 4) | (foreAlpha & 0xF);
		if (DD_OK != m_lpDDSOverlay->UpdateOverlay(&m_rcSrc, m_lpDDSPrimary, &m_rcDst, 0, &ovfx))
		{
			break;
		}
#endif

		m_xPositionAlignment = ddcaps.dwAlignBoundaryDest;

		HDC hdc = BeginDraw();
		HBRUSH brush = ::CreateSolidBrush(colorKey);
		RECT tmpRect = { 0, 0, width, height };
		if (hdc)
		{
			FillRect(hdc, &tmpRect, brush);
			EndDraw();
		}
		hdc = BeginDraw();
		if (hdc)
		{
			FillRect(hdc, &tmpRect, brush);
			EndDraw();
		}
		DeleteObject(brush);

		//ShowOverlay();
		SetPosition(x, y);
		//HideOverlay();

		succeeded = true;
		break;
	}

	if (!succeeded)
	{
		Clear();
	}

	return succeeded;
}

void MzDDrawOverlay86::Clear()
{
	if (m_lpDDSOverlay != NULL)
	{
		m_lpDDSOverlay->UpdateOverlay(NULL, m_lpDDSPrimary, NULL, DDOVER_HIDE, NULL);
		m_lpDDSOverlay->Release();
		m_lpDDSOverlay = NULL;
	}
	if (m_lpDDSPrimary != NULL)
	{
		m_lpDDSPrimary->Release();
		m_lpDDSPrimary = NULL;
	}
	if (m_lpDDraw != NULL)
	{
		m_lpDDraw->Release();
		m_lpDDraw = NULL;
	}

	m_lpDDSForDC = NULL;
}

HDC MzDDrawOverlay86::BeginDraw()
{
	if (m_lpDDSForDC == NULL)
	{
		return NULL;
	}
	if (m_dstDC)
	{
		m_lpDDSForDC->ReleaseDC(m_dstDC);
	}
	m_dstDC = NULL;

	if (FAILED(m_lpDDSForDC->GetDC(&m_dstDC)))
	{
		return NULL;
	}
	else
	{
		return m_dstDC;
	}
}

void MzDDrawOverlay86::RestoreAllSurfaces()
{
	if (m_lpDDSPrimary)
	{
		m_lpDDSPrimary->Restore();
	}

	if (m_lpDDSOverlay)
	{
		m_lpDDSOverlay->Restore();
		m_lpDDSOverlay->UpdateOverlay(&m_rcSrc, m_lpDDSPrimary, &m_rcDst, DDOVER_SHOW, NULL);
	}
}

void MzDDrawOverlay86::EndDraw(bool flip)
{
	if (m_dstDC)
	{
		m_lpDDSForDC->ReleaseDC(m_dstDC);
		m_dstDC = NULL;
	}
	if (flip && m_lpDDSBack != NULL)
	{
		if (DDERR_SURFACELOST == m_lpDDSOverlay->Flip(0, 0))		//交换表面
		{
			RestoreAllSurfaces();
		}
	}
}

bool MzDDrawOverlay86::LockData(DDSURFACEDESC *pDDSD)
{
	if (m_lpDDSOverlay == NULL || pDDSD == NULL)
	{
		return false;
	}

	memset(pDDSD, 0, sizeof(DDSURFACEDESC));
	pDDSD->dwSize = sizeof(DDSURFACEDESC);

	HRESULT ddrval = S_OK;
	if (m_lpDDSBack != NULL)
	{
		ddrval = m_lpDDSBack->Lock( NULL, pDDSD, DDLOCK_WAITNOTBUSY, NULL);
	}
	else
	{
		ddrval = m_lpDDSOverlay->Lock( NULL, pDDSD, DDLOCK_WAITNOTBUSY, NULL);
	}
	if (FAILED(ddrval))
	{
		return false;
	}
	return true;
}

bool MzDDrawOverlay86::UnLockData(bool flip)
{
	if (!m_lpDDSOverlay)
	{
		return false;
	}
	HRESULT ddrval = S_OK;
	if (m_lpDDSBack != NULL)
	{
		m_lpDDSBack->Unlock(NULL);
	}
	else
	{
		m_lpDDSOverlay->Unlock(NULL);
	}
	if (FAILED(ddrval))
	{
		return false;
	}
	if (flip && m_lpDDSBack != NULL)
	{
		if (DDERR_SURFACELOST == m_lpDDSOverlay->Flip(0, 0))		//交换表面
		{
			RestoreAllSurfaces();
		}
	}
	return true;
}



void MzDDrawOverlay86::SetTransparency(int iValue)
{
#ifndef MZFC_X86
	if (!m_lpDDSOverlay)
	{
		return;
	}
	if(iValue < 0)
		return;

	int i = iValue;
	if(iValue > 15)
		i=15;

	ovfx.dwAlphaConst &= 0xFFFFFFF0;
	ovfx.dwAlphaConst |= i & 0xF;
	m_lpDDSOverlay->UpdateOverlay(&m_rcSrc, m_lpDDSPrimary, &m_rcDst, 0, &ovfx);
#endif
}

void MzDDrawOverlay86::HideOverlay()
{
	if (!m_lpDDSOverlay)
	{
		return;
	}
	m_lpDDSOverlay->UpdateOverlay(0,m_lpDDSPrimary,0,DDOVER_HIDE,0);
	m_visible = false;
}

void MzDDrawOverlay86::SetPosition( int x, int y )
{
	if (!m_lpDDSOverlay)
	{
		return;
	}
	if (m_xPositionAlignment)
	{
		x = x - x % m_xPositionAlignment;
	}

	m_x = x;
	m_y = y;
	
	if (!m_visible)
	{
		return;
	}
	
	HRESULT hRet = m_lpDDSOverlay->SetOverlayPosition(x, y);
	if (DDERR_SURFACELOST == hRet)
	{
		RestoreAllSurfaces();
	}
}

void MzDDrawOverlay86::ShowOverlay()
{
	if (!m_lpDDSOverlay)
	{
		return;
	}
	m_lpDDSOverlay->UpdateOverlay(&m_rcSrc, m_lpDDSPrimary, &m_rcDst, DDOVER_SHOW, &ovfx);
	m_visible = true;
	SetPosition(m_x, m_y);
}

POINT MzDDrawOverlay86::GetPosition()
{
	POINT position = { INT_MAX, INT_MAX };
	if (!m_lpDDSOverlay)
	{
		return position;
	}
	if (S_OK == m_lpDDSOverlay->GetOverlayPosition(&position.x, &position.y))
	{
		return position;
	}
	else
	{
		position.x = INT_MAX;
		position.y = INT_MAX;
		return position;
	}
}

bool MzDDrawOverlay86::IsVisible()
{
	return m_visible;
}

void MzDDrawOverlay86::Flip()
{
	if (m_lpDDSBack != NULL)
	{
		if (DDERR_SURFACELOST == m_lpDDSOverlay->Flip(0, 0))		//交换表面
		{
			RestoreAllSurfaces();
		}
	}
}

BOOL MzDDrawOverlay86::GetExtendedProperty_MzDDrawOverlay( int nPropertyType, void *pValue, int nBufSize )
{
	switch(nPropertyType)
	{
	case 0:
		{
			// 检查nBufsize是否有效，并根据m_lpEP_XXX成员赋值给pValue
		}
		return TRUE;
	}
	return FALSE;
}

BOOL MzDDrawOverlay86::SetExtendedProperty_MzDDrawOverlay( int nPropertyType, void *pValue, int nBufSize )
{
	switch(nPropertyType)
	{
	case 0:
		{
			// 检查nBufsize是否有效，并根据pValue更新对象的m_lpEP_XXX成员
		}
		return TRUE;
	}
	return FALSE;
}
