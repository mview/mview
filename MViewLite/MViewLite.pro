#qmake -spec wince60m8sdk-armv6-msvc2008
TEMPLATE = app
CONFIG -= qt
CONFIG += debug_and_relase
CONFIG(debug, debug|release) { 
    TARGET = MViewLite_d
    DEFINES += MZFC_STATIC
#    DESTDIR = $$quote(./M8SDK (ARMV4I)/Debug ArmV6 MZFC Static)
#    OBJECTS_DIR = $$DESTDIR
    LIBS += -lmzfcsd
}
else { 
#    CONFIG += static
    TARGET = MViewLite
    DEFINES += MZFC_STATIC SHIP_BUILD QT_NODLL MVIEW_RETAIL
#    DESTDIR = $$quote(./M8SDK (ARMV4I)/Retail ArmV6 All Static)
#    OBJECTS_DIR = $$DESTDIR
    LIBS += -lmzfcs
}
QMAKE_LFLAGS_RELEASE += /NODEFAULTLIB:libcmt.lib /VERBOSE
DEPENDPATH += .
INCLUDEPATH += ./tmp \
    $$quote(G:\root\Projects\CompileFarm\qt\qt-wince-4.7.0-vc2008-armv6-static\include) \
    $$quote(G:\root\Projects\CompileFarm\qt\qt-wince-4.7.0-vc2008-armv6-static\include\Qt) \
    . \
    ./../FvMzMd \
    $$quote(G:\root\git\qt\qtlite\src\3rdparty)
LIBS += -lPlatformApi \
    -lPhoneAdapter \
    -L$$quote(G:\root\Projects\CompileFarm\qt\qt-wince-4.7.0-vc2008-armv6-static\lib) \
    -L$$quote(G:\root\Projects\CompileFarm\qt\qt-wince-4.7.0-vc2008-armv6-static\plugins\imageformats) \
    -lQtCore \
    -lQtGui \
    -lwinsock \
    -lqjpegmod \
    -lqgif
QTPLUGIN     += qjpegmod qgif
HEADERS += ./DbCentre.h \
    ./MainUnt.h \
    StringInterner.h \
    SelectorUnt.h \
    resource.h \
    ParareruImage.h \
    DirtyDefination.h \
    AddonFuncUnt.h \
    ../FvMzMd/FvMzKOL.h \
    Reader/exifdefine.h \
    Filter/PikohanMagicalu.h \
    Filter/QBaseFilter.h \
    Filter/QResample.h \
    Filter/waveletsharpen/plugin.h \
    Filter/waveletsharpen/sharpen.h \
    Reader/pkparser/pkparser.h

# Source files
SOURCES += ./DbCentre.cpp \
    ./main.cpp \
    ./MainUnt.cpp \
    SelectorUnt.cpp \
    ParareruImage.cpp \
    MainUntWalker.cpp \
    MainUntStocker.cpp \
    MainUntPainter.cpp \
    AddonFuncUnt.cpp \
    StringInterner.cpp \
    ../FvMzMd/FvMzKOL.cpp \
    Reader/pkparser/pkparser.cpp \
    Filter/PikohanMagicalu.cpp \
    Filter/QBaseFilter.cpp \
    Filter/QResample.cpp \
    Filter/waveletsharpen/colour.c \
    Filter/waveletsharpen/sharpen.cpp \
    Filter/waveletsharpen/wavelet.c

# Forms


# Resource file(s)

# Translation files

# Windows resource file
win32:RC_FILE = Addon.rc

# Compiler arg
# M8SDK treat wchar_t as built-in type
QMAKE_CFLAGS -= -Zc:wchar_t-
QMAKE_CXXFLAGS -= -Zc:wchar_t-