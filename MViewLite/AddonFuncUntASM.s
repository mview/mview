   AREA sample, CODE, READONLY  ; name this block of code
        
   EXPORT  armmemcpy

;
; Called from C as int armmemcpy(void*, const void*, int);
; The first 4 parameters are passed in r0-r3, more parameters would be passed on the stack
;
armmemcpy proc
	MOVS    R2, R2,LSL #31
	LDRCSH  R3, [R1],#2
	LDRMIB  R2, [R1],#1
	STRCSH  R3, [R0],#2
	STRMIB  R2, [R0],#1
	BX      LR
endp

    END