/************************************************************************/
/*  																	*/
/* 		Copyright (c) 2003-2007 MeiZu Technologies Co.,Ltd.				*/
/*  	珠海市魅族电子科技有限公司 版权所有 2003-2007  	  		  		*/

/************************************************************************
	created:	2008-05-13	   									  
	filename: 	MzDDrawOverlay86.h									 
	author:		周详													 
	purpose:																 
*************************************************************************/

#ifndef _MZ_DDRAW_OVERLAY_86_H_
#define _MZ_DDRAW_OVERLAY_86_H_

#include <ddraw.h>
#include <mzfc/mzfcdll.h>

#define RGB565(r,g,b)            (((r << 8) & 0xF800) | ((g << 3) & 0x07E0) | ((b >> 3) & 0x001F))

#ifndef _MZ_DDRAW_OVERLAY_H_
/**
* @brief DirectDraw的Overlay的像素格式类型
*/
enum OverlayPixelFormat
{
	PixFmtUnknown = -1,
	PixFmtYV12 = 0,
	PixFmtYUV422,
	PixFmtUYVY,
	PixFmtRGB555,
	PixFmtRGB565,
	PixFmtRGB24,
	PixFmtYUV420,
	PixFmtCount
};
#endif

/**
* @brief 对DirectDraw的Overlay的封装
*
* 使用这个类可以较方便的实现对DirectDraw的Overlay的操作
*/
class MzDDrawOverlay86
{
public:
	///构造函数，在没有调用Init之前，不会实际创建Overlay
	MzDDrawOverlay86();

	///析构函数，自行释放有关的资源
	~MzDDrawOverlay86();

	/**
	* @brief 创建并初始化一个DirectDraw的Overlay表面
	*
	* 注意：不要在发生锁屏背景灯熄灭后，使用Init和Clear，否则会无法唤醒屏幕（或者其他不正常的现象），锁屏消息是 WM_MZ_SHELL_ENTRY_LOCKPHONE
	* 锁屏后可以设置Overlay的透明度，最好设置为0，否则在系统锁屏界面会看到Overlay的内容
	*
	* @param hWnd [in] 指定Overlay所在应用的窗口，MzDDrawOverlay只将其视为非全屏的窗口
	* @param x [in] Overlay左上角的横坐标
	* @param y [in] Overlay左上角的纵坐标
	* @param width [in] Overlay的宽度
	* @param height [in] Overlay的高度
	* @param colorKey [in] 设置一个颜色，Overlay上填充这个颜色的地方，都将被视为背景区域，否则为前景区域
	* @param foreAlpha [in] 前景区域的透明度，0是全透，15是不透明
	* @param backAlpha [in] 背景区域的透明度，0是全透，15是不透明
	* @param needBackBuffer [in] 是否启用后备缓冲，MzDDrawOverlay只创建一个后背缓冲，或者不创建
	* @param pixFmt [in] 指定Overlay的像素格式
	* @return 是否创建并初始化成功
	*/
	bool Init(HWND hWnd, int x, int y, int width, int height, COLORREF colorKey, BYTE foreAlpha, BYTE backAlpha, bool needBackBuffer, OverlayPixelFormat pixFmt = PixFmtRGB565);

	/**
	* @brief 获得Overlay对应的HDC
	*
	* 获得Overlay对应的HDC，此时Overlay会被锁定，可调用各种WindowsAPI对HDC进行绘图操作，使用方便，但速度没有LockData的方式快
	* BeginDraw必须与EndDraw成对调用，否则BeginDraw可能会返回NULL，两者之间调用LockData也会失败
	* @return 返回获得的HDC
	*/
	HDC BeginDraw();

	/**
	* @brief 画图结束解除对Overlay的锁定
	*
	* @param flip [in] 如果在Init中指定了使用后备缓冲，可以选择是否结束后备缓冲的绘制，将后备缓冲显示出来
	*/
	void EndDraw(bool flip = true);

	/**
	* @brief 获得Overlay当前的各项参数
	*
	* Overlay会被锁定，可通过 DDSURFACEDESC 结构体中的 lpSurface 获得表面对应的数据的内存，直接对内存操作，比用HDC画图快
	* @param pDDSD [out] 获得的参数将被存放在pDDSD所指向的结构体中，有关 DDSURFACEDESC 结构体请参考DirectDraw的相关注释和文档
	* @return 是否锁定成功
	*/
	bool LockData(DDSURFACEDESC *pDDSD);

	/**
	* @brief 画图结束解除对Overlay的锁定
	*
	* @param flip [in] 如果在Init中指定了使用后备缓冲，可以选择是否结束后备缓冲的绘制，将后备缓冲显示出来
	* @return 是否成功
	*/
	bool UnLockData(bool flip = true);

	///如果在Init中指定了使用后备缓冲，将后备缓冲显示出来
	void Flip();

	///Overlay是否可见，可见状态下透明度可能为0
	bool IsVisible();

	///设置Overlay当前左上角位置，如果设置位置后，Overlay有区域超出屏幕范围，会导致设置失败或者图像不正确，对于Init调用也是这样
	void SetPosition(int x, int y);

	///获得Overlay当前左上角位置
	POINT GetPosition(); 

	///可动态设置前景区域的透明度
	void SetTransparency(int iValue = 15);

	///显示Overlay
	void ShowOverlay();

	///隐藏Overlay
	void HideOverlay();
	
	///将内部分配的资源释放，释放后，需要重新调用Init，否则Overlay不可用
	void Clear();

protected:
	
	void RestoreAllSurfaces();

	DDOVERLAYFX ovfx;

	LPDIRECTDRAW m_lpDDraw;
	LPDIRECTDRAWSURFACE m_lpDDSPrimary;
	LPDIRECTDRAWSURFACE m_lpDDSOverlay;
	LPDIRECTDRAWSURFACE	m_lpDDSBack;
	LPDIRECTDRAWSURFACE m_lpDDSForDC;
	HDC m_dstDC;
	DWORD m_xPositionAlignment;
	RECT m_rcSrc;
	RECT m_rcDst;
	bool m_visible;
	int m_x, m_y;

public:
	/**
	 * @brief 获取或设置对象的扩展属性值
	 *
	 * @param nPropertyType 要获取或设置的属性类型
	 * @param pValue 要获取或设置的属性值的指针
	 * @param nBufSize pValue所指向的变量的size大小（单位: 字节）
	 * @return 操作是否成功
	 */
	BOOL GetExtendedProperty_MzDDrawOverlay(int nPropertyType, void *pValue, int nBufSize);

	/**
	 * @brief 获取或设置对象的扩展属性值
	 *
	 * @param nPropertyType 要获取或设置的属性类型
	 * @param pValue 要设置的属性值的指针
	 * @param nBufSize pValue所指向的变量的size大小（单位: 字节）
	 * @return 操作是否成功
	 */
	BOOL SetExtendedProperty_MzDDrawOverlay(int nPropertyType, void *pValue, int nBufSize);

private:
	void* m_lpEP_MzDDrawOverlay;	// used by ExtendedProperty member

};
#endif
