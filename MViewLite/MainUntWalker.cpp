#include <FvMzKOL.h>
#include "MainUnt.h"
#include "AddonFuncUnt.h"
#include "resource.h"
#include "Reader/pkparser/pkparser.h"

#include <QtGui/QImageReader>
#include <QtGui/QPainter>
#include <QtCore/QFile>
#include <QtCore/QBuffer>
#include <algorithm>
//#include "Filter/qtimagefilters/QtImageFilterFactory"
#include "Filter/QBaseFilter.h"
#include "WincePlaceHolder.h"


int CMainWnd::NormalizeScrollIndex( int index )
{
    int workingindex = index < 0 ? fScrollFiles.size() + index: index;
    if (workingindex >= fScrollFiles.size()){
        workingindex = workingindex - fScrollFiles.size();
    }
    return workingindex;
}

int CMainWnd::CalculateIndexDistance( int baseindex, int sideindex ) {
    // 1, 178 = -3
    // 8, 2 = 6
    int Result = sideindex - baseindex; // max - min
    if (abs(int(min(baseindex, sideindex) - (max(baseindex, sideindex) - fScrollFiles.size()))) < abs(int(Result))) { // 177 -> -3
        Result = min(baseindex, sideindex) - (max(baseindex, sideindex) - fScrollFiles.size()); // 1 - (178 -180) = -3 (179,180,0)
    }
    return Result;
}

void CMainWnd::BuildExtraPrefetch(int destwidth, int destheight)
{
    // Build 5 image list in background
    // L2R4 +1 -1 +2 -2 +3 +4
    for (int i = 1; i <= max(fPrevScollDepth, fNextScrollDepth); i++) {
        // 0 depth get one build
        if (i <= fPrevScollDepth) {
            BuildSinglePrefetch(fCurrentScrollIndex + i, destwidth, destheight, false);
        }
        if (i <= fNextScrollDepth) {
            BuildSinglePrefetch(fCurrentScrollIndex - i, destwidth, destheight, false);
        }
    }
}


void CMainWnd::BuildSinglePrefetch( int index, int destwidth, int destheight, bool force /*= false */ )
{
    int workingindex = NormalizeScrollIndex(index);

    if (workingindex >= 0 && workingindex < fScrollFiles.size() && (workingindex != fCurrentScrollIndex || force) && /* workingindex != fPrefetchBuildingIndex*/
        fBuildingFitIndexes.count(workingindex) == 0)
    {
        TScrollFileRecItem& workingitem = fScrollFiles[workingindex];
        if (workingitem.fromarchive) {
            if (!FileExists(workingitem.archivename)) {
                return;
            }
        } else {
            if (!FileExists(workingitem.filename)) {
                return;
            }
        }

        TImageType mimetype;
        if (workingitem.fromarchive) {
            mimetype = itPKArchive;
        } else {
            mimetype = GuessMIMEType(workingitem.filename);
        }

        // TODO: Check file in PK
        if (force == false) {
            if (mimetype == itJPEG || mimetype == itPNG) {
                QImageReader reader(QString::fromUtf16((ushort*)(workingitem.filename.C_Str())), MIMETypeToExt(mimetype).toAscii());
                QSize orgsize = reader.size();
                if (mimetype == itJPEG && orgsize.width() * orgsize.height() > 6000 * 4000 || orgsize.width() * orgsize.height() < 1280*960) {
                    return;
                }
                if (mimetype == itPNG && orgsize.width() * orgsize.height() > 4000 * 3000 || orgsize.width() * orgsize.height() < 1024*768) {
                    return;
                }
            }
        }
        // pushdown
        int loadtime = -1, scaletime = -1;
        int orgwidth, orgheight;
        QImage qimage;
        VERBOSEMSG(true, (L"Try Locking fPreqcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
        EnterCriticalSection(&fPreqMapcs);
        VERBOSEMSG(true, (L"Locked fPreqcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
        bool insertion = !fFetchedQImages.contains(workingindex);
        TPrefetchRecItem& preqimage = fFetchedQImages[workingindex]; // silent insert?
        if (insertion) {
            preqimage.FitImage = NULL;
            preqimage.EmbThumbImage = NULL;
            preqimage.AlumbThumbImage = NULL;
            preqimage.AnimateStore = NULL;
            preqimage.LargeImage = NULL;
            preqimage.FitExpired = true;
        }
        VERBOSEMSG(true, (L"Try Leave fPreqcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
        LeaveCriticalSection(&fPreqMapcs);
        VERBOSEMSG(true, (L"Leaved fPreqcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
        if (insertion || preqimage.FitImage == NULL || preqimage.FitExpired || preqimage.DoublePageMode != fDoublePageMode) {
            RETAILMSG(!insertion, (L"Regenerate FitImage for %s in BuildSinglePrefetch.\n", fScrollFiles[workingindex].filename.C_Str()));
            InterlockedExchange((long*)&fPrefetchBuildingIndex, workingindex);
            fBuildingFitIndexes.insert(workingindex);
            VERBOSEMSG(true, (L"Try Locking fBuildingcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
            //EnterCriticalSection(&fBuildingcs);
            if (!TryEnterCriticalSection(&fBuildingcs)) {
                fBuildingFitIndexes.erase(workingindex);
                VERBOSEMSG(true, (L"Lock fBuildingcs Failed in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
                return;
            }
            VERBOSEMSG(true, (L"Locked fBuildingcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
            QImageReader* reader;
            QBuffer uncompressbuf; // prevent refcou
            if (workingitem.fromarchive) {
                QFile pkfile(QString::fromUtf16((ushort*)(workingitem.archivename.C_Str())));
                pkfile.open(QIODevice::ReadOnly);
                pkfile.seek(workingitem.streamoffset);
                quint32 sourcesize = workingitem.sourcesize;
                QByteArray packedstream = pkfile.read(workingitem.streamsize);
                pkfile.close();
                if (workingitem.streamcodec == scStored) {
                    uncompressbuf.setData(packedstream);
                } else if (workingitem.streamcodec == scDeflate) {
                    uncompressbuf.setData(qUncompressEx((uchar*)packedstream.constData(), packedstream.size(), sourcesize));
                }
                mimetype = GuessMIMEType(&uncompressbuf);
                uncompressbuf.open(QBuffer::ReadOnly);

                reader = new QImageReader(&uncompressbuf);
            } else {
                reader = new QImageReader(QString::fromUtf16((ushort*)(workingitem.filename.C_Str())), MIMETypeToExt(mimetype).toAscii());
            }

            QSize orgsize = reader->size();
            orgwidth = orgsize.width();
            orgheight = orgsize.height();
            if (mimetype == itJPEG) {
                // TODO: thumbnail
            }

            if (mimetype == itPNG || mimetype == itJPEG) {
                if (NicetoScaleOnFly(orgsize, mimetype == itPNG, mimetype == itJPEG)) {
                    reader->setScaledSize(orgsize);
                    reader->setQuality(49);
                }
            }
            int starttime = GetTickCount();
            int lastmem = GetFreePhysMemory();
            if (reader->read(&qimage) == false) {
                qimage = QImage();
            }
            loadtime = GetTickCount() - starttime;
            if (reader->supportsAnimation() && reader->canRead()) {
                // TODO: size detection
                preqimage.FromAnimate = true;
                preqimage.AnimateStore = reader;
            } else {
                delete reader;
            }
            //orgwidth = qimage.width();
            //orgheight = qimage.height();
        } else {
            //LeaveCriticalSection(&fBuildingcs);
            //InterlockedExchange((long*)&fPrefetchBuildingIndex, -1);
            return; // keep
        }
        // if continues. need Leave Building cs
        int width, height;
        if (qimage.isNull() == false) {
            width = qimage.width();
            height = qimage.height();
            // qimage may have wild size in insertion mode
            if (fDoublePageMode != mdpmSingle) {
                preqimage.DoublePage = ((float)orgwidth / orgheight) > 1.2;
            }
            if (preqimage.FitImage == NULL) {
                preqimage.FitRotated = false;
            }
            // TODO: add argument
            if (width > height) {
                if (preqimage.DoublePage && fDoublePageMode != mdpmSingle) {
                    // 721 * 961
                    if (width > destheight * 2 || height > destwidth) {
                        // we wanna an 720 * 960 90 rotated picture without any border optimize
                        preqimage.FitRotated = NiceToRotate(qimage.width(), qimage.height(), destwidth, destheight * 2);
                        PrepareScaledQPicture(qimage, destwidth, destheight * 2, fScaleFilter, scaletime); // auto rotate
                    }
                    // else only keep and wait for black border.
                } else if (height - destheight > 80 || width - destwidth > 120) {
                    // Crop Scale 800x600 -> 720x480 may not rotated
                    preqimage.FitRotated = NiceToRotate(width, height, destwidth, destheight);
                    PrepareScaledQPicture(qimage, destwidth, destheight, fScaleFilter, scaletime); // auto rotate
                }
            } else {
                // width <= height
                // if we get qimage from prefetch and less than 720*960, do nothing
                if (preqimage.DoublePage && fDoublePageMode != mdpmSingle) {
                    if (width <= destwidth && height <= destheight * 2) {
                        // less than 720 * 960, ok
                    } else {
                        preqimage.FitRotated = NiceToRotate(qimage.width(), qimage.height(), destwidth, destheight * 2);
                        PrepareScaledQPicture(qimage, destwidth, destheight * 2, fScaleFilter, scaletime); // auto rotate
                    }
                } else if (width - destheight > 80 || height - destwidth > 120) {
                    // Crop Scale 600x800 -> 720x480, sometimes rotated
                    preqimage.FitRotated = NiceToRotate(width, height, destwidth, destheight);
                    PrepareScaledQPicture(qimage, destwidth, destheight, fScaleFilter, scaletime); // auto rotate
                } else {
                    if (width != height) {
                        // Safe for manual Rotate?
                        // Cute image , 200x400 -> 480x720
                        QMatrix matrix;
                        matrix.rotate(-90);
                        qimage = qimage.transformed(matrix, Qt::FastTransformation);
                        preqimage.FitRotated = true;
                    } else {
                        // 96x96
                        preqimage.FitRotated = false;
                    }
                }
            }

            bool expired = false;
            if (fMangaOptimizeMode && preqimage.Sharpened == false) {
                // TODO: AutoLevel, SurfaceBlure, AWarpSharpen
                OptimizeMangaImage(qimage);
                expired = true;
            }
            // Store fitimage for next. we only store uncrop unchecker qimage
            if (preqimage.FitImage == NULL || insertion || expired) {
                QImage *smart = new QImage(qimage); // @CopyRecord
                EnterCriticalSection(&fPreqMapcs);
                /*QImage *oldsmart = */
                (void) InterlockedExchange((long*)&preqimage.FitImage, (long)smart);
                /*if (oldsmart) {
                    RETAILMSG(true, (L"Try Drop old FitImage in BuildSinglePrefetch"));
                    delete oldsmart;
                }*/
                preqimage.FitExpired = false;
                preqimage.FileIndex = fCurrentScrollIndex; // TODO: add in argument
                preqimage.DoublePageMode = fDoublePageMode;
                preqimage.Sharpened = fMangaOptimizeMode;
                LeaveCriticalSection(&fPreqMapcs);
                preqimage.ExtraInfo.originalwidth = orgwidth;
                preqimage.ExtraInfo.originalheight = orgheight;
                preqimage.ExtraInfo.loadtime = loadtime;
                preqimage.ExtraInfo.scaletime = scaletime;
            }
        }
        // always leave. if qimage load failed, we go here directly from Invalid check
        VERBOSEMSG(true, (L"Try Leaving fBuildingcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
        LeaveCriticalSection(&fBuildingcs);
        VERBOSEMSG(true, (L"Leaved fBuildingcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
        InterlockedExchange((long*)&fPrefetchBuildingIndex, -1);
        fBuildingFitIndexes.erase(workingindex);
    }
}

void CMainWnd::BuildExtraPrefetchFork( int destwidth, int destheight )
{
    if (!fBuildingPrefetch) {
        if (fPrefetchBuilderThread != 0) {
            //delete fPrefetchBuilderThread;
            //fPrefetchBuilderThread = 0;
            if (fPrefetchBuilderThread->GetTerminated()) {
                NKDbgPrintfW(L"Reuse fPrefetchBuilderThread.\n");
                fPrefetchBuilderThread->Execute();
            } else {
                // Wait
                NKDbgPrintfW(L"Wait next chance for fPrefetchBuilderThread.\n");
            }

        } else {
            fPrefetchWidth = destwidth;
            fPrefetchHeight = destheight;
            UFUNCTION func;
            func.Data = this;
            func.Code = &CMainWnd::OnPrefetchBuilderThreadExecute;

            fPrefetchBuilderThread = new TThread(func, THREAD_PRIORITY_BELOW_NORMAL);
        }
    }
}

void CMainWnd::BuildSingleAlumbnail(int index, int destwidth, int destheight, bool force /* = false */) {
    //  Fit resample > EXIF thumbnail > Original cheating resample
    bool hardscale = false;
    bool cached = fFetchedQImages.contains(index);
    if (cached) {
        // Fit, EXIF, Original
        TPrefetchRecItem& preqimage = fFetchedQImages[index]; // should not insert
        if (preqimage.AlumbThumbImage) {
            // alright
            return;
        } else if (preqimage.FitImage) {
            // AlumbThumbImage = 0
            if (preqimage.FitImage->isNull()) {
                hardscale = true;
            } else {
                QImage qthumb = preqimage.FitImage->scaled(destwidth, destheight, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
                qthumb = qthumb.copy((qthumb.width() - destwidth) / 2, (qthumb.height() - destheight) / 2, destwidth, destheight);
                // Store for next
                QImage *smart = new QImage(qthumb); // @CopyRecord
                InterlockedExchange((long*)&preqimage.AlumbThumbImage, (long)smart);

            }
        } else {
            hardscale = true;
        }
    } else {
        hardscale = true;
    }
    if (hardscale && force) {
        //BuildSinglePrefetch()
        //BuildSingleAlumbnail(index, destwidth, destheight, false);
    }
}

bool CMainWnd::MixdownAlumbPiece() {
    if (!fAlumbPiece.isNull()) {
        //delete &fAlumbPiece;
    } else {
        fAlumbPiece = QImage(180 * 5, 185 * 3, QImage::Format_RGB32);
    }
    QPainter painter(&fAlumbPiece);
    // make background stripe
    if (fAlumbFilmStripe.isNull()) {
        LoadQImageFromResource(fAlumbFilmStripe, MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_FILESTRIPE_VECBG));
    }
    QBrush striper;
    striper.setTextureImage(fAlumbFilmStripe);
    painter.fillRect(fAlumbPiece.rect(), striper);
    // make thumbnails
    QFont litefont(fRotisLoaded?"RotisSansSerifStd-Light":"Tahoma", 12);
    //litefont.setWeight(QFont::DemiBold);
    litefont.setStyleStrategy(QFont::PreferAntialias);
    painter.setFont(litefont);
    QPen fontpen(QColor(0xCC0E2545), 1, Qt::SolidLine);
    QPen linepen(QColor(0x80BB5C49), 1, Qt::SolidLine);
    QPen highlightpen(QColor(0x80BB5C49), 3, Qt::SolidLine);

    QVector <int> orderedindex;
    orderedindex.append(fCurrentScrollIndex);
    for (int i = 1; i <= min(fScrollFiles.size()  / 2, 7); i++) { // 2 -> 3 4 | 1 0
        orderedindex.append(NormalizeScrollIndex(fCurrentScrollIndex + i));
        orderedindex.append(NormalizeScrollIndex(fCurrentScrollIndex - i));
    }
    for (QVector<int>::iterator it = orderedindex.begin(); it != orderedindex.end(); it++) {
        int iworkingindex = *it;
        if (fAlumbMarkedIndex == iworkingindex) {
            PastleSingleAlumbThumb(iworkingindex, painter, highlightpen, fontpen, true);
        } else {
            PastleSingleAlumbThumb(iworkingindex, painter, linepen, fontpen);
        }
    }

    painter.end();
    return true;
}

int CMainWnd::PastleSingleAlumbThumb( int iworkingindex, QPainter &painter, QPen linepen, QPen fontpen, bool checked )
{
    int centreoffset = CalculateIndexDistance(fCurrentScrollIndex, iworkingindex);//iworkingindex - fCurrentScrollIndex;
    int hoffset = (((centreoffset + 7) % 5) - 2) * 180;
    int voffset = ((centreoffset + 7) / 5 - 1) * 185;

    QRect imagerect(QPoint(370 + hoffset, 223 + voffset), QSize(160, 100));
    bool missed = false;
    BuildSingleAlumbnail(iworkingindex, 160, 100);
    bool cached = fFetchedQImages.contains(iworkingindex);
    if (cached) {
        TPrefetchRecItem& preqimage = fFetchedQImages[iworkingindex]; // should not insert
        if (preqimage.AlumbThumbImage) {
            if (preqimage.AlumbThumbImage->isNull()) {
                missed = true;
            } else {
                painter.setOpacity(1);
                painter.drawImage(imagerect, *preqimage.AlumbThumbImage);
                painter.setPen(linepen);
                painter.setBrush(Qt::NoBrush);
                painter.setOpacity(0.8);
                painter.drawRect(imagerect);
                painter.setPen(fontpen);
                painter.drawText(378 + hoffset, 240 + voffset, QString("%1").arg(iworkingindex + 1));
            }
        } else {
            missed = true;
        }
    } else {
        missed = true;
    }
    if (missed) {
        painter.setPen(linepen);
        painter.setBrush(QBrush(checked?0xF1EEE8:0xEADFC9)); // background
        painter.setOpacity(0.8);
        painter.drawRect(imagerect);
        painter.setPen(fontpen);
        painter.drawText(378 + hoffset, 240 + voffset, QString("%1").arg(iworkingindex + 1));
    }
    if (checked) {
        QPen hollowpen(QColor(missed?0xC4A44E:0x0CAE00), 5, Qt::SolidLine);
        painter.setPen(hollowpen);
        painter.setBrush(Qt::NoBrush);
        painter.setOpacity(0.75);
        painter.drawRoundedRect(imagerect.adjusted(-8, -28, 8, 35), 4, 4);
    }
    //painter.end();
    return voffset;
}


tagPrefetchRecItem::tagPrefetchRecItem()
{
    InterlockedExchange((LONG*)&FitImage, 0);
    InterlockedExchange((LONG*)&LargeImage, 0);
    InterlockedExchange((LONG*)&EmbThumbImage, 0);
    InterlockedExchange((LONG*)&AlumbThumbImage, 0);
    InterlockedExchange((LONG*)&AnimateStore, 0);
    FitExpired = true;
    FitRotated = FromAnimate = FromGrayscale = Corrupted = DoublePage = Sharpened = false;
    FileIndex = RightGuideLineX = LeftGuideLineX -1;
    DoublePageMode = mdpmSingle;
}

void CMainWnd::CheckPrefetchHealthy( bool flush )
{
    if (fAlumbPiece.isNull() == false) {
        fAlumbPiece = QImage(); // feed an null and release previous one
    }
    if (fAlumbFilmStripe.isNull() == false) {
        fAlumbFilmStripe = QImage();
    }
    if (fAlumbCursor.isNull() == false) {
        fAlumbCursor = QImage();
    }
    for (QMap<int, TPrefetchRecItem>::iterator it = fFetchedQImages.begin(); it!= fFetchedQImages.end();/* it++*/) {
        bool nukefit = true, nukeemb = true, nukealumb = true;
        int distance = CalculateIndexDistance(fCurrentScrollIndex, it->FileIndex);
        if (!flush) {
            //nukefit = distance > 3; // TODO: page scroll direction
            if (distance < 0) {
                nukefit = -distance > fPrevScollDepth + 1; // 0
            } else {
                nukefit = distance > fNextScrollDepth + 1; // 7
            }
            nukeemb = abs(distance) > 8; // 17
            nukealumb = abs(distance) > 7; // 15
        }
        QImageReader* smart2 = (QImageReader*)InterlockedExchange(((long*)&it->AnimateStore), 0);
        if (smart2) {
            delete smart2;
            it->FromAnimate = false;
            nukefit = true;
        }
        QImage *smart;
        if (nukefit) {
            smart = (QImage*)InterlockedExchange(((long*)&it->FitImage), 0);
            if (smart) {
                delete smart;
            }
        }
        if (nukeemb) {
            smart = (QImage*)InterlockedExchange(((long*)&it->EmbThumbImage), 0);
            if (smart) {
                delete smart;
            }
        }
        if (nukealumb) {
            smart = (QImage*)InterlockedExchange(((long*)&it->AlumbThumbImage), 0);
            if (smart) {
                delete smart;
            }
        }
        smart = (QImage*)InterlockedExchange(((long*)&it->LargeImage), 0);
        if (smart) {
            delete smart;
        }
        // save is insertion code
        if (it->FitImage == NULL && it->EmbThumbImage == NULL && it->AlumbThumbImage == NULL && it->LargeImage == NULL) {
            //fFetchedQImages.remove(it.key());
            it = fFetchedQImages.erase(it);
        } else {
            it++;
        }
    }
}


int CMainWnd::OnPrefetchBuilderThreadExecute(TThread* Sender) {
    if (!fBuildingPrefetch) {
        // TODO: use set to run multi building
        VERBOSEMSG(true, (L"Try BuildExtraPrefetch in OnPrefetchBuilderThreadExecute (0x%x)...\n", Sender->GetThreadId()));
        EnterCriticalSection(&fPreqMapcs);
        fBuildingPrefetch = true;
        LeaveCriticalSection(&fPreqMapcs);
        VERBOSEMSG(true, (L"Try BuildExtraPrefetch 2 in OnPrefetchBuilderThreadExecute (0x%x)...\n", Sender->GetThreadId()));

        BuildExtraPrefetch(fPrefetchWidth, fPrefetchHeight);

        VERBOSEMSG(true, (L"Try BuildExtraPrefetch 3 in OnPrefetchBuilderThreadExecute (0x%x)...\n", Sender->GetThreadId()));
        EnterCriticalSection(&fPreqMapcs);
        fBuildingPrefetch = false;
        LeaveCriticalSection(&fPreqMapcs);

        VERBOSEMSG(true, (L"OnPrefetchBuilderThreadExecute Finished (0x%x).\n", Sender->GetThreadId()));
    }
    //Sender->Terminate();
    //delete Sender;
    return 0;
}

tagScrollFileRecItem::tagScrollFileRecItem() {
    filename = archivename = NULL;
    filesize = filetimestamp.dwHighDateTime = filetimestamp.dwLowDateTime = 0;
    fromarchive = false;
    streamoffset = streamsize = sourcesize = 0;
    streamcodec = scUnknown;
}

bool CMainWnd::RebuildScrollList()
{
    fScrollFiles.clear();
    TImageType mimetype = GuessMIMEType(fStagefile);
    if (mimetype == itPKArchive) {
        // TODO: Mix Parse
        fCurrentScrollIndex = 0;
        fLastBrowsePath = fStagefile; // Broken keep path in BrowseFile
        BuildPKArchiveScrollList(QString::fromUtf16((ushort*)fStagefile.C_Str()), fScrollFiles);
    } else {
        TDirList BaseDirList;
#ifdef _DEBUG
        BaseDirList.ScanDirectoryEx(fLastBrowsePath, _T("*.jpg;*.jpeg;*.png;*.gif;*.bmp;*.cr2;*.hdp;*.wdp"), 0);
#else
        BaseDirList.ScanDirectoryEx(fLastBrowsePath, _T("*.jpg;*.jpeg;*.png;*.gif;*.bmp"), 0);
#endif
        for (int i = 0; i < BaseDirList.GetCount(); i++) {
#ifndef UNDER_CE
            if (BaseDirList.GetNames(i).Compare(L".") == 0 || BaseDirList.GetNames(i).Compare(L"..") == 0) {
                continue;
            }
#endif
            CMzStringW NewDestFile = fLastBrowsePath + BaseDirList.GetNames(i);
            if (BaseDirList.GetIsDirectory(i)) {

            } else {

                TScrollFileRecItem item;
                item.filename = NewDestFile;
                item.filesize = BaseDirList.Get(i)->nFileSizeLow;
                *LPDWORD(LPBYTE(&item.filesize) + 4)  = BaseDirList.Get(i)->nFileSizeHigh;
                item.filetimestamp = BaseDirList.Get(i)->ftLastWriteTime;
                fScrollFiles.push_back(item);
                //if (fStagefile.Compare(NewDestFile) == 0) {
                //    fCurrentScrollIndex = i;
                //}
            }
        }

        // TODO: Smart Sort
        // HCG_1 HCG_10 (HCG=HCG) (1 < 10) explode on L->d d->L
        struct FilenameCompareAsc {
            bool operator()(const TScrollFileRecItem& rec1, const TScrollFileRecItem& rec2) {
                return rec1.filename.Compare(rec2.filename) < 0;
            }
        };
        sort(fScrollFiles.begin(), fScrollFiles.end(), FilenameCompareAsc());

        int i = 0;
        for (TScrollFileList::iterator it = fScrollFiles.begin(); it != fScrollFiles.end(); it++) {
            if (AnsiCompareStrNoCase(it->filename, fStagefile) == 0) {
                fCurrentScrollIndex = i;
            }
            DEBUGMSG(fCurrentScrollIndex != i, (L"%s != %s\n", it->filename.C_Str(), fStagefile.C_Str()));
            i++;
        }
    }
    return false;
}

bool CMainWnd::OnMouseDown( int x, int y, int& Rslt )
{
    bool scrolled = false;
    fLastMouseDownTick = GetTickCount();
    if (fStageShowMode == ssmFit) {
        // Widget emulation?
        // ssFit
        if (fDrawToolbar) {
            bool changed = false;
            for (QMap < int, TFakeWidgetRecItem >::iterator it = fFakeWidgets.begin(); it != fFakeWidgets.end(); it++ ) {
                if (it->TouchRect.contains(x, y)) {
                    if (it->Hovered == false) {
                        if (it->ParentID != -1) {
                            // Restore parent hover
                            if (fFakeWidgets.contains(it->ParentID)) {
                                if (fFakeWidgets[it->ParentID].Hovered) {
                                    it->Hovered = true;
                                    changed = true;
                                } else {
                                    // bypass click start
                                }
                            } else {
                                // something wrong
                            }
                        } else {
                            changed = true;
                            it->Hovered = true;
                        }
                        //if (it->SingleToggle == false) {
                        //    fDrawSubMenu = false;
                        //}
                    } else {
                        changed = true;
                        it->Hovered = false;
                    }
                } else {
                    // will clean other checked
                    // keep an expand one?
                    if (it->Hovered && it->SingleToggle == false) {
                        it->Hovered = false;
                        changed = true;
                    }
                }
            }
            if (changed) {
                // test sub menu click
                // Last Click Time
                fLastToolbarDrawTick = GetTickCount();
                SetTimer(m_hWnd, IDT_SOMEMARKTIMEOUT, 8080, NULL);
                PaintStage();
                return true;
            }
        }

        // move?
        if (fScrollFiles.size() <= 1) {
            Rslt = 0;
            return true;
        }

        // ScrollArea
        if (x > 30 && x < (636)) {
            if (y < 90) {
                // right, right
                scrolled = true;
                ShowPageMark(pmdRight);
                fLastPushDirection = pmdRight;
                //fCurrentScrollIndex++;
            } else if (y > 390) {
                // scroll left, left
                scrolled = true;
                ShowPageMark(pmdLeft);
                fLastPushDirection = pmdLeft;
                //fCurrentScrollIndex--;
            }
        }
        if (y > 30 && y < 450) {
            if (x < 90) {
                // Up, Up
                scrolled = true;
                ShowPageMark(pmdUp);
                fLastPushDirection = pmdUp;
            } else if (x > (630)) {
                // Down
                scrolled = true;
                ShowPageMark(pmdDown);
                fLastPushDirection = pmdDown;
            }
        }
    } else if (fStageShowMode == ssmAlumb) {
        //int combinedx = x + (fFullScreen?720:619) / 2 - fAlumbPiece.width() / 2 - fOriginalOffsetY; // 720
        //int combinedx =  fAlumbPiece.width() / 2 - (fFullScreen?720:619) / 2 - fOriginalOffsetY + x; // 720
        //int combinedy = fAlumbPiece.height() - y + fOriginalOffsetX; // 480
        int combinedx = x - (fStageSize.stage1width - fAlumbPiece.width()) / 2 - fOriginalOffsetX;
        int combinedy = y - (fStageSize.stage1height - fAlumbPiece.height()) / 2 - fOriginalOffsetY;
        if (combinedx % 180 > 10 && combinedx % 180 < 170) {
            if (combinedy % 185 > 38 && combinedy % 185 < 138) {
                //int hoffset = (((centreoffset + 7) % 5) - 2) * 180;
                //int voffset = ((centreoffset + 7) / 5 - 1) * 185;
                int targetindex = NormalizeScrollIndex(fCurrentScrollIndex - 7 + (combinedx / 180) + (combinedy / 185) * 5);
                if (fAlumbMarkedIndex != targetindex) {
                    fAlumbMarkedIndex = targetindex;
                    MixdownAlumbPiece();
                } else {
                    fAlumbMarkedCount++;
                }

                QPainter painter(&fAlumbPiece);
                //painter.setPen(QPen(QColor(0xCC0E2545), 1, Qt::SolidLine));
                //QRect rect(0, 0, 20, 20);
                //rect.moveCenter(QPoint(combinedx, combinedy));
                //painter.drawRect(rect);
                if (fAlumbCursor.isNull()) {
                    LoadQImageFromResource(fAlumbCursor, MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_XFREESELECT));
                }
                painter.drawImage(combinedx - 12, combinedy + 4, fAlumbCursor);
                painter.end();
                PaintStage();
            }
        }
        return true;
    }
    
    if (scrolled) {
        //ScrollPage();
        fDiscardLongPress = true;
        if (fStageShowMode == ssmOriginal) {
            fStageShowMode = ssmFit;
        }
        return true;
    }

    return false;
}


bool CMainWnd::OnMouseDblClick( int x, int y )
{
    if (fStageShowMode == ssmFit) {
        // Centre touch
        if ( x > 100 && x < (620) ) {
            if ( y > 100 && y < 380 ) {
                if (GetTickCount() - fLastMouseDownTick < 800) {
                    //ToggleFullScreen();
                    //PaintStage();
                    //CheckPrefetchHealthy();
                    // refuse user to check screen

                    TryStopAnimation();

                    fExtraInfo = ExtraInfoLevel(int(fExtraInfo) + 1);
                    if (fExtraInfo > eilBench) {
                        fExtraInfo = eilSilent;
                    }
                    fLastExtraInfoLevelHintTick = GetTickCount();
                    SetTimer(m_hWnd, IDT_SOMEHINTTIMEOUT, 2080, NULL);
                    //fDrawToolbar = !fDrawToolbar; // for up to nuke

                    //PaintStage();

                    fLastMouseDownTick = GetTickCount();
                    return true;
                }
            }
        }
    } else if (fStageShowMode == ssmOriginal) {
        if (GetTickCount() - fLastMouseDownTick < 360) {
            if (fZoomLevel > 1) {
                fZoomLevel = 1;
            } else {
                fStageShowMode = ssmFit;
            }
            PaintStage();
            fLastMouseDownTick = GetTickCount();
            return true;
        }
    } else if (fStageShowMode == ssmAlumb) {
        // check down index
        int combinedx = x - (fStageSize.stage1width - fAlumbPiece.width()) / 2 - fOriginalOffsetX;
        int combinedy = y - (fStageSize.stage1height - fAlumbPiece.height()) / 2 - fOriginalOffsetY;
        if (combinedx % 180 > 10 && combinedx % 180 < 170) {
            if (combinedy % 185 > 38 && combinedy % 185 < 138) {
                int targetindex = NormalizeScrollIndex(fCurrentScrollIndex - 7 + (combinedx / 180) + (combinedy / 185) * 5);
                fStageShowMode = ssmFit;
                if (fCurrentScrollIndex != targetindex) {
                    if (targetindex > fCurrentScrollIndex) {
                        UpdateScrollDepthLevel(pmdRight);
                    } else {
                        UpdateScrollDepthLevel(pmdLeft);
                    }
                    fCurrentScrollIndex = targetindex;
                    ScrollPage();
                } else {
                    PaintStage(); // untouchable
                }
                fLastMouseDownTick = GetTickCount();
            }
        }
        return true;
    }
    return false;
}

void CMainWnd::ScrollPage()
{
    /*if (fCurrentScrollIndex < 0) {
        fCurrentScrollIndex = fScrollFiles.size() - 1;
    }
    if (fCurrentScrollIndex >= fScrollFiles.size()) {
        fCurrentScrollIndex = 0;
    }*/
    fCurrentScrollIndex = NormalizeScrollIndex(fCurrentScrollIndex);

    //fStagefile = fScrollFiles[fCurrentScrollIndex].filename;
    EnterCriticalSection(&fAnimatecs);
    CheckPrefetchHealthy();
    LeaveCriticalSection(&fAnimatecs);
    fPauseAni = false; // Move, Move
    PaintStage();
}



void CMainWnd::ClickToNextPage(bool next)
{
    bool TouchScrollIndex = false;
    if (fDoublePageMode != mdpmSingle) {
        // TODO: manga style animation
        if (fFetchedQImages.contains(fCurrentScrollIndex)) {
            if (fFetchedQImages[fCurrentScrollIndex].DoublePage) {
                fInternelPageIndex = 1 - fInternelPageIndex;
                if (next) {
                    TouchScrollIndex = (fInternelPageIndex == 0);
                } else {
                    TouchScrollIndex = (fInternelPageIndex == 1);
                }
            }
        }
    } else {
        // Single
        TouchScrollIndex = true;
    }
    if (TouchScrollIndex) {
        if (next) {
            fCurrentScrollIndex++;
            UpdateScrollDepthLevel(pmdRight);
        } else {
            fCurrentScrollIndex--;
            UpdateScrollDepthLevel(pmdLeft);
        }
    }
}

bool CMainWnd::OnMouseMove(int x, int y) {
    if (fLastMouseX != -1 && fLastMouseY != -1) {
        int deltax, deltay;
        if (fStageShowMode == ssmOriginal ||fStageShowMode == ssmAlumb) {
            /*unsigned int step = -1;
            POINT pt[64];
            while (step != 0) {
                if (GetMouseMovePoints(&pt[0], _countof(pt), &step) == FALSE) {
                    break;
                }
                MapWindowPoints(HWND_DESKTOP, m_hWnd, &pt[0], step);
                for (int i = 0; i < step; i++) {
                    int tempx = pt[i].x / 4;
                    int tempy = pt[i].y / 4;
                    RenderFormatLocalPos(tempx, tempy);

                    deltax = (tempx - fLastMouseX);
                    deltay = (tempy - fLastMouseY);

                    fOriginalOffsetX += deltax;
                    fOriginalOffsetY += deltay;

                    PaintStage();

                    fLastMouseX = tempx;
                    fLastMouseY = tempy;
                }
                if (step < _countof(pt)) {
                    break;
                }
            }*/
            deltax = (x - fLastMouseX);
            deltay = (y - fLastMouseY);
            fOriginalOffsetX += deltax;
            fOriginalOffsetY += deltay;

            PaintStage();

            if (fStageShowMode == ssmAlumb) {
                fAlumbMarkedCount = 0;
            }
        } else {
            // fit?
            deltax = (x - fLastMouseX);
            deltay = (y - fLastMouseY);
            fOriginalOffsetX += deltax;
            fOriginalOffsetY += deltay;
        }
        CalculateMovingStep(deltax, deltay);
        // TODO: check toolbar action
        int action = GuessFingerAction();

        if (action != -1) {
            bool licked = false;
            switch(action) {
                case fmaDown:
                    {
                        // Extra Check
                        QRect ToolbarRect(650, 0, 80, 480);
                        if (fDrawToolbar == false && ToolbarRect.contains(x, y)) {
                            // Show Toolbar
                            TryStopAnimation();

                            //fExtraInfo = false;
                            //if (fExtraInfo == eilBench) {
                            //    fExtraInfo = eilSilent;
                            //}
                            fLastToolbarDrawTick = GetTickCount();
                            SetTimer(m_hWnd, IDT_SOMEMARKTIMEOUT, 8080, NULL);
                            fDrawToolbar = true;

                            PaintStage();
                            licked = true;
                        }
                    }
                    break;
                case fmaUp:
                    {
                        // Extra Check
                        QRect ToolbarRect(650, 0, 80, 480);
                        if (fDrawToolbar && ToolbarRect.contains(x, y)) {
                            // Show Toolbar
                            TryStopAnimation();

                            //fExtraInfo = false;
                            //if (fExtraInfo == eilBench) {
                            //    fExtraInfo = eilSilent;
                            //}
                            fDrawToolbar = false;

                            PaintStage();
                            licked = true;
                        }
                    }
                    break;
            }
            if (licked) {
                LeftMovingCount = RightMovingCount = UpMovingCount = DownMovingCount = 0;
                LeftUpMovingCount = RightUpMovingCount = LeftDownMovingCount = RightDownMovingCount = 0;
                fLastMouseX = fLastMouseY = -1; // dangerous
            }
        }
    }
    fLastMouseX = x;
    fLastMouseY = y;
    fDiscardLongPress = true;
    return true;
}

void CMainWnd::CalculateMovingStep( int deltax, int deltay )
{
    // calculate direction
    if (deltax == 0 || deltay == 0) {
        if (deltax > 0) { // +
            RightMovingCount++;
        }
        if (deltax < 0) { // -
            LeftMovingCount++;
        }
        if (deltay < 0) { // -
            UpMovingCount++;
        }
        if (deltay > 0) { // +
            DownMovingCount++;
        }
    } else if (fabs(double(min(abs(deltax), abs(deltay)))/max(abs(deltax), abs(deltay))) >= 0.75) { // fabs useless
        // X mark
        // X or Y offset always not 0
        if (deltax > 0) { // +
            if (deltay < 0) { // + -
                RightUpMovingCount++;
            } else if (deltay > 0) { // + +
                RightDownMovingCount++;
            }
        }
        if (deltax < 0) { // -
            if (deltay < 0) { // - -
                LeftUpMovingCount++;
            } else if (deltay > 0) { // - +
                LeftDownMovingCount++;
            }
        }
    } else {
        // + sign
        // X or Y offset always not 0
        if (abs(deltax) > abs(deltay)) {
            // Left, right
            if (deltax > 0) { // +
                RightMovingCount++;
            } else if (deltax < 0) { // -
                LeftMovingCount++;
            }
        } else {
            if (deltay < 0) { // -
                UpMovingCount++;
            } else if (deltay > 0) { // +
                DownMovingCount++;
            }
        }

    }
}

int CMainWnd::GuessFingerAction() {
    int allsum = LeftMovingCount + RightMovingCount + UpMovingCount + DownMovingCount \
        + LeftUpMovingCount + RightUpMovingCount + LeftDownMovingCount + RightDownMovingCount;
    if (allsum == 0 && allsum < 2) { // 2~5
        return -1; // violate div 0 error
     }
    // TODO: re factory to array cycle
    int thousand = 70;
    if (LeftMovingCount * 100 / allsum > thousand) {
        return fmaLeft;
    }
    if (RightMovingCount * 100 / allsum > thousand) {
        return fmaRight;
    }
    if (UpMovingCount * 100 / allsum > thousand) {
        return fmaUp;
    }
    if (DownMovingCount * 100 / allsum > thousand) {
        return fmaDown;
    }
    if (LeftUpMovingCount * 100 / allsum > thousand) {
        return fmaLeftUp;
    }
    if (RightUpMovingCount * 100 / allsum > thousand) {
        return fmaRightUp;
    }
    if (LeftDownMovingCount * 100 / allsum > thousand) {
        return fmaLeftDown;
    }
    if (RightDownMovingCount * 100 / allsum > thousand) {
        return fmaRightDown;
    }
    return -1;
}

void CMainWnd::UpdateScrollDepthLevel( PageMarkDirection direction )
{
    if (direction == pmdLeft || direction == pmdUp) {
        // 3 -> 2
        fNextScrollDepth++;
        if (fNextScrollDepth > 4) {
            fNextScrollDepth = 4;
        }
        fPrevScollDepth--;
        if (fPrevScollDepth < 0) {
            fPrevScollDepth = 0;
        }
    } else if (direction == pmdRight || direction == pmdDown) {
        // 3 -> 4
        fPrevScollDepth++;
        if (fPrevScollDepth > 4) {
            fPrevScollDepth = 4;
        }
        fNextScrollDepth--;
        if (fNextScrollDepth < 0) {
            fNextScrollDepth = 0;
        }
    }
}

bool CMainWnd::OnMouseUp( int x, int y ) {
    fDiscardLongPress = false;
    if (fStageShowMode == ssmOriginal) {
        // reset moving counting
        fLastMouseX = fLastMouseY = -1;
        return true;
    } else if (fStageShowMode == ssmAlumb) {
        int combinedx = x - (fStageSize.stage1width - fAlumbPiece.width()) / 2 - fOriginalOffsetX;
        int combinedy = y - (fStageSize.stage1height - fAlumbPiece.height()) / 2 - fOriginalOffsetY;
        if (combinedx % 180 > 10 && combinedx % 180 < 170) {
            if (combinedy % 185 > 38 && combinedy % 185 < 138) {
                int targetindex = NormalizeScrollIndex(fCurrentScrollIndex - 7 + (combinedx / 180) + (combinedy / 185) * 5);
                if (fAlumbMarkedCount > 0) {
                    fStageShowMode = ssmFit;
                    if (fCurrentScrollIndex != targetindex) {
                        if (targetindex > fCurrentScrollIndex) {
                            UpdateScrollDepthLevel(pmdRight);
                        } else {
                            UpdateScrollDepthLevel(pmdLeft);
                        }
                        fCurrentScrollIndex = targetindex;
                        ScrollPage();
                    } else {
                        PaintStage(); // untouchable
                    }
                }
            }
        }
        return true;
    } else if (fStageShowMode == ssmFit) {
        // release button
        int action = GuessFingerAction();
        LeftMovingCount = RightMovingCount = UpMovingCount = DownMovingCount = 0;
        LeftUpMovingCount = RightUpMovingCount = LeftDownMovingCount = RightDownMovingCount = 0;
        fLastMouseX = fLastMouseY = -1; // dangerous
        if (action != -1) {
            switch(action) {
                case fmaDown:
                    {
                        // Extra Check
                        QRect ToolbarRect(650, 0, 80, 480);
                        if (fDrawToolbar == false && ToolbarRect.contains(x, y)) {
                            // Show Toolbar
                            TryStopAnimation();

                            //fExtraInfo = false;
                            //if (fExtraInfo == eilBench) {
                            //    fExtraInfo = eilSilent;
                            //}
                            fLastToolbarDrawTick = GetTickCount();
                            SetTimer(m_hWnd, IDT_SOMEMARKTIMEOUT, 8080, NULL);
                            fDrawToolbar = true;

                            PaintStage();
                            return true;
                        } else if (ToolbarRect.contains(x, y)) {
                            // show on show
                            break;
                        }
                    }

                case fmaLeft:
                    {
                        fDiscardClick = true; // wait for flower set it to false
                        ShowFlowingOnStageFork(action==fmaDown?pmdRight:pmdDown);
                        //EnterCriticalSection(&fFlowingcs); // wait only
                        //LeaveCriticalSection(&fFlowingcs);
                        int start = GetTickCount();
                        while (fDiscardClick && GetTickCount() - start < 3874) {
                            Sleep(1);
                            ProcessMessage();
                        }
                        if (fDiscardClick) {
                            NKDbgPrintfW(L"Still Flowing?\n");
                            if (fFlowingKeeperThread) {
                                delete fFlowingKeeperThread;
                                fFlowingKeeperThread = 0;
                            }
                        }
                        ClickToNextPage(true);

                        ScrollPage();
                    }
                    break;
                case fmaUp:
                    {
                        // Extra Check
                        QRect ToolbarRect(650, 0, 80, 480);
                        if (fDrawToolbar && ToolbarRect.contains(x, y)) {
                            // Show Toolbar
                            TryStopAnimation();

                            //fExtraInfo = false;
                            //if (fExtraInfo == eilBench) {
                            //    fExtraInfo = eilSilent;
                            //}
                            fDrawToolbar = false;

                            PaintStage();
                            return true;
                        } else if (ToolbarRect.contains(x, y)) {
                            // hide on hide
                            break;
                        }
                    }
                case fmaRight:
                    {
                        fDiscardClick = true;
                        ShowFlowingOnStageFork(action==fmaUp?pmdLeft:pmdUp);
                        //EnterCriticalSection(&fFlowingcs); // wait only
                        //LeaveCriticalSection(&fFlowingcs);
                        int start = GetTickCount();
                        while (fDiscardClick && GetTickCount() - start < 3874) {
                            Sleep(1);
                            ProcessMessage();
                        }
                        if (fDiscardClick) {
                            NKDbgPrintfW(L"Still Flowing?\n");
                            if (fFlowingKeeperThread) {
                                delete fFlowingKeeperThread;
                                fFlowingKeeperThread = 0;
                            }
                        }
                        ClickToNextPage(false);

                        ScrollPage();
                    }
                    break;
                case fmaLeftUp:
                case fmaRightUp:
                case fmaLeftDown:
                case fmaRightDown:
                    // ever overlay or GDI Mode
                    if (fEmulatorDetected) {
                        fStageShowMode = ssmAlumb; // Switch to Alumb view
                        fAlumbMarkedIndex = fCurrentScrollIndex;
                        fAlumbMarkedCount = 0;
                        MixdownAlumbPiece();
                        fOriginalOffsetX = fOriginalOffsetY = 0;
                        PaintStage();
                    }

                    break;
            }
            return true;
        }

        // Centre touch
        if (( x > 100 && x < 620 ) && ( y > 100 && y < 380 )) {
            bool bypass = false;
            if (fDrawToolbar) {
                for (QMap < int, TFakeWidgetRecItem >::iterator it = fFakeWidgets.begin(); it != fFakeWidgets.end(); it++ ) {
                    if (it->TouchRect.contains(x, y)) {
                        if (it->ParentID != -1) {
                            // Restore parent hover
                            if (fFakeWidgets.contains(it->ParentID)) {
                                if (fFakeWidgets[it->ParentID].Hovered && it->Hovered) {
                                    bypass = true;
                                }
                            }
                        } else {
                            if (it->Hovered) {
                                bypass = true;
                            }
                        }
                    }
                    if (bypass) {
                        break;
                    }
                }
            }
            if (bypass == false) {
                // Check animation
                if (fAnimationKeeperThread == NULL) {
                    // Center Triggered
                    // fExtraInfo = !fExtraInfo;
                    //if (fExtraInfo) {
                    if (fDrawToolbar) {
                        fDrawToolbar = false;
                    } else {
                        fLastToolbarDrawTick = GetTickCount();
                        SetTimer(m_hWnd, IDT_SOMEMARKTIMEOUT, 8080, NULL);
                        fDrawToolbar = true;
                    }
                    PaintStage();
                    return true;
                } else if (!fAnimationKeeperThread->GetTerminated()) {
                    TryStopAnimation();

                    //fExtraInfo = false;
                    //if (fExtraInfo == eilBench) {
                    //    fExtraInfo = eilSilent;
                    //}
                    fLastToolbarDrawTick = GetTickCount();
                    SetTimer(m_hWnd, IDT_SOMEMARKTIMEOUT, 8080, NULL);
                    fDrawToolbar = true;

                    PaintStage();
                    return true;
                }
            }
        }

        // Widget emulation?
        // ssFit
        if (fDrawToolbar) {
            bool hitted = false, changed = false, licked = false;
            int CommandID = -1;
            for (QMap < int, TFakeWidgetRecItem >::iterator it = fFakeWidgets.begin(); it != fFakeWidgets.end(); it++ ) {
                if (it->TouchRect.contains(x, y)) {
                    if (it->ParentID != -1) {
                        // Restore parent hover
                        if (fFakeWidgets.contains(it->ParentID)) {
                            if (fFakeWidgets[it->ParentID].Hovered && it->Hovered) {
                                hitted = true;
                            }
                        }
                    } else {
                        if (it->Hovered) {
                            hitted = true;
                        }
                    }
                    if (hitted) {
                        //OnMzCommand(MZ_IDC_TOOLBAR1, it->ID);
                        CommandID = it->ID;
                    }
                }
                if (it->Hovered) {
                    // try restore each none toggle button
                    if (it->SingleToggle == false) {
                        it->Hovered = false;
                        changed = true;

                        if (it->ParentID != -1) {
                            // none toggle button clicked. parent over
                            if (fFakeWidgets.contains(it->ParentID)) {
                                fFakeWidgets[it->ParentID].Hovered = false;
                            }
                        }
                    } else {
                        // Hover, single
                        // like Divide, Menu button
                        if (it->ParentID != -1 && fFakeWidgets.contains(it->ParentID)) {
                            if (fFakeWidgets[it->ParentID].Hovered == false) {
                                it->Hovered = false;
                                changed = true;
                            }
                        }
                        licked = true;
                    }
                }
            }
            // check completed
            if (!hitted) {
                for (QMap < int, TFakeWidgetRecItem >::iterator it = fFakeWidgets.begin(); it != fFakeWidgets.end(); it++ ) {
                    if (it->SingleToggle == false) {
                        // no parents
                        it->Hovered = false;
                    } else if (it->ParentID != -1 && fFakeWidgets.contains(it->ParentID)) {
                        // SingleToggle, Have Parent
                        if (fFakeWidgets[it->ParentID].Hovered == false) {
                            it->Hovered = false;
                        }
                    }
                }
            }
            if (changed) {
                PaintStage();
            }
            if (hitted) {
                OnMzCommand(MZ_IDC_TOOLBAR1, CommandID);
                return true;
            }
            if (licked) {
                return true;
            }
        } else {
            for (QMap < int, TFakeWidgetRecItem >::iterator it = fFakeWidgets.begin(); it != fFakeWidgets.end(); it++ ) {
                //if (it->SingleToggle == false) {
                    it->Hovered = false;
                //}
            }
            // Original design is show toolbar again.
            // by pass for ScrollArea detection now.
        }

        // ScrollArea
        bool scrolled = false;
        if ( x > 30 && x < 636 ) {
            if ( y < 90 ) {
                // right, right
                if (fLastPushDirection == pmdRight) {
                    // TODO: more safe check
                    scrolled = true;

                    ClickToNextPage(true);
                } else {
                    NKDbgPrintfW(L"fLastPushDirection %d mismatch pmdRight\n", fLastPushDirection);
                }
            } else if ( y > 390 ) {
                // scroll left, left
                if (fLastPushDirection == pmdLeft) {
                    scrolled = true;
                    ClickToNextPage(false);
                }
            }
        }
        if (y > 30 && y < 450) {
            if (x < 90) {
                // Up, Up
                if (fLastPushDirection == pmdUp) {
                    scrolled = true;
                    ClickToNextPage(false);
                }
            } else if (x > 630) {
                // Down
                if (fLastPushDirection == pmdDown) {
                    scrolled = true;
                    ClickToNextPage(true);
                }
            }
        }

        if (scrolled) {
            ScrollPage();
            return true;
        }
    }
    return false;
}

bool CMainWnd::OnZoomIn(int x, int y) {
    // 50, 100, 200
    if (fStageShowMode == ssmFit) {
        // from fit to 100%, or last alumb restore
        if (GetTickCount() - fLastZoomTick > 500) {
            ShowZoomMark(zmaOriginal);
            fStageShowMode = ssmOriginal;
            fOriginalOffsetX = fOriginalOffsetY = 0;
            fZoomLevel = 1;
            PaintStage();
            fLastZoomTick = GetTickCount();
            return true;
        }
    } else if (fStageShowMode == ssmOriginal) {
        // continue zoom
        if (fZoomLevel < 4) {
            if (GetTickCount() - fLastZoomTick > 500) {
                fZoomLevel = fZoomLevel + 1;
                PaintStage();
                fLastZoomTick = GetTickCount();
                return true;
            }
        }
    } else if (fStageShowMode == ssmAlumb) {
        // alumb return to fit
        if (GetTickCount() - fLastZoomTick > 300) {
            //int combinedx =  fAlumbPiece.width() / 2 - (fStageSize.stage1width) / 2 - fOriginalOffsetY + y; // 720
            //int combinedy = fAlumbPiece.height() - x + fOriginalOffsetX; // 480
            int combinedx = x - (fStageSize.stage1width - fAlumbPiece.width()) / 2 - fOriginalOffsetX;
            int combinedy = y - (fStageSize.stage1height - fAlumbPiece.height()) / 2 - fOriginalOffsetY;
            if (combinedx % 180 > 10 && combinedx % 180 < 170) {
                if (combinedy % 185 > 38 && combinedy % 185 < 138) {
                    int targetindex = NormalizeScrollIndex(fCurrentScrollIndex - 7 + (combinedx / 180) + (combinedy / 185) * 5);
                    // now paint stage
                    fStageShowMode = ssmFit;
                    if (fCurrentScrollIndex != targetindex) {
                        if (targetindex > fCurrentScrollIndex) {
                            UpdateScrollDepthLevel(pmdRight);
                        } else {
                            UpdateScrollDepthLevel(pmdLeft);
                        }
                        fCurrentScrollIndex = targetindex;
                        ScrollPage();
                    } else {
                        PaintStage(); // untouchable
                    }
                }
            }
            fLastZoomTick = GetTickCount();
            return true;
        }
    }
    return false;
}

bool CMainWnd::OnZoomOut(int x, int y) {
    // 200, 100, 50
    if (fStageShowMode == ssmOriginal) {
        if (fZoomLevel > 1) {
            if (GetTickCount() - fLastZoomTick > 500) {
                fZoomLevel = fZoomLevel - 1;
                PaintStage();
                fLastZoomTick = GetTickCount();
                return true;
            }
        } else {
            if (GetTickCount() - fLastZoomTick > 200) {
                ShowZoomMark(zmaBestFit);
                fStageShowMode = ssmFit;
                PaintStage();
                fLastZoomTick = GetTickCount(); // no necessary
                return true;
            }
        }
    } else if (fStageShowMode == ssmFit) {
        if (GetTickCount() - fLastZoomTick > 200) {
            fStageShowMode = ssmAlumb;
            fAlumbMarkedIndex = fCurrentScrollIndex;
            fAlumbMarkedCount = 0;
            MixdownAlumbPiece();
            fOriginalOffsetX = fOriginalOffsetY = 0;
            PaintStage();
            fLastZoomTick = GetTickCount(); // no necessary
            return true;
        }
    }
    return false;
}

bool CMainWnd::OnZoomEnd(int x, int y) {
    return false;
}

bool CMainWnd::OnLongPressStart(int x, int y) {
    return true;
}

bool CMainWnd::OnLongPressTimeout(int x, int y) {
    if (fDiscardLongPress) {
        return false;
    }
    if (fDrawToolbar) {
        if (x >= 646) {
            return false;
        }
    }
    if (fStageShowMode == ssmFit) {
        return OnZoomIn(x, y);
    } else if (fStageShowMode == ssmOriginal) {
        return OnZoomOut(x, y);
    }  else if (fStageShowMode == ssmAlumb) {
        int combinedx = x - (fStageSize.stage1width - fAlumbPiece.width()) / 2 - fOriginalOffsetX;
        int combinedy = y - (fStageSize.stage1height - fAlumbPiece.height()) / 2 - fOriginalOffsetY;
        if (combinedx % 180 > 10 && combinedx % 180 < 170) {
            if (combinedy % 185 > 38 && combinedy % 185 < 138) {
                int targetindex = NormalizeScrollIndex(fCurrentScrollIndex - 7 + (combinedx / 180) + (combinedy / 185) * 5);
                // now paint stage
                if (fCurrentScrollIndex != targetindex) {
                    if (targetindex > fCurrentScrollIndex) {
                        UpdateScrollDepthLevel(pmdRight);
                    } else {
                        UpdateScrollDepthLevel(pmdLeft);
                    }
                    fCurrentScrollIndex = targetindex;
                    /*QPainter painter(&fAlumbPiece);
                    painter.setPen(QPen(QColor(0xCC0E2545), 1, Qt::SolidLine));
                    painter.drawPie(combinedx, combinedy, 10, 10, 8, 2);
                    painter.end();
                    PaintStage();*/
                    fStageShowMode = ssmFit;
                    ScrollPage();
                } else {
                    fStageShowMode = ssmFit;
                    //CheckPrefetchHealthy();
                    //PaintStage(); // untouchable
                    ScrollPage();
                }
            }
        }
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Wrapper from static to VMT/DMT
DWORD WINAPI ThreadFunc(LPVOID Thread) {
    return ((TThread*)Thread)->Execute();
}
////////////////////////////////////////////////////////////////////////////////////////////////////

TThread::TThread( UFUNCTION Proc, int Priority ) {
    FSuspended = true;
    FTerminated = false;
    FHandle = ::CreateThread( NULL,		// no security
        0,		// the same stack size
        &ThreadFunc,	// thread entry point
        this,			// parameter to pass to ThreadFunc
        CREATE_SUSPENDED,	// always SUSPENDED
        &FThreadID );		// receive thread ID

    //fOnExecute.Quad = Proc.Quad;
    fOnExecute.Code = Proc.Code;
    fOnExecute.Data = Proc.Data;

    SetThreadPriority(FHandle, Priority);

    Resume();
}

//destructor
TThread::~TThread(){
    if (!FTerminated) {
        Terminate();
    }
    WaitFor(2048);
    if (FHandle != 0) {
        CloseHandle(FHandle);
    }
}

int TThread::Execute() {
    int result = 0;

    if (fOnExecute.Code != NULL){
        result = ((CMainWnd*)fOnExecute.Data->*((TOnThreadExecute)fOnExecute.Code))(this);
    }
    FResult = result;
    FTerminated = true; // fake thread object (to prevent terminating while freeing)
    return result;
}

void TThread::Resume( void ) {
    FSuspended = false;
    if (::ResumeThread(FHandle) > 1){
        FSuspended = true;
    }
}

void TThread::Suspend( void ) {
    FSuspended = true;
    SuspendThread(FHandle);
}

void TThread::Terminate( void ) {
    TerminateThread(FHandle, 0);
    FTerminated = true;
}

int TThread::WaitFor( int tick ) {
    int Result = -1;
    if (FHandle == 0) 
        return Result;
    WaitForSingleObject(FHandle, tick);
    GetExitCodeThread(FHandle, (DWORD*)&Result);
    return Result;
}

int TThread::GetThreadId() {
    return FThreadID;
}

bool TThread::GetTerminated() {
    return FTerminated;
}